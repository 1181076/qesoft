import http from "../http-common";

class IngredientDataService {
  getAll() {
    return http.get("/sandwiches");
  }

  get(id) {
    return http.get(`/sandwiches/${id}`);
  }

  create(data) {
    console.log(data);
    return http.post("/sandwiches", data);
  }

  update(id, data) {
    return http.put(`/sandwiches/${id}`, data);
  }

  delete(id) {
    return http.delete(`/sandwiches/${id}`);
  }

  deleteAll() {
    return http.delete(`/sandwiches`);
  }

  // findByTitle(title) {
  //   return http.get(`/ingredients?title=${title}`);
  // }

  getSandwichesByPriceInterval(currency, minimumPrice, maximumPrice){
    return http.get(`/sandwiches/priceFilter/${currency}/${minimumPrice}/${maximumPrice}`);    
  }

  getSandwichesByIngredientList(list){   
    return http.get(`/sandwiches/ingredientsFilter/${list}`);    
  }
}

export default new IngredientDataService();
