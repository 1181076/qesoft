import http from "../http-common";

class DealDataService {
  getAll() {
    return http.get("/deals");
  }

  get(id) {
    return http.get(`/deals/${id}`);
  }

  create(data) {
    console.log(data)
    return http.post("/deals", data);
  }

  update(id, data) {
    return http.put(`/deals/${id}`, data);
  }

  delete(id) {
    return http.delete(`/deals/${id}`);
  }


  deleteAll() {
    return http.delete(`/deals`);
  }

  // findByTitle(title) {
  //   return http.get(`/ingredients?title=${title}`);
  // }
}

export default new DealDataService();