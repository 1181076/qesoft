import http from "../http-common";

class SupplierDataService {
  getAll() {
    return http.get("/suppliers");
  }

  get(id) {
    return http.get(`/suppliers/${id}`);
  }

  create(data) {
    console.log(data)
    return http.post("/suppliers", data);
  }

  update(id, data) {
    return http.put(`/suppliers/${id}`, data);
  }

  delete(id) {
    return http.delete(`/suppliers/${id}`);
  }

  deleteAll() {
    return http.delete(`/suppliers`);
  }

  // findByTitle(title) {
  //   return http.get(`/suppliers?title=${title}`);
  // }
}

export default new SupplierDataService();