import * as AT from "./authTypes";
import axios from "axios";

const AUTH_URL = "http://localhost:8081/api/authenticate";

export const authenticateUser = (email, password) => async (dispatch) => {
  dispatch(loginRequest());
  try {
    const response = await axios.post(AUTH_URL, {
      email: {value: email},
      password: {value: password},
    });
    localStorage.setItem("jwtToken", response.data.token);
    dispatch(success({ username: response.data.email, name: response.data.name, school:response.data.school, isLoggedIn: 'true', role: response.data.role }));
    return Promise.resolve(response.data);
  } catch (error) {
    dispatch(failure());
    return Promise.reject(error);
  }
};

export const fetchSchools = () => {
  return (dispatch) => {
    dispatch(loginRequest());
    axios
      .get(
        "http://localhost:8081/api/schools"
      )
      .then((response) => {
        dispatch(success(response.data));
      })
      .catch((error) => {
        dispatch(failure(error.message));
      });
  };
};

export const logoutUser = () => {
  return (dispatch) => {
    dispatch(logoutRequest());
    localStorage.removeItem("jwtToken");
    dispatch(success({ username: "", isLoggedIn: false }));
  };
};

const loginRequest = () => {
  return {
    type: AT.LOGIN_REQUEST,
  };
};

const logoutRequest = () => {
  return {
    type: AT.LOGOUT_REQUEST,
  };
};

const success = (isLoggedIn) => {
  return {
    type: AT.SUCCESS,
    payload: isLoggedIn,
  };
};

const failure = () => {
  return {
    type: AT.FAILURE,
    payload: false,
  };
};