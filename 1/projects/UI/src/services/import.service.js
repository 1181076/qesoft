// import http from "../http-common";

class ImportDataService {
    createFromFile(data) {
        var formData = new FormData();
        formData.append("file", data);
        fetch('http://localhost:8081/api/import/createFromFile', { method: "POST", body: formData });
    }
}

export default new ImportDataService();