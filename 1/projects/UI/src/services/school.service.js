import http from "../http-common";

class SchoolDataService {
  getAll() {
    return http.get("/schools");
  }

  get(id) {
    return http.get(`/schools/${id}`);
  }

  create(data) {
    console.log(data);
    return http.post("/schools", data);
  }

  update(id, data) {
    return http.put(`/schools/${id}`, data);
  }

  delete(id) {
    return http.delete(`/schools/${id}`);
  }

  deleteAll() {
    return http.delete(`/schools`);
  }

  registerSandwiches(id, sandwiches) {
    return http.post(`/schools/${id}/registerSandwiches`, sandwiches);
  }
}

export default new SchoolDataService();
