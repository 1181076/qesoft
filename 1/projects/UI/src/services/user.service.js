import http from "../http-common";

class UserDataService {
  
  registerSandwiches(id, school) {
    return http.post(`/users/${id}`, school);
  }
}

export default new UserDataService();
