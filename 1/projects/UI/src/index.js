import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";

import App from "./App";
import * as serviceWorker from "./serviceWorker";
import {Provider, useDispatch} from 'react-redux';
import rootReducer from "./services/rootReducer/rootReducer";
import {createStore, combineReducers} from 'redux';
import store from "./services/rootReducer/store";


ReactDOM.render(
  <BrowserRouter>
  
  <Provider store={store}>
    <App />
    </Provider>
  </BrowserRouter>,
  document.getElementById("root")
);

serviceWorker.unregister();

