import React from "react";
import OrderDataService from "../../services/order.service";
import SandwichesService from "../../services/sandwich.service";
// import 'react-day-picker/lib/style.css';

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import Autocomplete from "@material-ui/lab/Autocomplete";

export default class AddOrder extends React.Component {
  constructor(props) {
    super(props);
    this.onChangeDay = this.onChangeDay.bind(this);
    this.handleAddSandwichOrders = this.handleAddSandwichOrders.bind(this);
    this.handleRemoveSandwichOrders =
      this.handleRemoveSandwichOrders.bind(this);
    this.saveOrder = this.saveOrder.bind(this);
    this.onChangeTempSandwiches = this.onChangeTempSandwiches.bind(this);
    this.onChangeTempAmounts = this.onChangeTempAmounts.bind(this);

    this.state = {
      id: "",
      day: null,
      sandwichOrders: [],

      tempSandwiches: "",
      tempAmounts: null,

      submitted: false,

      styles: (theme) => ({
        paper: {
          maxWidth: 936,
          margin: "auto",
          overflow: "hidden",
        },
        searchBar: {
          borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
        },
        searchInput: {
          fontSize: theme.typography.fontSize,
        },
        block: {
          display: "block",
        },
        addUser: {
          marginRight: theme.spacing(1),
        },
        contentWrapper: {
          margin: "40px 16px",
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 200,
        },
        selectEmpty: {
          marginTop: theme.spacing(2),
        },
        formControl: {
          margin: theme.spacing(1),
          minWidth: 120,
        },
        root: {
          display: "flex",
          flexWrap: "wrap",
          background: "#eaeff1",
        },
      }),

      availableSandwiches: [],

      tags: null,
      error: undefined,
    };
  }

  componentDidMount() {
    this.retrieveSandwiches();
  }

  retrieveSandwiches() {
    SandwichesService.getAll()
      .then((response) => {
        this.setState({
          availableSandwiches: response.data,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  onChangeID = (e) => {
    this.setState({
      id: e.target.value,
    });
  };

  onChangeDay = (e) => {
    this.setState({
      day: e.target.value,
    });
  };

  handleAddSandwichOrders = () => {
    this.setState({
      sandwichOrders: this.state.sandwichOrders.concat([
        {
          sandwich: this.state.tempSandwiches.id.value,
          amount: this.state.tempAmounts,
        },
      ]),
    });

    document.getElementById("ordersss").innerHTML +=
      "<p> ➤ " +
      this.state.tempAmounts +
      " -> " +
      this.state.tempSandwiches.shortDescription.value +
      "</p>";
  };

  handleRemoveSandwichOrders = () => {
    this.setState({
      sandwichOrders: this.state.sandwichOrders.splice(-1, 1),
    });

    var a = document.getElementById("ordersss").innerHTML;
    var b = a.length - 4;
    var c = a.charAt(b);

    while (c != "/" && b != 0) {
      b--;
      c = a.charAt(b);
    }

    if (b != 0) {
      b = b + 3;
    }

    var d = a.substring(0, b);

    document.getElementById("ordersss").innerHTML = d;
  };

  onChangeTempSandwiches = (event, values) => {
    this.setState(function (prevState) {
      return {
        tempSandwiches: values,
      };
    });
  };

  onChangeTempAmounts = (e) => {
    this.setState({
      tempAmounts: e.target.value,
    });
  };

  saveOrder = (e) => {
    var data = {
      id: { value: this.state.id },
      day: { value: this.state.day },
      sandwichOrders: this.state.sandwichOrders,
    };

    OrderDataService.create(data)
      .then(() => {
        this.setState({
          error: null,
        });
      })
      .catch((error) => {
        this.setState({
          error: error.response.data.message,
        });
      });
  };

  render() {
    return (
      <div className={this.state.styles.root}>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/icon?family=Material+Icons"
        />
        <br></br>
        <br></br>
        <h4 id="title">Add Order</h4>

        <div
        // className="submit-form"
        >
          <br></br>
          <div>
            <label id="label0" htmlFor="id">
              ID
            </label>
            <TextField
              id="idField"
              variant="outlined"
              fullWidth
              value={this.state.id}
              style={{ background: "#e9f2e9" }}
              onChange={this.onChangeID}
            />
          </div>

          <br></br>

          <div>
            <label id="label3" htmlFor="date">
              Day
            </label>

            <br></br>
            <TextField
              id="day"
              type="date"
              variant="outlined"
              style={{ background: "#e9f2e9" }}
              className={this.state.styles.textField}
              InputLabelProps={{
                shrink: true,
              }}
              onChange={this.onChangeDay}
            />
          </div>

          <br></br>
          <br></br>

          <div>
            <label id="label7" htmlFor="suppliers">
              Sandwich Orders
            </label>
            <div id="ordersss" />

            <Autocomplete
              id="free-solo-demo"
              onChange={this.onChangeTempSandwiches}
              options={this.state.availableSandwiches}
              getOptionLabel={(option) => option.shortDescription.value}
              renderInput={(params) => (
                <TextField
                  {...params}
                  style={{ background: "#e9f2e9" }}
                  margin="normal"
                  variant="outlined"
                  height="1rem"
                />
              )}
            />

            <div>
              <TextField
                type="number"
                id="amount"
                variant="outlined"
                fullWidth
                value={this.state.tempAmounts}
                style={{ background: "#e9f2e9" }}
                onChange={this.onChangeTempAmounts}
              />
            </div>

            <br></br>

            <Button
              id="authorAdd"
              variant="contained"
              style={{ background: "#20428c", color: "#FFF" }}
              size="small"
              className={this.state.styles.addUser}
              onClick={this.handleAddSandwichOrders}
              className="small"
            >
              Add Sandwich Order
            </Button>

            <IconButton
              aria-label="delete"
              style={{ color: "#e6556f" }}
              onClick={this.handleRemoveSandwichOrders}
            >
              <DeleteIcon />
            </IconButton>
          </div>

          <br></br>

          <Button
            id="submit"
            variant="contained"
            color="default"
            size="small"
            className={this.state.styles.addUser}
            onClick={this.saveOrder}
            disabled={
              this.state.id === "" ||
              this.state.day === null ||
              this.state.sandwichOrders.length === 0
            }
          >
            Submit
          </Button>
        </div>

        <br></br>

        {this.state.error !== undefined ? (
          <p
            style={{
              color: this.state.error !== null ? "red" : "green",
            }}
          >
            {this.state.error !== null ? this.state.error : "Order Registered!"}
          </p>
        ) : undefined}
      </div>
    );
  }
}
