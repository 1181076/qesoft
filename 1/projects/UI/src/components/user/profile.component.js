import React, { Component } from "react";
import './style.css';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import Autocomplete from '@material-ui/lab/Autocomplete'; 

import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { logoutUser } from "../../services/rootReducer/auth";
import { Link } from "react-router-dom";

import ButtonGroup from "@material-ui/core/ButtonGroup";

const Profile = (props) => {

  
  const auth = useSelector((state) => state.auth);

  const dispatch = useDispatch();

  const logout = () => {
    dispatch(logoutUser());
    setTimeout(() => {
      props.history.push("/login");
    }, 2000);
  };

    return (
      <div>
        
      <br></br>
        <br></br>
        
          <div 
          // className="submit-form"
          >
          
          <div>
            
          <h2 id="title">Profile</h2>
          
          <br></br>

            <div className="form-group">
              <label id="label2" htmlFor="email"><b>Email:</b> {auth.username} </label>
              <label id="label3" htmlFor="role"><b>Role:</b> {auth.role} </label>
            </div>



            <Button id="submit" variant="contained" 
                style={{ background: "#c94254", color: "#FFF" }} size="small" onClick={logout} >
              Logout
          </Button> 
          
          {auth.school == "" ?  
                <Button
                  style={{
                    background: "#ffc107",
                    color: "#FFF",
                    textTransform: "none",
                  }}
                >

                  
                  <Link
                    id="edit"
                    className="badge badge-warning"
                    to={"/assignSchool/" + auth.name}
                  >
                    Associate School
                  </Link>
                </Button>:<a></a>}
            </div>

            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <br></br>

          
          
          </div>   
      </div>
    );
  }


  
export default Profile;
