import React from "react";
import Button from "@material-ui/core/Button";
import UserDataService from "../../services/user.service";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import Autocomplete from "@material-ui/lab/Autocomplete";
import SchoolDataService from "../../services/school.service";

export default class AssignSchool extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      schools: [],
      tempSchool: "",
    };
  }

  componentDidMount() {
    this.retrieveAllSchools();
  }

  retrieveAllSchools() { 
    SchoolDataService.getAll()
      .then((response) => {
        this.setState({
          schools: response.data,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }


  onChangeSchoolTemp = (event, values) => {
    this.setState({
        tempSchool: values,
    });
  };

  associateSchool = (e) => {
          
    UserDataService.associateSchool(
      this.state.tempSchool.id.value, this.props.match.params.id
    );
  };
  
  render() {

    return (

    <div className={this.state.styles.root}>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/icon?family=Material+Icons"
        />
        <br></br>
        <br></br>
        <h4 id="title">Choose School</h4>

        <div>
          <br></br>

          <div>
            <label id="label10" htmlFor="sandwiches">
              Schools
            </label>
            <div id="sandsss" />

            <Autocomplete
              id="free-solo-demo"
              freeSolo
              onChange={this.onChangeSchoolTemp}
              options={this.state.schools}
              getOptionLabel={(option) => option.name.value}
              renderInput={(params) => (
                <TextField
                  {...params}
                  style={{ background: "#e9f2e9" }}
                  label="Add"
                  margin="normal"
                  variant="outlined"
                />
              )}
            />

          </div>
          <br></br>
          <br></br>
          <Button
            id="submit"
            variant="contained"
            color="default"
            size="small"
            onClick={this.associateSchool}
          >
            Associate School
          </Button>
        </div>

        <br></br>
      </div>
    )
  }
}