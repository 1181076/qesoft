import React, { Component } from "react";
import "./style.css";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

import Autocomplete from "@material-ui/lab/Autocomplete";

import { useState } from "react";
import { useDispatch } from "react-redux";
import {
  authenticateUser,
  registerUser,
} from "../../services/rootReducer/auth";

const emailRegex = new RegExp(
  "(?:[a-z0-9!#$%&'*+\\/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+\\/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
);

const passwordRegex = new RegExp(
  "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
);

const Login = (props) => {
  const initialState = {
    email: "",
    password: "",
  };

  const [user, setUser] = useState(initialState);
  const [error, setError] = useState(undefined);

  const userChange = (event) => {
    const { name, value } = event.target;
    setUser({ ...user, [name]: value });
  };

  const dispatch = useDispatch();

  const saveUser = () => {
    if (!emailRegex.test(user.email)) {
      setError("The email introduced is not valid!");
      return;
    }

    if (!passwordRegex.test(user.password)) {
      setError(
        "The password must have at least 8 characters, one lowercase, one uppercase, one number and one special character!"
      );
      return;
    }

    dispatch(authenticateUser(user.email, user.password))
      .then((response) => {
        resetRegisterForm();
        setTimeout(() => {
          props.history.push("/");
        }, 2000);
      })
      .catch((error) => {
        console.log(error.message);
      });
  };

  const resetRegisterForm = () => {
    setUser(initialState);
  };

  return (
    <div>
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
      />
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/icon?family=Material+Icons"
      />
      <br></br>
      <br></br>

      <div
      // className="submit-form"
      >
        <div className="form">
          <h4 id="title">Login</h4>

          <br></br>

          <div className="form-group">
            <label id="label2" htmlFor="email">
              Email
            </label>
            <TextField
              id="presentationTitle"
              variant="outlined"
              required
              name="email"
              value={user.email}
              style={{ background: "#e9f2e9" }}
              onChange={userChange}
            />
          </div>

          <div className="form-group">
            <label id="label4" htmlFor="price">
              Password
            </label>

            <TextField
              required
              id="lowerLimit"
              variant="outlined"
              name="password"
              type="password"
              value={user.password}
              onChange={userChange}
              style={{ background: "#e9f2e9" }}
              name="password"
            />
            <br></br>
          </div>
          <Button
            id="submit"
            variant="contained"
            color="default"
            size="small"
            onClick={saveUser}
            disabled={user.email === "" || user.password === ""}
          >
            Login
          </Button>
          <br></br>
          {error !== undefined ? (
            <p style={{ color: "red" }}>{error}</p>
          ) : undefined}
        </div>

        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
      </div>
    </div>
  );
};

export default Login;
