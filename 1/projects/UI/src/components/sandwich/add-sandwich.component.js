import React from "react";
import SandwichDataService from "../../services/sandwich.service";
import IngredientsService from "../../services/ingredient.service";
// import 'react-day-picker/lib/style.css';

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import Autocomplete from "@material-ui/lab/Autocomplete";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

export default class AddSandwich extends React.Component {
  constructor(props) {
    super(props);
    this.onChangeShortDescription = this.onChangeShortDescription.bind(this);
    this.onChangeExtendedDescription =
      this.onChangeExtendedDescription.bind(this);
    this.onChangePriceAmount = this.onChangePriceAmount.bind(this);
    this.onChangePriceCurrency = this.onChangePriceCurrency.bind(this);
    this.handleAddIngredients = this.handleAddIngredients.bind(this);
    this.handleRemoveIngredients = this.handleRemoveIngredients.bind(this);
    this.saveSandwich = this.saveSandwich.bind(this);
    this.onChangeTempIngredients = this.onChangeTempIngredients.bind(this);

    this.state = {
      id: "",
      shortDescription: "",
      extendedDescription: "",
      amount: null,
      currency: null,
      ingredients: [],

      tempIngredients: "",

      submitted: false,

      styles: (theme) => ({
        paper: {
          maxWidth: 936,
          margin: "auto",
          overflow: "hidden",
        },
        searchBar: {
          borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
        },
        searchInput: {
          fontSize: theme.typography.fontSize,
        },
        block: {
          display: "block",
        },
        addUser: {
          marginRight: theme.spacing(1),
        },
        contentWrapper: {
          margin: "40px 16px",
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 200,
        },
        selectEmpty: {
          marginTop: theme.spacing(2),
        },
        formControl: {
          margin: theme.spacing(1),
          minWidth: 120,
        },
        root: {
          display: "flex",
          flexWrap: "wrap",
          background: "#eaeff1",
        },
      }),

      availableIngredients: [],

      tags: null,
      error: undefined,
    };
  }

  componentDidMount() {
    this.retrieveIngredients();
  }

  retrieveIngredients() {
    IngredientsService.getAll()
      .then((response) => {
        this.setState({
          availableIngredients: response.data,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  onChangeID = (e) => {
    this.setState({
      id: e.target.value,
    });
  };

  onChangeShortDescription = (e) => {
    this.setState({
      shortDescription: e.target.value,
    });
  };

  onChangeExtendedDescription = (e) => {
    this.setState({
      extendedDescription: e.target.value,
    });
  };

  onChangePriceAmount = (e) => {
    this.setState({
      amount: e.target.value,
    });
  };

  onChangePriceCurrency = (e) => {
    this.setState({
      currency: e.target.value,
    });
  };

  handleAddIngredients = () => {
    this.setState({
      ingredients: this.state.ingredients.concat([
        this.state.tempIngredients
      ]),
    });

    document.getElementById("suppliersss").innerHTML +=
      "<p> ➤ " + this.state.tempIngredients.description.value + "</p>";
  };

  handleRemoveIngredients = () => {
    this.state.ingredients.value.pop();

    var a = document.getElementById("suppliersss").innerHTML;
    var b = a.length - 4;
    var c = a.charAt(b);

    while (c != "/" && b != 0) {
      b--;
      c = a.charAt(b);
    }

    if (b != 0) {
      b = b + 3;
    }

    var d = a.substring(0, b);

    document.getElementById("suppliersss").innerHTML = d;
  };

  onChangeTempIngredients = (event, values) => {
    this.setState(function (prevState) {
      return {
        tempIngredients: values,
      };
    });
  };

  saveSandwich = (e) => {
    var data = {
      id: { value: this.state.id },
      shortDescription: { value: this.state.shortDescription },
      extendedDescription: { value: this.state.extendedDescription },
      price: {
        amount: this.state.amount,
        currency: this.state.currency,
      },
      ingredients: this.state.ingredients,
    };

    SandwichDataService.create(data)
      .then(() => {
        this.setState({
          error: null,
        });
      })
      .catch((error) => {
        this.setState({
          error: error.response.data.message,
        });
      });
  };

  newSandwich() {
    this.setState({
      id: null,
      shortDescription: null,
      extendedDescription: null,
      price: null,
      ingredients: [],
      quantity: null,
      lastUpdatedDate: null,
      upperLimit: null,
      lowerLimit: null,
      units: null,
      ingredients: [],
      tempIngredients: "",

      submitted: false,
    });
  }

  render() {
    return (
      <div className={this.state.styles.root}>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/icon?family=Material+Icons"
        />
        <br></br>
        <br></br>
        <h4 id="title">Add Sandwich</h4>

        <div
        // className="submit-form"
        >
          <br></br>
          <div>
            <label id="label0" htmlFor="id">
              ID
            </label>
            <TextField
              id="presentationTitle"
              variant="outlined"
              fullWidth
              value={this.state.id}
              style={{ background: "#e9f2e9" }}
              onChange={this.onChangeID}
            />
          </div>

          <div>
            <label id="label1" htmlFor="shortDescription">
              Short Description
            </label>
            <TextField
              id="presentationTitle"
              variant="outlined"
              fullWidth
              value={this.state.shortDescription}
              style={{ background: "#e9f2e9" }}
              onChange={this.onChangeShortDescription}
            />
          </div>

          <div>
            <label id="label2" htmlFor="extendedDescription">
              Extended Description
            </label>
            <TextField
              id="presentationTitle"
              variant="outlined"
              fullWidth
              value={this.state.extendedDescription}
              style={{ background: "#e9f2e9" }}
              onChange={this.onChangeExtendedDescription}
            />
          </div>

          <div>
            <label id="label4" htmlFor="price">
              Price Amount
            </label>

            <TextField
              type="number"
              id="lowerLimit"
              variant="outlined"
              fullWidth
              value={this.state.amount}
              onChange={this.onChangePriceAmount}
              style={{ background: "#e9f2e9" }}
              name="amount"
            />

            <label id="label5" htmlFor="price">
              Price Currency
            </label>

            <Select
              labelId="labelCurrencyFilter"
              id="CurrencyFilter"
              variant="outlined"
              fullWidth
              value={this.state.currency}
              onChange={this.onChangePriceCurrency}
              style={{ background: "#e9f2e9" }}
            >
              <MenuItem id="Euro" value="Euro">
                Euro
              </MenuItem>
              <MenuItem id="US Dollar" value="US Dollar">
                US Dollar
              </MenuItem>
              <MenuItem id="Australian Dollar" value="Australian Dolla">
                Australian Dolla
              </MenuItem>
              <MenuItem id="Canadian Dollar" value="Canadian Dollar">
                Canadian Dollar
              </MenuItem>
              <MenuItem id="Pound" value="Pound">
                Pound
              </MenuItem>
              <MenuItem id="Yen" value="Yen">
                Yen
              </MenuItem>
              <MenuItem id="Franc" value="Franc">
                Franc
              </MenuItem>
              <MenuItem id="Won" value="Won">
                Won
              </MenuItem>
              <MenuItem id="Peso" value="Peso">
                Peso
              </MenuItem>
              <MenuItem id="Rupee" value="Rupee">
                Rupee
              </MenuItem>
            </Select>
          </div>

          <br></br>

          <div>
            <label id="label7" htmlFor="suppliers">
              Ingredients
            </label>
            <div id="suppliersss" />

            <Autocomplete
              id="free-solo-demo"
              onChange={this.onChangeTempIngredients}
              options={this.state.availableIngredients}
              getOptionLabel={(option) => option.description.value}
              renderInput={(params) => (
                <TextField
                  {...params}
                  style={{ background: "#e9f2e9" }}
                  label="Add"
                  margin="normal"
                  variant="outlined"
                />
              )}
            />

            <Button
              id="authorAdd"
              variant="contained"
              style={{ background: "#20428c", color: "#FFF" }}
              size="small"
              className={this.state.styles.addUser}
              onClick={this.handleAddIngredients}
              className="small"
            >
              Add Ingredient
            </Button>

            <IconButton
              aria-label="delete"
              style={{ color: "#e6556f" }}
              onClick={this.handleRemoveIngredients}
            >
              <DeleteIcon />
            </IconButton>
          </div>

          <br></br>

          <Button
            id="submit"
            variant="contained"
            color="default"
            size="small"
            className={this.state.styles.addUser}
            onClick={this.saveSandwich}
            disabled={
              this.state.id === "" ||
              this.state.shortDescription === "" ||
              this.state.extendedDescription === "" ||
              this.state.amount === null ||
              this.state.currency === null ||
              this.state.ingredients.length === 0
            }
          >
            Submit
          </Button>
        </div>

        <br></br>

        {this.state.error !== undefined ? (
          <p
            style={{
              color: this.state.error !== null ? "red" : "green",
            }}
          >
            {this.state.error !== null
              ? this.state.error
              : "Sandwich Registered!"}
          </p>
        ) : undefined}
      </div>
    );
  }
}
