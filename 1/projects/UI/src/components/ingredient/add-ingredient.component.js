import React from "react";
import IngredientDataService from "../../services/ingredient.service";

import SuppliersService from "../../services/supplier.service";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";

import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

import Autocomplete from "@material-ui/lab/Autocomplete";

export default class AddIngredient extends React.Component {
  constructor(props) {
    super(props);
    this.onChangeDescription = this.onChangeDescription.bind(this); 
    this.onChangeQuantity = this.onChangeQuantity.bind(this);
    this.onChangeUpperLimit = this.onChangeUpperLimit.bind(this);
    this.onChangeLowerLimit = this.onChangeLowerLimit.bind(this);
    this.onChangeUnits = this.onChangeUnits.bind(this);
    this.saveIngredient = this.saveIngredient.bind(this);

    this.state = {
      id: "",
      description: "",
      quantity: null,
      lastUpdatedDate: [],
      upperLimit: null,
      lowerLimit: null,
      units: "",

      submitted: false,

      styles: (theme) => ({
        paper: {
          maxWidth: 936,
          margin: "auto",
          overflow: "hidden",
        },
        searchBar: {
          borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
        },
        searchInput: {
          fontSize: theme.typography.fontSize,
        },
        block: {
          display: "block",
        },
        addUser: {
          marginRight: theme.spacing(1),
        },
        contentWrapper: {
          margin: "40px 16px",
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 200,
        },
        selectEmpty: {
          marginTop: theme.spacing(2),
        },
        formControl: {
          margin: theme.spacing(1),
          minWidth: 120,
        },
        root: {
          display: "flex",
          flexWrap: "wrap",
          background: "#eaeff1",
        },
      }),

      availableSuppliers: [],

      tags: null,
      error: undefined,
    };
  }

  componentDidMount() {
  }

  onChangeID = (e) => {
    this.setState({
      id: e.target.value,
    });
  };

  onChangeDescription = (e) => {
    this.setState({
      description: e.target.value,
    });
  };

  onChangeQuantity = (e) => {
    this.setState({
      quantity: e.target.value,
    });
  };

  onChangeUpperLimit = (e) => {
    this.setState({
      upperLimit: e.target.value,
    });
  };

  onChangeLowerLimit = (e) => {
    this.setState({
      lowerLimit: e.target.value,
    });
  };

  onChangeUnits = (e) => {
    this.setState({
      units: e.target.value,
    });
  };

  saveIngredient = (e) => {

    var today = new Date();    
    var currentDate = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

    var data = {
      id: { value: this.state.id },
      description: { value: this.state.description },
      quantity: { value: this.state.quantity },
      lastUpdatedDate: [{ date: currentDate, amount: this.state.quantity }],
      upperLimit: { value: this.state.upperLimit },
      lowerLimit: { value: this.state.lowerLimit },
      units: { value: this.state.units }
    };

    if (parseInt(data.upperLimit.value) < parseInt(data.lowerLimit.value)) {
      this.setState({
        error: "The upper limit cannot be below the lower limit!",
      });
      return;
    }

    if (
      parseInt(data.quantity.value) < parseInt(data.lowerLimit.value) ||
      parseInt(data.quantity.value) > parseInt(data.upperLimit.value)
    ) {
      this.setState({
        error: "The quantity must be between the lower and upper limit!",
      });
      return;
    }
    console.log(data);

    IngredientDataService.create(data)
      .then(() => {
        this.setState({
          error: null,
        });
      })
      .catch((error) => {
        this.setState({
          error: error.response.data.message,
        });
      });
  };

  render() {
    return (
      <div className={this.state.styles.root}>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/icon?family=Material+Icons"
        />
        <br></br>
        <br></br>
        <h4 id="title">Add Ingredient</h4>

        <div
        // className="submit-form"
        >
          <br></br>
          <div>
            <label id="label0" htmlFor="id">
              ID
            </label>
            <TextField
              id="presentationTitle"
              variant="outlined"
              fullWidth
              value={this.state.id}
              style={{ background: "#e9f2e9" }}
              onChange={this.onChangeID}
            />
          </div>

          <div>
            <label id="label1" htmlFor="description">
              Description
            </label>
            <TextField
              id="presentationTitle"
              variant="outlined"
              fullWidth
              value={this.state.description}
              style={{ background: "#e9f2e9" }}
              onChange={this.onChangeDescription}
            />
          </div>

          <div>
            <label id="label2" htmlFor="host">
              Quantity
            </label>
            <TextField
              type="number"
              id="host"
              variant="outlined"
              fullWidth
              value={this.state.quantity}
              style={{ background: "#e9f2e9" }}
              onChange={this.onChangeQuantity}
            />
          </div>

          <br></br>

          <div>
            <label id="label4" htmlFor="upperLimit">
              Upper Limit
            </label>
            <TextField
              type="number"
              id="upperLimit"
              variant="outlined"
              fullWidth
              value={this.state.upperLimit}
              style={{ background: "#e9f2e9" }}
              onChange={this.onChangeUpperLimit}
            />
          </div>

          <br></br>

          <div>
            <label id="label5" htmlFor="lowerLimit">
              Lower Limit
            </label>

            <TextField
              type="number"
              id="lowerLimit"
              variant="outlined"
              fullWidth
              value={this.state.lowerLimit}
              onChange={this.onChangeLowerLimit}
              style={{ background: "#e9f2e9" }}
              name="lowerLimit"
            />
          </div>

          <br></br>

          <div>
            <label id="label6" htmlFor="units">
              Units
            </label>

            <Select
              labelId="labelCurrencyFilter"
              id="CurrencyFilter"
              variant="outlined"
              fullWidth
              value={this.state.units}
              onChange={this.onChangeUnits}
              style={{ background: "#e9f2e9" }}
            >
              <MenuItem id="Grams" value="Grams">
                Grams
              </MenuItem>
              <MenuItem id="Kilograms" value="Kilograms">
                Kilograms
              </MenuItem>
              <MenuItem id="Milligrams" value="Milligrams">
                Milligrams
              </MenuItem>
              <MenuItem id="Liters" value="Liters">
                Liters
              </MenuItem>
              <MenuItem id="Milliliters" value="Milliliters">
                Milliliters
              </MenuItem>
              <MenuItem id="Pounds" value="Pounds">
                Pounds
              </MenuItem>
              <MenuItem id="Ounces" value="Ounces">
                Ounces
              </MenuItem>
            </Select>
          </div>

          <br></br>

          <br></br>

          <Button
            id="submit"
            variant="contained"
            color="default"
            size="small"
            className={this.state.styles.addUser}
            onClick={this.saveIngredient}
            disabled={
              this.state.id === "" ||
              this.state.description === "" ||
              this.state.quantity === null ||
              this.state.lowerLimit === null ||
              this.state.upperLimit === null ||
              this.state.units === "" ||
              this.state.lastUpdatedDate === null
            }
          >
            Submit
          </Button>
        </div>

        <br></br>

        {this.state.error !== undefined ? (
          <p
            style={{
              color: this.state.error !== null ? "red" : "green",
            }}
          >
            {this.state.error !== null
              ? this.state.error
              : "Ingredient Registered!"}
          </p>
        ) : undefined}
      </div>
    );
  }
}
