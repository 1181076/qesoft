import React, { Component } from "react";
import IngredientDataService from "../../services/ingredient.service";

import AddIngredient from "./add-ingredient.component";

import { Switch, Route, Link } from "react-router-dom";

import ButtonGroup from '@material-ui/core/ButtonGroup';

import AppsTwoToneIcon from '@material-ui/icons/AppsTwoTone';
import EditTwoToneIcon from '@material-ui/icons/EditTwoTone';
import PublishTwoToneIcon from '@material-ui/icons/PublishTwoTone';
import GetAppTwoToneIcon from '@material-ui/icons/GetAppTwoTone';
import MergeTypeTwoToneIcon from '@material-ui/icons/MergeTypeTwoTone';
 
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import { withStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import RefreshIcon from '@material-ui/icons/Refresh';
import DeleteIcon from '@material-ui/icons/Delete';



import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';



export default class IngredientsList extends Component {
  constructor(props) {
    super(props);
    this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this);
    this.retrieveIngredients = this.retrieveIngredients.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveIngredient = this.setActiveIngredient.bind(this);
    this.setStockHistory = this.setStockHistory.bind(this);
    this.removeAllIngredients = this.removeAllIngredients.bind(this);
    

    this.state = {
      ingredients: [],
      currentIngredient: null,
      currentIndex: -1,
      searchTitle: "",
      stockHistory: "",


      styles : (theme) => ({
        paper: {
          maxWidth: 936,
          margin: 'auto',
          overflow: 'hidden',
        },
        searchBar: {
          borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
        },
        searchInput: {
          fontSize: theme.typography.fontSize,
        },
        block: {
          display: 'block',
        },
        addUser: {
          marginRight: theme.spacing(1),
          textTransform: 'none',
        },
        contentWrapper: {
          margin: '40px 16px',
        },
      })
    };

    
    this.MyLink = props => <Link to="/addIngredient" {...props} />

  }

  componentDidMount() {
    this.retrieveIngredients();
    
  }

  onChangeSearchTitle(e) {
    const searchTitle = e.target.value;

    this.setState({
      searchTitle: searchTitle
    });
  }

  retrieveIngredients() {
    IngredientDataService.getAll()
      .then(response => {
        this.setState({
          ingredients: response.data
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  refreshList() {
    this.retrieveIngredients();
    this.setState({
      currentIngredient: null,
      currentIndex: -1
    });
  }

  setActiveIngredient(ingredient, index) {

    if(document.getElementById("stockHistoryContent")!=null){
      document.getElementById("stockHistoryContent").innerHTML = "";
      
      document.getElementById('history').style.visibility = 'visible';
    }

    this.setState({
      currentIngredient: ingredient,
      currentIndex: index,
    });
  }

  setStockHistory() {

    document.getElementById('history').style.visibility = 'hidden';

    document.getElementById("stockHistoryContent").innerHTML += "<label><strong>Stock History:</strong></label><br><br>";

    var i;
    for (i = 0; i < this.state.currentIngredient.lastUpdatedDate.length; i++) {
      document.getElementById("stockHistoryContent").innerHTML += "➤ <strong>Stock:</strong> " + this.state.currentIngredient.lastUpdatedDate[i].amount + " / <strong>Date:</strong> " + this.state.currentIngredient.lastUpdatedDate[i].date +  "<br/>";
    }

  }

  removeAllIngredients() {
    IngredientDataService.deleteAll()
      .then(response => {
        this.refreshList();
      })
      .catch(e => {
        console.log(e);
      });
  }

  searchTitle() {
    // this.setState({
    //   currentIngredient: null,
    //   currentIndex: -1
    // });

    // IngredientDataService.findByTitle(this.state.searchTitle)
    //   .then(response => {
    //     this.setState({
    //       ingredients: response.data
    //     });
    //   })
    //   .catch(e => {
    //     console.log(e);
    //   });
  }

  render() {
    const { searchTitle, ingredients, currentIngredient, currentIndex } = this.state;

    return (

      
      
      <div  className="list row"> 
      
          
        <br></br>
        <br></br>
          

        <div className="col-md-8">

          
        <br></br>
        <br></br>
            
          <h4 id="listIngredientsTitle"> Ingredients List</h4>
            
          <br></br>
              

<Paper className={this.state.styles.paper}>
  
<AppBar className={this.state.styles.searchBar} position="static" color="default" elevation={0}>
<Toolbar>
          <Grid container spacing={2} alignItems="center">
            <Grid item>
              <IconButton>
                  <SearchIcon className={this.state.styles.block} color="inherit" onClick={this.searchTitle} />
                </IconButton>
            </Grid>
            <Grid item xs>
              <TextField
                fullWidth
                placeholder="Search"
                InputProps={{
                  disableUnderline: true,
                  className: this.state.styles.searchInput,
                }}
                onChange={this.onChangeSearchTitle}
              />
            </Grid>
            <Grid item>
              <Tooltip title="Reload">
                <IconButton>
                  <RefreshIcon className={this.state.styles.block} color="inherit" onClick={() => this.setActiveIngredient(null, -1)}/>
                </IconButton>
              </Tooltip>
            </Grid>
          </Grid>
        </Toolbar>
        </AppBar>
        </Paper>
        <br></br>
        </div>

            

        <div className="col-md-6" >
          
        

          <ul className="list-group">
            {ingredients &&
              ingredients.map((ingredient, index) => (
                <li 
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveIngredient(ingredient, index)}
                  key={index}
                >
                  {ingredient.description.value}
                </li>
              ))}
          </ul>



          <br></br>
          <Button id="add" variant="contained" style ={{ background: '#20428c',  color:"#FFF", textTransform: 'none'}} size="small" className={this.state.styles.addUser} component={this.MyLink}>
              Add Ingredient
            </Button>

          <Button id="removeAll" variant="contained" style ={{ background: '#e6556f', color:"#FFF", textTransform: 'none'}} size="small" className={this.state.styles.addUser} startIcon={<DeleteIcon />}
            onClick={this.removeAllIngredients}
          >
            Remove All
          </Button>

          
          <br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
          

          <br></br>
          
          <br></br>
          
        </div>
        <div className="col-md-6">
            {currentIngredient ? (
              <div>
                <h4>Ingredient</h4>
                <div id="description">
                  <label>
                    <strong>Description:</strong>
                  </label>{" "}
                  {/* <br></br> */}
                  { currentIngredient.description!=null? currentIngredient.description.value: '-----'}  
                </div>
                <div id="quantity">
                  <label>
                    <strong>Quantity:</strong>
                  </label>{" "}
                  { currentIngredient.quantity!=null? currentIngredient.quantity.value: '-----'}
                </div>
                <div  id="upperLimit">
                  <label>
                    <strong>Upper Limit:</strong>
                  </label>{" "}
                  { currentIngredient.upperLimit!=null? currentIngredient.upperLimit.value: '-----'}
                </div>
                <div  id="lowerLimit">
                  <label>
                    <strong>Lower Limit:</strong>
                  </label>{" "}
                  { currentIngredient.lowerLimit!=null? currentIngredient.lowerLimit.value: '-----'}
                </div>
                <div  id="units">
                  <label>
                    <strong>Units:</strong>
                  </label>{" "}
                  {/* <br></br> */}
                  { currentIngredient.units!=null? currentIngredient.units.value: '-----'}
                </div>

                <hr></hr>

                  <div>
                    <Button id="history" size="small" 
                    variant="contained"
                    style={{
                        textTransform: "none",
                    }}
                    onClick={this.setStockHistory}>
                    Show Stock History
                    </Button>
                    </div>
                    
                  <div  id="stockHistoryContent">

                    </div>
                  
                <hr></hr>
                  <br></br>

                <ButtonGroup  variant="contained" >
                  <Button style ={{ background: '#ffc107', color:"#FFF",  textTransform: 'none'}}>
                    <Link id="edit" className="badge badge-warning"
                      to={"/ingredients/" + currentIngredient.id.value}
                    >
                      Edit
                    </Link>
                  </Button>
                </ButtonGroup>
                
                </div>
            ) : (
              <div>
                <br />
                
              </div>
            )}
          </div>
          
         

            
        
        
        
      </div>

      
    );
  }
}

