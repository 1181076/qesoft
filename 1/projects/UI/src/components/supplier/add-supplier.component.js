import React from "react";
import SupplierDataService from "../../services/supplier.service";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";

import Autocomplete from "@material-ui/lab/Autocomplete";
import dealService from "../../services/deal.service";

export default class AddSupplier extends React.Component {
  constructor(props) {
    super(props);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeAddress = this.onChangeAddress.bind(this);
    this.onChangeEmails = this.onChangeEmails.bind(this);
    this.onChangeContacts = this.onChangeContacts.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeDeals = this.onChangeDeals.bind(this);
    this.handleAddEmails = this.handleAddEmails.bind(this);
    this.handleRemoveEmails = this.handleRemoveEmails.bind(this);
    this.handleAddContacts = this.handleAddContacts.bind(this);
    this.handleRemoveContacts = this.handleRemoveContacts.bind(this);
    this.handleAddDeals = this.handleAddDeals.bind(this);
    this.handleRemoveDeals = this.handleRemoveDeals.bind(this);
    this.saveSupplier = this.saveSupplier.bind(this);
    this.onChangeTempContact = this.onChangeTempContact.bind(this);
    this.onChangeTempEmail = this.onChangeTempEmail.bind(this);
    this.onChangeTempDeal = this.onChangeTempDeal.bind(this);
    this.retrieveDeals = this.retrieveDeals.bind(this);

    this.state = {
      id: "",
      name: "",
      address: "",
      emails: [],
      contacts: [],
      description: "",
      deals: [],

      tempContact: "",
      tempEmail: "",
      tempDeal: "",

      submitted: false,

      styles: (theme) => ({
        paper: {
          maxWidth: 936,
          margin: "auto",
          overflow: "hidden",
        },
        searchBar: {
          borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
        },
        searchInput: {
          fontSize: theme.typography.fontSize,
        },
        block: {
          display: "block",
        },
        addUser: {
          marginRight: theme.spacing(1),
        },
        contentWrapper: {
          margin: "40px 16px",
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 200,
        },
        selectEmpty: {
          marginTop: theme.spacing(2),
        },
        formControl: {
          margin: theme.spacing(1),
          minWidth: 120,
        },
        root: {
          display: "flex",
          flexWrap: "wrap",
          background: "#eaeff1",
        },
      }),
      availableDeals: [],
      tags: null,
      error: undefined,
    };
  }

  componentDidMount() {
    this.retrieveDeals();
  }

  retrieveDeals() {
    dealService
      .getAll()
      .then((response) => {
        if (response.data != null) {
          this.setState({
            availableDeals: response.data,
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }

  onChangeID = (e) => {
    this.setState({
      id: e.target.value,
    });
  };

  onChangeName = (e) => {
    this.setState({
      name: e.target.value,
    });
  };

  onChangeAddress = (e) => {
    this.setState({
      address: e.target.value,
    });
  };

  onChangeEmails = (e) => {
    this.setState({
      emails: e.target.value,
    });
  };

  onChangeContacts = (e) => {
    this.setState({
      contacts: e.target.value,
    });
  };

  onChangeDescription = (e) => {
    this.setState({
      description: e.target.value,
    });
  };

  onChangeDeals = (e) => {
    this.setState({
      deals: e.target.value,
    });
  };

  handleAddEmails = () => {
    this.setState({
      emails: this.state.emails.concat([{ value: this.state.tempEmail }]),
    });
    document.getElementById("emailsss").innerHTML +=
      "<p> ➤ " + this.state.tempEmail + "</p>";
  };

  handleRemoveEmails = () => {
    this.setState({
      emails: this.state.emails.splice(-1, 1),
    });
    var a = document.getElementById("emailsss").innerHTML;
    var b = a.length - 4;
    var c = a.charAt(b);
    while (c != "/" && b != 0) {
      b--;
      c = a.charAt(b);
    }
    if (b != 0) {
      b = b + 3;
    }
    var d = a.substring(0, b);
    document.getElementById("emailsss").innerHTML = d;
  };

  handleAddContacts = () => {
    this.setState({
      contacts: this.state.contacts.concat([{ value: this.state.tempContact }]),
    });
    document.getElementById("contactsss").innerHTML +=
      "<p> ➤ " + this.state.tempContact + "</p>";
  };

  handleRemoveContacts = () => {
    this.setState({
      contacts: this.state.contacts.splice(-1, 1),
    });
    var a = document.getElementById("contactsss").innerHTML;
    var b = a.length - 4;
    var c = a.charAt(b);
    while (c != "/" && b != 0) {
      b--;
      c = a.charAt(b);
    }
    if (b != 0) {
      b = b + 3;
    }
    var d = a.substring(0, b);
    document.getElementById("contactsss").innerHTML = d;
  };

  handleAddDeals = () => {
    this.setState({
      deals: this.state.deals.concat([
        this.state.tempDeal,
      ]),
    });
    document.getElementById("dealsss").innerHTML +=
      "<p> ➤ " + this.state.tempDeal.id.value + "</p>";
  };

  handleRemoveDeals = () => {
    this.state.deals.pop();

    var a = document.getElementById("dealsss").innerHTML;
    var b = a.length - 4;
    var c = a.charAt(b);
    while (c != "/" && b != 0) {
      b--;
      c = a.charAt(b);
    }
    if (b != 0) {
      b = b + 3;
    }
    var d = a.substring(0, b);
    document.getElementById("dealsss").innerHTML = d;
  };

  onChangeTempEmail = (e) => {
    this.setState({
      tempEmail: e.target.value,
    });
  };

  onChangeTempContact = (e) => {
    console.log(e);
    this.setState({
      tempContact: e.target.value,
    });
  };

  onChangeTempDeal = (e, values) => {
    this.setState({
      tempDeal: values,
    });
  };

  saveSupplier = (e) => {
    var data = {
      id: { value: this.state.id },
      name: { value: this.state.name },
      address: { value: this.state.address },
      emails: this.state.emails,
      contacts: this.state.contacts,
      description: { value: this.state.description },
      deals: this.state.deals,
    };
    SupplierDataService.create(data)
      .then(() => {
        this.setState({
          error: null,
        });
      })
      .catch((error) => {
        this.setState({
          error: error.response.data.message,
        });
      });
  };

  render() {
    return (
      <div className={this.state.styles.root}>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/icon?family=Material+Icons"
        />
        <br></br>
        <br></br>
        <h4 id="title">Add Supplier</h4>
        <div>
          <br></br>
          <div>
            <label id="label0" htmlFor="id">
              ID
            </label>
            <TextField
              id="presentationTitle"
              variant="outlined"
              fullWidth
              value={this.state.id}
              style={{ background: "#e9f2e9" }}
              onChange={this.onChangeID}
            />
          </div>
          <br></br>
          <div>
            <label id="label1" htmlFor="name">
              Name
            </label>
            <TextField
              id="name"
              variant="outlined"
              fullWidth
              value={this.state.name}
              style={{ background: "#e9f2e9" }}
              onChange={this.onChangeName}
            />
          </div>
          <br></br>
          <div>
            <label id="label2" htmlFor="address">
              Address
            </label>
            <TextField
              id="address"
              variant="outlined"
              fullWidth
              value={this.state.address}
              style={{ background: "#e9f2e9" }}
              onChange={this.onChangeAddress}
            />
          </div>
          <br></br>
          <div>
            <label id="label3" htmlFor="emails">
              Emails
            </label>
            <div id="emailsss" />
            <div>
              <TextField
                id="tempEmail"
                variant="outlined"
                fullWidth
                value={this.state.tempEmail}
                onChange={this.onChangeTempEmail}
                style={{ background: "#e9f2e9" }}
                name="tempEmail"
              />
            </div>
            <Button
              id="authorAdd"
              variant="contained"
              style={{ background: "#20428c", color: "#FFF" }}
              size="small"
              className={this.state.styles.addUser}
              onClick={this.handleAddEmails}
              className="small"
            >
              Add Email
            </Button>
            <IconButton
              aria-label="delete"
              style={{ color: "#e6556f" }}
              onClick={this.handleRemoveEmails}
            >
              <DeleteIcon />
            </IconButton>
          </div>
          <br></br>
          <div>
            <label id="label4" htmlFor="contacts">
              Phone Contacts
            </label>
            <div id="contactsss" />
            <div>
              <TextField
                type="number"
                id="tempContact"
                variant="outlined"
                fullWidth
                value={this.state.tempContact}
                onChange={this.onChangeTempContact}
                style={{ background: "#e9f2e9" }}
                name="tempContact"
              />
            </div>
            <Button
              id="authorAdd"
              variant="contained"
              style={{ background: "#20428c", color: "#FFF" }}
              size="small"
              className={this.state.styles.addUser}
              onClick={this.handleAddContacts}
              className="small"
            >
              Add Contact
            </Button>
            <IconButton
              aria-label="delete"
              style={{ color: "#e6556f" }}
              onClick={this.handleRemoveContacts}
            >
              <DeleteIcon />
            </IconButton>
          </div>
          <br></br>
          <div>
            <label id="label5" htmlFor="deals">
              Deals
            </label>
            <div id="dealsss" />
            <Autocomplete
              id="free-solo-demo"
              freeSolo
              onChange={this.onChangeTempDeal}
              options={this.state.availableDeals}
              getOptionLabel={(option) => option.id.value}
              renderInput={(params) => (
                <TextField
                  {...params}
                  style={{ background: "#e9f2e9" }}
                  label="Add"
                  margin="normal"
                  variant="outlined"
                />
              )}
            />

            <Button
              id="authorAdd"
              variant="contained"
              style={{ background: "#20428c", color: "#FFF" }}
              size="small"
              className={this.state.styles.addUser}
              onClick={this.handleAddDeals}
              className="small"
            >
              Add Deal
            </Button>
            <IconButton
              aria-label="delete"
              style={{ color: "#e6556f" }}
              onClick={this.handleRemoveDeals}
            >
              <DeleteIcon />
            </IconButton>
          </div>
          <br></br>
          <div>
            <label id="label6" htmlFor="description">
              Description
            </label>
            <TextField
              id="description"
              variant="outlined"
              fullWidth
              value={this.state.description}
              style={{ background: "#e9f2e9" }}
              onChange={this.onChangeDescription}
            />
          </div>
          <br></br>
          <Button
            id="submit"
            variant="contained"
            color="default"
            size="small"
            className={this.state.styles.addUser}
            onClick={this.saveSupplier}
            disabled={
              this.state.id === "" ||
              this.state.address === "" ||
              this.state.description === "" ||
              this.state.name === "" ||
              this.state.contacts.length === 0 ||
              this.state.emails.length === 0 
            }
          >
            Submit
          </Button>
          <br></br>
          <br></br>

          {this.state.error !== undefined ? (
            <p
              style={{
                color: this.state.error !== null ? "red" : "green",
              }}
            >
              {this.state.error !== null
                ? this.state.error
                : "Supplier Registered!"}
            </p>
          ) : undefined}
        </div>
        <br></br>
      </div>
    );
  }
}
