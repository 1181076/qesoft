import React from "react";
import DealDataService from "../../services/deal.service";
import IngredientsService from "../../services/ingredient.service";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Autocomplete from "@material-ui/lab/Autocomplete";

export default class AddDeal extends React.Component {
  constructor(props) {
    super(props);
    this.onChangeUnit = this.onChangeUnit.bind(this);
    this.onChangeAmount = this.onChangeAmount.bind(this);
    this.onChangeExpirationPriceDate =
      this.onChangeExpirationPriceDate.bind(this);
    this.onChangeIngredient = this.onChangeIngredient.bind(this);

    this.state = {
      id: "",
      unit: null,
      amount: null,
      expirationPriceDate: null,
      ingredient: null,

      submitted: false,

      styles: (theme) => ({
        paper: {
          maxWidth: 936,
          margin: "auto",
          overflow: "hidden",
        },
        searchBar: {
          borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
        },
        searchInput: {
          fontSize: theme.typography.fontSize,
        },
        block: {
          display: "block",
        },
        addUser: {
          marginRight: theme.spacing(1),
        },
        contentWrapper: {
          margin: "40px 16px",
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 200,
        },
        selectEmpty: {
          marginTop: theme.spacing(2),
        },
        formControl: {
          margin: theme.spacing(1),
          minWidth: 120,
        },
        root: {
          display: "flex",
          flexWrap: "wrap",
          background: "#eaeff1",
        },
      }),

      availableIngredients: [],
      tags: null,
      error: undefined,
    };
  }

  componentDidMount() {
    this.retrieveIngredients();
  }

  retrieveIngredients() {
    IngredientsService.getAll()
      .then((response) => {
        if (response.data != null) {
          this.setState({
            availableIngredients: response.data,
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }

  onChangeId = (e) => {
    this.setState({
      id: e.target.value,
    });
  };

  onChangeUnit = (e) => {
    this.setState({
      unit: e.target.value,
    });
  };

  onChangeAmount = (e) => {
    this.setState({
      amount: e.target.value,
    });
  };

  onChangeExpirationPriceDate = (e) => {
    this.setState({
      expirationPriceDate: e.target.value,
    });
  };

  onChangeIngredient = (event, values) => {
    this.setState(function (prevState) {
      return {
        ingredient: values,
      };
    });
  };

  saveDeal = (e) => {
    var data = {
      id: { value: this.state.id },
      unitPrice: { unit: this.state.unit, value: this.state.amount },
      expirationPriceDate: { value: this.state.expirationPriceDate },
      ingredient: { dealIngredientID: this.state.ingredient },
    };

    DealDataService.create(data)
      .then(() => {
        this.setState({
          error: null,
        });
      })
      .catch((error) => {
        this.setState({
          error: error.response.data.message,
        });
      });
  };

  render() {
    return (
      <div className={this.state.styles.root}>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/icon?family=Material+Icons"
        />
        <br></br>
        <br></br>
        <h4 id="title">Add Deal</h4>

        <div>
          <br></br>
          <div>
            <label id="label1" htmlFor="id">
              Id
            </label>
            <TextField
              id="presentationTitle"
              variant="outlined"
              fullWidth
              value={this.state.id}
              style={{ background: "#e9f2e9" }}
              onChange={this.onChangeId}
            />
          </div>

          <div>
            <label id="label4" htmlFor="price">
              Unit Price Amount
            </label>

            <TextField
              type="number"
              id="lowerLimit"
              variant="outlined"
              fullWidth
              value={this.state.amount}
              onChange={this.onChangeAmount}
              style={{ background: "#e9f2e9" }}
              name="amount"
            />
            <br></br>
          </div>
          <div>
            <label id="label5" htmlFor="price">
              Unit Price Currency
            </label>

            <Select
              labelId="labelCurrencyFilter"
              id="CurrencyFilter"
              variant="outlined"
              fullWidth
              value={this.state.unit}
              onChange={this.onChangeUnit}
              style={{ background: "#e9f2e9" }}
            >
              <MenuItem id="Euro" value="Euro">
                Euro
              </MenuItem>
              <MenuItem id="US Dollar" value="US Dollar">
                US Dollar
              </MenuItem>
              <MenuItem id="Australian Dollar" value="Australian Dolla">
                Australian Dolla
              </MenuItem>
              <MenuItem id="Canadian Dollar" value="Canadian Dollar">
                Canadian Dollar
              </MenuItem>
              <MenuItem id="Pound" value="Pound">
                Pound
              </MenuItem>
              <MenuItem id="Yen" value="Yen">
                Yen
              </MenuItem>
              <MenuItem id="Franc" value="Franc">
                Franc
              </MenuItem>
              <MenuItem id="Won" value="Won">
                Won
              </MenuItem>
              <MenuItem id="Peso" value="Peso">
                Peso
              </MenuItem>
              <MenuItem id="Rupee" value="Rupee">
                Rupee
              </MenuItem>
            </Select>
          </div>

          <div>
            <label id="label3" htmlFor="date">
              Price Expiration Date
            </label>
            <br></br>
            <TextField
              id="date"
              type="date"
              variant="outlined"
              style={{ background: "#e9f2e9" }}
              className={this.state.styles.textField}
              InputLabelProps={{
                shrink: true,
              }}
              onChange={this.onChangeExpirationPriceDate}
            />
          </div>
          <br></br>

          <div>
            <label id="label7" htmlFor="ingredients">
              Ingredients
            </label>
            <div id="ingredientsss" />

            <Autocomplete
              id="free-solo-demo"
              onChange={this.onChangeIngredient}
              options={this.state.availableIngredients}
              getOptionLabel={(option) => option.description.value}
              renderInput={(params) => (
                <TextField
                  {...params}
                  style={{ background: "#e9f2e9" }}
                  margin="normal"
                  variant="outlined"
                />
              )}
            />
          </div>

          <br></br>

          <Button
            id="submit"
            variant="contained"
            color="default"
            size="small"
            className={this.state.styles.addUser}
            onClick={this.saveDeal}
            disabled={
              this.state.id === "" ||
              this.state.amount === null ||
              this.state.expirationPriceDate === null ||
              this.state.ingredient === null ||
              this.state.unit === null
            }
          >
            Submit
          </Button>
          <br></br>
          <br></br>

          {this.state.error !== undefined ? (
            <p
              style={{
                color: this.state.error !== null ? "red" : "green",
              }}
            >
              {this.state.error !== null
                ? this.state.error
                : "Deal Registered!"}
            </p>
          ) : undefined}
        </div>
      </div>
    );
  }
}
