import React, { Component } from "react";
import DealDataService from "../../services/deal.service";
import IngredientDataService from "../../services/ingredient.service";

import AddDeal from "./add-deal.component";

import { Switch, Route, Link } from "react-router-dom";

import ButtonGroup from '@material-ui/core/ButtonGroup';

import AppsTwoToneIcon from '@material-ui/icons/AppsTwoTone';
import EditTwoToneIcon from '@material-ui/icons/EditTwoTone';
import PublishTwoToneIcon from '@material-ui/icons/PublishTwoTone';
import GetAppTwoToneIcon from '@material-ui/icons/GetAppTwoTone';
import MergeTypeTwoToneIcon from '@material-ui/icons/MergeTypeTwoTone';
 
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import { withStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import RefreshIcon from '@material-ui/icons/Refresh';
import DeleteIcon from '@material-ui/icons/Delete';



import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';



export default class DealsList extends Component {
  constructor(props) {
    super(props);
    this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this);
    this.retrieveDeals = this.retrieveDeals.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveDeal = this.setActiveDeal.bind(this);
    this.removeAllDeals = this.removeAllDeals.bind(this);
    

    this.state = {
      deals: [],
      currentDeal: null,
      currentIndex: -1,
      searchTitle: "",


      styles : (theme) => ({ 
        paper: {
          maxWidth: 936,
          margin: 'auto',
          overflow: 'hidden',
        },
        searchBar: {
          borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
        },
        searchInput: {
          fontSize: theme.typography.fontSize,
        },
        block: {
          display: 'block',
        },
        addUser: {
          marginRight: theme.spacing(1),
          textTransform: 'none',
        },
        contentWrapper: {
          margin: '40px 16px',
        },
      })
    };

    
    this.MyLink = props => <Link to="/addDeal" {...props} />

  }

  componentDidMount() {
    this.retrieveDeals();
    
  }

  onChangeSearchTitle(e) {
    const searchTitle = e.target.value;

    this.setState({
      searchTitle: searchTitle
    });
  }

  retrieveDeals() {
    DealDataService.getAll()
      .then(response => {
        this.setState({
          deals: response.data
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  refreshList() {
    this.retrieveDeals();
    this.setState({
      currentDeal: null,
      currentIndex: -1
    });
  }

  setActiveDeal(deal, index) {

    this.setState({
      currentDeal: deal,
      currentIndex: index
    });
  }

  removeAllDeals() {
    DealDataService.deleteAll()
      .then(response => {
        this.refreshList();
      })
      .catch(e => {
        console.log(e);
      });
  }

  searchTitle() {
  }

  render() {
    const { searchTitle, deals, currentDeal, currentIndex } = this.state;

    return (

      <div  className="list row"> 
                
        <br></br>
        <br></br>
          
        <div className="col-md-8">
     
        <br></br>
        <br></br>
            
          <h4 id="listDealsTitle"> Deals List</h4>
            
          <br></br>
              

<Paper className={this.state.styles.paper}>
  
<AppBar className={this.state.styles.searchBar} position="static" color="default" elevation={0}>
<Toolbar>
          <Grid container spacing={2} alignItems="center">
            <Grid item>
              <IconButton>
                  <SearchIcon className={this.state.styles.block} color="inherit" onClick={this.searchTitle} />
                </IconButton>
            </Grid>
            <Grid item xs>
              <TextField
                fullWidth
                placeholder="Search"
                InputProps={{
                  disableUnderline: true,
                  className: this.state.styles.searchInput,
                }}
                onChange={this.onChangeSearchTitle}
              />
            </Grid>
            <Grid item>
              <Tooltip title="Reload">
                <IconButton>
                  <RefreshIcon className={this.state.styles.block} color="inherit" onClick={() => this.setActiveDeal(null, -1)}/>
                </IconButton>
              </Tooltip>
            </Grid>
          </Grid>
        </Toolbar>
        </AppBar>
        </Paper>
        <br></br>
        </div>

            

        <div className="col-md-6" >
                    <ul className="list-group">
                        {deals &&
                            deals.map((deal, index) => (
                                <li
                                    className={
                                        "list-group-item " +
                                        (index === currentIndex ? "active" : "")
                                    }
                                    onClick={() => this.setActiveDeal(deal, index)}
                                    key={index}
                                >
                                    {deal.id.value}
                                </li>
                            ))}
                    </ul>

          <br></br>
          <Button id="add" variant="contained" style ={{ background: '#20428c',  color:"#FFF", textTransform: 'none'}} size="small" className={this.state.styles.addUser} component={this.MyLink}>
              Add Deal
            </Button>

          <Button id="removeAll" variant="contained" style ={{ background: '#e6556f', color:"#FFF", textTransform: 'none'}} size="small" className={this.state.styles.addUser} startIcon={<DeleteIcon />}
            onClick={this.removeAllDeals}
          >
            Remove All
          </Button>
       
          <br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
          
        </div>
        <div className="col-md-6">
            {currentDeal ? (
              <div>
                <h4>Deal</h4>
                <div id="unitPrice">
                  <label>
                    <strong>Unit Price:</strong>
                  </label>{" "}
                  {/* <br></br> */}
                  { currentDeal.unitPrice!=null ? currentDeal.unitPrice.value + ' ' + currentDeal.unitPrice.unit : '-----'}  
                </div>
                <div id="expirationPriceDate">
                  <label>
                    <strong>Expiration Price Date:</strong>
                  </label>{" "}
                  { currentDeal.expirationPriceDate!=null? currentDeal.expirationPriceDate.value: '-----'}
                </div>                
                <div id="ingredient">
                  <label>
                    <strong>Ingredient:</strong>
                  </label>{" "}
                  { currentDeal.ingredient!=null? currentDeal.ingredient.description.value: '-----'}
                </div>
                <hr></hr>
                  <br></br>
                  
                  <ButtonGroup variant="contained" >
                                <Button style={{ background: '#ffc107', color: "#FFF", textTransform: 'none' }}>
                                    <Link id="edit" className="badge badge-warning" to={"/deals/" + currentDeal.id.value}> Edit </Link>
                                </Button>
                            </ButtonGroup>
                </div>
            ) : (
              <div>
                <br/>       
              </div>
            )}
          </div>  
      </div>     
    );
  }
}

