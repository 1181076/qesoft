import React, { Component } from "react";
import SupplierDataService from "../../services/supplier.service";
import DealsService from "../../services/deal.service";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import Autocomplete from "@material-ui/lab/Autocomplete"; 

export default class Supplier extends Component {
  constructor(props) {
    super(props);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeAddress = this.onChangeAddress.bind(this);
    this.onChangeEmails = this.onChangeEmails.bind(this);
    this.onChangeContacts = this.onChangeContacts.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeDeals = this.onChangeDeals.bind(this);
    this.handleAddEmails = this.handleAddEmails.bind(this);
    this.handleRemoveEmails = this.handleRemoveEmails.bind(this);
    this.handleAddContacts = this.handleAddContacts.bind(this);
    this.handleRemoveContacts = this.handleRemoveContacts.bind(this);
    this.handleAddDeals = this.handleAddDeals.bind(this);
    this.handleRemoveDeals = this.handleRemoveDeals.bind(this);
    this.updateSupplier = this.updateSupplier.bind(this);
    this.deleteSupplier = this.deleteSupplier.bind(this);
    this.getSupplier = this.getSupplier.bind(this);
    this.onChangeTempContact = this.onChangeTempContact.bind(this);
    this.onChangeTempEmail = this.onChangeTempEmail.bind(this);
    this.onChangeTempDeal = this.onChangeTempDeal.bind(this);
    // this.getEmails = this.getEmails.bind(this);
    // this.getContacts = this.getContacts.bind(this);
    // this.getDeals = this.getDeals.bind(this);

    this.state = {
      currentSupplier: {
        id: "",
        name: "",
        address: "",
        emails: [],
        contacts: [],
        description: "",
        deals: [],
      },

      availableDeals: [],
      tempEmail: "",
      tempContact: "",
      tempDeal: "",
      submitted: false,
      styles: (theme) => ({
        paper: {
          maxWidth: 936,
          margin: "auto",
          overflow: "hidden",
        },
        searchBar: {
          borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
        },
        searchInput: {
          fontSize: theme.typography.fontSize,
        },
        block: {
          display: "block",
        },
        addUser: {
          marginRight: theme.spacing(1),
        },
        contentWrapper: {
          margin: "40px 16px",
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 200,
        },
        selectEmpty: {
          marginTop: theme.spacing(2),
        },
        formControl: {
          margin: theme.spacing(1),
          minWidth: 120,
        },
        root: {
          display: "flex",
          flexWrap: "wrap",
          background: "#eaeff1",
        },
      }),
      tags: null,
      error: undefined,
    };
  }

  componentDidMount() {
    this.retrieveDeals();
    this.getSupplier(this.props.match.params.id);
  }

  retrieveDeals() {
    DealsService.getAll()
      .then((response) => {
        if (response.data != null) {
          this.setState({
            availableDeals: response.data,
          });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }

  onChangeName = (e) => {
    const name = e.target.value;
    this.setState(function (prevState) {
      return {
        currentSupplier: {
          ...prevState.currentSupplier,
          name: { value: name },
        },
      };
    });
  };

  onChangeAddress = (e) => {
    const address = e.target.value;
    this.setState(function (prevState) {
      return {
        currentSupplier: {
          ...prevState.currentSupplier,
          address: { value: address },
        },
      };
    });
  };

  onChangeEmails = (e) => {
    const vals = e.target.value;
    this.setState(function (prevState) {
      return {
        currentSupplier: {
          ...prevState.currentSupplier,
          emails: vals,
        },
      };
    });
  };

  onChangeContacts = (e) => {
    const vals = e.target.value;
    this.setState(function (prevState) {
      return {
        currentSupplier: {
          ...prevState.currentSupplier,
          contacts: vals,
        },
      };
    });
  };

  onChangeDeals = (e) => {
    const vals = e.target.value;
    this.setState(function (prevState) {
      return {
        currentSupplier: {
          ...prevState.currentSupplier,
          deals: vals,
        },
      };
    });
  };

  onChangeDescription = (e) => {
    const description = e.target.value;
    this.setState(function (prevState) {
      return {
        currentSupplier: {
          ...prevState.currentSupplier,
          description: { value: description },
        },
      };
    });
  };

  onChangeTempEmail = (e) => {
    this.setState({
      tempEmail: e.target.value,
    });
  };

  onChangeTempContact = (e) => {
    this.setState({
      tempContact: e.target.value,
    });
  };

  onChangeTempDeal = (e, values) => {
    this.setState({
      tempDeal: values,
    });
  };

  handleAddEmails = () => {
    this.setState(function (prevState) {
      return {
        currentSupplier: {
          ...prevState.currentSupplier,
          emails: prevState.currentSupplier.emails.concat([
            { value: this.state.tempEmail },
          ]),
        },
        tempSelf: false,
      };
    });

    document.getElementById("emailsss").innerHTML +=
      "<p> ➤ " + this.state.tempEmail + "</p>";
  };

  handleRemoveEmails = () => {
    this.setState(function (prevState) {
      return {
        currentSupplier: {
          ...prevState.currentSupplier,
          emails: prevState.currentSupplier.emails.splice(-1, 1),
        },
      };
    });

    var a = document.getElementById("emailsss").innerHTML;
    var b = a.length - 4;
    var c = a.charAt(b);

    while (c != "/" && b != 0) {
      b--;
      c = a.charAt(b);
    }

    if (b != 0) {
      b = b + 3;
    }

    var d = a.substring(0, b);

    document.getElementById("emailsss").innerHTML = d;
  };

  handleAddContacts = () => {
    this.setState(function (prevState) {
      return {
        currentSupplier: {
          ...prevState.currentSupplier,
          contacts: prevState.currentSupplier.contacts.concat([
            { value: this.state.tempContact },
          ]),
        },
        tempSelf: false,
      };
    });

    document.getElementById("contactsss").innerHTML +=
      "<p> ➤ " + this.state.tempContact + "</p>";
  };

  handleRemoveContacts = () => {
    this.setState(function (prevState) {
      return {
        currentSupplier: {
          ...prevState.currentSupplier,
          contacts: prevState.currentSupplier.contacts.splice(-1, 1),
        },
      };
    });

    var a = document.getElementById("contactsss").innerHTML;
    var b = a.length - 4;
    var c = a.charAt(b);

    while (c != "/" && b != 0) {
      b--;
      c = a.charAt(b);
    }

    if (b != 0) {
      b = b + 3;
    }

    var d = a.substring(0, b);

    document.getElementById("contactsss").innerHTML = d;
  };

  handleAddDeals = () => {
    this.setState(function (prevState) {
      return {
        currentSupplier: {
          ...prevState.currentSupplier,
          deals: prevState.currentSupplier.deals.concat([
            this.state.tempDeal,  
          ]),
        },
        tempSelf: false,
      };
    });

    document.getElementById("dealsss").innerHTML +=
      "<p> ➤ " + this.state.tempDeal.id.value + "</p>";
  };

  handleRemoveDeals = () => {
    this.setState(function (prevState) {
      deals: prevState.currentSupplier.deals.pop();
    });

    var a = document.getElementById("dealsss").innerHTML;
    var b = a.length - 4;
    var c = a.charAt(b);

    while (c != "/" && b != 0) {
      b--;
      c = a.charAt(b);
    }

    if (b != 0) {
      b = b + 3;
    }

    var d = a.substring(0, b);

    document.getElementById("dealsss").innerHTML = d;
  };

  getSupplier(id) {
    SupplierDataService.get(id)
      .then((response) => {
        this.setState({
          currentSupplier: response.data,
        });
        console.log(response.data);

        var dcontacts = response.data.contacts;
        var demails = response.data.emails;
        var ddeals = response.data.deals;

        console.log(response);
        this.getContacts(dcontacts);
        this.getEmails(demails);
        this.getDeals(ddeals);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  getContacts(dcontacts) {
    var i,
      s = "";
    for (i = 0; i < dcontacts.length; i++) {
      s += "<p> ➤ " + dcontacts[i].value + "</p>";
    }
    document.getElementById("contactsss").innerHTML = s;
  }

  getEmails(demails) {
    var i,
      s = "";
    for (i = 0; i < demails.length; i++) {
      s += "<p> ➤ " + demails[i].value + "</p>";
    }
    document.getElementById("emailsss").innerHTML = s;
  }

  getDeals(ddeals) {
    var i,
      j,
      s = "";
    for (i = 0; i < ddeals.length; i++) {
          s += "<p> ➤ " + ddeals[i].id.value + "</p>";
        }
    document.getElementById("dealsss").innerHTML = s;
  }

  updateSupplier() {
    SupplierDataService.update(
      this.state.currentSupplier.id.value,
      this.state.currentSupplier
    )
      .then(() => {
        this.setState({
          error: null,
        });

        this.props.history.push("/suppliers");
      })
      .catch((error) => {
        this.setState({
          error: error.response.data.message,
        });
      });
  }

  deleteSupplier() {
    SupplierDataService.delete(this.state.currentSupplier.id.value)
      .then((response) => {
        this.props.history.push("/suppliers");
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const { currentSupplier } = this.state;

    return (
      <div>
        {currentSupplier ? (
          <div className={this.state.styles.root}>
            <link
              rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            />
            <link
              rel="stylesheet"
              href="https://fonts.googleapis.com/icon?family=Material+Icons"
            />
            <br></br>
            <br></br>
            <h4 id="editCPTitle">Edit Supplier</h4>
            <div>
              <div>
                <label id="label1" htmlFor="name">
                  Name
                </label>
                <TextField
                  id="presentationTitle"
                  variant="outlined"
                  fullWidth
                  value={
                    currentSupplier.name != null
                      ? currentSupplier.name.value
                      : ""
                  }
                  style={{ background: "#e9f2e9" }}
                  onChange={this.onChangeName}
                />
              </div>
              <br></br>
              <div>
                <label id="label2" htmlFor="address">
                  Address
                </label>
                <TextField
                  id="address"
                  variant="outlined"
                  fullWidth
                  value={
                    currentSupplier.address != null
                      ? currentSupplier.address.value
                      : ""
                  }
                  style={{ background: "#e9f2e9" }}
                  onChange={this.onChangeAddress}
                />
              </div>
              <br></br>
              <div>
                <label id="label3" htmlFor="emails">
                  Emails
                </label>
                <div id="emailsss" />
                <div>
                  <TextField
                    id="emails"
                    variant="outlined"
                    fullWidth
                    value={this.state.currentSupplier.tempEmail}
                    onChange={this.onChangeTempEmail}
                    style={{ background: "#e9f2e9" }}
                    name="emails"
                  />
                </div>
                <Button
                  id="emailsAdd"
                  variant="contained"
                  style={{ background: "#20428c", color: "#FFF" }}
                  size="small"
                  className={this.state.styles.addUser}
                  onClick={this.handleAddEmails}
                  className="small"
                >
                  Add Email
                </Button>

                <IconButton
                  aria-label="delete"
                  style={{ color: "#e6556f" }}
                  onClick={this.handleRemoveEmails}
                >
                  <DeleteIcon />
                </IconButton>
              </div>
              <br></br>
              <div>
                <label id="label4" htmlFor="contacts">
                  Contacts
                </label>
                <div id="contactsss" />
                <div>
                  <TextField
                    type="number"
                    id="contacts"
                    variant="outlined"
                    fullWidth
                    value={this.state.currentSupplier.tempContact}
                    onChange={this.onChangeTempContact}
                    style={{ background: "#e9f2e9" }}
                    name="contacts"
                  />
                </div>
                <Button
                  id="contactsAdd"
                  variant="contained"
                  style={{ background: "#20428c", color: "#FFF" }}
                  size="small"
                  className={this.state.styles.addUser}
                  onClick={this.handleAddContacts}
                  className="small"
                >
                  Add Contact
                </Button>
                <IconButton
                  aria-label="delete"
                  style={{ color: "#e6556f" }}
                  onClick={this.handleRemoveContacts}
                >
                  <DeleteIcon />
                </IconButton>
              </div>
              <br></br>
              <div>
                <label id="label5" htmlFor="deals">
                  Deals
                </label>
                <div id="dealsss" />
                <div>
                <Autocomplete
                  id="free-solo-demo"
                  freeSolo
                  onChange={this.onChangeTempDeal}
                  options={this.state.availableDeals}
                  getOptionLabel={(option) => option.id.value}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      style={{ background: "#e9f2e9" }}
                      label="Add"
                      margin="normal"
                      variant="outlined"
                    />
                  )}
                />
                </div>
                <Button
                  id="dealsAdd"
                  variant="contained"
                  style={{ background: "#20428c", color: "#FFF" }}
                  size="small"
                  className={this.state.styles.addUser}
                  onClick={this.handleAddDeals}
                  className="small"
                >
                  Add Deal
                </Button>
                <IconButton
                  aria-label="delete"
                  style={{ color: "#e6556f" }}
                  onClick={this.handleRemoveDeals}
                >
                  <DeleteIcon />
                </IconButton>
              </div>
              <br></br>
              <div>
                <label id="label6" htmlFor="">
                  Description
                </label>
                <TextField
                  id="description"
                  variant="outlined"
                  fullWidth
                  value={
                    currentSupplier.description != null
                      ? currentSupplier.description.value
                      : ""
                  }
                  style={{ background: "#e9f2e9" }}
                  onChange={this.onChangeDescription}
                />
              </div>
              <br></br>
            </div>
            <br></br>
            <br></br>
            <Button
              id="upd"
              variant="contained"
              style={{ background: "#a6ce39", color: "#FFF" }}
              size="small"
              onClick={this.updateSupplier}
              disabled={
                this.state.currentSupplier.id === "" ||
                this.state.currentSupplier.address === "" ||
                this.state.currentSupplier.description === "" ||
                this.state.currentSupplier.name === "" 
              }
            >
              Update
            </Button>
            &nbsp;
            <Button
              id="del"
              variant="contained"
              style={{ background: "#c94254", color: "#FFF" }}
              size="small"
              onClick={this.deleteSupplier}
            >
              Delete
            </Button>
            <br></br>
            <br></br>
            {this.state.error !== undefined ? (
              <p
                style={{
                  color: this.state.error !== null ? "red" : "green",
                }}
              >
                {this.state.error !== null
                  ? this.state.error
                  : "Supplier Updated!"}
              </p>
            ) : undefined}
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on an Supplier...</p>
          </div>
        )}
      </div>
    );
  }
}
