import React, { Component } from "react";
import SchoolDataService from "../../services/school.service";

import { Link } from "react-router-dom";

import ButtonGroup from "@material-ui/core/ButtonGroup";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import RefreshIcon from "@material-ui/icons/Refresh";
import DeleteIcon from "@material-ui/icons/Delete";

export default class SchoolsList extends Component {
  constructor(props) {
    super(props);
    this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this);
    this.retrieveSchools = this.retrieveSchools.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveSchool = this.setActiveSchool.bind(this);
    this.removeAllSchools = this.removeAllSchools.bind(this);

    this.state = {
      schools: [],
      currentSchool: null,
      currentIndex: -1,
      searchId: "",

      styles: (theme) => ({
        paper: {
          maxWidth: 936,
          margin: "auto",
          overflow: "hidden",
        },
        searchBar: {
          borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
        },
        searchInput: {
          fontSize: theme.typography.fontSize,
        },
        block: {
          display: "block",
        },
        addUser: {
          marginRight: theme.spacing(1),
          textTransform: "none",
        },
        contentWrapper: {
          margin: "40px 16px",
        },
      }),
    };

    this.addSchoolLink = (props) => <Link to="/addSchool" {...props} />;
  }

  componentDidMount() {
    this.retrieveSchools();
  }

  onChangeSearchTitle(e) {
    const searchTitle = e.target.value;

    this.setState({
      searchTitle: searchTitle,
    });
  }

  retrieveSchools() {
    SchoolDataService.getAll()
      .then((response) => {
        this.setState({
          schools: response.data,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  refreshList() {
    this.retrieveSchools();
    this.setState({
      currentSchool: null,
      currentIndex: -1,
    });
  }

  setActiveSchool(school, index) {
    this.setState({
      currentSchool: school,
      currentIndex: index,
    });
  }

  removeAllSchools() {
    SchoolDataService.deleteAll()
      .then((response) => {
        this.refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  }

  searchTitle() {
    // this.setState({
    //   currentSandwich: null,
    //   currentIndex: -1
    // });
    // SandwichDataService.findByTitle(this.state.searchTitle)
    //   .then(response => {
    //     this.setState({
    //       sandwiches: response.data
    //     });
    //   })
    //   .catch(e => {
    //     console.log(e);
    //   });
  }

  render() {
    const { searchTitle, schools, currentSchool, currentIndex } = this.state;

    return (
      <div className="list row">
        <br></br>
        <br></br>

        <div className="col-md-8">
          <br></br>
          <br></br>

          <h4 id="listSchoolsTitle"> Schools List</h4>

          <br></br>

          <Paper className={this.state.styles.paper}>
            <AppBar
              className={this.state.styles.searchBar}
              position="static"
              color="default"
              elevation={0}
            >
              <Toolbar>
                <Grid container spacing={2} alignItems="center">
                  <Grid item>
                    <IconButton>
                      <SearchIcon
                        className={this.state.styles.block}
                        color="inherit"
                        onClick={this.searchTitle}
                      />
                    </IconButton>
                  </Grid>
                  <Grid item xs>
                    <TextField
                      fullWidth
                      placeholder="Search"
                      InputProps={{
                        disableUnderline: true,
                        className: this.state.styles.searchInput,
                      }}
                      onChange={this.onChangeSearchTitle}
                    />
                  </Grid>
                  <Grid item>
                    <Tooltip title="Reload">
                      <IconButton>
                        <RefreshIcon
                          className={this.state.styles.block}
                          color="inherit"
                          onClick={() => this.setActiveSchool(null, -1)}
                        />
                      </IconButton>
                    </Tooltip>
                  </Grid>
                </Grid>
              </Toolbar>
            </AppBar>
          </Paper>
          <br></br>
        </div>

        <div className="col-md-6">
          <ul className="list-group">
            {schools &&
              schools.map((school, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveSchool(school, index)}
                  key={index}
                >
                  {school.id.value}
                </li>
              ))}
          </ul>

          <br></br>
          <Button
            id="add"
            variant="contained"
            style={{
              background: "#20428c",
              color: "#FFF",
              textTransform: "none",
            }}
            size="small"
            className={this.state.styles.addUser}
            component={this.addSchoolLink}
          >
            Add School
          </Button>

          <Button
            id="removeAll"
            variant="contained"
            style={{
              background: "#e6556f",
              color: "#FFF",
              textTransform: "none",
            }}
            size="small"
            className={this.state.styles.addUser}
            startIcon={<DeleteIcon />}
            onClick={this.removeAllSchools}
          >
            Remove All
          </Button>

          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>

          <br></br>

          <br></br>
        </div>
        <div className="col-md-6">
          {currentSchool ? (
            <div>
              <h4>School</h4>
              <div id="internalid">
                <label>
                  <strong>Internal ID:</strong>
                </label>{" "}
                {/* <br></br> */}
                {currentSchool.internalID != null
                  ? currentSchool.internalID.value
                  : "-----"}
              </div>
              <div id="name">
                <label>
                  <strong>Name:</strong>
                </label>{" "}
                {currentSchool.name != null
                  ? currentSchool.name.value
                  : "-----"}
              </div>
              <div id="address">
                <label>
                  <strong>Address:</strong>
                </label>{" "}
                {currentSchool.address != null
                  ? currentSchool.address.value
                  : "-----"}
              </div>
              <div id="sandwiches">
                <label>
                  <strong>Sandwiches:</strong>
                </label>{" "}
                {currentSchool.sandwiches != null
                  ? currentSchool.sandwiches.length
                  : "-----"}
              </div>

              <hr></hr>
              <br></br>

              <ButtonGroup variant="contained">
                <Button
                  style={{
                    background: "#ffc107",
                    color: "#FFF",
                    textTransform: "none",
                  }}
                >
                  <Link
                    id="edit"
                    className="badge badge-warning"
                    to={"/schools/" + currentSchool.id.value}
                  >
                    Edit
                  </Link>
                </Button>
                <Button
                  style={{
                    background: "#ffc107",
                    color: "#FFF",
                    textTransform: "none",
                    marginLeft: 10,
                  }}
                >
                  <Link
                    id="edit"
                    className="badge badge-warning"
                    to={
                      "/schools/" +
                      currentSchool.id.value +
                      "/registerSandwiches"
                    }
                  >
                    Register Sandwiches
                  </Link>
                </Button>
              </ButtonGroup>
            </div>
          ) : (
            <div>
              <br />
            </div>
          )}
        </div>
      </div>
    );
  }
}
