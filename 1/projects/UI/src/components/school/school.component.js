import React, { Component } from "react";
import SchoolDataService from "../../services/school.service";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

export default class School extends Component {
  constructor(props) {
    super(props);
    this.onChangeInternalID = this.onChangeInternalID.bind(this);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeAddress = this.onChangeAddress.bind(this);
    this.updateSchool = this.updateSchool.bind(this);
    this.deleteSchool = this.deleteSchool.bind(this);
    this.getSchool = this.getSchool.bind(this);

    this.state = {
      currentSchool: {
        id: "",
        internalID: "",
        name: "",
        address: "",
        sandwiches: [],
      },

      tempSandwiches: "",

      submitted: false,

      styles: (theme) => ({
        paper: {
          maxWidth: 936,
          margin: "auto",
          overflow: "hidden",
        },
        searchBar: {
          borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
        },
        searchInput: {
          fontSize: theme.typography.fontSize,
        },
        block: {
          display: "block",
        },
        addUser: {
          marginRight: theme.spacing(1),
        },
        contentWrapper: {
          margin: "40px 16px",
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 200,
        },
        selectEmpty: {
          marginTop: theme.spacing(2),
        },
        formControl: {
          margin: theme.spacing(1),
          minWidth: 120,
        },
        root: {
          display: "flex",
          flexWrap: "wrap",
          background: "#eaeff1",
        },
      }),

      suppliers: [],

      tags: null,
      error: undefined,
    };
  }

  componentDidMount() {
    this.getSchool(this.props.match.params.id);
  }

  onChangeInternalID = (e) => {
    const internalID = e.target.internalID;

    this.setState(function (prevState) {
      return {
        currentSchool: {
          ...prevState.currentSchool,
          internalID: { value: internalID },
        },
      };
    });
  };

  onChangeName = (e) => {
    const name = e.target.value;

    this.setState(function (prevState) {
      return {
        currentSchool: {
          ...prevState.currentSchool,
          name: { value: name },
        },
      };
    });
  };

  onChangeAddress = (e) => {
    const address = e.target.value;

    this.setState(function (prevState) {
      return {
        currentSchool: {
          ...prevState.currentSchool,
          address: { value: address },
        },
      };
    });
  };

  getSchool(id) {
    SchoolDataService.get(id)
      .then((response) => {
        console.log(response.data);
        this.setState({
          currentSchool: response.data,
        });
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  postSchool(data) {
    try {
      SchoolDataService.create(data);
    } catch (err) {
      console.log(err);
    }
  }

  updateSchool() {
    SchoolDataService.update(
      this.state.currentSchool.id.value,
      this.state.currentSchool
    )
      .then(() => {
        this.setState({
          error: null,
        });

        this.props.history.push("/schools");
      })
      .catch((error) => {
        this.setState({
          error: error.response.data.message,
        });
      });
  }

  deleteSchool() {
    SchoolDataService.delete(this.state.currentSchool.id.value)
      .then((response) => {
        console.log(response.data);
        this.props.history.push("/schools");
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const { currentSchool } = this.state;

    return (
      <div>
        {currentSchool ? (
          <div className={this.state.styles.root}>
            <link
              rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            />
            <link
              rel="stylesheet"
              href="https://fonts.googleapis.com/icon?family=Material+Icons"
            />
            <br></br>
            <br></br>
            <h4 id="editCPTitle">Edit School</h4>

            <div>
              <div>
                <label id="label2" htmlFor="name">
                  Internal ID
                </label>
                <label id="label1" htmlFor="internalID"></label>
                <TextField
                  id="presentationTitle"
                  variant="outlined"
                  fullWidth
                  value={
                    currentSchool.internalID != null
                      ? currentSchool.internalID.value
                      : ""
                  }
                  style={{ background: "#e9f2e9" }}
                  onChange={this.onChangeInternalID}
                />
              </div>
              <br></br>
              <div>
                <label id="label2" htmlFor="name">
                  Name
                </label>
                <TextField
                  id="presentationTitle"
                  variant="outlined"
                  fullWidth
                  value={
                    currentSchool.name != null ? currentSchool.name.value : ""
                  }
                  style={{ background: "#e9f2e9" }}
                  onChange={this.onChangeName}
                />
              </div>
              <br></br>
              <div>
                <label id="label2" htmlFor="address">
                  Address
                </label>
                <TextField
                  id="presentationTitle"
                  variant="outlined"
                  fullWidth
                  value={
                    currentSchool.address != null
                      ? currentSchool.address.value
                      : ""
                  }
                  style={{ background: "#e9f2e9" }}
                  onChange={this.onChangeAddress}
                />
              </div>
              <br></br>
              <br></br>
              <Button
                id="upd"
                variant="contained"
                style={{ background: "#a6ce39", color: "#FFF" }}
                size="small"
                className={this.state.styles.addUser}
                onClick={this.updateSandwich}
                disabled={
                  this.state.currentSchool.id === "" ||
                  this.state.currentSchool.internalID === "" ||
                  this.state.currentSchool.address === "" ||
                  this.state.currentSchool.name === ""
                }
              >
                Update
              </Button>
              &nbsp;
              <Button
                id="del"
                variant="contained"
                style={{ background: "#c94254", color: "#FFF" }}
                size="small"
                className={this.state.styles.addUser}
                onClick={this.deleteSchool}
              >
                Delete
              </Button>
              <br></br>
            </div>

            <br></br>

            {this.state.error !== undefined ? (
              <p
                style={{
                  color: this.state.error !== null ? "red" : "green",
                }}
              >
                {this.state.error !== null
                  ? this.state.error
                  : "School Updated!"}
              </p>
            ) : undefined}
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on an Sandwich...</p>
          </div>
        )}
      </div>
    );
  }
}
