import React, { Component } from 'react';
import { Switch, Route, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';

import Avatar from '@material-ui/core/Avatar';

import NotificationsIcon from '@material-ui/icons/Notifications';

import { NavDropdown } from 'react-bootstrap';

import Home from './components/other/home.component';

//Ingredient
import IngredientsList from './components/ingredient/ingredients-list.component';
import AddIngredient from './components/ingredient/add-ingredient.component';
import Ingredient from './components/ingredient/ingredient.component';

//Supplier
import SuppliersList from './components/supplier/suppliers-list.component';
import AddSupplier from './components/supplier/add-supplier.component';
import Supplier from './components/supplier/supplier.component';

//Sandwich
import SandwichesList from './components/sandwich/sandwiches-list.component';
import AddSandwich from './components/sandwich/add-sandwich.component';
import Sandwich from './components/sandwich/sandwich.component';

//Deal
import DealsList from './components/supplier/deals-list.component';
import AddDeal from './components/supplier/add-deal.component';
import Deal from './components/supplier/deal.component';

//School
import AddSchool from './components/school/add-school.component';
import School from './components/school/school.component';
import SchoolsList from './components/school/schools-list.component';
import RegisterSandwiches from './components/school/register-sandwiches.component';

import OrdersList from './components/user/orders-list.component';
import AddOrder from './components/user/add-order.component';
import Order from './components/user/order.component';

import Import from './components/import/import.component';
import Register from './components/register/register.component';

import { useDispatch, useSelector } from 'react-redux';
import Login from './components/other/login.component';
import Profile from './components/user/profile.component';
import AssignSchool from './components/user/assign-school.component';

const lightColor = 'rgba(312, 66, 140, 1)';
const onDrawerToggle = PropTypes.func.isRequired;
const styles = (theme) => ({
  paper: {
    maxWidth: 936,
    margin: 'auto',
    overflow: 'hidden',
  },
  searchBar: {
    borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
  },
  searchInput: {
    fontSize: theme.typography.fontSize,
  },
  block: {
    display: 'block',
  },
  addUser: {
    marginRight: theme.spacing(1),
  },
  contentWrapper: {
    margin: '40px 16px',
  },

  secondaryBar: {
    zIndex: 0,
  },
  menuButton: {
    marginLeft: -theme.spacing(1),
  },
  iconButtonAvatar: {
    padding: 4,
  },
  link: {
    textDecoration: 'none',
    color: lightColor,
    '&:hover': {
      color: theme.palette.common.white,
    },
  },
  button: {
    borderColor: lightColor,
  },
  categoryHeader: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  categoryHeaderPrimary: {
    color: theme.palette.common.white,
  },
  item: {
    paddingTop: 1,
    paddingBottom: 1,
    color: 'rgba(255, 255, 255, 0.7)',
    '&:hover,&:focus': {
      backgroundColor: 'rgba(255, 255, 255, 0.08)',
    },
  },
  itemCategory: {
    backgroundColor: '#232f3e',
    boxShadow: '0 -1px 0 #404854 inset',
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  firebase: {
    fontSize: 24,
    color: theme.palette.common.white,
  },
  itemActiveItem: {
    color: '#4fc3f7',
  },
  itemPrimary: {
    fontSize: 'inherit',
  },
  itemIcon: {
    minWidth: 'auto',
    marginRight: theme.spacing(2),
  },
  divider: {
    marginTop: theme.spacing(2),
  },
});

// const studentLinks

const App = () => {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const userLinks = (
    <>
      <Grid item>
        <div className='navbar-nav mr-auto'>
          <li id='homeLink' className='nav-item'>
            <Link to={'/'} className='nav-link' style={{ color: '#FFF' }}>
              Home
            </Link>
          </li>
        </div>
      </Grid>{' '}
      <Grid item xs />
      <Grid item>
        <div className='navbar-nav mr-auto'>
          <li className='nav-item'>
            <Link
              id='ordersLink'
              to={'/orders'}
              className='nav-link'
              style={{ color: '#FFF' }}
            >
              Orders
            </Link>
          </li>
        </div>
      </Grid>{' '}
      <Grid item xs />
      <Grid item>
        <Tooltip title='Alerts • No alerts'>
          <IconButton color='inherit'>
            <NotificationsIcon />
          </IconButton>
        </Tooltip>
      </Grid>
      <Grid item xs />
      <Grid item>
        <Link
          id='profileLink'
          to={'/profile'}
          className='nav-link'
          style={{ color: '#FFF' }}
        >
          <IconButton color='inherit' className={styles.iconButtonAvatar}>
            <Avatar src='/static/images/avatar/1.jpg' alt='My Avatar' />
          </IconButton>
        </Link>
      </Grid>
      <Grid item xs />
    </>
  );

  const supplierManagerLinks = (
    <>
      <Grid item>
        <div className='navbar-nav mr-auto'>
          <li id='homeLink' className='nav-item'>
            <Link to={'/'} className='nav-link' style={{ color: '#FFF' }}>
              Home
            </Link>
          </li>
        </div>
      </Grid>{' '}
      <Grid item xs />
      <Grid item>
        <div className='navbar-nav mr-auto'>
          <li id='suppliersLink' className='nav-item'>
            <Link
              to={'/suppliers'}
              className='nav-link'
              style={{ color: '#FFF' }}
            >
              Suppliers
            </Link>
          </li>
        </div>
      </Grid>{' '}
      <Grid item xs />
      <Grid item>
        <div className='navbar-nav mr-auto'>
          <li className='nav-item'>
            <Link
              id='dealsLink'
              to={'/deals'}
              className='nav-link'
              style={{ color: '#FFF' }}
            >
              Deals
            </Link>
          </li>
        </div>
      </Grid>{' '}
      <Grid item xs />
      <Grid item>
        <div className='navbar-nav mr-auto'>
          <li className='nav-item'>
            <Link
              id='importLink'
              to={'/import'}
              className='nav-link'
              style={{ color: '#FFF' }}
            >
              CSV Import
            </Link>
          </li>
        </div>
      </Grid>
      <Grid item xs />
      <Grid item>
        <Tooltip title='Alerts • No alerts'>
          <IconButton color='inherit'>
            <NotificationsIcon />
          </IconButton>
        </Tooltip>
      </Grid>
      <Grid item xs />
      <Grid item>
        <Link
          id='profileLink'
          to={'/profile'}
          className='nav-link'
          style={{ color: '#FFF' }}
        >
          <IconButton color='inherit' className={styles.iconButtonAvatar}>
            <Avatar src='/static/images/avatar/1.jpg' alt='My Avatar' />
          </IconButton>
        </Link>
      </Grid>
      <Grid item xs />
    </>
  );

  const stockManagerLinks = (
    <>
      <Grid item>
        <div className='navbar-nav mr-auto'>
          <li id='homeLink' className='nav-item'>
            <Link to={'/'} className='nav-link' style={{ color: '#FFF' }}>
              Home
            </Link>
          </li>
        </div>
      </Grid>{' '}
      <Grid item xs />
      <Grid item>
        <div className='navbar-nav mr-auto'>
          <li id='ingredientsLink' className='nav-item'>
            <Link
              to={'/ingredients'}
              className='nav-link'
              style={{ color: '#FFF' }}
            >
              Ingredients
            </Link>
          </li>
        </div>
      </Grid>{' '}
      <Grid item xs />
      <Grid item>
        <div className='navbar-nav mr-auto'>
          <li id='sandwichsLink' className='nav-item'>
            <Link
              to={'/sandwiches'}
              className='nav-link'
              style={{ color: '#FFF' }}
            >
              Sandwiches
            </Link>
          </li>
        </div>
      </Grid>{' '}
      <Grid item xs />
      <Grid item>
        <div className='navbar-nav mr-auto'>
          <li className='nav-item'>
            <Link
              id='importLink'
              to={'/import'}
              className='nav-link'
              style={{ color: '#FFF' }}
            >
              CSV Import
            </Link>
          </li>
        </div>
      </Grid>
      <Grid item xs />
      <Grid item>
        <Tooltip title='Alerts • No alerts'>
          <IconButton color='inherit'>
            <NotificationsIcon />
          </IconButton>
        </Tooltip>
      </Grid>
      <Grid item xs />
      <Grid item>
        <Link
          id='profileLink'
          to={'/profile'}
          className='nav-link'
          style={{ color: '#FFF' }}
        >
          <IconButton color='inherit' className={styles.iconButtonAvatar}>
            <Avatar src='/static/images/avatar/1.jpg' alt='My Avatar' />
          </IconButton>
        </Link>
      </Grid>
      <Grid item xs />
    </>
  );

  const schoolManagerLinks = (
    <>
      <Grid item>
        <div className='navbar-nav mr-auto'>
          <li id='homeLink' className='nav-item'>
            <Link to={'/'} className='nav-link' style={{ color: '#FFF' }}>
              Home
            </Link>
          </li>
        </div>
      </Grid>{' '}
      <Grid item xs />
      <Grid item>
        <div className='navbar-nav mr-auto'>
          <li id='schoolsLink' className='nav-item'>
            <Link
              to={'/schools'}
              className='nav-link'
              style={{ color: '#FFF' }}
            >
              Schools
            </Link>
          </li>
        </div>
      </Grid>{' '}
      <Grid item xs />
      <Grid item>
        <div className='navbar-nav mr-auto'>
          <li id='schoolsLink' className='nav-item'>
            <Link to={'/import'} className='nav-link' style={{ color: '#FFF' }}>
              CSV Import
            </Link>
          </li>
        </div>
      </Grid>{' '}
      <Grid item xs />
      <Grid item>
        <Tooltip title='Alerts • No alerts'>
          <IconButton color='inherit'>
            <NotificationsIcon />
          </IconButton>
        </Tooltip>
      </Grid>
      <Grid item xs />
      <Grid item>
        <Link
          id='profileLink'
          to={'/profile'}
          className='nav-link'
          style={{ color: '#FFF' }}
        >
          <IconButton color='inherit' className={styles.iconButtonAvatar}>
            <Avatar src='/static/images/avatar/1.jpg' alt='My Avatar' />
          </IconButton>
        </Link>
      </Grid>
      <Grid item xs />
    </>
  );

  const regLink = (
    <>
      <Grid item>
        <div className='navbar-nav mr-auto'>
          <li id='registerLink' className='nav-item'>
            <Link
              to={'/register'}
              className='nav-link'
              style={{ color: '#FFF' }}
            >
              Register
            </Link>
          </li>
        </div>
      </Grid>{' '}
      <Grid item xs />
    </>
  );

  const userRoutes = (
    <>
      <Switch>
        <Route exact path={['/']} component={Home} />
        <Route exact path={['/orders']} component={OrdersList} />
        <Route exact path={['/addOrder']} component={AddOrder} />
        <Route exact path={['/orders/:id']} component={Order} />
        <Route exact path={['/profile']} component={Profile} />
        <Route exact path={['/import']} component={Import} />
        <Route exact path={['/assignSchool/:email']} component={AssignSchool} />
      </Switch>
    </>
  );

  const supplierManagerRoutes = (
    <>
      <Switch>
        <Route exact path={['/']} component={Home} />
        <Route exact path={['/suppliers']} component={SuppliersList} />
        <Route exact path={['/addSupplier']} component={AddSupplier} />
        <Route path='/suppliers/:id' component={Supplier} />
        <Route exact path={['/deals']} component={DealsList} />
        <Route exact path={['/addDeal']} component={AddDeal} />
        <Route exact path={['/deals/:id']} component={Deal} />
        <Route exact path={['/import']} component={Import} />
        <Route exact path={['/profile']} component={Profile} />
      </Switch>
    </>
  );

  const stockManagerRoutes = (
    <>
      <Switch>
        <Route exact path={['/']} component={Home} />
        <Route exact path={['/ingredients']} component={IngredientsList} />
        <Route exact path={['/addIngredient']} component={AddIngredient} />
        <Route path='/ingredients/:id' component={Ingredient} />
        <Route exact path={['/sandwiches']} component={SandwichesList} />
        <Route exact path={['/addSandwich']} component={AddSandwich} />
        <Route path='/sandwiches/:id' component={Sandwich} />
        <Route exact path={['/import']} component={Import} />
        <Route exact path={['/profile']} component={Profile} />
      </Switch>
    </>
  );

  const schoolManagerRoutes = (
    <>
      <Switch>
        <Route exact path={['/']} component={Home} />
        <Route exact path={['/schools']} component={SchoolsList} />
        <Route exact path={['/addSchool']} component={AddSchool} />
        <Route exact path={['/schools/:id']} component={School} />
        <Route
          exact
          path={['/schools/:id/registerSandwiches']}
          component={RegisterSandwiches}
        />
        <Route exact path={['/import']} component={Import} />
        <Route exact path={['/profile']} component={Profile} />
      </Switch>
    </>
  );

  const regLogRoutes = (
    <>
      <Switch>
        <Route exact path={['/register']} component={Register} />
        <Route exact path={['/login']} component={Login} />
      </Switch>
    </>
  );

  const logLink = (
    <>
      <Grid item>
        <div className='navbar-nav mr-auto'>
          <li id='loginLink' className='nav-item'>
            <Link to={'/login'} className='nav-link' style={{ color: '#FFF' }}>
              Login
            </Link>
          </li>
        </div>
      </Grid>{' '}
      <Grid item xs />
    </>
  );

  return (
    <Paper className={styles.paper}>
      <div styles={{ backgroundColor: 'blue' }}>
        <React.Fragment>
          <AppBar
            style={{ background: '#479c36' }}
            position='sticky'
            elevation={0}
          >
            <Toolbar>
              <Grid container spacing={1} alignItems='center'>
                <Grid item>
                  <a className='navbar-brand' href='#'>
                    <img
                      id='sandwichLogo'
                      src='sandwichLogo.PNG'
                      width='30'
                      height='30'
                      alt=''
                    />
                  </a>
                </Grid>{' '}
                <Grid item xs />
                {auth.isLoggedIn ? <a></a> : regLink}
                <Grid item>
                  <div className='navbar-nav mr-auto'>
                    <Link
                      id='Home'
                      className='navbar-brand'
                      style={{ color: '#FFF', fontSize: '30px' }}
                    >
                      <b>GorgeousSandwich</b>
                    </Link>
                  </div>
                </Grid>{' '}
                <Grid item xs />
                <Grid item></Grid>
                {auth.role == 'STUDENT' ? userLinks : <Grid item></Grid>}
                {auth.role == 'SUPPLIERMANAGER' ? (
                  supplierManagerLinks
                ) : (
                  <Grid item></Grid>
                )}
                {auth.role == 'STOCKMANAGER' ? (
                  stockManagerLinks
                ) : (
                  <Grid item></Grid>
                )}
                {auth.role == 'SCHOOLMANAGER' ? (
                  schoolManagerLinks
                ) : (
                  <Grid item></Grid>
                )}
                {auth.isLoggedIn ? <a></a> : logLink}
              </Grid>
            </Toolbar>
          </AppBar>
        </React.Fragment>
      </div>
      <div className='container mt-3'>
        {auth.role == 'STOCKMANAGER' ? stockManagerRoutes : <a></a>}
        {auth.role == 'SUPPLIERMANAGER' ? supplierManagerRoutes : <a></a>}
        {auth.role == 'SCHOOLMANAGER' ? schoolManagerRoutes : <a></a>}
        {auth.role == 'STUDENT' ? userRoutes : <a></a>}
        {auth.isLoggedIn ? <a></a> : regLogRoutes}
        <Switch></Switch>
      </div>
      <Typography variant='body2' color='textSecondary' align='center'>
        {'Copyright © '}
        <Button
          id='external'
          color='inherit'
          target='_blank'
          href='https://www.isep.ipp.pt/'
        >
          Gorgeous Sandwich
        </Button>{' '}
        {new Date().getFullYear()}
        {'.'}
      </Typography>
    </Paper>
  );
};

export default App;
// = () => <SwaggerUI url="https://petstore.swagger.io/v2/swagger.json" />;
