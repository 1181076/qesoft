package arqsoft2.GorgeousSandwich.controller.supplier;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import arqsoft2.GorgeousSandwich.model.supplier.ID;
import arqsoft2.GorgeousSandwich.model.supplier.Supplier;
import arqsoft2.GorgeousSandwich.model.supplier.SupplierDTO;
import arqsoft2.GorgeousSandwich.service.supplier.SupplierService;

@CrossOrigin(origins = "http://localhost:8082")
@RestController
@RequestMapping("/api")
public class SupplierController {
    
    @Autowired
	SupplierService supplierService;

    public SupplierController(SupplierService supplierService){
        this.supplierService = supplierService;
    }

    @GetMapping("/suppliers")
    public ResponseEntity<List<SupplierDTO>> getAllSuppliers(@RequestParam(required = false) String title){
        try {
			List<SupplierDTO> suppliers = new ArrayList<SupplierDTO>();

			suppliers = supplierService.getAllSuppliers(title);

			if (suppliers.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(suppliers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

    @GetMapping("/suppliers/{id}")
	public ResponseEntity<SupplierDTO> getSupplierById(@PathVariable("id") String id) {
		SupplierDTO supplierData = supplierService.getSupplierById(id);

		if (supplierData != null) {
			return new ResponseEntity<>(supplierData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

    @PostMapping("/suppliers")
	public ResponseEntity<SupplierDTO> createSupplier(@RequestBody Supplier supplier) {

		try {
			SupplierDTO _supplier = supplierService
						.createSupplier(supplier);
			return new ResponseEntity<>(_supplier, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @PutMapping("/suppliers/{id}")
	public ResponseEntity<SupplierDTO> updateSupplier(@PathVariable("id") String id, @RequestBody Supplier supplier) throws ParseException {
		SupplierDTO supplierData = supplierService.updateSupplier(new ID(id), supplier);

		if (supplierData != null) {
			return new ResponseEntity<>(supplierData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/suppliers/{id}")
	public ResponseEntity<HttpStatus> deleteSupplier(@PathVariable("id") String id) {
			HttpStatus st = supplierService.deleteSupplier(new ID(id));
			return new ResponseEntity<>(st);
	}

	@DeleteMapping("/suppliers")
	public ResponseEntity<HttpStatus> deleteAllSuppliers() {
			HttpStatus st = supplierService.deleteAllSuppliers();
			return new ResponseEntity<>(st);
		
	}

}
