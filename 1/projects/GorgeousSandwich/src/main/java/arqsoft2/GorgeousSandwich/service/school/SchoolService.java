package arqsoft2.GorgeousSandwich.service.school;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.http.HttpClient;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import arqsoft2.GorgeousSandwich.model.sandwich.ID;
import arqsoft2.GorgeousSandwich.model.sandwich.Sandwich;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import arqsoft2.GorgeousSandwich.model.school.InternalID;
import arqsoft2.GorgeousSandwich.model.school.ExternalID;
import arqsoft2.GorgeousSandwich.model.school.Name;
import arqsoft2.GorgeousSandwich.model.school.Address;
import arqsoft2.GorgeousSandwich.model.school.School;
import arqsoft2.GorgeousSandwich.model.school.SchoolDTO;

import arqsoft2.GorgeousSandwich.repository.school.SchoolRepository;
import arqsoft2.GorgeousSandwich.repository.school.SchoolRepositoryImpl;

@Component
public class SchoolService {
    @Autowired
    SchoolRepository schoolRepository;

    @Autowired
    SchoolRepositoryImpl schoolRepositoryImpl;

    public SchoolService(SchoolRepository schoolRepository, SchoolRepositoryImpl schoolRepositoryImpl) {
        this.schoolRepository = schoolRepository;
        this.schoolRepositoryImpl = schoolRepositoryImpl;
    }

    public List<SchoolDTO> getAllSchools(@RequestParam(required = false) String title) {
        try {
            List<School> schools = schoolRepositoryImpl.findAllSchools();

            List<SchoolDTO> ret = new ArrayList<>();

            schools.forEach(e -> ret.add(e.toDTO()));

            return ret;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public SchoolDTO getSchoolById(@PathVariable("id") String externalId) {

        // VERSION WITH REPOSITORYIMPL 
        School schoolData = schoolRepositoryImpl.findSchoolById(externalId);

        if (schoolData!=null) {
            return schoolData.toDTO();
        } else {
            return null;
        }

        // Optional<School> schoolData = schoolRepository.findById(new ExternalID(externalId));

        // if (schoolData.isPresent()) {
        //     return schoolData.get().toDTO();
        // } else {
        //     return null;
        // }
    }

    public SchoolDTO createSchool(@RequestBody School school) {

        try {
            School _school = schoolRepository.save(school);

            return _school.toDTO();
        } catch (Exception e) {
            return null;
        }
    }

    public SchoolDTO updateSchool(@PathVariable("id") ExternalID externalID, @RequestBody School school) {
        Optional<School> schoolData = schoolRepository.findById(externalID);

        if (schoolData.isPresent()) {
            School _school = schoolData.get();
            _school.update(school);

            return schoolRepository.save(_school).toDTO();
        } else {
            return null;
        }
    }

    public HttpStatus deleteSchool(@PathVariable("id") ExternalID externalId) {
        try {
            schoolRepository.deleteById(externalId);
            return HttpStatus.NO_CONTENT;
        } catch (Exception e) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

    public HttpStatus deleteAllSchools() {
        try {
            schoolRepository.deleteAll();
            return HttpStatus.NO_CONTENT;
        } catch (Exception e) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

    }

    public SchoolDTO registerSandwiches(@PathVariable("schoolID") ExternalID id, @RequestBody List<Sandwich> sandwiches) {

            Optional<School> schoolData = schoolRepository.findById(id);

                    System.out.println(sandwiches);

            if (schoolData.isPresent()) {
                School _school = schoolData.get();
                _school.registerSandwiches(sandwiches);

                return schoolRepository.save(_school).toDTO();
            } else {
                return null;
            }

    }
}