package arqsoft2.GorgeousSandwich.model.supplier;

import arqsoft2.GorgeousSandwich.model.sandwich.Ingredient;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class DealDTO {
    
    private ID id;
    private UnitPrice unitPrice;
    private ExpirationPriceDate expirationPriceDate;
    private Ingredient ingredient;

    public DealDTO(){
    }

    public DealDTO(ID id, UnitPrice unitPrice, ExpirationPriceDate expirationPriceDate, Ingredient ingredient){
        this.id = id;
        this.unitPrice = unitPrice;
        this.expirationPriceDate = expirationPriceDate;
        this.ingredient = ingredient;
    }

    public ID getId() {
        return this.id;
    }

    public UnitPrice getUnitPrice() {
        return this.unitPrice;
    }

    public ExpirationPriceDate getExpirationPriceDate() {
        return this.expirationPriceDate;
    }

    public Ingredient getIngredient() {
        return this.ingredient;
    }

    @Override
    public boolean equals(Object k){
        return true;
    }
}
