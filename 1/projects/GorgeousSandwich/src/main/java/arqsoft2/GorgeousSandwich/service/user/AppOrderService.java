package arqsoft2.GorgeousSandwich.service.user;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import arqsoft2.GorgeousSandwich.model.user.ID;
import arqsoft2.GorgeousSandwich.model.user.AppOrder;
import arqsoft2.GorgeousSandwich.model.user.AppOrderDTO;
import arqsoft2.GorgeousSandwich.model.user.Day;
import arqsoft2.GorgeousSandwich.repository.user.AppOrderRepository;
import arqsoft2.GorgeousSandwich.repository.user.AppOrderRepositoryImpl;

@Component
public class AppOrderService { 

    @Autowired
    AppOrderRepositoryImpl orderRepositoryImpl;
    
    @Autowired
    AppOrderRepository orderRepository;

    public AppOrderService(AppOrderRepositoryImpl orderRepositoryImpl, AppOrderRepository orderRepository) {
        this.orderRepositoryImpl = orderRepositoryImpl;
        this.orderRepository = orderRepository;
    }

    public List<AppOrderDTO> getAllOrders(@RequestParam(required = false) String title) {
        try {
            List<AppOrder> orders = orderRepositoryImpl.findAllOrders();
            List<AppOrderDTO> ret = new ArrayList<>();
            orders.forEach(element -> ret.add(element.toDTO()));
            return ret;
        } catch (Exception error) {
            return new ArrayList<>();
        }
    }

    public AppOrderDTO getOrderById(@PathVariable("id") String id) {

        // VERSION WITH REPOSITORYIMPL
        AppOrder orderData = orderRepositoryImpl.findOrderById(id);
        
        if (orderData != null) {
            return orderData.toDTO();
        } else {
            return null;
        }

    //    Optional<AppOrder> orderData = orderRepository.findById(new ID(id));

    //     if (orderData.isPresent()) {
    //         return orderData.get().toDTO();
    //     } else {
    //         return null;
    //     }
    }

    public List<AppOrderDTO> getOrdersWithDayFilter(@PathVariable("startDay") Date startDay, @PathVariable("endDay") Date endDay) {
        try {
       
            // VERSION WITH REPOSITORYIMPL
            List<AppOrder> sandwiches = orderRepositoryImpl.getOrdersByInterval(startDay, endDay);

            // List<AppOrder> sandwiches = orderRepository.findByDayBetween(new Day(startDay), new Day(endDay));

            List<AppOrderDTO> ret = new ArrayList<>();

            sandwiches.forEach(e -> ret.add(e.toDTO()));

            return ret;

        } catch (Exception e) {
            System.out.println(e);
            return new ArrayList<>();
        }
    }


    public AppOrderDTO createOrder(@RequestBody AppOrder order) {

        System.out.println(order.getId().idValue());

        try {
            AppOrder _order = orderRepository.save(order);
            return _order.toDTO();
        } catch (Exception error) {
            
        System.out.println(error);
            return null;
        }
    }

    public AppOrderDTO updateOrder(@PathVariable("id") ID id, @RequestBody AppOrder order) {
        Optional<AppOrder> orderData = orderRepository.findById(id);

        if (orderData.isPresent()) {
            AppOrder _order = orderData.get();
            _order.update(order);
            return orderRepository.save(_order).toDTO();
        } else {
            return null;
        }
    }

    public HttpStatus deleteOrder(@PathVariable("id") ID id) {
        try {
            orderRepository.deleteById(id);
            return HttpStatus.NO_CONTENT;
        } catch (Exception error) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

    public HttpStatus deleteAllOrders() {
        try {
            orderRepository.deleteAll();
            return HttpStatus.NO_CONTENT;
        } catch (Exception error) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }
}
