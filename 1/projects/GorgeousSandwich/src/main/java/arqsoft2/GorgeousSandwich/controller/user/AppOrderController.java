package arqsoft2.GorgeousSandwich.controller.user;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import arqsoft2.GorgeousSandwich.model.user.ID;
import arqsoft2.GorgeousSandwich.service.user.AppOrderService;
import arqsoft2.GorgeousSandwich.model.user.AppOrder;
import arqsoft2.GorgeousSandwich.model.user.AppOrderDTO;

@CrossOrigin(origins = "http://localhost:8082")
@RestController
@RequestMapping("/api")
public class AppOrderController {

    @Autowired
    AppOrderService orderService;

    public AppOrderController(AppOrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/orders")
    public ResponseEntity<List<AppOrderDTO>> getAllOrders(@RequestParam(required = false) String title) {
        try {
            List<AppOrderDTO> orders = new ArrayList<AppOrderDTO>();

            orders = orderService.getAllOrders(title);

            if (orders.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(orders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity<AppOrderDTO> getOrderById(@PathVariable("id") String id) {
        AppOrderDTO orderData = orderService.getOrderById(id);

        if (orderData != null) {
            return new ResponseEntity<>(orderData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/orders/dayInterval/{startDay}/{endDay}")
    public ResponseEntity<List<AppOrderDTO>> getOrdersWithDayFilter(@PathVariable("startDay") Date startDay, @PathVariable("endDay") Date endDay) {
        try {
            List<AppOrderDTO> orders = new ArrayList<AppOrderDTO>();

            orders = orderService.getOrdersWithDayFilter(startDay, endDay);

            if (orders.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(orders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
    // @PreAuthorize("hasAuthority('STUDENT')")
    @PostMapping("/orders")
    public ResponseEntity<AppOrderDTO> createOrder(@RequestBody AppOrder order) {

        try {
            AppOrderDTO _order = orderService.createOrder(order);
            return new ResponseEntity<>(_order, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/orders/{id}")
    public ResponseEntity<AppOrderDTO> updateOrder(@PathVariable("id") String id, @RequestBody AppOrder order)
            throws ParseException {
        AppOrderDTO orderData = orderService.updateOrder(new ID(id), order);

        if (orderData != null) {
            return new ResponseEntity<>(orderData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity<HttpStatus> deleteOrder(@PathVariable("id") String id) {
        HttpStatus st = orderService.deleteOrder(new ID(id));
        return new ResponseEntity<>(st);
    }

    @DeleteMapping("/orders")
    public ResponseEntity<HttpStatus> deleteAllOrders() {
        HttpStatus st = orderService.deleteAllOrders();
        return new ResponseEntity<>(st);

    }
}
