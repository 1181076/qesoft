package arqsoft2.GorgeousSandwich.controller.importer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.net.http.WebSocket.Listener;

import org.apache.el.parser.ParseException;
import org.apache.el.parser.TokenMgrError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import arqsoft2.GorgeousSandwich.service.importer.ImportService;

@CrossOrigin(origins = "http://localhost:8082")
@RestController
@RequestMapping("/api")
public class ImportController {

	@Autowired
	ImportService importService;

	public ImportController(ImportService importService) {
		this.importService = importService;
	}

	@PostMapping("/import/createFromFile")
	public ResponseEntity<HttpStatus> importEntities(@RequestParam MultipartFile file)
			throws IllegalStateException, IOException, TokenMgrError, ParseException {
		try {
			
			return importService.importEntities(file);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
