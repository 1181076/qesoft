package arqsoft2.GorgeousSandwich.repository.user;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import arqsoft2.GorgeousSandwich.model.user.ID;
import arqsoft2.GorgeousSandwich.model.user.AppOrder;
import arqsoft2.GorgeousSandwich.model.user.Day;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AppOrderRepositoryImpl implements AppOrderCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<AppOrder> getOrdersByInterval(Date startDay, Date endDay) {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AppOrder> cq = cb.createQuery(AppOrder.class);
        Root<AppOrder> root = cq.from(AppOrder.class);
        Path<Date> day = root.get("day").<Date>get("value");
        
        cq.select(root).where(cb.between(day, startDay, endDay));

        TypedQuery<AppOrder> q = em.createQuery(cq);

        return q.getResultList();
    
    }

    @Override
    public List<AppOrder> findAllOrders(){

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AppOrder> cq = cb.createQuery(AppOrder.class);
        Root<AppOrder> root = cq.from(AppOrder.class);
        
        cq.select(root);

        TypedQuery<AppOrder> q = em.createQuery(cq);

        return q.getResultList();

    }

    @Override
    public AppOrder findOrderById(String id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AppOrder> cq = cb.createQuery(AppOrder.class);
        Root<AppOrder> root = cq.from(AppOrder.class);
        Path<String> idValue = root.get("id").get("idValue");
        
        cq.select(root).where(cb.equal(idValue, id));

        TypedQuery<AppOrder> q = em.createQuery(cq);

        return q.getSingleResult();
    }


}
