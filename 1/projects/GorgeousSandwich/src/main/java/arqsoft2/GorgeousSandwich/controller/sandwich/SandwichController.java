package arqsoft2.GorgeousSandwich.controller.sandwich;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import arqsoft2.GorgeousSandwich.utils.ErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import arqsoft2.GorgeousSandwich.model.sandwich.ID;
import arqsoft2.GorgeousSandwich.model.sandwich.Sandwich;
import arqsoft2.GorgeousSandwich.model.sandwich.SandwichDTO;
import arqsoft2.GorgeousSandwich.service.sandwich.SandwichService;

@CrossOrigin(origins = "http://localhost:8082")
@RestController
@RequestMapping("/api")
public class SandwichController {

    @Autowired
    SandwichService sandwichService;


    public SandwichController(SandwichService sandwichService){
        this.sandwichService = sandwichService;
    }

    @GetMapping("/sandwiches")
    public ResponseEntity<List<SandwichDTO>> getAllSandwiches(@RequestParam(required = false) String title) {
        try {
            List<SandwichDTO> sandwiches = new ArrayList<SandwichDTO>();

            sandwiches = sandwichService.getAllSandwiches(title);

            if (sandwiches.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(sandwiches, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/sandwiches/{id}")
    public ResponseEntity<SandwichDTO> getSandwichById(@PathVariable("id") String id) {
        SandwichDTO sandwichData = sandwichService.getSandwichById(id);

        if (sandwichData != null) {
            return new ResponseEntity<>(sandwichData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/sandwiches/priceFilter/{currency}/{minimumPrice}/{maximumPrice}")
    public ResponseEntity<List<SandwichDTO>> getSandwichesWithPriceFilter(@PathVariable("currency") String currency, @PathVariable("minimumPrice") double minimumPrice, @PathVariable("maximumPrice") double maximumPrice) {
        try {
            List<SandwichDTO> sandwiches = new ArrayList<SandwichDTO>();

            sandwiches = sandwichService.getSandwichesWithPriceFilter(currency, minimumPrice, maximumPrice);

            if (sandwiches.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(sandwiches, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/sandwiches/ingredientsFilter/{list}")
    public ResponseEntity<List<SandwichDTO>> getSandwichesWithIngredientFilter(@PathVariable("list") String[] list) {
        try {
            List<SandwichDTO> sandwiches = new ArrayList<SandwichDTO>();

            sandwiches = sandwichService.getSandwichesWithIngredientFilter(list);

            if (sandwiches.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(sandwiches, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/sandwiches")
    public ResponseEntity<SandwichDTO> createSandwich(@RequestBody Sandwich sandwich) {
        
        try {
            SandwichDTO _sandwich = sandwichService
                    .createSandwich(sandwich);
            return new ResponseEntity<>(_sandwich, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/sandwiches/{id}")
    public ResponseEntity<SandwichDTO> updateSandwich(@PathVariable("id") String id, @RequestBody Sandwich sandwich) throws ParseException {
        SandwichDTO sandwichData = sandwichService.updateSandwich(new ID(id), sandwich);

        if (sandwichData != null) {
            return new ResponseEntity<>(sandwichData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/sandwiches/{id}")
    public ResponseEntity<HttpStatus> deleteSandwich(@PathVariable("id") String id) {
        HttpStatus st = sandwichService.deleteSandwich(new ID(id));
        return new ResponseEntity<>(st);
    }

    @DeleteMapping("/sandwiches")
    public ResponseEntity<HttpStatus> deleteAllSandwiches() {
        HttpStatus st = sandwichService.deleteAllSandwiches();
        return new ResponseEntity<>(st);

    }
}
