package arqsoft2.GorgeousSandwich.model.sandwich;

import java.util.List;

import javax.persistence.*;

import arqsoft2.GorgeousSandwich.model.supplier.Supplier;
import eapli.framework.representations.dto.DTOable;

@Entity
public class Ingredient implements IngredientInterface, DTOable<IngredientDTO>{
	
    @EmbeddedId
    private ID id;

    private Description description;
    private Quantity quantity;
	
	@ElementCollection
    private List<LastUpdatedDate> lastUpdatedDate;
    private UpperLimit upperLimit;
    private LowerLimit lowerLimit;
    private Units units;

    public Ingredient(){

    }

    public Ingredient(ID id, Description description, Quantity quantity, List<LastUpdatedDate> lastUpdatedDate, UpperLimit upperLimit, LowerLimit lowerLimit, Units units){
        if(upperLimit.getValue() < lowerLimit.getValue()){
        	throw new IllegalArgumentException("The upper limit cannot be below the lower limit!");
		}

        if(quantity.getValue() < lowerLimit.getValue() || quantity.getValue() > upperLimit.getValue()){
        	throw new IllegalArgumentException("The quantity must be between the lower and upper limit!");
		}

    	this.id = id;
        this.description = description;
        this.quantity = quantity;
        this.lastUpdatedDate = lastUpdatedDate;
        this.upperLimit = upperLimit;
        this.lowerLimit = lowerLimit;
        this.units = units;
    }

	public ID getId() {
		return this.id;
	}

	public Description getDescription() {
		return this.description;
	}

	public Quantity getQuantity() {
		return this.quantity;
	}

	public List<LastUpdatedDate> getlastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public UpperLimit getUpperLimit() {
		return this.upperLimit;
	}

	public LowerLimit getLowerLimit() {
		return this.lowerLimit;
	}

	public Units getUnits() {
		return this.units;
	}

    @Override
    public IngredientDTO toDTO() {
        return new IngredientDTO(id, description, quantity, lastUpdatedDate, upperLimit, lowerLimit, units);
	}

	public void update(Ingredient ing){
		this.description = ing.description;
		this.quantity = ing.quantity;
		this.lastUpdatedDate = ing.lastUpdatedDate;
		this.upperLimit = ing.upperLimit;
		this.lowerLimit = ing.lowerLimit;
		this.units = ing.units;
	} 


}
