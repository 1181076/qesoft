package arqsoft2.GorgeousSandwich.repository.supplier;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import arqsoft2.GorgeousSandwich.model.supplier.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SupplierRepositoryImpl implements SupplierCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Supplier> findAllSuppliers(){

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Supplier> cq = cb.createQuery(Supplier.class);
        Root<Supplier> root = cq.from(Supplier.class);
        
        cq.select(root);

        TypedQuery<Supplier> q = em.createQuery(cq);

        return q.getResultList();

    }

    @Override
    public Supplier findSupplierById(String id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Supplier> cq = cb.createQuery(Supplier.class);
        Root<Supplier> root = cq.from(Supplier.class);
        Path<String> idValue = root.get("id").get("value");
        
        cq.select(root).where(cb.equal(idValue, id));

        TypedQuery<Supplier> q = em.createQuery(cq);

        return q.getSingleResult();
    }



}
