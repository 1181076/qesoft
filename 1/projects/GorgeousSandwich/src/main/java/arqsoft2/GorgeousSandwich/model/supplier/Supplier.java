package arqsoft2.GorgeousSandwich.model.supplier;

import java.util.List;

import javax.persistence.*;

import eapli.framework.representations.dto.DTOable;

@Entity
public class Supplier implements SupplierInterface, DTOable<SupplierDTO> {

    @EmbeddedId
    private ID id;

    private Name name;
    private Address address;
    
	@ElementCollection
    private List<Email> emails;
    
	@ElementCollection
    private List<Contact> contacts;
    private Description description;
    
    @ManyToMany
    private List<Deal> deals;

    public Supplier() {
    }

    public Supplier(ID id, Name name, Address address, List<Email> emails, List<Contact> contacts, Description description, List<Deal> deals) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.emails = emails;
        this.contacts = contacts;
        this.description = description;
        this.deals = deals;
    }

    public ID getId() {
        return this.id;
    }

    public Name getName() {
        return this.name;
    }

    public Address getAddress() {
        return this.address;
    }

    public List<Email> getEmails() {
        return this.emails;
    }

    public List<Contact> getContacts() {
        return this.contacts;
    }

    public Description getDescription() {
        return this.description;
    }

    public List<Deal> getDeals() {
        return this.deals;
    }

    public void update(Supplier supplier){
        this.name = supplier.name;
        this.address = supplier.address;
        this.emails = supplier.emails;
        this.contacts = supplier.contacts;
        this.description = supplier.description;
        this.deals = supplier.deals;
    }

    @Override
    public SupplierDTO toDTO() {
        return new SupplierDTO(id, name, address, emails, contacts, description, deals);
    }

}
