package arqsoft2.GorgeousSandwich.repository.supplier;

import org.springframework.data.jpa.repository.JpaRepository;

import arqsoft2.GorgeousSandwich.model.supplier.ID;
import arqsoft2.GorgeousSandwich.model.supplier.Supplier;

public interface SupplierRepository extends JpaRepository<Supplier, ID>, SupplierCustomRepository {
    
}
