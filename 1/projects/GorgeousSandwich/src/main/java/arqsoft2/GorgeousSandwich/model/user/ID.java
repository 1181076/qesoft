package arqsoft2.GorgeousSandwich.model.user;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;

public final class ID implements ValueObject {

    private final String idValue;

    public ID(){
        idValue = "default";
    }

    public final String getIdValue() {
        return this.idValue;
    }

    @JsonCreator
    public ID(@JsonProperty("value") String idValue) {
        if(idValue.isEmpty()){
            throw new IllegalArgumentException("The ID cannot be empty!");
        }
        this.idValue = idValue;
    }

    public String idValue() {
        return idValue;
    }

    @Override
    public boolean equals(Object o) {

        ID id = (ID) o;

        return id.idValue().equals(this.idValue);

    }
}
