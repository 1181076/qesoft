package arqsoft2.GorgeousSandwich.model.supplier;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;

public final class Contact implements ValueObject {
    
    private final int value;

    @JsonCreator
    public Contact(@JsonProperty("value") int value){
        if(value < 100000000 || value > 999999999){
            throw new IllegalArgumentException("The contact needs to have exactly 9 digits!");
        }
        this.value= value;
    }
    
    public int value(){
        return value;
    }

    public final int getValue() {
		return this.value;
	}
}
