package arqsoft2.GorgeousSandwich.model.supplier;

import java.util.List;

import javax.persistence.*;

import eapli.framework.representations.dto.DTOable;

public interface SupplierInterface {

    public ID getId();
    public Name getName();
    public Address getAddress();
    public List<Email> getEmails();
    public List<Contact> getContacts();
    public Description getDescription();
    public List<Deal> getDeals();

    public void update(Supplier supplier);
    
    public SupplierDTO toDTO();

}
