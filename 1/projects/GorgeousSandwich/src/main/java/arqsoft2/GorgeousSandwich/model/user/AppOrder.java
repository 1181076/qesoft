package arqsoft2.GorgeousSandwich.model.user;

import java.util.List;

import javax.persistence.*;
import eapli.framework.representations.dto.DTOable;

@Entity
public class AppOrder implements DTOable<AppOrderDTO> {

    @EmbeddedId
    private ID id;
    
    @Embedded
    private Day day;
    
    @ElementCollection    
    private List<SandwichOrder> sandwichOrders;

    public AppOrder() {
    }

    public AppOrder(Day day, List<SandwichOrder> sandwichOrders) {
        this.day = day;
        this.sandwichOrders = sandwichOrders;
    }

    public ID getId() {
        return this.id;
    }

    public Day getDay() {
        return this.day;
    }

    public List<SandwichOrder> getSandwichOrders() {
        return this.sandwichOrders;
    }

    public void update(AppOrder order) {
        this.day = order.day;
        this.sandwichOrders = order.sandwichOrders;
    }

    @Override
    public AppOrderDTO toDTO() {
        return new AppOrderDTO(id, day, sandwichOrders);
    }
}
