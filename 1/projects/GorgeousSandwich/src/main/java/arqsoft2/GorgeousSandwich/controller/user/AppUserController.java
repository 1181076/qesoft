package arqsoft2.GorgeousSandwich.controller.user;

import java.text.ParseException;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import arqsoft2.GorgeousSandwich.model.user.Email;
import arqsoft2.GorgeousSandwich.model.user.Password;
import arqsoft2.GorgeousSandwich.security.JwtTokenProvider;
import arqsoft2.GorgeousSandwich.model.user.AppUser;
import arqsoft2.GorgeousSandwich.model.user.AppUserDTO;
import arqsoft2.GorgeousSandwich.service.user.AppUserService;


@CrossOrigin(origins = "http://localhost:8082")
@RestController
@RequestMapping("/api")
public class AppUserController {

	@Autowired
	AppUserService appUserService;

	
	@Autowired
	private AuthenticationManager authenticationManager;

	
	@Autowired
	private JwtTokenProvider tokenProvider;


	public AppUserController(AppUserService appUserService){
		this.appUserService = appUserService;
	}

	// @GetMapping("/appUsers")	
	// public ResponseEntity<List<AppUserDTO>> getAllAppUsers(@RequestParam(required = false) String title) {
	// 	try {
	// 		List<AppUserDTO> appUsers = new ArrayList<AppUserDTO>();

	// 		appUsers = appUserService.getAllAppUsers(title);

	// 		if (appUsers.isEmpty()) {
	// 			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	// 		}

	// 		return new ResponseEntity<>(appUsers, HttpStatus.OK);
	// 	} catch (Exception e) {
	// 		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	// 	}
	// }

	// @GetMapping("/appUsers/{id}")
	// public ResponseEntity<AppUserDTO> getAppUserById(@PathVariable("id") String email) {
	// 	AppUserDTO appUserData = appUserService.getAppUserById(email);

	// 	if (appUserData != null) {
	// 		return new ResponseEntity<>(appUserData, HttpStatus.OK);
	// 	} else {
	// 		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	// 	}
	// }

	@PostMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AppUserDTO> createAppUser(@RequestBody AppUser user) {
		try {
			user.update(new AppUser(
				user.getEmail(), 
				user.getName(), 
				new Password(new BCryptPasswordEncoder().encode(user.getPassword().getValue())), 
				user.getRole(), 
				user.getOrders(),
				user.getSchool()));

			AppUserDTO savedUser = appUserService.createAppUser(user);
			
			return new ResponseEntity<>(savedUser, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			
		System.out.println("22222 -> " + user);
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
	}

	@PostMapping(value = "/authenticate", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> authenticate(@RequestBody AppUser user) {
		
		JSONObject sampleObject = new JSONObject();
		
		JSONObject jsonObject = new JSONObject();

		try {
			
			Authentication authentication = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(user.getEmail().getValue(), user.getPassword().getValue()));
			
			if (authentication.isAuthenticated()) {
				String email = user.getEmail().getValue();
				AppUserDTO a = appUserService.getAppUserById(email);
				sampleObject.put("email", authentication.getName());
				sampleObject.put("name", a.getName().getValue());
				// sampleObject.put("school", a.getSchoolID().getReferenceID());
				sampleObject.put("role", authentication.getAuthorities().iterator().next().getAuthority());
				sampleObject.put("authorities", authentication.getAuthorities());
				sampleObject.put("token", tokenProvider.createToken(email, appUserService.getAppUserById(email).getRole()));
				return new ResponseEntity<String>(sampleObject.toString(), HttpStatus.OK);
			}
		} catch (JSONException e) {
			try {
				jsonObject.put("exception", e.getMessage());
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			return new ResponseEntity<String>(jsonObject.toString(), HttpStatus.UNAUTHORIZED);
		}
		return null;
	}

	@PutMapping("/appUsers/{id}")
	public ResponseEntity<AppUserDTO> updateAppUser(@PathVariable("id") String email, @RequestBody AppUser appUser) throws ParseException {
		AppUserDTO appUserData = appUserService.updateAppUser(new Email(email), appUser);

		if (appUserData != null) {
			return new ResponseEntity<>(appUserData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/appUsers/{id}")
	public ResponseEntity<HttpStatus> deleteAppUser(@PathVariable("id") String email) {
			HttpStatus st = appUserService.deleteAppUser(new Email(email));
			return new ResponseEntity<>(st);
	}

	@DeleteMapping("/appUsers")
	public ResponseEntity<HttpStatus> deleteAllAppUsers() {
			HttpStatus st = appUserService.deleteAllAppUsers();
			return new ResponseEntity<>(st);
		
	}

	// @PostMapping("/appUsers/${id}")
	// public ResponseEntity<AppUserDTO> assignSchool(@PathVariable("id") String email, @RequestBody String school) {
	// 	AppUserDTO appUserData = appUserService.assignSchool(email, school);

	// 	if (appUserData != null) {
	// 		return new ResponseEntity<>(appUserData, HttpStatus.OK);
	// 	} else {
	// 		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	// 	}
	// }
    
}
