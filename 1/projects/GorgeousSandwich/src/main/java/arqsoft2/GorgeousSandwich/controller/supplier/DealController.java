package arqsoft2.GorgeousSandwich.controller.supplier;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import arqsoft2.GorgeousSandwich.model.supplier.ID;
import arqsoft2.GorgeousSandwich.model.supplier.Deal;
import arqsoft2.GorgeousSandwich.model.supplier.DealDTO;
import arqsoft2.GorgeousSandwich.service.supplier.DealService;

@CrossOrigin(origins = "http://localhost:8082")
@RestController
@RequestMapping("/api")
public class DealController {
    
    @Autowired
	DealService dealService;


	public DealController(DealService dealService){
		this.dealService = dealService;
	}

    @GetMapping("/deals")
	public ResponseEntity<List<DealDTO>> getAllDeals(@RequestParam(required = false) String title) {
		try {
			List<DealDTO> deals = new ArrayList<DealDTO>();

			deals = dealService.getAllDeals(title);

			if (deals.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(deals, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @GetMapping("/deals/{id}")
	public ResponseEntity<DealDTO> getDealById(@PathVariable("id") String id) {
		DealDTO dealData = dealService.getDealById(id);

		if (dealData != null) {
			return new ResponseEntity<>(dealData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/deals")
	public ResponseEntity<DealDTO> createDeal(@RequestBody Deal deal) {

		System.out.println(deal);

		try {
			DealDTO _deal = dealService
						.createDeal(deal);
			return new ResponseEntity<>(_deal, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @PutMapping("/deals/{id}")
	public ResponseEntity<DealDTO> updateDeal(@PathVariable("id") String id, @RequestBody Deal deal) throws ParseException {
		DealDTO dealData = dealService.updateDeal(new ID(id), deal);

		if (dealData != null) {
			return new ResponseEntity<>(dealData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/deals/{id}")
	public ResponseEntity<HttpStatus> deleteDeal(@PathVariable("id") String id) {
			HttpStatus st = dealService.deleteDeal(new ID(id));
			return new ResponseEntity<>(st);
	}

	@DeleteMapping("/deals")
	public ResponseEntity<HttpStatus> deleteAllDeals() {
			HttpStatus st = dealService.deleteAllDeals();
			return new ResponseEntity<>(st);
		
	}
}
