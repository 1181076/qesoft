package arqsoft2.GorgeousSandwich.repository.sandwich;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import arqsoft2.GorgeousSandwich.model.sandwich.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class IngredientRepositoryImpl implements IngredientCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Ingredient> findAllIngredients(){

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Ingredient> cq = cb.createQuery(Ingredient.class);
        Root<Ingredient> root = cq.from(Ingredient.class);
        
        cq.select(root);

        TypedQuery<Ingredient> q = em.createQuery(cq);

        return q.getResultList();

    }

    @Override
    public Ingredient findIngredientById(String id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Ingredient> cq = cb.createQuery(Ingredient.class);
        Root<Ingredient> root = cq.from(Ingredient.class);
        Path<String> idValue = root.get("id").get("value");
        
        cq.select(root).where(cb.equal(idValue, id));

        TypedQuery<Ingredient> q = em.createQuery(cq);

        return q.getSingleResult();
    }



}
