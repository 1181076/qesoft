package arqsoft2.GorgeousSandwich.model.supplier;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;

public final class UnitPrice implements ValueObject{

    private final String unit;
    private final double value;

    @JsonCreator
    public UnitPrice(@JsonProperty("value") double value,@JsonProperty("unit") String unit){
        if(value < 0){
            throw new IllegalArgumentException("The price value cannot be negative!");
        }

        if(unit.isEmpty()){
            throw new IllegalArgumentException("The unit price cannot be empty!");
        }
        this.value= value;
        this.unit = unit;
    }
    
    public double value(){
        return value;
    }

    public final double getValue() {
		return this.value;
	}

    public String unit(){
        return unit;
    }

    public final String getUnit() {
		return this.unit;
	}
}
