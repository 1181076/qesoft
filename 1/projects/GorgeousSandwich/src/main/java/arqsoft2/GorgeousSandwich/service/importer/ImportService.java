package arqsoft2.GorgeousSandwich.service.importer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.sql.Date;
import java.util.HashMap;
import java.net.http.HttpClient;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.text.SimpleDateFormat;

import org.apache.el.parser.TokenMgrError;
import org.springframework.web.multipart.MultipartFile;
import org.hibernate.annotations.SourceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import arqsoft2.GorgeousSandwich.repository.sandwich.IngredientRepository;
import arqsoft2.GorgeousSandwich.model.sandwich.Description;
import arqsoft2.GorgeousSandwich.model.sandwich.ID;
import arqsoft2.GorgeousSandwich.model.sandwich.Ingredient;
import arqsoft2.GorgeousSandwich.model.sandwich.LastUpdatedDate;
import arqsoft2.GorgeousSandwich.model.sandwich.LowerLimit;
import arqsoft2.GorgeousSandwich.model.sandwich.Price;
import arqsoft2.GorgeousSandwich.model.sandwich.Quantity;
// import arqsoft2.GorgeousSandwich.model.sandwich.ID;
import arqsoft2.GorgeousSandwich.repository.sandwich.SandwichRepository;
import arqsoft2.GorgeousSandwich.model.sandwich.Sandwich;
import arqsoft2.GorgeousSandwich.model.sandwich.ShortDescription;
import arqsoft2.GorgeousSandwich.model.sandwich.Units;
import arqsoft2.GorgeousSandwich.model.sandwich.UpperLimit;
import arqsoft2.GorgeousSandwich.repository.school.SchoolRepository;
import arqsoft2.GorgeousSandwich.model.school.School;
import arqsoft2.GorgeousSandwich.repository.supplier.DealRepository;
import arqsoft2.GorgeousSandwich.model.supplier.Deal;
import arqsoft2.GorgeousSandwich.repository.supplier.SupplierRepository;
import arqsoft2.GorgeousSandwich.model.supplier.Supplier;

@Component
public class ImportService {

    private String[] IngredientType = { "ï»¿id", "description", "quantity", "lastUpdateDate", "upperLimit", "lowerLimit",
            "units" };
    private String[] SandwichType = { "ï»¿id", "shortDescription", "extendedDescription", "price", "ingredients" };
    private String[] SchoolType = { "ï»¿id", "internalID", "name", "address", "sandwiches" };
    private String[] DealType = { "ï»¿id", "unitPrice", "expirationDate", "ingredient" };
    private String[] SupplierType = { "ï»¿id", "name", "address", "emails", "contacts", "description", "deals" };

    @Autowired
    IngredientRepository ingredientRepository;
    @Autowired
    SandwichRepository sandwichRepository;
    @Autowired
    SchoolRepository schoolRepository;
    @Autowired
    DealRepository dealRepository;
    @Autowired
    SupplierRepository supplierRepository;

    public ImportService(IngredientRepository ingredientRepository, SandwichRepository sandwichRepository,
            SchoolRepository schoolRepository, DealRepository dealRepository, SupplierRepository supplierRepository) {
        this.ingredientRepository = ingredientRepository;
        this.sandwichRepository = sandwichRepository;
        this.schoolRepository = schoolRepository;
        this.dealRepository = dealRepository;
        this.supplierRepository = supplierRepository;
    }

    public ResponseEntity<HttpStatus> importEntities(@RequestParam MultipartFile file)
            throws IllegalStateException, IOException, TokenMgrError, ParseException {
        try {
            File newFile = new File(System.getProperty("java.io.tmpdir") + "/" + "temp");
            file.transferTo(newFile);
            Reader reader = new FileReader(newFile);
            BufferedReader buffer = new BufferedReader(reader);
            String line = " ";
            String[] tempArr;
            String[] dates;

            line = buffer.readLine();
            if (line != null) {
                tempArr = line.split(";");
                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>> " + line);
                switch (this.checkTypeEntity(tempArr)) {
                case "Ingredient":
                    System.out.print("It's ingredient");
                    while ((line = buffer.readLine()) != null) {
                        List<Supplier> arrayIds = new ArrayList<Supplier>();
                        tempArr = line.split(";");
                        dates = tempArr[3].split(",");                        
                        List<LastUpdatedDate> lastUpdatedDates = new ArrayList<LastUpdatedDate>();
                        for (String date: dates){                            
                            lastUpdatedDates.add(new LastUpdatedDate(Date.valueOf(date.split(":")[0]), Integer.parseInt(date.split(":")[1])));
                        }
                        try {
                            Ingredient ingredient = new Ingredient(
                                    new arqsoft2.GorgeousSandwich.model.sandwich.ID(tempArr[0].trim()),
                                    new Description(tempArr[1].trim()),
                                    new Quantity(Integer.parseInt(tempArr[2].trim())),
                                    lastUpdatedDates,
                                    new UpperLimit(Integer.parseInt(tempArr[4].trim())),
                                    new LowerLimit(Integer.parseInt(tempArr[5].trim())), new Units(tempArr[6].trim()));
                            ingredientRepository.save(ingredient);
                        } catch (Exception error) {
                            // TO DO
                            // Write to csv of fails
                            System.out.println(error);
                        }
                    }
                    break;
                case "Sandwich":
                    System.out.print("It's Sandwich");
                    while ((line = buffer.readLine()) != null) {
                        List<Ingredient> arrayIds = new ArrayList<Ingredient>();
                        tempArr = line.split(";");
                        String[] referencedIds = tempArr[4].split(",");
                        for (String id : referencedIds) {
                            arrayIds.add(ingredientRepository.findIngredientById(id.trim()));
                        }
                        String[] priceValues = tempArr[3].split(",");
                        try {
                            Sandwich sandwich = new Sandwich(
                                    new arqsoft2.GorgeousSandwich.model.sandwich.ID(tempArr[0].trim()),
                                    new ShortDescription(tempArr[1].trim()), new Description(tempArr[2].trim()),
                                    new Price(Integer.parseInt(priceValues[0]), priceValues[1]), arrayIds);
                            sandwichRepository.save(sandwich);
                        } catch (Exception error) {
                            // TO DO
                            // Write to csv of fails
                            System.out.println(error);
                        }
                    }
                    break;
                case "School":
                    System.out.print("It's Schools");
                    while ((line = buffer.readLine()) != null) {
                        List<Sandwich> arrayIds = new ArrayList<Sandwich>();
                        tempArr = line.split(";");
                        String[] referencedIds = tempArr[4].split(",");
                        for (String id : referencedIds) {
                            arrayIds.add(sandwichRepository.getById(new ID(id.trim())));
                        }
                        try {
                            School school = new School(
                                    new arqsoft2.GorgeousSandwich.model.school.ExternalID(tempArr[0].trim()),
                                    new arqsoft2.GorgeousSandwich.model.school.InternalID(tempArr[1].trim()),
                                    new arqsoft2.GorgeousSandwich.model.school.Name(tempArr[2].trim()),
                                    new arqsoft2.GorgeousSandwich.model.school.Address(tempArr[3].trim()), arrayIds);
                            schoolRepository.save(school);
                        } catch (Exception error) {
                            // TO DO
                            // Write to csv of fails
                            System.out.println(error);
                        }
                    }
                    break;
                case "Deal":
                    System.out.print("It's Deals");
                    while ((line = buffer.readLine()) != null) {
                        tempArr = line.split(";");
                        String[] unitPrice = tempArr[1].split(",");
                        try {
                            Deal deal = new Deal(new arqsoft2.GorgeousSandwich.model.supplier.ID(tempArr[0].trim()),
                                    new arqsoft2.GorgeousSandwich.model.supplier.UnitPrice(
                                            Double.parseDouble(unitPrice[1]), unitPrice[0]),
                                    new arqsoft2.GorgeousSandwich.model.supplier.ExpirationPriceDate(
                                            Date.valueOf(tempArr[2].trim())),
                                    ingredientRepository.findIngredientById(tempArr[3].trim()));
                            dealRepository.save(deal);
                        } catch (Exception error) {
                            // TO DO
                            // Write to csv of fails
                            System.out.println(error);
                        }
                    }
                    break;
                case "Supplier":
                    System.out.print("It's Suppliers");
                    while ((line = buffer.readLine()) != null) {
                        List<Deal> arrayIds = new ArrayList<Deal>();
                        List<arqsoft2.GorgeousSandwich.model.supplier.Email> emails = new ArrayList<arqsoft2.GorgeousSandwich.model.supplier.Email>();
                        List<arqsoft2.GorgeousSandwich.model.supplier.Contact> contacts = new ArrayList<arqsoft2.GorgeousSandwich.model.supplier.Contact>();
                        tempArr = line.split(";");
                        System.out.println("1-----------------> " + tempArr);
                        String[] referencedIds = tempArr[6].split(",");
                        for (String id : referencedIds) {
                            arrayIds.add(dealRepository.findDealById(id.trim()));
                        }
                        System.out.println("2-----------------> " + tempArr);
                        String[] arrayMails = tempArr[3].split(",");
                        for (String mail : arrayMails) {
                            emails.add(new arqsoft2.GorgeousSandwich.model.supplier.Email(mail.trim()));
                        }
                        System.out.println("3-----------------> " + tempArr);
                        String[] arrayContacts = tempArr[4].split(",");
                        System.out.println("3-----------------> " + tempArr[4]);
                        System.out.println("3-----------------> " + arrayContacts.length);
                        for (String contact : arrayContacts) {
                            contacts.add(new arqsoft2.GorgeousSandwich.model.supplier.Contact(
                                    Integer.parseInt(contact.trim())));
                        }                        
                        System.out.println("4-----------------> " + tempArr);
                        try {
                            Supplier supplier = new Supplier(
                                    new arqsoft2.GorgeousSandwich.model.supplier.ID(tempArr[0].trim()),
                                    new arqsoft2.GorgeousSandwich.model.supplier.Name(tempArr[1].trim()),
                                    new arqsoft2.GorgeousSandwich.model.supplier.Address(tempArr[2].trim()), emails,
                                    contacts,
                                    new arqsoft2.GorgeousSandwich.model.supplier.Description(tempArr[5].trim()),
                                    arrayIds);
                                    
                            System.out.println(supplier);
                            supplierRepository.save(supplier);
                        } catch (Exception error) {
                            // TO DO
                            // Write to csv of fails
                            System.out.println(error);
                        }
                    }
                    break;
                default:
                    System.out.print("INVALID_FILE");
                    break;
                }
                buffer.close();
                return new ResponseEntity<>(HttpStatus.CREATED);
            } else {
                buffer.close();
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private String checkTypeEntity(String[] firstLine) {
        if (isIngredient(firstLine)) {
            return "Ingredient";
        } else {
            if (isSandwich(firstLine)) {
                return "Sandwich";
            } else {
                if (isSchool(firstLine)) {
                    return "School";
                } else {
                    if (isDeal(firstLine)) {
                        return "Deal";
                    } else {
                        if (isSupplier(firstLine)) {
                            return "Supplier";
                        } else {
                            return "";
                        }
                    }
                }
            }
        }
    }

    private Boolean isIngredient(String[] firstLine) {
        if (!firstLine[0].trim().toString().equals(this.IngredientType[0])
                && !firstLine[0].trim().substring(1).toString().equals(this.IngredientType[0])) {
            return false;
        } else {
            for (int i = 1; i < firstLine.length; i++) {
                if (!this.IngredientType[i].equals(firstLine[i].trim())) {
                    return false;
                }
            }
        }
        return true;
    }

    private Boolean isSandwich(String[] firstLine) {
        if (!firstLine[0].trim().toString().equals(this.SandwichType[0])
                && !firstLine[0].trim().substring(1).toString().equals(this.SandwichType[0])) {
            return false;
        } else {
            for (int i = 1; i < firstLine.length; i++) {
                if (!this.SandwichType[i].equals(firstLine[i].trim())) {
                    return false;
                }
            }
        }
        return true;
    }

    private Boolean isDeal(String[] firstLine) {
        if (!firstLine[0].trim().toString().equals(this.DealType[0])
                && !firstLine[0].trim().substring(1).toString().equals(this.DealType[0])) {
            return false;
        } else {
            for (int i = 1; i < firstLine.length; i++) {
                if (!this.DealType[i].equals(firstLine[i].trim())) {
                    return false;
                }
            }
        }
        return true;
    }

    private Boolean isSupplier(String[] firstLine) {
        if (!firstLine[0].trim().toString().equals(this.SupplierType[0])
                && !firstLine[0].trim().substring(1).toString().equals(this.SupplierType[0])) {
            return false;
        } else {
            for (int i = 1; i < firstLine.length; i++) {
                if (!this.SupplierType[i].equals(firstLine[i].trim())) {
                    return false;
                }
            }
        }
        return true;
    }

    private Boolean isSchool(String[] firstLine) {
        if (!firstLine[0].trim().toString().equals(this.SchoolType[0])
                && !firstLine[0].trim().substring(1).toString().equals(this.SchoolType[0])) {
            return false;
        } else {
            for (int i = 1; i < firstLine.length; i++) {
                if (!this.SchoolType[i].equals(firstLine[i].trim())) {
                    return false;
                }
            }
        }
        return true;
    }
}
