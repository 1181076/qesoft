package arqsoft2.GorgeousSandwich.model.user;

import java.util.List;

import javax.persistence.*;
import eapli.framework.representations.dto.DTOable;

public interface AppOrderInterface{

    public ID getId();
    public Day getDay();
    public List<SandwichOrder> getSandwichOrders();

    public void update(AppOrder order);

    public AppOrderDTO toDTO();
}
