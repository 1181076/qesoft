package arqsoft2.GorgeousSandwich.model.sandwich;

import java.util.List;

import javax.persistence.*;

import eapli.framework.representations.dto.DTOable;

public interface SandwichInterface {

    public ID getId();
    public ShortDescription getShortDescription();
    public Description getExtendedDescription();
    public Price getPrice();
    public List<Ingredient> getIngredients();

    public void update(Sandwich sandwich);

    public SandwichDTO toDTO();
}
