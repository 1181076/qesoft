package arqsoft2.GorgeousSandwich.repository.user;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import arqsoft2.GorgeousSandwich.model.user.AppOrder;

public interface AppOrderCustomRepository {

    public AppOrder findOrderById(String Id);
    
    public List<AppOrder> findAllOrders();
    
    public List<AppOrder> getOrdersByInterval(Date startDay, Date endDay);
    
}
