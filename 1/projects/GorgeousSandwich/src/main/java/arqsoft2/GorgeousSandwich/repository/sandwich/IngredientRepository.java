package arqsoft2.GorgeousSandwich.repository.sandwich;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import arqsoft2.GorgeousSandwich.model.sandwich.ID;
import arqsoft2.GorgeousSandwich.model.sandwich.Ingredient;

public interface IngredientRepository extends JpaRepository<Ingredient, ID>, IngredientCustomRepository {

}
