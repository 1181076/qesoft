package arqsoft2.GorgeousSandwich.service.supplier;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import arqsoft2.GorgeousSandwich.model.supplier.ID;
import arqsoft2.GorgeousSandwich.model.supplier.Deal;
import arqsoft2.GorgeousSandwich.model.supplier.DealDTO;
import arqsoft2.GorgeousSandwich.repository.supplier.DealRepository;
import arqsoft2.GorgeousSandwich.repository.supplier.DealRepositoryImpl;

@Component
public class DealService {
    
    @Autowired
    DealRepository dealRepository;

    @Autowired
    DealRepositoryImpl dealRepositoryImpl;

    public DealService(DealRepository dealRepository, DealRepositoryImpl dealRepositoryImpl){
        this.dealRepository = dealRepository;
        this.dealRepositoryImpl = dealRepositoryImpl;
    }

    public List<DealDTO> getAllDeals(@RequestParam(required = false) String title) {
        try {
            List<Deal> deals = dealRepositoryImpl.findAllDeals();
            List<DealDTO> ret = new ArrayList<>();
            deals.forEach(element -> ret.add(element.toDTO()));
            return ret;
        } catch (Exception error) {
            return new ArrayList<>();
        }
    }

    public DealDTO getDealById(@PathVariable("id") String id) {

        // VERSION WITH REPOSITORYIMPL 
        Deal dealData = dealRepositoryImpl.findDealById(id);

        if (dealData!=null) {
            return dealData.toDTO();
        } else {
            return null;
        }

        // Optional<Deal> dealData = dealRepository.findById(new ID(id));

        // if (dealData.isPresent()) {
        //     return dealData.get().toDTO();
        // } else {
        //     return null;
        // }
    }

    public DealDTO createDeal(@RequestBody Deal deal) {
        try {
            Deal _deal = dealRepository.save(deal);
            return _deal.toDTO();
        } catch (Exception error) {
            System.out.println(error);
            return null;
        }
    }

    public DealDTO updateDeal(@PathVariable("id") ID id, @RequestBody Deal deal) {
        Optional<Deal> dealData = dealRepository.findById(id);

        if (dealData.isPresent()) {
            Deal _deal = dealData.get();
            _deal.update(deal);
            return dealRepository.save(_deal).toDTO();
        } else {
            return null;
        }
    }

    public HttpStatus deleteDeal(@PathVariable("id") ID id) {
        try{
            dealRepository.deleteById(id);
            return HttpStatus.NO_CONTENT;
        } catch ( Exception error){
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

    public HttpStatus deleteAllDeals() {
        try {
            dealRepository.deleteAll();
            return HttpStatus.NO_CONTENT;
        } catch (Exception error){
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }
}
