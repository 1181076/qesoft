package arqsoft2.GorgeousSandwich.repository.supplier;

import org.springframework.data.jpa.repository.JpaRepository;

import arqsoft2.GorgeousSandwich.model.supplier.ID;
import arqsoft2.GorgeousSandwich.model.supplier.Deal;

public interface DealRepository extends JpaRepository<Deal, ID>, DealCustomRepository {

}
