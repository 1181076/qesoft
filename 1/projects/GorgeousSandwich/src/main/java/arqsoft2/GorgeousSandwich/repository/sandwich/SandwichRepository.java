package arqsoft2.GorgeousSandwich.repository.sandwich;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import arqsoft2.GorgeousSandwich.model.sandwich.ID;
import arqsoft2.GorgeousSandwich.model.sandwich.Sandwich;

public interface SandwichRepository extends JpaRepository<Sandwich, ID>, SandwichCustomRepository {

}
