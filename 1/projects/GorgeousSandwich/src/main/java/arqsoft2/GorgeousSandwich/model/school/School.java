package arqsoft2.GorgeousSandwich.model.school;

import javax.persistence.*;

import arqsoft2.GorgeousSandwich.model.sandwich.Sandwich;
import eapli.framework.representations.dto.DTOable;

import java.util.ArrayList;
import java.util.List;

@Entity
public class School implements SchoolInterface, DTOable<SchoolDTO> {

    @EmbeddedId
    private ExternalID id;

    private InternalID internalID;
    private Name name;
    private Address address;
    
    @ManyToMany
    private List<Sandwich> sandwiches;

    public School() {

    }

    public School(ExternalID id, InternalID internalID, Name name, Address address, List<Sandwich> sandwiches ) {
        this.id = id;
        this.internalID = internalID;
        this.name = name;
        this.address = address;
        this.sandwiches = sandwiches;
    }

    public ExternalID getId() {
        return this.id;
    }

    public InternalID getInternalID() {
        return this.internalID;
    }

    public Name getName() {
        return this.name;
    }

    public Address getAddress() {
        return this.address;
    }

    public List<Sandwich> getSandwiches() {
        return this.sandwiches;
    }

    public void update(School school) {
        this.internalID = school.internalID;
        this.name = school.name;
        this.address = school.address;
        this.sandwiches = school.sandwiches;
    }

    public SchoolDTO toDTO() {
        return new SchoolDTO(id, internalID, name, address, sandwiches);
    }

    public void registerSandwiches(List<Sandwich> sandwiches){
        this.sandwiches = sandwiches; 
    }
}