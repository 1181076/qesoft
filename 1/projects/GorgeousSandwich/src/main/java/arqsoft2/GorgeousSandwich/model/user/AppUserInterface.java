package arqsoft2.GorgeousSandwich.model.user;

import java.util.List;

import javax.persistence.*;

import arqsoft2.GorgeousSandwich.model.school.School;
import eapli.framework.representations.dto.DTOable;

public interface AppUserInterface{
	
	public Email getEmail();
	public Name getName();
	public Password getPassword();
	public Role getRole();
	public List<AppOrder> getOrders();
	public School getSchool();

    public AppUserDTO toDTO();

	public void update(AppUser ing);


}
