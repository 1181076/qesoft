package arqsoft2.GorgeousSandwich.repository.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import arqsoft2.GorgeousSandwich.model.user.ID;
import arqsoft2.GorgeousSandwich.model.user.AppOrder;
import arqsoft2.GorgeousSandwich.model.user.Day;

public interface AppOrderRepository extends JpaRepository<AppOrder, ID>, AppOrderCustomRepository {

    List<AppOrder> findByDayBetween(Day startDay, Day endDay);

}
