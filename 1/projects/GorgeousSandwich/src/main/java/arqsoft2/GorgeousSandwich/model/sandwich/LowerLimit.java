package arqsoft2.GorgeousSandwich.model.sandwich;

import eapli.framework.domain.model.ValueObject;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public final class LowerLimit implements ValueObject {

    private final int value;

    @JsonCreator
    public LowerLimit(@JsonProperty("value")int value){
        if(value < 0){
            throw new IllegalArgumentException("The lower limit quantity cannot be negative!");
        }
        this.value= value;
    }
    
    public int value(){
        return value;
    }

    public final int getValue() {
		return this.value;
	}

}
