package arqsoft2.GorgeousSandwich.model.sandwich;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;

public final class ID implements ValueObject{

    private final String value;

    public final String getValue() {
		return this.value;
	}

    public ID(){
        value = "default";
    }

    @JsonCreator
    public ID(@JsonProperty("value") String value){
        if(value.isEmpty()){
            throw new IllegalArgumentException("The ID cannot be empty!");
        }
        this.value= value;
    }
    
    public String value(){
        return value;
    }

    @Override
    public boolean equals(Object o){

        ID id = (ID) o;

        return id.value().equals(this.value);

    }

}
