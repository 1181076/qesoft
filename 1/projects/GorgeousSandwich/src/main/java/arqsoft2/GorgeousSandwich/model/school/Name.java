package arqsoft2.GorgeousSandwich.model.school;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;

public class Name implements ValueObject {
    private final String value;

    @JsonCreator
    public Name(@JsonProperty("value") String value) {
        if(value.isEmpty()){
            throw new IllegalArgumentException("The name cannot be empty!");
        }
        this.value = value;
    }

    public String value() {
        return value;
    }

    public final String getValue() {
        return this.value;
    }

}
