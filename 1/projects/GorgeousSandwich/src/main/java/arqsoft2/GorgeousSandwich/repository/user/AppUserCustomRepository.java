package arqsoft2.GorgeousSandwich.repository.user;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import arqsoft2.GorgeousSandwich.model.user.AppUser;

public interface AppUserCustomRepository {

    public AppUser findUserById(String Id);
    
    public AppUser findUserByName(String name);
    
    public List<AppUser> findAllUsers();
    
}
