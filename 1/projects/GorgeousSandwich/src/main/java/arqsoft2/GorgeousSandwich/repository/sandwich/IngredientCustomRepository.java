package arqsoft2.GorgeousSandwich.repository.sandwich;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import arqsoft2.GorgeousSandwich.model.sandwich.*;

public interface IngredientCustomRepository {

    public Ingredient findIngredientById(String Id);
    
    public List<Ingredient> findAllIngredients();
    
}
