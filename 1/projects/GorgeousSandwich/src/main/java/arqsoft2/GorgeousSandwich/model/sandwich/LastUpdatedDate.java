package arqsoft2.GorgeousSandwich.model.sandwich;


import eapli.framework.domain.model.ValueObject;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Date;
import java.util.Calendar;

public final class LastUpdatedDate implements ValueObject {

    private final int amount;
    
    private final Date date;

    @JsonCreator
    public LastUpdatedDate(@JsonProperty("date")Date date, @JsonProperty("amount") int amount){
        Date currentDate = new Date(Calendar.getInstance().getTime().getTime());
        if(date.after(currentDate)){
            throw new IllegalArgumentException("The last updated date cannot be above the current date!");
        }
        this.date= date;
        this.amount= amount;
    }
    
    public Date date(){
        return date;
    }

    public final Date getDate() {
		return this.date;
	}

    public int amount(){
        return amount;
    }

    public final int getAmount() {
		return this.amount;
	}

}
