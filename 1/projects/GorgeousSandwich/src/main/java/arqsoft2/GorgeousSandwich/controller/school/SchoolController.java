package arqsoft2.GorgeousSandwich.controller.school;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import arqsoft2.GorgeousSandwich.model.sandwich.ID;
import arqsoft2.GorgeousSandwich.model.sandwich.Sandwich;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import arqsoft2.GorgeousSandwich.model.school.InternalID;
import arqsoft2.GorgeousSandwich.model.school.ExternalID;
import arqsoft2.GorgeousSandwich.model.school.Name;
import arqsoft2.GorgeousSandwich.model.school.Address;
import arqsoft2.GorgeousSandwich.model.school.School;
import arqsoft2.GorgeousSandwich.model.school.SchoolDTO;
import arqsoft2.GorgeousSandwich.service.school.SchoolService;

@CrossOrigin(origins = "http://localhost:8082")
@RestController
@RequestMapping("/api")
public class SchoolController {

    @Autowired
    SchoolService schoolService;

    public SchoolController(SchoolService schoolService) {
        this.schoolService = schoolService;
    }

    @GetMapping("/schools")
    public ResponseEntity<List<SchoolDTO>> getAllSchools(@RequestParam(required = false) String title) {
        try {
            List<SchoolDTO> schools = new ArrayList<SchoolDTO>();

            schools = schoolService.getAllSchools(title);

            if (schools.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(schools, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/schools/{id}")
    public ResponseEntity<SchoolDTO> getSchoolByExternalId(@PathVariable("id") String id) {
        SchoolDTO schoolData = schoolService.getSchoolById(id);

        if (schoolData != null) {
            return new ResponseEntity<>(schoolData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/schools")
    public ResponseEntity<SchoolDTO> createSchool(@RequestBody School school) {

        try {
            SchoolDTO _school = schoolService.createSchool(school);
            return new ResponseEntity<>(_school, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/schools/{id}")
    public ResponseEntity<SchoolDTO> updateSchool(@PathVariable("id") String id, @RequestBody School school)
            throws ParseException {
        SchoolDTO schoolData = schoolService.updateSchool(new ExternalID(id), school);

        if (schoolData != null) {
            return new ResponseEntity<>(schoolData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/schools/{id}")
    public ResponseEntity<HttpStatus> deleteSchool(@PathVariable("id") String id) {
        HttpStatus st = schoolService.deleteSchool(new ExternalID(id));
        return new ResponseEntity<>(st);
    }

    @DeleteMapping("/schools")
    public ResponseEntity<HttpStatus> deleteAllSchools() {
        HttpStatus st = schoolService.deleteAllSchools();
        return new ResponseEntity<>(st);

    }

    @PostMapping("/schools/{schoolID}/registerSandwiches")
    public ResponseEntity<SchoolDTO> registerSandwiches(@PathVariable("schoolID") String schoolID, @RequestBody List<Sandwich> sandwiches) throws ParseException {
        try {
            SchoolDTO _school = schoolService.registerSandwiches(new ExternalID(schoolID), sandwiches);
            return new ResponseEntity<>(_school, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
