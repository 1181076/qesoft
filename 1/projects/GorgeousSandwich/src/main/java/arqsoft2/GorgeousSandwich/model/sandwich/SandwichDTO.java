package arqsoft2.GorgeousSandwich.model.sandwich;

import java.util.List;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class SandwichDTO {

    private ID id;
    private ShortDescription shortDescription;
    private Description extendedDescription;
    private Price price;
    private List<Ingredient> ingredients;

    public SandwichDTO(){

    }

    public SandwichDTO(ID id, ShortDescription shortDescription, Description extendedDescription, Price price, List<Ingredient> ingredients){
        this.id = id;
        this.shortDescription = shortDescription;
        this.extendedDescription = extendedDescription;
        this.price = price;
        this.ingredients = ingredients;
    }

    public ID getId() {
        return this.id;
    }

    public ShortDescription getShortDescription() {
        return this.shortDescription;
    }

    public void setShortDescription(ShortDescription shortDescription) {
        this.shortDescription = shortDescription;
    }

    public Description getExtendedDescription() {
        return this.extendedDescription;
    }

    public void setExtendedDescription(Description extendedDescription) {
        this.extendedDescription = extendedDescription;
    }

    public Price getPrice() {
        return this.price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public List<Ingredient> getIngredients() {
        return this.ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }


    @Override
    public boolean equals(Object o) {

        return true;

    }
}
