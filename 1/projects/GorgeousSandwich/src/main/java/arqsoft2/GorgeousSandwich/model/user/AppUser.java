package arqsoft2.GorgeousSandwich.model.user;

import java.util.List;

import javax.persistence.*;

import arqsoft2.GorgeousSandwich.model.school.School;
import eapli.framework.representations.dto.DTOable;

@Entity
public class AppUser implements DTOable<AppUserDTO>{
	
    @EmbeddedId
    private Email email;

    private Name name;
    private Password password;
    private Role role;
   
    @OneToMany  
    private List<AppOrder> orders;

	@ManyToOne
	private School school;

    public AppUser(){

    }

    public AppUser(Email email, Name name, Password password, Role role, List<AppOrder> orderIds, School schoolID){
        this.email = email;
        this.name = name;
        this.password = password;
        this.role = role;
        this.orders = orderIds;
		this.school = schoolID;
    }

	public Email getEmail() {
		return this.email;
	}

	public Name getName() {
		return this.name;
	}

	public Password getPassword() {
		return this.password;
	}

	public Role getRole() {
		return this.role;
	}

	public List<AppOrder> getOrders() {
		return this.orders;
	}

	public School getSchool() {
		return this.school;
	}

    @Override
    public AppUserDTO toDTO() {
        return new AppUserDTO(email, name, password, role, orders, school);
	}

	public void update(AppUser ing){
		this.name = ing.name;
		this.password = ing.password;
		this.role = ing.role;
		this.orders = ing.orders;
		this.school = ing.school;
	} 


}
