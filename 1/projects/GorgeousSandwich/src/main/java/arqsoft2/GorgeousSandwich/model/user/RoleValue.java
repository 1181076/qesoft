package arqsoft2.GorgeousSandwich.model.user;

public enum RoleValue {
    SCHOOLREPRESENTATIVE, SUPPLIERMANAGER, STOCKMANAGER, SCHOOLMANAGER
}
