package arqsoft2.GorgeousSandwich.controller.sandwich;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import arqsoft2.GorgeousSandwich.model.sandwich.ID;
import arqsoft2.GorgeousSandwich.model.sandwich.Ingredient;
import arqsoft2.GorgeousSandwich.model.sandwich.IngredientDTO;
import arqsoft2.GorgeousSandwich.service.sandwich.IngredientService;


@CrossOrigin(origins = "http://localhost:8082")
@RestController
@RequestMapping("/api")
public class IngredientController {

	@Autowired
	IngredientService ingredientService;


	public IngredientController(IngredientService ingredientService){
		this.ingredientService = ingredientService;
	}

	@GetMapping("/ingredients")	
	public ResponseEntity<List<IngredientDTO>> getAllIngredients(@RequestParam(required = false) String title) {
		try {
			List<IngredientDTO> ingredients = new ArrayList<IngredientDTO>();

			ingredients = ingredientService.getAllIngredients(title);

			if (ingredients.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(ingredients, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/ingredients/{id}")
	public ResponseEntity<IngredientDTO> getIngredientById(@PathVariable("id") String id) {
		IngredientDTO ingredientData = ingredientService.getIngredientById(id);

		if (ingredientData != null) {
			return new ResponseEntity<>(ingredientData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/ingredients")
	public ResponseEntity<IngredientDTO> createIngredient(@RequestBody Ingredient ingredient) {

		try {
			IngredientDTO _ingredient = ingredientService
						.createIngredient(ingredient);
			return new ResponseEntity<>(_ingredient, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/ingredients/{id}")
	public ResponseEntity<IngredientDTO> updateIngredient(@PathVariable("id") String id, @RequestBody Ingredient ingredient) throws ParseException {
		IngredientDTO ingredientData = ingredientService.updateIngredient(new ID(id), ingredient);

		if (ingredientData != null) {
			return new ResponseEntity<>(ingredientData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/ingredients/{id}")
	public ResponseEntity<HttpStatus> deleteIngredient(@PathVariable("id") String id) {
			HttpStatus st = ingredientService.deleteIngredient(new ID(id));
			return new ResponseEntity<>(st);
	}

	@DeleteMapping("/ingredients")
	public ResponseEntity<HttpStatus> deleteAllIngredients() {
			HttpStatus st = ingredientService.deleteAllIngredients();
			return new ResponseEntity<>(st);
		
	}
    
}
