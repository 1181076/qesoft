package arqsoft2.GorgeousSandwich.repository.supplier;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import arqsoft2.GorgeousSandwich.model.supplier.*;

public interface DealCustomRepository {

    public Deal findDealById(String Id);
    
    public List<Deal> findAllDeals();
    
}
