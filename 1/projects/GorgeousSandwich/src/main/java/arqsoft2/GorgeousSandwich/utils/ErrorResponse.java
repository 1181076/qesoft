package arqsoft2.GorgeousSandwich.utils;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ErrorResponse {
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private String message;

    public ErrorResponse(String message) {
        this.message = message;
    }
}
