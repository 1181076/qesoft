package arqsoft2.GorgeousSandwich.service.user;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import arqsoft2.GorgeousSandwich.model.user.AppUser;
import arqsoft2.GorgeousSandwich.repository.user.AppUserRepositoryImpl;

@Service
public class AppUserDetailsService implements UserDetailsService {

	@Autowired
	private AppUserRepositoryImpl userRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		AppUser user = userRepository.findUserById(email);
		
		if (user == null) {
			throw new UsernameNotFoundException("Email " + email + " not found");
		}
		return new org.springframework.security.core.userdetails.User(user.getEmail().getValue(), user.getPassword().getValue(),
				getGrantedAuthority(user));
	}

	private Collection<GrantedAuthority> getGrantedAuthority(AppUser user) {
		Collection<GrantedAuthority> authorities = new ArrayList<>();        
		authorities.add(new SimpleGrantedAuthority(user.getRole().getValue().name()));
		return authorities;
	}
}