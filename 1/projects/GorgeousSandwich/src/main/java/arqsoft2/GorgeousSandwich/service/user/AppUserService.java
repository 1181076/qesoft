package arqsoft2.GorgeousSandwich.service.user;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.http.HttpClient;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import arqsoft2.GorgeousSandwich.model.user.Email;
import arqsoft2.GorgeousSandwich.model.school.School;
import arqsoft2.GorgeousSandwich.model.user.AppUser;
import arqsoft2.GorgeousSandwich.model.user.AppUserDTO;
import arqsoft2.GorgeousSandwich.repository.user.AppUserRepository;
import arqsoft2.GorgeousSandwich.repository.user.AppUserRepositoryImpl;



@Component
public class AppUserService {

	@Autowired
	AppUserRepository appUserRepository;
	
	@Autowired
	AppUserRepositoryImpl appUserRepositoryImpl;
	

	public AppUserService(AppUserRepository appUserRepository, AppUserRepositoryImpl appUserRepositoryImpl){
		this.appUserRepository = appUserRepository;
		this.appUserRepositoryImpl = appUserRepositoryImpl;
	}

	public List<AppUserDTO> getAllAppUsers(@RequestParam(required = false) String title) {
		try {
			List<AppUser> appUsers = appUserRepositoryImpl.findAllUsers();
			
			List<AppUserDTO> ret = new ArrayList<>();

			
			appUsers.forEach(e -> ret.add(e.toDTO()));

			return ret;
		} catch (Exception e) {
			return new ArrayList<>();
		}
	}

	public AppUserDTO getAppUserById(@PathVariable("id") String email) {

		// VERSION WITH REPOSITORYIMPL 
        AppUser appUserData = appUserRepositoryImpl.findUserById(email);

        if (appUserData!=null) {
            return appUserData.toDTO();
        } else {
            return null;
        }

		// Optional<AppUser> appUserData = appUserRepository.findById(new Email(email));

		// if (appUserData.isPresent()) {
		// 	return appUserData.get().toDTO();
		// } else {
		// 	return null;
		// }
	}


	public AppUserDTO createAppUser(@RequestBody AppUser appUser) {
		
		try {
			AppUser _appUser = appUserRepository
					.save(appUser);
						
			return _appUser.toDTO();
		} catch (Exception e) {
			
		System.out.println("e11111 -> " + e);
			return null;
		}
	}

	public AppUserDTO updateAppUser(@PathVariable("id") Email email, @RequestBody AppUser appUser) {
		Optional<AppUser> appUserData = appUserRepository.findById(email);
		
		if (appUserData.isPresent()) {
			AppUser _appUser = appUserData.get();
			_appUser.update(appUser);
			
			return appUserRepository.save(_appUser).toDTO();
		} else {
			return null;
		}
	}

	public HttpStatus deleteAppUser(@PathVariable("id") Email email) {
		try {
			appUserRepository.deleteById(email);
			return HttpStatus.NO_CONTENT;
		} catch (Exception e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
	}

	public HttpStatus deleteAllAppUsers() {
		try {
			appUserRepository.deleteAll();
			return HttpStatus.NO_CONTENT;
		} catch (Exception e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}

	}

	public AppUserDTO assignSchool(@PathVariable("id") String email, @PathVariable("school") School school) {
		Optional<AppUser> appUserData = appUserRepository.findById(new Email(email));
		
		if (appUserData.isPresent()) {
			AppUser _appUser = appUserData.get();
			AppUser temp = new AppUser(_appUser.getEmail(), _appUser.getName(), _appUser.getPassword(), _appUser.getRole(), _appUser.getOrders(), school);
			_appUser.update(temp);
			
			return appUserRepository.save(_appUser).toDTO();
		} else {
			return null;
		}
	}
}
