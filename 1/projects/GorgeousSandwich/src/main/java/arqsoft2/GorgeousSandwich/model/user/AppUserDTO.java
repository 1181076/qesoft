package arqsoft2.GorgeousSandwich.model.user;


import java.util.List;

import arqsoft2.GorgeousSandwich.model.school.School;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class AppUserDTO {

    
    private Email email;
    private Name name;
    private Password password;
    private Role role;
    private List<AppOrder> orders;
	private School school;
    
    public AppUserDTO(){

    }

    public AppUserDTO(Email email, Name name, Password password, Role role, List<AppOrder> orders, School school){
        this.email = email;
        this.name = name;
        this.password = password;
        this.role = role;
        this.orders = orders;
        this.school = school;
    }

    public Email getEmail() {
		return this.email;
	}

	public Name getName() {
		return this.name;
	}

	public Password getPassword() {
		return this.password;
	}

	public Role getRole() {
		return this.role;
	}

	public List<AppOrder> getOrders() {
		return this.orders;
	}

    public School getSchool() {
		return this.school;
	}


    @Override
    public boolean equals(Object o) {
  
		AppUser u = (AppUser) o;

        return u.getEmail().equals(this.email); 
          
    }
}
