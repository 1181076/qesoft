package arqsoft2.GorgeousSandwich.model.school;

import javax.persistence.*;

import arqsoft2.GorgeousSandwich.model.sandwich.Sandwich;
import eapli.framework.representations.dto.DTOable;

import java.util.ArrayList;
import java.util.List;

public interface SchoolInterface {

    public ExternalID getId();
    public InternalID getInternalID();
    public Name getName();
    public Address getAddress();    
    public List<Sandwich> getSandwiches();

    public void update(School school);

    public SchoolDTO toDTO();

    public void registerSandwiches(List<Sandwich> sandwiches);
}