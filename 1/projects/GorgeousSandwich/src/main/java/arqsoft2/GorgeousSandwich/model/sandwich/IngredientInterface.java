package arqsoft2.GorgeousSandwich.model.sandwich;

import java.util.List;

import javax.persistence.*;

import arqsoft2.GorgeousSandwich.model.supplier.Supplier;
import eapli.framework.representations.dto.DTOable;

public interface IngredientInterface{
	
	public ID getId();
	public Description getDescription();
	public Quantity getQuantity();
	public List<LastUpdatedDate> getlastUpdatedDate();
	public UpperLimit getUpperLimit();
	public LowerLimit getLowerLimit();
	public Units getUnits();

    public IngredientDTO toDTO();

	public void update(Ingredient ing);
}
