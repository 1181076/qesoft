package arqsoft2.GorgeousSandwich.service.sandwich;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.NamedNativeQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import arqsoft2.GorgeousSandwich.model.sandwich.ID;
import arqsoft2.GorgeousSandwich.model.sandwich.Sandwich;
import arqsoft2.GorgeousSandwich.model.sandwich.SandwichDTO;
import arqsoft2.GorgeousSandwich.repository.sandwich.SandwichRepository;
import arqsoft2.GorgeousSandwich.repository.sandwich.SandwichRepositoryImpl;

@Component
public class SandwichService {

    @Autowired
    SandwichRepository sandwichRepository;

    
    @Autowired
    SandwichRepositoryImpl sandwichRepositoryImpl;

    public SandwichService(SandwichRepository sandwichRepository, SandwichRepositoryImpl sandwichRepositoryImpl){
        this.sandwichRepository = sandwichRepository;
        this.sandwichRepositoryImpl = sandwichRepositoryImpl;
    }

    public List<SandwichDTO> getAllSandwiches(@RequestParam(required = false) String title) {
        try {
            List<Sandwich> sandwiches = sandwichRepositoryImpl.findAllSandwiches();

            List<SandwichDTO> ret = new ArrayList<>();


            sandwiches.forEach(e -> ret.add(e.toDTO()));

            return ret;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public SandwichDTO getSandwichById(@PathVariable("id") String id) {

        // VERSION WITH REPOSITORYIMPL 
        Sandwich sandwichData = sandwichRepositoryImpl.findSandwichById(id);

        if (sandwichData!=null) {
            return sandwichData.toDTO();
        } else {
            return null;
        }

        // Optional<Sandwich> sandwichData = sandwichRepository.findById(new ID(id));

        // if (sandwichData.isPresent()) {
        //     return sandwichData.get().toDTO();
        // } else {
        //     return null;
        // }
    }

    public List<SandwichDTO> getSandwichesWithPriceFilter(@PathVariable("currency") String currency, @PathVariable("minimumPrice") double minimumPrice, @PathVariable("maximumPrice") double maximumPrice) {
        try {

            List<Sandwich> sandwiches = sandwichRepositoryImpl.getSandwichesByPriceInterval(currency, minimumPrice, maximumPrice);

            List<SandwichDTO> ret = new ArrayList<>();


            sandwiches.forEach(e -> ret.add(e.toDTO()));

            return ret;
        } catch (Exception e) {
            System.out.println(e);
            return new ArrayList<>();
        }
    }


    public List<SandwichDTO> getSandwichesWithIngredientFilter(@PathVariable("list") String[] list) {
        try {

            // VERSION WITH REPOSITORYIMPL 
            List<Sandwich> sandwiches = sandwichRepositoryImpl.findSandwichesWithIngredients(list);

            List<SandwichDTO> ret = new ArrayList<>();

            for(Sandwich s: sandwiches){
                ret.add(s.toDTO());
            }

            return ret;

            // List<Sandwich> sandwiches = sandwichRepositoryImpl.findAllSandwiches();

            // List<SandwichDTO> ret = new ArrayList<>();

            // for(Sandwich s: sandwiches){
                    
            //     if(Arrays.equals(s.getIngredients().getList(), list)){
            //         ret.add(s.toDTO());
            //     }
            // }

            // return ret;
        } catch (Exception e) {
            System.out.println(e);
            return new ArrayList<>();
        }
    }

    public SandwichDTO createSandwich(@RequestBody Sandwich sandwich) {

        try {
            Sandwich _sandwich = sandwichRepository
                    .save(sandwich);

            return _sandwich.toDTO();
        } catch (Exception e) {
            return null;
        }
    }

    public SandwichDTO updateSandwich(@PathVariable("id") ID id, @RequestBody Sandwich sandwich) {
        Optional<Sandwich> sandwichData = sandwichRepository.findById(id);

        if (sandwichData.isPresent()) {
            Sandwich _sandwich = sandwichData.get();
            _sandwich.update(sandwich);

            return sandwichRepository.save(_sandwich).toDTO();
        } else {
            return null;
        }
    }

    public HttpStatus deleteSandwich(@PathVariable("id") ID id) {
        try {
            sandwichRepository.deleteById(id);
            return HttpStatus.NO_CONTENT;
        } catch (Exception e) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

    public HttpStatus deleteAllSandwiches() {
        try {
            sandwichRepository.deleteAll();
            return HttpStatus.NO_CONTENT;
        } catch (Exception e) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

    }

}