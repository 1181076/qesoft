package arqsoft2.GorgeousSandwich.model.sandwich;

import java.util.List;

import javax.persistence.*;

import eapli.framework.representations.dto.DTOable;

@Entity
public class Sandwich implements SandwichInterface, DTOable<SandwichDTO>{

    @EmbeddedId
    private ID id;

    private ShortDescription shortDescription;
    
    private Description extendedDescription;

    @Embedded
    private Price price;
    
    @ManyToMany
    private List<Ingredient> ingredients;

    public Sandwich(){

    }

    public Sandwich(ID id, ShortDescription shortDescription, Description extendedDescription, Price price, List<Ingredient> ingredients){
        this.id = id;
        this.shortDescription = shortDescription;
        this.extendedDescription = extendedDescription;
        this.price = price;
        this.ingredients = ingredients;
    }

    public ID getId() {
        return this.id;
    }

    public ShortDescription getShortDescription() {
        return this.shortDescription;
    }

    public Description getExtendedDescription() {
        return this.extendedDescription;
    }

    public Price getPrice() {
        return this.price;
    }

    public List<Ingredient> getIngredients() {
        return this.ingredients;
    }

    public void update(Sandwich sandwich){
        this.shortDescription = sandwich.shortDescription;
        this.extendedDescription = sandwich.extendedDescription;
        this.price = sandwich.price;
        this.ingredients = sandwich.ingredients;
    }

    public SandwichDTO toDTO() {
        return new SandwichDTO(id, shortDescription, extendedDescription, price, ingredients);
    }
}
