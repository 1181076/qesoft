package arqsoft2.GorgeousSandwich.model.supplier;

import javax.persistence.*;

import arqsoft2.GorgeousSandwich.model.sandwich.Ingredient;
import eapli.framework.representations.dto.DTOable;

@Entity
public class Deal implements DealInterface, DTOable<DealDTO> {

    @EmbeddedId
    private ID id;
    private UnitPrice unitPrice;
    private ExpirationPriceDate expirationPriceDate;
    
	@ManyToOne(cascade = {CascadeType.ALL})
    private Ingredient ingredient;

    public Deal(){
    }

    public Deal(ID id, UnitPrice unitPrice, ExpirationPriceDate expirationPriceDate, Ingredient ingredient){
        this.id = id;
        this.unitPrice = unitPrice;
        this.expirationPriceDate = expirationPriceDate;
        this.ingredient = ingredient;
    }

    public ID getId() {
        return this.id;
    }

    public UnitPrice getUnitPrice() {
        return this.unitPrice;
    }

    public ExpirationPriceDate getExpirationPriceDate() {
        return this.expirationPriceDate;
    }
    
    public Ingredient getIngredient() {
        return this.ingredient;
    }


    public void update(Deal deal){
        this.unitPrice = deal.unitPrice;
        this.expirationPriceDate = deal.expirationPriceDate;
        this.ingredient = deal.ingredient;
    }

    @Override
    public DealDTO toDTO() {
        return new DealDTO(id, unitPrice, expirationPriceDate,ingredient);
    }
}
