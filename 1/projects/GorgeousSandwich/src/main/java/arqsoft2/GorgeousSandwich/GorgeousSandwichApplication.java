package arqsoft2.GorgeousSandwich;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import arqsoft2.GorgeousSandwich.controller.user.AppUserController;
import arqsoft2.GorgeousSandwich.model.user.Email;
import arqsoft2.GorgeousSandwich.model.user.AppOrder;
import arqsoft2.GorgeousSandwich.model.user.AppUser;
import arqsoft2.GorgeousSandwich.model.user.Name;
import arqsoft2.GorgeousSandwich.model.user.Password;
import arqsoft2.GorgeousSandwich.model.user.Role;
import arqsoft2.GorgeousSandwich.model.user.RoleValue;
import arqsoft2.GorgeousSandwich.service.user.AppUserService;

@SpringBootApplication
public class GorgeousSandwichApplication implements CommandLineRunner{
	
	@Autowired
	private AppUserService userService;
	
	@Autowired
	private AppUserController userController;

	public static void main(String[] args) {
		SpringApplication.run(GorgeousSandwichApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		if (userService.getAllAppUsers("").isEmpty()) {

			AppUser stockManager = new AppUser(
				new Email("stock@arqsoft.com"), 
				new Name("DefaultStockManager"), 
				new Password("Arqsoft1!"), 
				new Role( RoleValue.STOCKMANAGER), 
				new ArrayList<AppOrder>(),
				null);

			AppUser schoolManager = new AppUser(
				new Email("school@arqsoft.com"), 
				new Name("DefaultSchoolManager"), 
				new Password("Arqsoft1!"), 
				new Role( RoleValue.SCHOOLMANAGER), 
				new ArrayList<AppOrder>(),
				null);

			AppUser supplierManager = new AppUser(
				new Email("supplier@arqsoft.com"), 
				new Name("DefaultSupplierManager"), 
				new Password("Arqsoft1!"), 
				new Role( RoleValue.SUPPLIERMANAGER), 
				new ArrayList<AppOrder>(),
				null);


			userController.createAppUser(stockManager);
			userController.createAppUser(schoolManager);
			userController.createAppUser(supplierManager);
		}
	}
}
