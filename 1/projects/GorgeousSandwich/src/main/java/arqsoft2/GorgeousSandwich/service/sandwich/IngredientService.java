package arqsoft2.GorgeousSandwich.service.sandwich;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.http.HttpClient;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import arqsoft2.GorgeousSandwich.model.sandwich.ID;
import arqsoft2.GorgeousSandwich.model.sandwich.Ingredient;
import arqsoft2.GorgeousSandwich.model.sandwich.IngredientDTO;
import arqsoft2.GorgeousSandwich.repository.sandwich.IngredientRepository;
import arqsoft2.GorgeousSandwich.repository.sandwich.IngredientRepositoryImpl;



@Component
public class IngredientService {

	@Autowired
	IngredientRepository ingredientRepository;
	
	@Autowired
	IngredientRepositoryImpl ingredientRepositoryImpl;
	

	public IngredientService(IngredientRepository ingredientRepository, IngredientRepositoryImpl ingredientRepositoryImpl){
		this.ingredientRepository = ingredientRepository;
		this.ingredientRepositoryImpl = ingredientRepositoryImpl;
	}

	public List<IngredientDTO> getAllIngredients(@RequestParam(required = false) String title) {
		try {
			List<Ingredient> ingredients = ingredientRepositoryImpl.findAllIngredients();
			
			List<IngredientDTO> ret = new ArrayList<>();

			
			ingredients.forEach(e -> ret.add(e.toDTO()));

			return ret;
		} catch (Exception e) {
			return new ArrayList<>();
		}
	}

	public IngredientDTO getIngredientById(@PathVariable("id") String id) {

		// VERSION WITH REPOSITORYIMPL 
        Ingredient ingredientData = ingredientRepositoryImpl.findIngredientById(id);

        if (ingredientData!=null) {
            return ingredientData.toDTO();
        } else {
            return null;
        }

		// Optional<Ingredient> ingredientData = ingredientRepository.findById(new ID(id));

		// if (ingredientData.isPresent()) {
		// 	return ingredientData.get().toDTO();
		// } else {
		// 	return null;
		// }
	}

	public IngredientDTO createIngredient(@RequestBody Ingredient ingredient) {
		
		try {
			Ingredient _ingredient = ingredientRepository
					.save(ingredient);
						
			return _ingredient.toDTO();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}

	public IngredientDTO updateIngredient(@PathVariable("id") ID id, @RequestBody Ingredient ingredient) {
		Optional<Ingredient> ingredientData = ingredientRepository.findById(id);
		
		if (ingredientData.isPresent()) {
			Ingredient _ingredient = ingredientData.get();
			_ingredient.update(ingredient);
			
			return ingredientRepository.save(_ingredient).toDTO();
		} else {
			return null;
		}
	}

	public HttpStatus deleteIngredient(@PathVariable("id") ID id) {
		try {
			ingredientRepository.deleteById(id);
			return HttpStatus.NO_CONTENT;
		} catch (Exception e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
	}

	public HttpStatus deleteAllIngredients() {
		try {
			ingredientRepository.deleteAll();
			return HttpStatus.NO_CONTENT;
		} catch (Exception e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}

	}

}