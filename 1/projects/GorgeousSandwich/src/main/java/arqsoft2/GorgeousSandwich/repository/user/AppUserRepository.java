package arqsoft2.GorgeousSandwich.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;

import arqsoft2.GorgeousSandwich.model.user.Email;
import arqsoft2.GorgeousSandwich.model.user.AppUser;


public interface AppUserRepository extends JpaRepository<AppUser, Email>, AppUserCustomRepository {

}
