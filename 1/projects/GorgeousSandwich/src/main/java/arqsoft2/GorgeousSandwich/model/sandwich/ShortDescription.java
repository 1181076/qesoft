package arqsoft2.GorgeousSandwich.model.sandwich;

import javax.persistence.Embeddable;
import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;



@Embeddable
public final class ShortDescription{

    @Column(name = "shortDescription_value")
    private final String value;

    public ShortDescription(){
        value = "";
    }

    @JsonCreator
    public ShortDescription(@JsonProperty("value") String value){
        if(value.isEmpty()){
            throw new IllegalArgumentException("Short Description cannot be empty!");
        }

        if(value.trim().length() > 50){
            throw new IllegalArgumentException("Short Description cannot be over 50 characters!");
        }

        this.value= value;
    }
    
    public String value(){
        return value;
    }

    public final String getValue() {
		return this.value;
	}

}
