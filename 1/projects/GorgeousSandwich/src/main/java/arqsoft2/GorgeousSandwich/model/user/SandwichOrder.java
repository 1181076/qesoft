package arqsoft2.GorgeousSandwich.model.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;

public class SandwichOrder implements ValueObject {

    private final String sandwich;
    private final int amount;

    public final int getAmount() {
        return this.amount;
    }

    public final String getSandwich() {
        return this.sandwich;
    }

    public SandwichOrder(){
        amount = 0;
        sandwich = "default";
    }

    @JsonCreator
    public SandwichOrder(@JsonProperty("sandwich") String sandwich, @JsonProperty("amount") int amount) {
        if(sandwich.isEmpty()){
            throw new IllegalArgumentException("The sandwich order must contain a sandwich id!");
        }

        if (amount < 0) {
            throw new IllegalArgumentException("The amount of the order cannot be negative!");
        }

        this.amount = amount;
        this.sandwich = sandwich;
    }

    public int amount() {
        return amount;
    }

    public String sandwich() {
        return sandwich;
    }

}
