package arqsoft2.GorgeousSandwich.repository.school;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import arqsoft2.GorgeousSandwich.model.school.*;

public interface SchoolCustomRepository {

    public School findSchoolById(String Id);
    
    public List<School> findAllSchools();
    
}
