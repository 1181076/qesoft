package arqsoft2.GorgeousSandwich.repository.sandwich;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import arqsoft2.GorgeousSandwich.model.sandwich.*;

public interface SandwichCustomRepository {

    public Sandwich findSandwichById(String Id);
    
    public List<Sandwich> findAllSandwiches();
    
    public List<Sandwich> getSandwichesByPriceInterval(String currency, double minimumPrice, double maximumPrice);
    
    public List<Sandwich> findSandwichesWithIngredients(String[] list);
    
}
