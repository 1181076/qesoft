package arqsoft2.GorgeousSandwich.model.supplier;

import javax.persistence.*;

import arqsoft2.GorgeousSandwich.model.sandwich.Ingredient;
import eapli.framework.representations.dto.DTOable;

public interface DealInterface {

    public ID getId();
    public UnitPrice getUnitPrice();
    public ExpirationPriceDate getExpirationPriceDate();
    public Ingredient getIngredient();


    public void update(Deal deal);

    public DealDTO toDTO();
}
