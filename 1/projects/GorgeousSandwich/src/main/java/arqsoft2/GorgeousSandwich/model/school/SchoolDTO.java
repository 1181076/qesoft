package arqsoft2.GorgeousSandwich.model.school;

import java.util.List;

import arqsoft2.GorgeousSandwich.model.sandwich.ID;
import arqsoft2.GorgeousSandwich.model.sandwich.Sandwich;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class SchoolDTO {
    private InternalID internalID;
    private ExternalID id;
    private Name name;
    private Address address;
    private List<Sandwich> sandwiches;

    public SchoolDTO() {

    }

    public SchoolDTO(ExternalID id, InternalID internalID, Name name, Address address, List<Sandwich> sandwiches) {
        this.id = id;
        this.internalID = internalID;
        this.name = name;
        this.address = address;
        this.sandwiches = sandwiches;
    }

    public ExternalID getId() {
        return this.id;
    }

    public InternalID getInternalID() {
        return this.internalID;
    }

    public Name getName() {
        return this.name;
    }

    public Address getAddress() {
        return this.address;
    }

    public List<Sandwich> getSandwiches() {return this.sandwiches;}

    @Override
    public boolean equals(Object o) {

        return true;

    }
}