package arqsoft2.GorgeousSandwich.service.supplier;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.StackWalker.Option;
import java.net.http.HttpClient;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import arqsoft2.GorgeousSandwich.model.supplier.ID;
import arqsoft2.GorgeousSandwich.model.supplier.Supplier;
import arqsoft2.GorgeousSandwich.model.supplier.SupplierDTO;
import arqsoft2.GorgeousSandwich.repository.supplier.SupplierRepository;
import arqsoft2.GorgeousSandwich.repository.supplier.SupplierRepositoryImpl;

@Component
public class SupplierService {

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    SupplierRepositoryImpl supplierRepositoryImpl;

    public SupplierService(SupplierRepository supplierRepository, SupplierRepositoryImpl supplierRepositoryImpl){
        this.supplierRepository = supplierRepository;
        this.supplierRepositoryImpl = supplierRepositoryImpl;
    }

    public List<SupplierDTO> getAllSuppliers(@RequestParam(required = false) String title) {
        try {
            List<Supplier> suppliers = supplierRepositoryImpl.findAllSuppliers();
            List<SupplierDTO> ret = new ArrayList<>();
            suppliers.forEach(element -> ret.add(element.toDTO()));
            return ret;
        } catch (Exception error) {
            return new ArrayList<>();
        }
    }

    public SupplierDTO getSupplierById(@PathVariable("id") String id) {

        // VERSION WITH REPOSITORYIMPL 
        Supplier supplierData = supplierRepositoryImpl.findSupplierById(id);

        if (supplierData!=null) {
            return supplierData.toDTO();
        } else {
            return null;
        }

        // Optional<Supplier> supplierData = supplierRepository.findById(new ID(id));

        // if (supplierData.isPresent()) {
        //     return supplierData.get().toDTO();
        // } else {
        //     return null;
        // }
    }

    public SupplierDTO createSupplier(@RequestBody Supplier supplier) {
        try {
            Supplier _supplier = supplierRepository.save(supplier);
            return _supplier.toDTO();
        } catch (Exception error) {
            return null;
        }
    }

    public SupplierDTO updateSupplier(@PathVariable("id") ID id, @RequestBody Supplier supplier) {
        Optional<Supplier> supplierData = supplierRepository.findById(id);

        if (supplierData.isPresent()) {
            Supplier _supplier = supplierData.get();
            _supplier.update(supplier);
            return supplierRepository.save(_supplier).toDTO();
        } else {
            return null;
        }
    }

    public HttpStatus deleteSupplier(@PathVariable("id") ID id) {
        try{
            supplierRepository.deleteById(id);
            return HttpStatus.NO_CONTENT;
        } catch ( Exception error){
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

    public HttpStatus deleteAllSuppliers() {
        try {
            supplierRepository.deleteAll();
            return HttpStatus.NO_CONTENT;
        } catch (Exception error){
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }
}
