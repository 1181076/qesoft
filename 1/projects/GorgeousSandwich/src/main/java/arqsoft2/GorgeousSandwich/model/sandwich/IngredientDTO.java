package arqsoft2.GorgeousSandwich.model.sandwich;

import java.util.List;

import arqsoft2.GorgeousSandwich.model.supplier.Supplier;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class IngredientDTO {

    private ID id;
    private Description description;
    private Quantity quantity;
    private List<LastUpdatedDate> lastUpdatedDate;
    private UpperLimit upperLimit;
    private LowerLimit lowerLimit;
    private Units units;
    
    public IngredientDTO(){

    }

    public IngredientDTO(ID id, Description description, Quantity quantity, List<LastUpdatedDate> lastUpdatedDate, UpperLimit upperLimit, LowerLimit lowerLimit, Units units){
        this.id = id;
        this.description = description;
        this.quantity = quantity;
        this.lastUpdatedDate = lastUpdatedDate;
        this.upperLimit = upperLimit;
        this.lowerLimit = lowerLimit;
        this.units = units;
    }

    public ID getId() {
		return this.id;
	}

	public Description getDescription() {
		return this.description;
	}

	public void setDescription(Description description) {
		this.description = description;
	}

	public Quantity getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Quantity quantity) {
		this.quantity = quantity;
	}

	public List<LastUpdatedDate> getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(List<LastUpdatedDate> lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public UpperLimit getUpperLimit() {
		return this.upperLimit;
	}

	public void setUpperLimit(UpperLimit upperLimit) {
		this.upperLimit = upperLimit;
	}

	public LowerLimit getLowerLimit() {
		return this.lowerLimit;
	}

	public void setLowerLimit(LowerLimit lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	public Units getUnits() {
		return this.units;
	}

	public void setUnits(Units units) {
		this.units = units;
	}

    @Override
    public boolean equals(Object o) {
  
        return true; 
          
    }
}
