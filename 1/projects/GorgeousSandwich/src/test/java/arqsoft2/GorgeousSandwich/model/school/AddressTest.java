package arqsoft2.GorgeousSandwich.model.school;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class AddressTest {
    @Test
    void verifyIfAddressIsEmpty(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Address(""); });
        assertEquals("The address cannot be empty!", e.getMessage());
    }

    @Test
    void verifyIfAddressIsCorrect(){
        new Address("Rua da Faculdade");
        assertTrue(true);
    }
}