package arqsoft2.GorgeousSandwich.model.user;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class IDTest {
    @Test
    void verifyIfIDisEmpty(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new ID(""); });
        assertEquals("The ID cannot be empty!", e.getMessage());
    }

    @Test
    void verifyIfIDIsCorrect(){
        new ID("ID1");
        assertTrue(true);
    }
}