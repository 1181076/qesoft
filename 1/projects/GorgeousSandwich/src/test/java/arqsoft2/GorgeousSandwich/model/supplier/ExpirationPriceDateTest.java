package arqsoft2.GorgeousSandwich.model.supplier;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.sql.Date;

import static org.junit.jupiter.api.Assertions.*;

class ExpirationPriceDateTest {
    @Test
    void verifyIfExpirationPriceDateIsBelowCurrentDate(){
        LocalDate localDate = LocalDate.parse("2019-02-14");
        Date date = Date.valueOf(localDate);
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new ExpirationPriceDate(date); });
        assertEquals("The expiration price date must be above the current date!", e.getMessage());
    }

    @Test
    void verifyIfExpirationPriceDateIsCorrect(){
        LocalDate localDate = LocalDate.parse("2022-02-14");
        Date date = Date.valueOf(localDate);
        new ExpirationPriceDate(date);
        assertTrue(true);
    }
}