package arqsoft2.GorgeousSandwich.model.sandwich;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UpperLimitTest {
    @Test
    void verifyIfUpperLimitIsNegative(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new UpperLimit(-21); });
        assertEquals("The upper limit quantity cannot be negative!", e.getMessage());
    }

    @Test
    void verifyIfUpperLimitIsCorrect(){
        new UpperLimit(21);
        assertTrue(true);
    }
}