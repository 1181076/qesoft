package arqsoft2.GorgeousSandwich.model.school;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ExternalIDTest {
    @Test
    void verifyIfExternalIDIsEmpty(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new ExternalID(""); });
        assertEquals("The external ID cannot be empty!", e.getMessage());
    }

    @Test
    void verifyIfExternalIDIsCorrect(){
        new ExternalID("externalID1");
        assertTrue(true);
    }
}