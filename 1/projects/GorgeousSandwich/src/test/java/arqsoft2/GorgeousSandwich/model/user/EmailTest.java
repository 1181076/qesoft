package arqsoft2.GorgeousSandwich.model.user;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class EmailTest {
    @Test
    void verifyIfEmailIsIncorrect(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Email("1180964@isep"); });
        assertEquals("The email introduced is not valid!", e.getMessage());
    }

    @Test
    void verifyIfEmailIsCorrect(){
        new Email("1180964@isep.ipp.pt");
        assertTrue(true);
    }
}