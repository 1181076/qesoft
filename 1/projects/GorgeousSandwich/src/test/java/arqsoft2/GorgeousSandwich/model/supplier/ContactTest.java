package arqsoft2.GorgeousSandwich.model.supplier;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ContactTest {
    @Test
    void verifyIfContactIsBelow9Digits(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Contact(100000); });
        assertEquals("The contact needs to have exactly 9 digits!", e.getMessage());
    }

    @Test
    void verifyIfContactIsAbove9Digits(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Contact(1000000000); });
        assertEquals("The contact needs to have exactly 9 digits!", e.getMessage());
    }

    @Test
    void verifyIfContactIsCorrect(){
        new Contact(111111111);
        assertTrue(true);
    }
}