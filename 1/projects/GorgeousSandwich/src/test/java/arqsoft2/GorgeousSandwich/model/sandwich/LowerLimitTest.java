package arqsoft2.GorgeousSandwich.model.sandwich;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class LowerLimitTest {
    @Test
    void verifyIfLowerLimitIsNegative(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new LowerLimit(-21); });
        assertEquals("The lower limit quantity cannot be negative!", e.getMessage());
    }

    @Test
    void verifyIfLowerLimitIsCorrect(){
        new LowerLimit(21);
        assertTrue(true);
    }
}