package arqsoft2.GorgeousSandwich.model.user;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class SandwichOrderTest {
    @Test
    void verifyIfSandwichOrderAmountIsNegative(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new SandwichOrder("san1", -20); });
        assertEquals("The amount of the order cannot be negative!", e.getMessage());
    }

    @Test
    void verifyIfSandwichIsEmpty(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new SandwichOrder("", 20); });
        assertEquals("The sandwich order must contain a sandwich id!", e.getMessage());
    }

    @Test
    void verifyIfSandwichOrderIsCorrect(){
        new SandwichOrder("san1", 20);
        assertTrue(true);
    }
}