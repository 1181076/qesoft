package arqsoft2.GorgeousSandwich.model.sandwich;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ShortDescriptionTest {
    @Test
    void verifyIfShortDescriptionIsEmpty(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new ShortDescription(""); });
        assertEquals("Short Description cannot be empty!", e.getMessage());
    }

    @Test
    void verifyIfShortDescriptionIsLongerThan200Characters(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new ShortDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec iaculis elementum risus at mollis. Sed interdum augue nec nisi accumsan, id hendrerit est finibus. Pellentesque malesuada sit amet nisl in fringilla. Mauris ultricies venenatis lorem in commodo. Praesent ut lacus erat. Vestibulum purus eros, tempor a fermentum vel, bibendum sed lacus. Nunc tempus fermentum risus, rhoncus iaculis mi efficitur a. Morbi fermentum magna at justo consectetur euismod. Nam viverra ultrices purus maximus semper. Phasellus ultricies libero vel mi tempus, id accumsan neque ultrices. Ut vestibulum metus at urna tempus sollicitudin. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed commodo at enim sed volutpat. Nullam vitae sem ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec iaculis elementum risus at mollis. Sed interdum augue nec nisi accumsan, id hendrerit est finibus. Pellentesque malesuada sit amet nisl in fringilla. Mauris ultricies venenatis lorem in commodo. Praesent ut lacus erat. Vestibulum purus eros, tempor a fermentum vel, bibendum sed lacus. Nunc tempus fermentum risus, rhoncus iaculis mi efficitur a. Morbi fermentum magna at justo consectetur euismod. Nam viverra ultrices purus maximus semper. Phasellus ultricies libero vel mi tempus, id accumsan neque ultrices. Ut vestibulum metus at urna tempus sollicitudin. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed commodo at enim sed volutpat. Nullam vitae sem ex."); });
        assertEquals("Short Description cannot be over 50 characters!", e.getMessage());
    }

    @Test
    void verifyIfShortDescriptionIsCorrect(){
        new ShortDescription("This is a short description");
        assertTrue(true);
    }
}