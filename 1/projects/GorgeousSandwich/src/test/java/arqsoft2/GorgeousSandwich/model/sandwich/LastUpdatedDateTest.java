package arqsoft2.GorgeousSandwich.model.sandwich;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.sql.Date;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class LastUpdatedDateTest {
    @Test
    void verifyIfLastUpdatedDateIsAboveCurrentDate(){
        LocalDate localDate = LocalDate.parse("2022-02-14");
        Date date = Date.valueOf(localDate);
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> {new LastUpdatedDate(date, 20); });
        assertEquals("The last updated date cannot be above the current date!", e.getMessage());
    }

    @Test
    void verifyIfLastUpdatedDateIsCorrect(){
        LocalDate localDate = LocalDate.parse("2020-02-14");
        Date date = Date.valueOf(localDate);
        new LastUpdatedDate(date, 20);
        assertTrue(true);
    }
}