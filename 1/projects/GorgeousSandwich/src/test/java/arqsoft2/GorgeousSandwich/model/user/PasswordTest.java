package arqsoft2.GorgeousSandwich.model.user;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PasswordTest {
    @Test
    void verifyIfPasswordDoesNotHaveAtLeast8Characters(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Password("P1s$"); });
        assertEquals("The password must have at least 8 characters, one lowercase, one uppercase, one number and one special character!", e.getMessage());
    }

    @Test
    void verifyIfPasswordDoesNotHaveLowerCaseCharacter(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Password("P1S$WORD"); });
        assertEquals("The password must have at least 8 characters, one lowercase, one uppercase, one number and one special character!", e.getMessage());
    }

    @Test
    void verifyIfPasswordDoesNotHaveUpperCaseCharacter(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Password("p1s$word"); });
        assertEquals("The password must have at least 8 characters, one lowercase, one uppercase, one number and one special character!", e.getMessage());
    }

    @Test
    void verifyIfPasswordDoesNotHaveNumber(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Password("pas$word"); });
        assertEquals("The password must have at least 8 characters, one lowercase, one uppercase, one number and one special character!", e.getMessage());
    }

    @Test
    void verifyIfPasswordDoesNotHaveSpecialCharacter(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Password("pa1sword"); });
        assertEquals("The password must have at least 8 characters, one lowercase, one uppercase, one number and one special character!", e.getMessage());
    }

    @Test
    void verifyIfPasswordIsCorrect(){
        new Password("P1s$word");
        assertTrue(true);
    }
}