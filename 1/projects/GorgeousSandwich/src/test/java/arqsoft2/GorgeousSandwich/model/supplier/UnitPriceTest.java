package arqsoft2.GorgeousSandwich.model.supplier;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UnitPriceTest {
    @Test
    void verifyIfUnitPriceValueIsNegative(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new UnitPrice(-21, "grams"); });
        assertEquals("The price value cannot be negative!", e.getMessage());
    }

    @Test
    void verifyIfUnitPriceIsEmpty(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new UnitPrice(21, ""); });
        assertEquals("The unit price cannot be empty!", e.getMessage());
    }

    @Test
    void verifyIfUnitPriceIsCorrect(){
        new UnitPrice(21, "grams");
        assertTrue(true);
    }
}