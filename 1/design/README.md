# Design 

# Architectural Drivers

## Use Case Diagram

![UseCaseDiagram.png](./VP_Documentation/UseCaseDiagram.png)

## Functional Requirements

| Use Case                                                     | Description                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| UC-1: Create Sandwich                                        | A stock manager creates a sandwich with an internal and an external identification, a sales price, a short description. One sandwich has one or more ingredients with their specific quantities. |
| UC-2: Create School                                          | A school manager creates a school with an internal and an external identification, name and address. |
| UC-3: Register Sandwich                                      | A school manager associates a created sandwich to a specific school. |
| UC-4: Register sandwich quantity to deliver on a specific day | A school user wants to place an order about sandwiches' quantity to be delivered on a day that the school user specifies. |
| UC-5: Create ingredient                                      | A stock manager creates an ingredient with an identifier and a description. It also has an unit of measure for orders and another one for sandwiches. Lastly it has a threshold inventory quantity for both the lower and upper limit. |
| UC-6: Manage ingredient quantities                           | A stock manager can increase or reduce quantities of a certain ingredient. Those changes cannot surpass the established limits. |
| UC-7: Create supplier                                        | A supplier manager creates a supplier with an identifier, a name, an address, several emails and contacts, and a description. |
| UC-8: Creat deal                                             | A supplier manager creates a deal for a specific ingredient specifying the identifier, the unit price (currency and amount) and the expiration date. |
| UC-9: Get data from CSV file                                 | A stock manager has a CSV file with domain entities that can be entered in the system. Once the file is uploaded, each line will be read and the system will create and save the valid domain entities. If there is invalid values, those lines are discarded and stored in a `.log` file with a time stamp. |
| UC-10: Query the application                                 | All users can query the application to get information about domain entities depending on their specific role. Some examples of those informations are: **Orders placed between two periods**, **Changes in item's quantity recorded between two periods**, **Sandwiches that have one or more specific ingredients**, **Sandwiches or ingredients with prices between two values** |
| UC-11: Log in                                                | All users of the application (school user, school manager, supplier manager and stock manager) can sign in with an email and password to have access to all the different use cases according to the role associated to that user. |
| UC-12: Register                                              | An unknown user can register in the application in order to be a school user. The unknown user will need to specify a name, an email, a password and a school to be associated with. |

## Quality Attributes

| ID   | Quality Atribute | Scenario                                                     | Associated Use Case |
| ---- | ---------------- | ------------------------------------------------------------ | ------------------- |
| QA-1 | Securability     | The presence of user roles ensures that no user other than the one who was the right permissions can interact with certain use cases. Also it is important to mention that past security problems with previous software highlighted the importance of security in the application, so it is important to apply some principles of **secure by design** to try mitigate those threats. | All                 |
| QA-2 | Authenticity     | In order to use the system, an user needs to be authenticated by providing an email and a password. Also, it is necessary to take into account that unknown users can register in the system. | UC-11               |
| QA-3 | Modifiability    | The application must be architectured and developed to accommodate possible modifications, as some functionalities may be incorporated or changed until the final application is available. | All                 |
| QA-4 | Testability      | Although not all functionalities need to be presented on the web application front-end, they still need to be tested to improve code quality and to reduce bugs. | All                 |
| QA-5 | Usability        | The website that will be made to access the application (web application front-end) must be simple, as it needs to provide the available sandwiches in the school, as well as their ingredients. Other information can be taken into consideration. | All                 |

## Constraints

| ID    | Constraint                                                   |
| ----- | ------------------------------------------------------------ |
| CON-1 | Application must be monolithic                               |
| CON-2 | Application must be accessible through a web browser in any operating system |
| CON-3 | Application must be developed with Java Spring               |
| CON-4 | Application is an on-prem solution                           |
| CON-5 | Application must be deployed on virtual machines or Docker   |
| CON-6 | Queries to the API need to be included                       |
| CON-7 | Only open-source tools and technologies are allowed          |
| CON-8 | All requisites of the application must be done within a 4 week period. |

## Type of system that is being designed

- Greenfield in mature domain:

  - Web Application:

    - API back-end

    - Website front-end

## Concerns

| ID    | Concern                                                      |
| ----- | ------------------------------------------------------------ |
| CRN-1 | Some technologies might be adopted in the future (e.g. GraphQL for the API) |
| CRN-2 | Remaining CRUD applications could be developed               |

# Technologies adopted

### API/Backend

- **Java Spring**

  Reasons to choose:

  - Mandatory for this project.

- **Java Persistence API (JPA)**

  Reasons to choose:

  - Open-Source technology;
  - Technology to facilitate the process of persisting Java objects into databases.

### Frontend

- **React**

  Reasons to choose:

  - Open-Source technology;

  - All members of the group are familiared with the framework;
  - It is one of the most well-known frameworks to build web applications, with a very large community and third party libraries and components;
  - Although it has some security flaws like possible XSS, XEE, SQL injection, Broken Authentication and Zip Slip according to [Third Rock Techkno](https://www.thirdrocktechkno.com/blog/5-react-security-vulnerabilities-serious-enough-to-break-your-application/), there are possible ways to try mitigate those problems.

### Database

- **PostgreSQL**

  Reasons to choose:

  - Open-Source technology;
  - All members of the group are familiared with SQL;
  - Relational database option is a better choise for this project to organize the information of the several entities;
  - Advanced relational database with over 30 years of development that combines strong reliability, features and performance;
  - According to [Satori](https://satoricyber.com/postgres-security/3-pillars-of-postgresql-security/), PostgreSQL is based on three security pillars:
    - Network-level security: Unix Domain and TCP/IP sockets and firewalls;
    - Transport-level security: SSL/TLS communication;
    - Database-level security: Roles and permissions, row level security and auditing.
  - Even with those security features, some practices need to be taken into consideration when building the database.