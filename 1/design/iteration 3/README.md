# ADD

## Iteration 3

### Step 2: Iteration Goal

The main goal of this iteration is to support the partially and not addressed quality attributes, and some of the use cases that can have an impact in the application structure (e.g. Login, CSV File Import and queries to the application entities).

#### Kanban Board

| Not Addressed | Partialy Addressed | Fully Addressed |
| ------------- | ------------------ | --------------- |
| UC-9          |                    |                 |
| UC-10         |                    |                 |
| UC-11         |                    |                 |
|               | QA-1               |                 |
| QA-2          |                    |                 |
|               | QA-4               |                 |
|               | QA-5               |                 |
| CON-6         |                    |                 |

### Step 3: Choose what to refine

For this iteration the main objective is to refine the client and server side web application to accomodate the other aspects (architectural drivers specified in Step 2) of the application, such as the authentication, read from CSV file and queries to the application.

### Step 4: Choose design concepts

| Design Decision and Location                                 | Rationale and Assumptions                                    |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Use Spring Security framework for authentication and authorization | Since implementing a system that supports both authentication and authorization takes a lot of time to develop, it is possible to use the Spring Security Framework since it is easier to integrate with the Spring Boot Framework and it has already some of the functionalities already developed and ready to use. |
| Data Mapper Pattern                                          | Following the diagram of architectural layers created on iteration 1, the introduction of the data mapper pattern can help specify some custom queries to the database in order to retrieve the specific information wanted by the users (CON-6). |
| Use Spring Multipart object for file uploading               | Java Spring offers support for multipart file uploading, which makes it easier to read the file retrieved from the API. |

### Step 5: Instiate architectural elements:

| Elements                     | Responsibility                                               |
| ---------------------------- | ------------------------------------------------------------ |
| Security Module              | This module will handle the authentication and authorization process (QA-2) process that will be implemented using Spring Security, as well as all some Secure by Design concepts (QA-1). |
| Custom Repository Interfaces | Custom interfaces depending on the entity that can get custom information from the database, in order for the user to be able to make queries to the application. |
| CSV File Uploading Endpoint  | Dedicated endpoint which will support the Spring Multipart object and be able to read the CSV files imported by the users via "multipart/form-data" requests. |

### Step 6: Sketch views and record design decisions:

The diagram represented in iteration 1 will be updated to support the authentication proccess of the application and enhance QA-1:

![webapp_v2.png](./diagrams/webapp_v2.png)

Regarding the custom repository interfaces, some of the functions to implement to one of the entities (e.g. Sandwich) are:

| **Method Name**                                       | **Description**                                              |
| ----------------------------------------------------- | ------------------------------------------------------------ |
| `public List<Sandwich> getSandwichesByPriceInterval`  | Returns a collection with all the sandwiches that are in between the price amount interval specified by the user. |
| `public List<Sandwich> findSandwichesWithIngredients` | Returns a collection with all the sandwiches that have the ingredients specified by the user. |

**Note:** These functions are examples of queries to be implemented. This structure and planning will be adopted to the rest of the entities that allow querying.

Lastly, the endpoint will be implemented following this specification:

| **Method Name**                                              | **Description**                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| `@PostMapping('/import/createFromFile') public importCSVFile(@RequestParam('file') MultipartFile file)` | Endpoint that will receive a multipart file from the API, in order to import the entities inside that CSV file. |

### Step 7: Analyse current design and review iteration goal(s):

#### Current state of the Kanban Board

| Not Addressed | Partialy Addressed | Fully Addressed | Observations                                                 |
| ------------- | ------------------ | --------------- | ------------------------------------------------------------ |
|               |                    | UC-9            |                                                              |
|               |                    | UC-10           |                                                              |
|               |                    | UC-11           |                                                              |
|               |                    | QA-1            |                                                              |
|               |                    | QA-2            |                                                              |
|               | QA-4               |                 | Although this quality attribute was not directly addressed by the design concepts, it was partially addressed because the design concepts adopted simplify the testability of the application |
|               |                    | QA-5            |                                                              |
|               |                    | CON-6           |                                                              |
