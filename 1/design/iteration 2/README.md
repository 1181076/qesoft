# ADD

## Iteration 2

### Step 2: Iteration Goal

Create the base of the application with the main functionalities.

#### Kanban Board

| Not Addressed | Partialy Addressed | Fully Addressed |
| ------------- | ------------------ | --------------- |
| UC-1          |                    |                 |
| UC-2          |                    |                 |
| UC-5          |                    |                 |
| UC-7          |                    |                 |
| UC-8          |                    |                 |
| UC-12         |                    |                 |
| QA-4          |                    |                 |
|               | QA-5               |                 |

### Step 3: Choose what to refine

For this iteration the main objective is to refine the Use Cases related to the main entities of the system.

### Step 4: Choose design concepts

Architectural Patterns - Layered pattern, Client-server pattern

| Design Decision and Location                | Rationale and Assumptions                                    |
| ------------------------------------------- | ------------------------------------------------------------ |
| Create the Domain Model for the application | Before starting the implementation of the use cases, it's necessary to create a domain model which is able to respond to all domain needs. In order to achieve that, all entities and its relations will be created. |
| Identify Domain Objects                     | Each functional element of the application needs to be encapsulated in Domain Object. |
| Use JPA                                     | JPA drivers implement the defined interfaces, for interacting with the database server. |

### Step 5: Instiate architectural elements:

| Elements                                   | Responsibility                                               |
| ------------------------------------------ | ------------------------------------------------------------ |
| Create Domain Model                        | Create a domain model which contains all the entities that pariticipate in all use cases |
| Map the system use cases to domain objects | Initial identification of the domain objects which embraces all the use cases identified. |
| Decompose the domain objects               | Define in which layer the domain objects belong to           |

### Step 6: Sketch views and record design decisions:

This diagram represents the Domain Model

![DomainModel](./diagrams/DomainModel.jpg)

This diagram represents the Domain Objects within all the layers

![DomainObjects](./diagrams/DomainObjects.png)

Lastly, a representation of the DDD Diagram can be seen in the image below:

![DDD](./diagrams/DDD.png)

### Step 7: Analyse current design and review iteration goal(s):

#### Current state of the Kanban Board

| Not Addressed | Partialy Addressed | Fully Addressed |
| ------------- | ------------------ | --------------- |
|               |                    | UC-1            |
|               |                    | UC-2            |
|               |                    | UC-5            |
|               |                    | UC-7            |
|               |                    | UC-8            |
|               |                    | UC-12           |
|               | QA-4               |                 |
|               | QA-5               |                 |
