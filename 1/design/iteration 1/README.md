# ADD

### Step 1: Review Inputs

| Scenario Id | Importance to the customer | Dificulty of implementation |
| ----------- | -------------------------- | --------------------------- |
| QA-1        | High                       | Medium                      |
| QA-2        | High                       | Medium                      |
| QA-3        | High                       | High                        |
| QA-4        | High                       | Low                         |
| QA-5        | High                       | Medium                      |

| Category                        | Details                                                      |
| ------------------------------- | ------------------------------------------------------------ |
| Design purpose                  | It's necessary to create a solid design to suport the construction of the system that is a greenfield system from a mature domain |
| Primary functional requirements | From all use cases presented, the primary ones are: **UC-1**, **UC-2**, **UC-5**, **UC-7** and **UC-12**. Some other use cases such as **UC-10** and **UC-11** can have an impact in the application structure to accomodate those functionalities. |
| Quality attribute scenarios     | **QA-1**, **QA-2**, **QA-3**, **QA-4** and **QA-5**          |
| Constraints                     | All constraints previously presented are included as drivers |
| Architectural concerns          | All architectural concerns previously presented are included as drivers |

## Iteration 1

### Step 2: Iteration Goal

Since this is the first iteration the goal is to structure the system by keeping in mind all the drivers who may have influence on it.

#### Kanban Board

| Not Addressed | Partialy Addressed | Fully Addressed |
| ------------- | ------------------ | --------------- |
| QA-1          |                    |                 |
| QA-3          |                    |                 |
| QA-5          |                    |                 |
| CON-1         |                    |                 |
| CON-2         |                    |                 |
| CON-3         |                    |                 |
| CON-4         |                    |                 |
| CON-5         |                    |                 |
| CON-7         |                    |                 |
| CON-8         |                    |                 |
| CRN-1         |                    |                 |
| CRN-2         |                    |                 |

### Step 3: Choose what to refine

For this first iteration, since this is a Greenfield System in a mature domain, the element to be refined is the system structure.

### Step 4: Choose design concepts

Reference Arquiteture - Client Side Web Application & Service Side Service Application

Deployment Patterns - On-prem solution

| Design Decision and Location           | Rationale and Assumptions                                                                              |
| -------------------------------------- | ------------------------------------------------------------------------------------------------------ |
| Client and Server Side Web Application | CON-2 (Allows the user to use the application in a browser) and CON-1 (This application is monolithic) |
| Deployment                             | CON-5 (Front-end, back-end and DB will be deployed in docker containers)                               |

### Step 5: Instiate architectural elements:

| Elements                             | Responsibility                                                                           |
| ------------------------------------ | ---------------------------------------------------------------------------------------- |
| Presentation Layer (Web Application) | This layer contains components which control user interaction and use case control flow. |
| Business Layer (Web Application)     | This layer contains the business logic of the application.                               |
| Data Layer (Web Application)         | This layer provides the data access and service agent components.                        |
| Deployment                           | This element provides the deployment of the application on docker containers.            |

### Step 6: Sketch views and record design decisions:

This first diagram represents the three architectural layers to be implemented.

![webapp_v1.png](./diagrams/webapp_v1.png)

This diagram represents the deployment of the application on a Docker Container.

![dockerDeployment](./diagrams/dockerDeployment.jpg)

### Step 7: Analyse current design and review iteration goal(s):

#### Current state of the Kanban Board

| Not Addressed | Partialy Addressed | Fully Addressed |
| ------------- | ------------------ | --------------- |
|               | QA-1               |                 |
|               |                    | QA-3            |
|               | QA-5               |                 |
|               |                    | CON-1           |
|               |                    | CON-2           |
|               |                    | CON-3           |
|               |                    | CON-4           |
|               |                    | CON-5           |
|               |                    | CON-7           |
|               |                    | CON-8           |
|               |                    | CRN-1           |
|               |                    | CRN-2           |
