# Design 

# Architectural Drivers

## Use Case Diagram

![UseCaseDiagram.png](./VPP_Documentation/UseCaseDiagram.png)

## Functional Requirements

| Use Case                                                     | Description                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| UC-1: Create Sandwich                                        | A stock manager creates a sandwich with all the previous attributes, as well as associating one or more categories. |
| UC-2: Create School                                          | A school manager creates a school with an internal and an external identification, name and address. None of these fields can be empty or only with spaces. The designation must start with a letter. |
| UC-4: Register sandwich quantity to deliver on a specific day | A school representative can order sandwiches for delivery on any given day outside of non-availability periods. |
| UC-6: Manage ingredient quantities                           | The system needs to register the updating dates and times when the ingredient quantity changes. |
| UC-13: Create Category                                       | A stock manager creates a category that has an attribute that identifies it without ambiguity, also a description and its possible values. |
| UC-14: Manage application downtime                           | The system must manage downtime allowing periods of availability and unavailabilty. |

## Quality Attributes

| ID   | Quality Atribute | Scenario                                                     | Associated Use Case |
| ---- | ---------------- | ------------------------------------------------------------ | ------------------- |
| QA-1 | Securability     | The presence of user roles ensures that no user other than the one who was the right permissions can interact with certain use cases. Also it is important to mention that past security problems with previous software highlighted the importance of security in the application, so it is important to apply some principles of **secure by design** to try mitigate those threats. | All                 |
| QA-2 | Authenticity     | In order to use the system, an user needs to be authenticated by providing an email and a password. Also, it is necessary to take into account that unknown users can register in the system. | UC-11               |
| QA-3 | Modifiability    | The application must be architectured and developed to accommodate possible modifications, as some functionalities may be incorporated or changed until the final application is available. In this case, it is necessary to accomodate new technologies like GraphQL. | All                 |
| QA-4 | Testability      | Although not all functionalities need to be presented on the web application front-end, they still need to be tested to improve code quality and to reduce bugs. | All                 |
| QA-5 | Usability        | The website that will be made to access the application (web application front-end) must be simple, as it needs to provide the available sandwiches in the school, their categories as well as their ingredients ordered by their quantities, appearing first the heaviest ones. Other information can be taken into consideration. | All                 |
| QA-6 | Discoverability  | Since the application will have a microservice architecture, code discoverability is an important aspect to find the specific microservice already existing in the back-end. | All                 |
| QA-7 | Modularity       | The application needs to have a microservices architecture to response to needs of peak utilization and to be able to develop services and functionalities with more efficiency. | All                 |
| QA-8 | Immutability     | The application needs to ensure that the entity objects created inside the microservices are immutable to prevent denial of service attacks | All                 |

## Constraints

| ID    | Constraint                                                   |
| ----- | ------------------------------------------------------------ |
| CON-1 | Application must be migrated to microservices                |
| CON-2 | Application must be accessible through a web browser in any operating system |
| CON-3 | Application must use GraphQL                                 |
| CON-4 | Application is an on-prem solution                           |
| CON-5 | Application must be deployed on Docker                       |
| CON-6 | Queries to the API need to be included                       |
| CON-7 | Application must have automated tests                        |
| CON-8 | API documentation must be written with OpenAPI (Swagger)     |
| CON-9 | During migration, data cannot be lost.                       |

## Concerns

| ID    | Concern                                                      |
| ----- | ------------------------------------------------------------ |
| CRN-1 | API clients should be aware that new sandwiches were created |
| CRN-2 | Remaining CRUD applications for the new entities could be developed |

## Type of system that is being designed

- Brownfield in mature domain:

  - Web Application:

    - Back-end migration using Microservices
    - GraphQL API to connect all microservices
    - Website front-end

# Technologies adopted

### Backend - Microservices

**Java Spring**

Reasons to choose:

- Open Source framework;
- It was mandatory in the previous part of the project;
- Since the group already has the codebase implemented in this framework, the group will migrate the various entities into microservices.

**Java Persistence API (JPA)**

  Reasons to choose:

  - Open-Source technology;
  - Technology to facilitate the process of persisting Java objects into databases.

**Node.JS (with Typescript)**

Reasons to choose:

- It is mandatory to implement microservices architecture with at least two different programming languages;

- Open-Source language;

- All members of the group are familiared with the language;

- According to [Gilad David Maayan from keen.ethics](https://keenethics.com/blog/nodejs-security), it is important to take into consideration that Node.js can become much more insecure by using third-party libraries, like older versions of Express. Although a Node.js application will certantly have some third-party software, it is possible to mitigate some of those problems by using other third-party libraries.

  For example, the older versions of Express were not developed with security in mind, but it is possible to use a library called **Helmet** to help improve the security of HTTP Headers sent in a HTTP request.

**Express**:

  Reasons to choose:

  - Open-Source technology;
  - Creates a REST API server to be able to access the various endpoints defined by the group;
  - It is one of the most popular libraries for creating web applications and API's;
  - Since it is a popular library, it has a vaste community;
  - Older versions of Express had some security problems, but newer versions of the library seem to have resolved some of the problems.

**Helmet**:

  Reasons to choose:

  - Open-Source technology;
  - Middleware for Express that enhances security in Express by setting various HTTP Headers to the request, like XSS attacks, CORS policy, and others.

**TypeORM**:

  Reasons to choose:

  - Open-Source technology;
  - ORM Tool to help communicate with the database. Like an ORM Tool like JPA, it allows the definition of entities and their relationships, and the use of repositories to persist or retrieve information from the database.
  - It has a query builder for simple and complex queries, and to avoid SQL Injection attacks.

### API

**GraphQL**

  Reasons to choose:  
  
  - Mandatory use
  - Gathers all the possible system requests into one single gateway, saving bandwidth and reducing waterfall requests;
  - Smaller payloads;
  - Strict and secure interfaces;
  - According to [Kentaro Wakayama](https://codersociety.com/blog/articles/graphql-reasons), GraphQL significantly reduces the effort to have a well-documented API, making it an integral and natural part of the coding process.
  
  
**Apollo**

  Reasons to choose:
  
  - Open-Source technology;
  - Handles GraphQL in the server and client side of application;
  - As stated by [Rajat S](https://blog.bitsrc.io/should-i-use-apollo-for-graphql-936129de72fe), Apollo has a significant and active community support and widespread use.
  - Interoperability with other frameworks, such as React, which was used for the Front-End of the application.
  
**Swagger**

  Reasons to choose:
  
  - Mandatory use;
  - Open-Source technology;
  - Allows for an organized documentation of the various possible requests and their requirements, providing an overlook of the application functionalities;


### Service Discovery

**Netflix Eureka**

  Reasons to choose:

  - Open-Source technology;
  - With Netflix Eureka each client can simultaneously act as a server, to replicate its status to a connected peer. In other words, a client retrieves a list of all connected peers of a service registry and makes all further requests to any other services through a load-balancing algorithm.

### Frontend

**React**

  Reasons to choose:

  - Open-Source technology;

  - All members of the group are familiared with the framework;
  - It is one of the most well-known frameworks to build web applications, with a very large community and third party libraries and components;
  - Although it has some security flaws like possible XSS, XEE, SQL injection, Broken Authentication and Zip Slip according to [Third Rock Techkno](https://www.thirdrocktechkno.com/blog/5-react-security-vulnerabilities-serious-enough-to-break-your-application/), there are possible ways to try mitigate those problems.

### Database

**PostgreSQL**

  Reasons to choose:

  - Open-Source technology;
  - All members of the group are familiared with SQL;
  - Relational database option is a better choise for this project to organize the information of the several entities;
  - Advanced relational database with over 30 years of development that combines strong reliability, features and performance;
  - According to [Satori](https://satoricyber.com/postgres-security/3-pillars-of-postgresql-security/), PostgreSQL is based on three security pillars:
    - Network-level security: Unix Domain and TCP/IP sockets and firewalls;
    - Transport-level security: SSL/TLS communication;
    - Database-level security: Roles and permissions, row level security and auditing.
  - Even with those security features, some practices need to be taken into consideration when building the database.
