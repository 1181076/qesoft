# ADD

## Iteration 3

### Step 2: Iteration Goal

The main goal of this iteration is to support the partially and not addressed quality attributes, such as testability, security and immutability. For security and immutability, it is necessary to specify secure by design principles to mitigate security issues. For testability, the ability to structure a way to support the automation of tests to ensure code quality needs to be taken into account as well. 

#### Kanban Board

| Not Addressed | Partialy Addressed | Fully Addressed |
| ------------- | ------------------ | --------------- |
|               | QA-1               |                 |
| QA-4          |                    |                 |
| QA-8          |                    |                 |
| CON-6         |                    |                 |
| CON-7         |                    |                 |

### Step 3: Choose what to refine

For this iteration the main objective is to refine the client and server side microservice applications to accomodate the other aspects (architectural drivers specified in Step 2) of the application, such as the implementation of security methods and automatic tests.

### Step 4: Choose design concepts

| Design Decision and Location                                 | Rationale and Assumptions                                    |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Implementation of system tests to test API endpoints         | In the previous iteration of the project, code quality was ensured by implementing unit tests to the business rules of each entity of the domain. In order to ensure automated tests, system tests (that involve the whole application) will be developed, in conjuction to the previously developed unit tests. |
| Make properties of class object immutable to avoid denial of service attacks | One form of denial of service attack is to illegally update and modify the value properties of a given class object. To avoid that, it is necessary to ensure that those properties cannot be changed during the life cycle of the object. |
| Pass an access token to the headers of every request         | To avoid most of the denial of service attacks, it is necessary that the client attaches an access token (such as JWT) to ensure that the origin of the data is trustworthy. |
| Check request content size to avoid denial of service attacks | To avoid denial of service attacks, it is advisable to check the content length of the request. If the content size is bigger than a certain defined number, that request must be ignored. |
| Check content data to avoid denial of service attacks        | After cheking the content size of the request, it is important to check the size of individual property data to see if the content sent is not putting stress over the server. |

**Note:** There are other aspects to keep into consideration when refering to denial of service attacks (e.g. checking lexical content of data, checking the data syntax, checking the data semantics, etc.), however those aspects will not be implemented during this project because of development time restrictions. The last four mentioned design decisions will cover most of the denial of service prevention tactics.

In addition, other secure by design principles such as failing fast were already applied during the first part of the project, so it is not necessary to repeat those design decisions again.

### Step 5: Instiate architectural elements:

| Elements                               | Responsibility                                               |
| -------------------------------------- | ------------------------------------------------------------ |
| Postman System Tests Collections       | This type of automatic tests will test the application as a whole and ensure code quality. |
| Immutable Domain Primitives            | Immutable domain primitives will make the domain primitives keep their original values throughout their life cycle. |
| Authorization Header on HTTP Requests  | Header that supports a token to authorize the client to access the system services. The backend will decipher the access token sent in order to see if it's a valid one. |
| Content-Length Header on HTTP Requests | Header that mentions the size of the data sent by the HTTP Request. The backend will then decide if the size of the data is allowed or not. |
| Content data validation Methods        | Methods responsible for analysing the data sent by the client, in order to see if they are meeting the size restrictions of the domain primitive. Those methods will avoid big data to be persisted and denial of service attacks. |

### Step 6: Sketch views and record design decisions:

Immutable domain primitive examples:

- Non-creation and elimination of `set` methods. Those methods are responsible of changing the properties of a given class, making them mutable and vulnerable to denial of service attacks.
- Change domain primitive declarations. Example:

| **Domain Primitive Declaration** | **Description**                                              |
| -------------------------------- | ------------------------------------------------------------ |
| `private final Name name`        | By specifying the `final` keyword on the `Name` domain primitive, it ensures that the value assigned to that variable will not change throughout it's life cycle. |

It is also necessary to establish limits to the content size of every HTTP request in both back-end programming languages:

| **Content Size HTTP Request Limit Declaration** | **Description**                                              |
| ----------------------------------------------- | ------------------------------------------------------------ |
| server.max-http-header-size                     | Specifying the value of that property inside application.properties file in Java Spring Boot will limit the handling of requests that surpass the written value |
| app.use(express.json({ limit: 10 }))            | Specifying the limit configuration inside an Express server will limit the handling of requests that surpass the written value |



### Step 7: Analyse current design and review iteration goal(s):

#### Current state of the Kanban Board

| Not Addressed | Partialy Addressed | Fully Addressed |
| ------------- | ------------------ | --------------- |
|               |                    | QA-1            |
|               |                    | QA-4            |
|               |                    | QA-8            |
|               |                    | CON-6           |
|               |                    | CON7            |
