# ADD

## Iteration 2

### Step 2: Iteration Goal

Support the new functionalities of the system.

#### Kanban Board

| Not Addressed | Partialy Addressed | Fully Addressed |
| ------------- | ------------------ | --------------- |
| UC-13         |                    |                 |
| UC-14         |                    |                 |
|               | QA-1               |                 |
|               | QA-3               |                 |
|               | QA-7               |                 |
| CRN-2         |                    |                 |

### Step 3: Choose what to refine

For this iteration the main objective is to refine the Use Cases related to the system and support the new functionalities.

### Step 4: Choose design concepts

| Design Decision and Location               | Rationale and Assumptions                                    |
| ------------------------------------------ | ------------------------------------------------------------ |
| Update the Domain Model of the application | Before starting the implementation of the use cases, it's necessary to update the domain model previously done in part 1 which is able to answer to all domain needs. In order to achieve that, all entities and its relations will be created. |
| Update Domain Objects                      | Each functional element of the application needs to be encapsulated in Domain Object. |

### Step 5: Instiate architectural elements:

| Elements                                   | Responsibility                                               |
| ------------------------------------------ | ------------------------------------------------------------ |
| Update Domain Model                        | Update the domain model previously done in part 1 which contains all the entities that participate in all use cases |
| Map the system use cases to domain objects | Initial identification of the domain objects which embraces all the use cases identified. |
| Decompose the domain objects               | Define in which layer the domain objects belong to           |

### Step 6: Sketch views and record design decisions:

This diagram represents the Domain Model:

![DomainModel](./diagrams/DomainModel.png)

This diagram represents the Domain Objects within all the layers:

![DomainObjects](./diagrams/DomainObjects.png)

There is, as well, a representation of the DDD Diagram can be seen in the image below:

![DDD](./diagrams/DDD.png)

Lastly, there are the updated microservice and database diagrams, that contain the new Calendar section of the system:

![MicroserviceArchitecture.png](./diagrams/MicroserviceArchitecture.png)

![dockerDeployment](./diagrams/Deployment.png)

### Step 7: Analyse current design and review iteration goal(s):

#### Current state of the Kanban Board

| Not Addressed | Partialy Addressed | Fully Addressed |
| ------------- | ------------------ | --------------- |
|               |                    | UC-13           |
|               |                    | UC-14           |
|               | QA-1               |                 |
|               |                    | QA-3            |
|               |                    | QA-7            |
|               |                    | CRN-2           |
