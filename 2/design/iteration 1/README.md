# ADD

### Step 1: Review Inputs

| Scenario Id | Importance to the customer | Dificulty of implementation |
| ----------- | -------------------------- | --------------------------- |
| QA-1        | High                       | Medium                      |
| QA-2        | High                       | Low                         |
| QA-3        | High                       | High                        |
| QA-4        | High                       | Medium                      |
| QA-5        | High                       | Low                         |
| QA-6        | Low                        | Medium                      |
| QA-7        | High                       | High                        |

| Category                        | Details                                                      |
| ------------------------------- | ------------------------------------------------------------ |
| Design purpose                  | It's necessary to create a solid design to migrate the greenfield previously done in the first part of the project, into a microservice architecture with more than two programming languages used in the back-end. With that, this ADD will cover the modification of a brownfield project in a mature domain. |
| Primary functional requirements | From all use cases presented, the new ones are: **UC-13** and **UC-14**. The rest of the functionalities to be implemented will be a refactoring of the following use cases: **UC-1**, **UC-2**, **UC-4** and **UC-6**. |
| Quality attribute scenarios     | **QA-1**, **QA-2**, **QA-3**, **QA-4**, **QA-5**, **QA-6** and **QA-7** |
| Constraints                     | All constraints previously presented are included as drivers. |
| Concerns                        | All architectural concerns previously presented are included as drivers. |

## Iteration 1

### Step 2: Iteration Goal

Since this is the first iteration the goal is to structure the system to accomodate the transition from a monolithic application to a microservice application.

#### Kanban Board

| Not Addressed | Partialy Addressed | Fully Addressed |
| ------------- | ------------------ | --------------- |
| QA-1          |                    |                 |
|               | QA-2               |                 |
| QA-3          |                    |                 |
| QA-5          |                    |                 |
| QA-6          |                    |                 |
| QA-7          |                    |                 |
| CON-1         |                    |                 |
|               |                    | CON-2           |
| CON-3         |                    |                 |
|               | CON-4              |                 |
|               | CON-5              |                 |
| CON-8         |                    |                 |
| CON-9         |                    |                 |
| CRN-1         |                    |                 |

**Note:** Since some of the quality attributes and constraints were previously addressed in the first part of the project, and they are again quality attributes and constraints to be addressed in the second part of the project, those architectural drivers were not placed in the **Not Addressed** column.

### Step 3: Choose what to refine

For this first iteration, since this is a migration from a monolithic application to a microservice application, the element to be refined is the system structure.

### Step 4: Choose design concepts

**Reference Arquiteture** - Microservice pattern/architecture:

- Client-Side Web Application
- API Gateway
- Server-Side Service Application

| Design Decision and Location       | Rationale and Assumptions                                    |
| ---------------------------------- | ------------------------------------------------------------ |
| Strangler Pattern                  | This pattern will be used to help the transition from a monolithic application to a microservice application (CON-1) (QA-3) (QA-7). The new use cases will be created outside the monolithic application, while the other use cases will be gradually transferred to new services. |
| Decomposition of the microservices | To apply the microservice architecture to the brownfield application, it is necessary to identify how to divide the current monolithic application into several microservices (QA-7). |
| Database per service pattern       | In the monolithic application, a shared database was used and all use cases had access to all the data. Since it is necessary to decouple the application into several seperate services, the data needs to be decoupled as well (QA-7). To accomplish that, it is necessary to use the database per service pattern to be able to seperate the data. |
| Saga Pattern                       | When using the database per service pattern, there might be some problems during transactions that encompass multiple services at a time. For that, it is used the saga pattern to implement transactions that span multiple services. |
| Service Discovery                  | When dealing with microservice applications, services might need to call other services, but most of the time the number of instances of that service and their locations (hosts and ports) change constantly. It is necessary to implement a service discovery to redirect the request to an available service instance (QA-6). |
| API Gateway Pattern                | Instead of requesting each service individually, it is implemented an API Gateway to work as a single entry point to the various services. The API Gateway can implement GraphQL to access all the information in the client (QA-3) (CON-3). |
| API Composition Pattern            | When using the database per service pattern, there might be some problems during queries that encompass multiple services at a time. For that, it is used the API Composition Pattern inside the API Gateway to retrieve the information of several services. (CON-3) |
| Access Token Pattern               | In order for the client to have access to the various functionalities of the server, it is necessary to be authenticated to the system (QA-2). For that, it is implemented in the API Gateway the Access Token pattern that will handle the authentication and retrieve a JWT Token to the client. |

### Step 5: Instiate architectural elements:

| Elements                                                     | Responsibility                                               |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| Strangler Facade                                             | To communicate properly between the two types of applications (monolithic and microservices), it will be used a strangler facade that will connect both parts. The monolithic will shrink over time, while the microservice application will grow. |
| Decompose the microservices by DDD subdomain/aggregate       | One common practice to identify microservices is to divide the DDD entities into seperate services. All of the functionalities and use cases associated to a specific entity will be transferred into a seperate service. |
| Use schema-per-service option                                | Instead of having a physical database per microservice, it will be adopted the schema-per-service solution to asssociate each service to a private database schema, using only one database. |
| Implement Service Discovery by using Netflix Eureka to orchestrate the Docker containers | Netflix Eureka will give each microservice it's own IP Address and a single DNS name to a set of containers in order to be able to load-balance across them. |
| Implement an API Gateway by using Apollo              | API Gateway with native support for GraphQL that will simplify the process of API Composition. It is also possible to make real-time notifications to notify the users of various things that are happening in the system (CRN-1). |

### Step 6: Sketch views and record design decisions:

The first diagram shows the various microservices that constitute the application, and how they communicate with each other:

![MicroserviceArchitecture.png](./diagrams/MicroserviceArchitecture.png)

The next diagram shows how the application structure diagram developed in the greenfield project will be adapted to microservices:

![WebApplication.png](./diagrams/WebApplication.png)

**Note:** The Server-Side part only represents a single microservice in the application. All other microservices should have a similar architecture.

The last diagram shows how the database per service pattern will be implemented, as one database will have a private schema associated to a seperate service:

![dockerDeployment](./diagrams/Deployment.png)

**Note:** Even though the previous diagram does not represent the deployment aspect in Docker containers, each node can be represented as a container.

### Step 7: Analyse current design and review iteration goal(s):

#### Current state of the Kanban Board

| Not Addressed | Partialy Addressed | Fully Addressed |
| ------------- | ------------------ | --------------- |
|               | QA-1               |                 |
|               |                    | QA-2            |
|               | QA-3               |                 |
|               |                    | QA-5            |
|               |                    | QA-6            |
|               | QA-7               |                 |
|               |                    | CON-1           |
|               |                    | CON-2           |
|               |                    | CON-3           |
|               |                    | CON-4           |
|               |                    | CON-5           |
|               |                    | CON-8           |
|               |                    | CON-9           |
|               |                    | CRN-1           |
