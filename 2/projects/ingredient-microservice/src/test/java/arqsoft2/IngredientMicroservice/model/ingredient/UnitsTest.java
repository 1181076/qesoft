package arqsoft2.IngredientMicroservice.model.ingredient;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UnitsTest {
    @Test
    void verifyIfUnitsIsEmpty(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Units(""); });
        assertEquals("The unit cannot be empty!", e.getMessage());
    }

    @Test
    void verifyIfUnitsIsCorrect(){
        new Description("grams");
        assertTrue(true);
    }
}