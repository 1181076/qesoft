package arqsoft2.IngredientMicroservice.model.ingredient;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class QuantityTest {
    @Test
    void verifyIfQuantityIsNegative(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Quantity(-21); });
        assertEquals("The quantity cannot be negative!", e.getMessage());
    }

    @Test
    void verifyIfQuantityIsCorrect(){
        new Quantity(21);
        assertTrue(true);
    }
}