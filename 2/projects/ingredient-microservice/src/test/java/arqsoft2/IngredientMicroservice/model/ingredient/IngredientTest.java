package arqsoft2.IngredientMicroservice.model.ingredient;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class IngredientTest {
    private static IngredientID ingredientId;
    private static Description description;
    private static Quantity quantity;
    private static List<LastUpdatedDate> lastUpdatedDate;
    private static UpperLimit upperLimit;
    private static LowerLimit lowerLimit;
    private static Units units;

    @BeforeAll
    static void beforeAll(){
        ingredientId = new IngredientID("ing1");
        description = new Description("This is a description!");
        quantity = new Quantity(20);
        LocalDate localDate = LocalDate.parse("2020-02-14");
        Date date = Date.valueOf(localDate);
        List<LastUpdatedDate> lastUpdatedDate = new ArrayList<>();
        lastUpdatedDate.add(new LastUpdatedDate(date, 20));
        upperLimit = new UpperLimit(30);
        lowerLimit = new LowerLimit(10);
        units = new Units("meters");
    }

    @Test
    void verifyIfIngredientQuantityIsAboveUpperLimit(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Ingredient(ingredientId, description, new Quantity(40), lastUpdatedDate, upperLimit, lowerLimit, units); });
        assertEquals("The quantity must be between the lower and upper limit!", e.getMessage());
    }

    @Test
    void verifyIfIngredientQuantityIsBelowLowerLimit(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Ingredient(ingredientId, description, new Quantity(1), lastUpdatedDate, upperLimit, lowerLimit, units); });
        assertEquals("The quantity must be between the lower and upper limit!", e.getMessage());
    }

    @Test
    void verifyIfIngredientLowerLimitIsAboveUpperLimit(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Ingredient(ingredientId, description, quantity, lastUpdatedDate, upperLimit, new LowerLimit(9000), units); });
        assertEquals("The upper limit cannot be below the lower limit!", e.getMessage());
    }

    @Test
    void verifyIfIngredientIsCorrect(){
        new Ingredient(ingredientId, description, quantity, lastUpdatedDate, upperLimit, lowerLimit, units);
        assertTrue(true);
    }
}