package arqsoft2.IngredientMicroservice.model.ingredient;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class IngredientIDTest {
    @Test
    void verifyIfIDisEmpty(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new IngredientID(""); });
        assertEquals("The ID cannot be empty!", e.getMessage());
    }

    @Test
    void verifyIfIDIsCorrect(){
        new IngredientID("ID1");
        assertTrue(true);
    }
}