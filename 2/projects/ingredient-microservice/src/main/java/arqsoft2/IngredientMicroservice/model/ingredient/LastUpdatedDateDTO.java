package arqsoft2.IngredientMicroservice.model.ingredient;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class LastUpdatedDateDTO {
    private final int amount;
    private final String date;

    public LastUpdatedDateDTO(@JsonProperty("amount") int amount, @JsonProperty("date") String date){
        this.date= date;
        this.amount= amount;
    }

    public int getAmount() {
        return amount;
    }

    public String getDate() {
        return date;
    }
}

