package arqsoft2.IngredientMicroservice.model.ingredient;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import eapli.framework.representations.dto.DTOable;
import io.swagger.v3.oas.annotations.media.Schema;

@Entity
public class Ingredient implements IngredientInterface, DTOable<InputIngredientDTO>{
	
    @EmbeddedId
	@Schema(name = "id")
    private IngredientID id;

	@Schema(name = "description")
    private Description description;
	@Schema(name = "quantity")
    private Quantity quantity;

	@ElementCollection
	@Schema(name = "lastUpdatedDate")
    private List<LastUpdatedDate> lastUpdatedDate;
	@Schema(name = "upperLimit")
    private UpperLimit upperLimit;
	@Schema(name = "lowerLimit")
    private LowerLimit lowerLimit;
	@Schema(name = "units")
    private Units units;

    public Ingredient(){

    }

    public Ingredient(IngredientID id, Description description, Quantity quantity, List<LastUpdatedDate> lastUpdatedDate, UpperLimit upperLimit, LowerLimit lowerLimit, Units units){
        if(upperLimit.getValue() < lowerLimit.getValue()){
        	throw new IllegalArgumentException("The upper limit cannot be below the lower limit!");
		}

        if(quantity.getValue() < lowerLimit.getValue() || quantity.getValue() > upperLimit.getValue()){
        	throw new IllegalArgumentException("The quantity must be between the lower and upper limit!");
		}

    	this.id = id;
        this.description = description;
        this.quantity = quantity;
        this.lastUpdatedDate = lastUpdatedDate;
        this.upperLimit = upperLimit;
        this.lowerLimit = lowerLimit;
        this.units = units;
    }

	public IngredientID getId() {
		return this.id;
	}

	public Description getDescription() {
		return this.description;
	}

	public Quantity getQuantity() {
		return this.quantity;
	}

	public List<LastUpdatedDate> getlastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public UpperLimit getUpperLimit() {
		return this.upperLimit;
	}

	public LowerLimit getLowerLimit() {
		return this.lowerLimit;
	}

	public Units getUnits() {
		return this.units;
	}

    @Override
    public InputIngredientDTO toDTO() {
        return new InputIngredientDTO(id, description, quantity, lastUpdatedDate, upperLimit, lowerLimit, units);
	}

    @Override
    public ReturnIngredientDTO toReturnDTO() {
    	List<LastUpdatedDateDTO> lastUpdatedDateDTOs = new ArrayList<>();
		for (LastUpdatedDate lud: lastUpdatedDate) {
			lastUpdatedDateDTOs.add(lud.toDTO());
		}

        return new ReturnIngredientDTO(id.getValue(), description.getValue(), quantity.getValue(), lastUpdatedDateDTOs, upperLimit.getValue(), lowerLimit.getValue(), units.getValue());
	}

	public void update(Ingredient ing){
		this.description = ing.description;
		this.quantity = ing.quantity;
		this.lastUpdatedDate = ing.lastUpdatedDate;
		this.upperLimit = ing.upperLimit;
		this.lowerLimit = ing.lowerLimit;
		this.units = ing.units;
	} 


}
