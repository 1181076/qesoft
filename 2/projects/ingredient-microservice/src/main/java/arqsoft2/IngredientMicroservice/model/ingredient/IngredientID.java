package arqsoft2.IngredientMicroservice.model.ingredient;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;

public final class IngredientID implements ValueObject{

    private final String value;

    public final String getValue() {
		return this.value;
	}

    public IngredientID(){
        value = "default";
    }

    @JsonCreator
    public IngredientID(@JsonProperty("value") String value){
        if(value.isEmpty()){
            throw new IllegalArgumentException("The ID cannot be empty!");
        }
        this.value= value;
    }
    
    public String value(){
        return value;
    }

    @Override
    public boolean equals(Object o){

        IngredientID ingredientId = (IngredientID) o;

        return ingredientId.value().equals(this.value);

    }

}
