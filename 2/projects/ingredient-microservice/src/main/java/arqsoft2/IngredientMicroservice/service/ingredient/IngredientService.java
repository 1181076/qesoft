package arqsoft2.IngredientMicroservice.service.ingredient;

import java.util.ArrayList;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import arqsoft2.IngredientMicroservice.model.ingredient.IngredientID;
import arqsoft2.IngredientMicroservice.model.ingredient.Ingredient;
import arqsoft2.IngredientMicroservice.model.ingredient.InputIngredientDTO;
import arqsoft2.IngredientMicroservice.model.ingredient.ReturnIngredientDTO;
import arqsoft2.IngredientMicroservice.repository.ingredient.IngredientRepository;
import arqsoft2.IngredientMicroservice.repository.ingredient.IngredientRepositoryImpl;



@Component
public class IngredientService {

	@Autowired
	IngredientRepository ingredientRepository;
	
	@Autowired
	IngredientRepositoryImpl ingredientRepositoryImpl;
	

	public IngredientService(IngredientRepository ingredientRepository, IngredientRepositoryImpl ingredientRepositoryImpl){
		this.ingredientRepository = ingredientRepository;
		this.ingredientRepositoryImpl = ingredientRepositoryImpl;
	}

	public List<ReturnIngredientDTO> getAllIngredients() {
		try {
			List<Ingredient> ingredients = ingredientRepositoryImpl.findAllIngredients();
			
			List<ReturnIngredientDTO> ret = new ArrayList<>();

			
			ingredients.forEach(e -> ret.add(e.toReturnDTO()));

			return ret;
		} catch (Exception e) {
			return new ArrayList<>();
		}
	}

	public ReturnIngredientDTO getIngredientById(@PathVariable("id") String id) {
		// VERSION WITH REPOSITORYIMPL 
        Ingredient ingredientData = ingredientRepositoryImpl.findIngredientById(id);

        if (ingredientData!=null) {
            return ingredientData.toReturnDTO();
        } else {
            return null;
        }
	}

	public InputIngredientDTO createIngredient(@RequestBody Ingredient ingredient) {
		
		try {
			Ingredient _ingredient = ingredientRepository
					.save(ingredient);
						
			return _ingredient.toDTO();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}

	public InputIngredientDTO updateIngredient(@PathVariable("id") IngredientID ingredientId, @RequestBody Ingredient ingredient) {
		Optional<Ingredient> ingredientData = ingredientRepository.findById(ingredientId);
		
		if (ingredientData.isPresent()) {
			Ingredient _ingredient = ingredientData.get();
			_ingredient.update(ingredient);
			
			return ingredientRepository.save(_ingredient).toDTO();
		} else {
			return null;
		}
	}

	public HttpStatus deleteIngredient(IngredientID id) {
		try {
			ingredientRepository.deleteById(id);
			return HttpStatus.OK;
		} catch (Exception e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
	}

	public HttpStatus deleteAllIngredients() {
		try {
			ingredientRepository.deleteAll();
			return HttpStatus.OK;
		} catch (Exception e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}

	}

	public int getStockChange(String id, Date startDate, Date endDate) {
        try {
       
            int closestStartDateStock = ingredientRepositoryImpl.getClosestDateStock(id, startDate);
            int closestEndDateStock = ingredientRepositoryImpl.getClosestDateStock(id, endDate);

            return closestEndDateStock - closestStartDateStock;

        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
    }

}