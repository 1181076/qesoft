package arqsoft2.IngredientMicroservice.model.ingredient;

import eapli.framework.domain.model.ValueObject;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public final class Quantity implements ValueObject {

    private final int value;

    @JsonCreator
    public Quantity(@JsonProperty("value")int value){
        if(value < 0){
            throw new IllegalArgumentException("The quantity cannot be negative!");
        }
        this.value= value;
    }
    
    public int value(){
        return value;
    }

    public Quantity add(final Quantity addend) {                   
        return new Quantity(value + addend.value);
     }

     public final int getValue() {
		return this.value;
	}
}
