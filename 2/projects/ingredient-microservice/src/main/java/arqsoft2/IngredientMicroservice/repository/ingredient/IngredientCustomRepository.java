package arqsoft2.IngredientMicroservice.repository.ingredient;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import arqsoft2.IngredientMicroservice.model.ingredient.*;

public interface IngredientCustomRepository {

    public Ingredient findIngredientById(String Id);
    
    public List<Ingredient> findAllIngredients();
    
    public int getClosestDateStock(String id, Date date);

    
}
