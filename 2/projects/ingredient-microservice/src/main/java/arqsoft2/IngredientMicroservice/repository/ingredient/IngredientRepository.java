package arqsoft2.IngredientMicroservice.repository.ingredient;

import org.springframework.data.jpa.repository.JpaRepository;

import arqsoft2.IngredientMicroservice.model.ingredient.IngredientID;
import arqsoft2.IngredientMicroservice.model.ingredient.Ingredient;

public interface IngredientRepository extends JpaRepository<Ingredient, IngredientID>, IngredientCustomRepository {

}
