package arqsoft2.IngredientMicroservice.model.ingredient;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class InputIngredientDTO {

	@Schema(name = "id")
    private IngredientID id;
	@Schema(name = "description")
    private Description description;
	@Schema(name = "quantity")
    private Quantity quantity;
	@Schema(name = "lastUpdatedDate")
    private List<LastUpdatedDate> lastUpdatedDate;
	@Schema(name = "upperLimit")
    private UpperLimit upperLimit;
	@Schema(name = "lowerLimit")
    private LowerLimit lowerLimit;
	@Schema(name = "units")
    private Units units;

    public InputIngredientDTO(){

    }

    public InputIngredientDTO(IngredientID id, Description description, Quantity quantity, List<LastUpdatedDate> lastUpdatedDate, UpperLimit upperLimit, LowerLimit lowerLimit, Units units){
        this.id = id;
        this.description = description;
        this.quantity = quantity;
        this.lastUpdatedDate = lastUpdatedDate;
        this.upperLimit = upperLimit;
        this.lowerLimit = lowerLimit;
        this.units = units;
    }

    public IngredientID getId() {
		return this.id;
	}

	public Description getDescription() {
		return this.description;
	}

	public void setDescription(Description description) {
		this.description = description;
	}

	public Quantity getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Quantity quantity) {
		this.quantity = quantity;
	}

	public List<LastUpdatedDate> getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(List<LastUpdatedDate> lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public UpperLimit getUpperLimit() {
		return this.upperLimit;
	}

	public void setUpperLimit(UpperLimit upperLimit) {
		this.upperLimit = upperLimit;
	}

	public LowerLimit getLowerLimit() {
		return this.lowerLimit;
	}

	public void setLowerLimit(LowerLimit lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	public Units getUnits() {
		return this.units;
	}

	public void setUnits(Units units) {
		this.units = units;
	}

    @Override
    public boolean equals(Object o) {
  
        return true; 
          
    }
}
