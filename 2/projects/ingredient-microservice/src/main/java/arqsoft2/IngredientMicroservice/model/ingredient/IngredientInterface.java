package arqsoft2.IngredientMicroservice.model.ingredient;

import java.util.List;

public interface IngredientInterface{
	
	public IngredientID getId();
	public Description getDescription();
	public Quantity getQuantity();
	public List<LastUpdatedDate> getlastUpdatedDate();
	public UpperLimit getUpperLimit();
	public LowerLimit getLowerLimit();
	public Units getUnits();

    public InputIngredientDTO toDTO();
    public ReturnIngredientDTO toReturnDTO();

	public void update(Ingredient ing);
}
