package arqsoft2.IngredientMicroservice.controller.ingredient;

import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import arqsoft2.IngredientMicroservice.model.ingredient.IngredientID;
import arqsoft2.IngredientMicroservice.model.ingredient.Ingredient;
import arqsoft2.IngredientMicroservice.model.ingredient.InputIngredientDTO;
import arqsoft2.IngredientMicroservice.service.ingredient.IngredientService;
import arqsoft2.IngredientMicroservice.model.ingredient.ReturnIngredientDTO;


@CrossOrigin(origins = "http://localhost:8088")
@RestController
@Tag(name = "Ingredient Controller", description = "All ingredient operations")
@RequestMapping("/api")
public class IngredientController {

	@Autowired
	IngredientService ingredientService;


	public IngredientController(IngredientService ingredientService){
		this.ingredientService = ingredientService;
	}

	@GetMapping("/ingredients")
	@Operation(summary = "Get all ingredients", operationId = "getIngredients", description = "Get all registered ingredients")
	public ResponseEntity<List<ReturnIngredientDTO>> getAllIngredients() {
		try {
			List<ReturnIngredientDTO> ingredients = ingredientService.getAllIngredients();
			return new ResponseEntity<>(ingredients, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/ingredient")
	@Operation(summary = "Get a specific ingredient", operationId = "getIngredient", description = "Get a specific ingredient by providing its identifier")
	public ResponseEntity<ReturnIngredientDTO> getIngredientById(@RequestParam(name = "id") String id) {
		ReturnIngredientDTO ingredientData = ingredientService.getIngredientById(id);
		if (ingredientData != null) {
			return new ResponseEntity<>(ingredientData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/ingredients/stockChange/{id}/{startDate}/{endDate}")
	@Operation(summary = "Get a specific ingredient stock change", operationId = "getIngredientStockChange", description = "Get a specific ingredient stock change by providing its identifier")
    public ResponseEntity<Integer> getStockChange(@PathVariable("id") String id, @PathVariable("startDate") Date startDate, @PathVariable("endDate") Date endDate) {
        try {
            int change = ingredientService.getStockChange(id, startDate, endDate);

            return new ResponseEntity<>(change, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

	@PostMapping("/ingredients")
	@Operation(summary = "Create an ingredient", operationId = "createIngredient", description = "Create an ingredient by providing its information")
	public ResponseEntity<InputIngredientDTO> createIngredient(@RequestBody Ingredient ingredient) {

		try {
			InputIngredientDTO _ingredient = ingredientService
						.createIngredient(ingredient);
			return new ResponseEntity<>(_ingredient, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/ingredients/{id}")
	@Operation(summary = "Update a specific ingredient", operationId = "updateIngredient", description = "Update a specific ingredient by providing its identifier")
	public ResponseEntity<InputIngredientDTO> updateIngredient(@PathVariable("id") String id, @RequestBody Ingredient ingredient) throws ParseException {
		InputIngredientDTO ingredientData = ingredientService.updateIngredient(new IngredientID(id), ingredient);

		if (ingredientData != null) {
			return new ResponseEntity<>(ingredientData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/ingredients/{id}")
	@Operation(summary = "Delete a specific ingredient", operationId = "deleteIngredient", description = "Delete a specific ingredient by providing its identifier")
	public ResponseEntity<Integer> deleteIngredient(@PathVariable("id") String id) {
			HttpStatus st = ingredientService.deleteIngredient(new IngredientID(id));
			if(st.equals(HttpStatus.INTERNAL_SERVER_ERROR)){
				return new ResponseEntity<>(0, st);
			} else {
				return new ResponseEntity<>(1, st);
			}
	}

	@DeleteMapping("/ingredients")
	@Operation(summary = "Delete all ingredients", operationId = "deleteIngredients", description = "Delete all registered ingredients")
	public ResponseEntity<Integer> deleteAllIngredients() {
			HttpStatus st = ingredientService.deleteAllIngredients();
		if(st.equals(HttpStatus.INTERNAL_SERVER_ERROR)){
			return new ResponseEntity<>(0, st);
		} else {
			return new ResponseEntity<>(1, st);
		}
		
	}
    
}
