package arqsoft2.IngredientMicroservice.model.ingredient;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class ReturnIngredientDTO {

	@Schema(name = "id")
    private String id;
	@Schema(name = "description")
    private String description;
	@Schema(name = "quantity")
    private int quantity;
	@Schema(name = "lastUpdatedDate")
    private List<LastUpdatedDateDTO> lastUpdatedDate;
	@Schema(name = "upperLimit")
    private int upperLimit;
	@Schema(name = "lowerLimit")
    private int lowerLimit;
	@Schema(name = "units")
    private String units;
    
    public ReturnIngredientDTO(){

    }

    public ReturnIngredientDTO(String id, String description, int quantity, List<LastUpdatedDateDTO> lastUpdatedDate, int upperLimit, int lowerLimit, String units){
        this.id = id;
        this.description = description;
        this.quantity = quantity;
        this.lastUpdatedDate = lastUpdatedDate;
        this.upperLimit = upperLimit;
        this.lowerLimit = lowerLimit;
        this.units = units;
    }

    public String getId() {
		return this.id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public List<LastUpdatedDateDTO> getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(List<LastUpdatedDateDTO> lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public int getUpperLimit() {
		return this.upperLimit;
	}

	public void setUpperLimit(int upperLimit) {
		this.upperLimit = upperLimit;
	}

	public int getLowerLimit() {
		return this.lowerLimit;
	}

	public void setLowerLimit(int lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	public String getUnits() {
		return this.units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

    @Override
    public boolean equals(Object o) {
  
        return true; 
          
    }
}
