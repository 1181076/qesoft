package arqsoft2.IngredientMicroservice.model.ingredient;

import eapli.framework.domain.model.ValueObject;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public final class UpperLimit implements ValueObject {

    private final int value;

    @JsonCreator
    public UpperLimit(@JsonProperty("value")int value){
        if(value < 0){
            throw new IllegalArgumentException("The upper limit quantity cannot be negative!");
        }
        this.value= value;
    }
    
    public int value(){
        return value;
    }


    public final int getValue() {
		return this.value;
	}
}
