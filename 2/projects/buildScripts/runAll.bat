
@echo off

start servicediscovery.bat
start ingredientMicroservice.bat
start sandwichMicroservice.bat
start supplierMicroservice.bat
start userMicroservice.bat
start schoolMicroservice.bat
start UI.bat

TIMEOUT /T 60

start api-gateway.bat