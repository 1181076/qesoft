package arqsoft2.SchoolMicroservice.model.school;

import java.util.List;

import arqsoft2.SchoolMicroservice.model.sandwich.ReturnSandwichDTO;
import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class ReturnSchoolDTO {
    private String internalID;
    private String id;
    private String name;
    private String address;
    private List<ReturnSandwichDTO> sandwiches;

    public ReturnSchoolDTO() {

    }

    public ReturnSchoolDTO(@JsonProperty("id") String id, @JsonProperty("internalID") String internalID, @JsonProperty("name") String name, @JsonProperty("address") String address, @JsonProperty("sandwiches") List<ReturnSandwichDTO> sandwiches) {
        this.id = id;
        this.internalID = internalID;
        this.name = name;
        this.address = address;
        this.sandwiches = sandwiches;
    }

    public String getId() {
        return this.id;
    }

    public String getInternalID() {
        return this.internalID;
    }

    public String getName() {
        return this.name;
    }

    public String getAddress() {
        return this.address;
    }

    public List<ReturnSandwichDTO> getSandwiches() {return this.sandwiches;}

    @Override
    public boolean equals(Object o) {

        return true;

    }
}