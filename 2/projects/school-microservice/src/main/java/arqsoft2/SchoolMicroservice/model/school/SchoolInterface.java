package arqsoft2.SchoolMicroservice.model.school;

import arqsoft2.SchoolMicroservice.model.sandwich.SandwichID;

import java.util.List;

public interface SchoolInterface {

    public ExternalID getId();
    public InternalID getInternalID();
    public Name getName();
    public Address getAddress();    
    public List<SandwichID> getSandwiches();

    public void update(School school);

    public InputSchoolDTO toDTO();

    public void registerSandwiches(List<SandwichID> sandwiches);
}