package arqsoft2.SchoolMicroservice.service.school;

import arqsoft2.SchoolMicroservice.model.sandwich.ReturnSandwichDTO;
import arqsoft2.SchoolMicroservice.model.sandwich.SandwichID;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class SandwichMicroserviceCommunication {
    @Autowired
    static EurekaClient eurekaClient;

    public SandwichMicroserviceCommunication(EurekaClient ec) {
        eurekaClient = ec;
    }

    public static ReturnSandwichDTO getSandwich(SandwichID sandwichID){
        Application application = eurekaClient.getApplication("Sandwich_Microservice");
        InstanceInfo instanceInfo = application.getInstances().get(0);
        String url = "http://" + instanceInfo.getIPAddr() + ":" + instanceInfo.getPort() + "/api/sandwiches/" + sandwichID.getValue();
        ReturnSandwichDTO dto = new RestTemplate().getForObject(url, ReturnSandwichDTO.class);
        return dto;
    }
}
