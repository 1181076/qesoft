package arqsoft2.SchoolMicroservice.model.sandwich;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class InputCategoryDTO {

    private Description description;
    
    public InputCategoryDTO(){

    }

    public InputCategoryDTO(Description description){
        this.description = description;
    }

	public Description getDescription() {
		return this.description;
	}

	public void setDescription(Description description) {
		this.description = description;
	}


    @Override
    public boolean equals(Object o) {
  
        return true; 
          
    }
}
