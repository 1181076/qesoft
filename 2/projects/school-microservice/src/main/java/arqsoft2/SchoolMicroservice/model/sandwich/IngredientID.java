package arqsoft2.SchoolMicroservice.model.sandwich;

import eapli.framework.domain.model.ValueObject;

public class IngredientID implements ValueObject {
    private final String value;

    public IngredientID(){
        value = "default";
    }

    public final String getValue() {
        return this.value;
    }

    public IngredientID(String value) {
        if(value.isEmpty()){
            throw new IllegalArgumentException("The ID cannot be empty!");
        }
        this.value = value;
    }
}
