package arqsoft2.SchoolMicroservice.controller.school;

import java.text.ParseException;
import java.util.List;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import arqsoft2.SchoolMicroservice.model.school.*;
import arqsoft2.SchoolMicroservice.service.school.SchoolService;
import arqsoft2.SchoolMicroservice.model.sandwich.*;


@CrossOrigin(origins = "http://localhost:8088")
@RestController
@Tag(name = "School Controller", description = "All school operations")
@RequestMapping("/api")
public class SchoolController {

	@Autowired
	SchoolService schoolService;


	public SchoolController(SchoolService schoolService) {
        this.schoolService = schoolService;
    }

	@GetMapping("/schools")
	@Operation(summary = "Get all schools", operationId = "getSchools", description = "Get all registered schools")
	public ResponseEntity<List<ReturnSchoolDTO>> getAllSchools() {
		try {
			List<ReturnSchoolDTO> schools = schoolService.getAllSchools();
			return new ResponseEntity<>(schools, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/schools/{id}")
	@Operation(summary = "Get a specific school", operationId = "getSchool", description = "Get a specific school by providing its external identifier")
	public ResponseEntity<ReturnSchoolDTO> getSchoolById(@PathVariable("id") String id) {
		ReturnSchoolDTO schoolData = schoolService.getSchoolById(id);
		if (schoolData != null) {
			return new ResponseEntity<>(schoolData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/schools")
	@Operation(summary = "Create a school", operationId = "createSchool", description = "Create a school by providing its information")
	public ResponseEntity<InputSchoolDTO> createSchool(@RequestBody School school) {

		try {
			InputSchoolDTO _school = schoolService.createSchool(school);
			return new ResponseEntity<>(_school, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/schools/{id}")
	@Operation(summary = "Update a specific school", operationId = "updateSchool", description = "Update a specific school by providing its identifier")
	public ResponseEntity<InputSchoolDTO> updateSchool(@PathVariable("id") String id, @RequestBody School school) throws ParseException {
		InputSchoolDTO schoolData = schoolService.updateSchool(new ExternalID(id), school);

		if (schoolData != null) {
			return new ResponseEntity<>(schoolData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/schools/{id}")
	@Operation(summary = "Delete a specific school", operationId = "deleteSchool", description = "Delete a specific school by providing its identifier")
	public ResponseEntity<Integer> deleteSchool(@PathVariable("id") String id) {
			HttpStatus st = schoolService.deleteSchool(new ExternalID(id));
			if(st.equals(HttpStatus.INTERNAL_SERVER_ERROR)){
				return new ResponseEntity<>(0, st);
			} else {
				return new ResponseEntity<>(1, st);
			}
	}

	@DeleteMapping("/schools")
	@Operation(summary = "Delete all schools", operationId = "deleteSchools", description = "Delete all registered schools")
	public ResponseEntity<HttpStatus> deleteAllSchools() {
			HttpStatus st = schoolService.deleteAllSchools();
			return new ResponseEntity<>(st);
		
	}
    
	@PostMapping("/schools/{schoolID}/registerSandwiches")
	@Operation(summary = "Associate sandwiches to a school", operationId = "registerSandwiches", description = "Associate sandwiches to a school")
    public ResponseEntity<InputSchoolDTO> registerSandwiches(@PathVariable("schoolID") String schoolID, @RequestBody List<SandwichID> sandwiches) {
        try {
            InputSchoolDTO _school = schoolService.registerSandwiches(new ExternalID(schoolID), sandwiches);
            return new ResponseEntity<>(_school, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
