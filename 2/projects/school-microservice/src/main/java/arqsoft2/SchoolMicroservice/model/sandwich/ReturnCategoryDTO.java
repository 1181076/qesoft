package arqsoft2.SchoolMicroservice.model.sandwich;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class ReturnCategoryDTO {

    private String description;
    
    public ReturnCategoryDTO(){

    }

    public ReturnCategoryDTO(String description){
        this.description = description;
    }

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


    @Override
    public boolean equals(Object o) {
  
        return true; 
          
    }
}
