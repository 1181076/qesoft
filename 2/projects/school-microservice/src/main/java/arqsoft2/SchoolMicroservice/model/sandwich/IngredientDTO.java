package arqsoft2.SchoolMicroservice.model.sandwich;

import java.util.List;

public class IngredientDTO {

    private String id;
    private String description;
    private int quantity;
    private List<LastUpdatedDate> lastUpdatedDate;
    private int upperLimit;
    private int lowerLimit;
    private String units;

    public IngredientDTO(){

    }

    public IngredientDTO(String id, String description, int quantity, List<LastUpdatedDate> lastUpdatedDate, int upperLimit, int lowerLimit, String units){
        this.id = id;
        this.description = description;
        this.quantity = quantity;
        this.lastUpdatedDate = lastUpdatedDate;
        this.upperLimit = upperLimit;
        this.lowerLimit = lowerLimit;
        this.units = units;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public int getQuantity() {
        return quantity;
    }

    public List<LastUpdatedDate> getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public int getUpperLimit() {
        return upperLimit;
    }

    public int getLowerLimit() {
        return lowerLimit;
    }

    public String getUnits() {
        return units;
    }
}
