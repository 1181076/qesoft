package arqsoft2.SchoolMicroservice.repository.school;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import arqsoft2.SchoolMicroservice.model.sandwich.*;
import arqsoft2.SchoolMicroservice.model.school.School;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SchoolRepositoryImpl implements SchoolCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<School> findAllSchools(){

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<School> cq = cb.createQuery(School.class);
        Root<School> root = cq.from(School.class);
        
        cq.select(root);

        TypedQuery<School> q = em.createQuery(cq);

        return q.getResultList();

    }

    @Override
    public School findSchoolById(String id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<School> cq = cb.createQuery(School.class);
        Root<School> root = cq.from(School.class);
        Path<String> idValue = root.get("id").get("value");
        
        cq.select(root).where(cb.equal(idValue, id));

        TypedQuery<School> q = em.createQuery(cq);

        return q.getSingleResult();
    }



}
