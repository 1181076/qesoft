package arqsoft2.SchoolMicroservice.repository.school;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import arqsoft2.SchoolMicroservice.*;
import arqsoft2.SchoolMicroservice.model.school.School;

public interface SchoolCustomRepository {

    public School findSchoolById(String Id);
    
    public List<School> findAllSchools();
    
}
