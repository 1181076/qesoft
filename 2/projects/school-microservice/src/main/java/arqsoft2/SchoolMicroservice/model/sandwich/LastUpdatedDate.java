package arqsoft2.SchoolMicroservice.model.sandwich;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Date;

public final class LastUpdatedDate {
    private final int amount;
    private final String date;

    public LastUpdatedDate(@JsonProperty("date") String date, @JsonProperty("amount") int amount){
        this.date= date;
        this.amount= amount;
    }

    public int getAmount() {
        return amount;
    }

    public String getDate() {
        return date;
    }
}

