package arqsoft2.SchoolMicroservice.model.school;

import java.util.List;

import arqsoft2.SchoolMicroservice.model.sandwich.SandwichID;
import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class InputSchoolDTO {
    private InternalID internalID;
    private ExternalID id;
    private Name name;
    private Address address;
    private List<SandwichID> sandwiches;

    public InputSchoolDTO() {

    }

    public InputSchoolDTO(@JsonProperty("id") ExternalID id, @JsonProperty("internalID") InternalID internalID, @JsonProperty("name") Name name, @JsonProperty("address") Address address, @JsonProperty("sandwiches") List<SandwichID> sandwiches) {
        this.id = id;
        this.internalID = internalID;
        this.name = name;
        this.address = address;
        this.sandwiches = sandwiches;
    }

    public ExternalID getId() {
        return this.id;
    }

    public InternalID getInternalID() {
        return this.internalID;
    }

    public Name getName() {
        return this.name;
    }

    public Address getAddress() {
        return this.address;
    }

    public List<SandwichID> getSandwiches() {return this.sandwiches;}

    @Override
    public boolean equals(Object o) {

        return true;

    }
}