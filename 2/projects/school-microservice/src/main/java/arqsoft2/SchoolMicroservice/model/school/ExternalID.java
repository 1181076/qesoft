package arqsoft2.SchoolMicroservice.model.school;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;

public class ExternalID implements ValueObject {
    private final String value;


    public ExternalID(){
        value = "default";
    }

    @JsonCreator
    public ExternalID(@JsonProperty("value") String value) {
        if(value.isEmpty()){
            throw new IllegalArgumentException("The external ID cannot be empty!");
        }

        if(value.trim().equals("")){
            throw new IllegalArgumentException("The external ID cannot contain only spaces!");
        }

        this.value = value;
    }

    public final String getValue() {
        return this.value;
    }

    public String value() {
        return value;
    }
}