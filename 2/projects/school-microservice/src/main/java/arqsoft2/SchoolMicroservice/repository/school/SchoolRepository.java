package arqsoft2.SchoolMicroservice.repository.school;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import arqsoft2.SchoolMicroservice.model.school.School;
import arqsoft2.SchoolMicroservice.model.school.ExternalID;

public interface SchoolRepository extends JpaRepository<School, ExternalID>, SchoolCustomRepository {

}
