package arqsoft2.SchoolMicroservice.model.sandwich;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;

public final class Price implements ValueObject{

    private double amount;

    private String currency;

    private Price(){
        amount = 0;
        currency = "";
    }


    @JsonCreator
    public Price(@JsonProperty("amount")double amount, @JsonProperty("currency")String currency){
        if(amount < 0){
            throw new IllegalArgumentException("The price cannot be negative!");
        }
        if(currency.isEmpty()){
            throw new IllegalArgumentException("The currency must not be empty!");
        }
        this.amount = amount;
        this.currency = currency;
    }

    public double amount(){
        return amount;
    }

    public final double getAmount() {
        return this.amount;
    }

    public String currency(){
        return currency;
    }

    public final String getCurrency() {
        return this.currency;
    }

}
