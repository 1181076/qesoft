package arqsoft2.SchoolMicroservice.service.school;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import arqsoft2.SchoolMicroservice.model.sandwich.ReturnSandwichDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import arqsoft2.SchoolMicroservice.model.school.ExternalID;
import arqsoft2.SchoolMicroservice.model.school.ReturnSchoolDTO;
import arqsoft2.SchoolMicroservice.model.school.School;
import arqsoft2.SchoolMicroservice.model.school.InputSchoolDTO;
import arqsoft2.SchoolMicroservice.model.sandwich.SandwichID;

import arqsoft2.SchoolMicroservice.repository.school.SchoolRepository;
import arqsoft2.SchoolMicroservice.repository.school.SchoolRepositoryImpl;

@Component
public class SchoolService {
    @Autowired
    SchoolRepository schoolRepository;

    @Autowired
    SchoolRepositoryImpl schoolRepositoryImpl;

    public SchoolService(SchoolRepository schoolRepository, SchoolRepositoryImpl schoolRepositoryImpl) {
        this.schoolRepository = schoolRepository;
        this.schoolRepositoryImpl = schoolRepositoryImpl;
    }

    public List<ReturnSchoolDTO> getAllSchools() {
        try {
            List<School> schools = schoolRepository.findAll();

            List<ReturnSchoolDTO> ret = new ArrayList<>();

            schools.forEach(e -> {
                List<ReturnSandwichDTO> listReturnSandwichDTO = new ArrayList<>();
                for (SandwichID sandwichID : e.getSandwiches()) {
                    //Get Sandwich by id
                    ReturnSandwichDTO sandwichDTO = SandwichMicroserviceCommunication.getSandwich(sandwichID);
                    listReturnSandwichDTO.add(sandwichDTO);
                }

                ret.add(e.toReturnDTO(listReturnSandwichDTO));
            });



            return ret;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public ReturnSchoolDTO getSchoolById(@PathVariable("id") String externalId) {

        // VERSION WITH REPOSITORYIMPL 
        School schoolData = schoolRepositoryImpl.findSchoolById(externalId);

        List<ReturnSandwichDTO> listReturnSandwichDTO = new ArrayList<ReturnSandwichDTO>();

        for (SandwichID sandwichID : schoolData.getSandwiches()) {
            //Get Sandwich by id
            ReturnSandwichDTO sandwichDTO = SandwichMicroserviceCommunication.getSandwich(sandwichID);
            listReturnSandwichDTO.add(sandwichDTO);
        }
       
        if (schoolData!=null) {

            return schoolData.toReturnDTO(listReturnSandwichDTO);
        } 
        
        return null;
        
    }

    public InputSchoolDTO createSchool(@RequestBody School school) {

        try {
            School _school = schoolRepository.save(school);

            return _school.toDTO();
        } catch (Exception e) {
            return null;
        }
    }

    public InputSchoolDTO updateSchool(@PathVariable("id") ExternalID externalID, @RequestBody School school) {
        Optional<School> schoolData = schoolRepository.findById(externalID);

        if (schoolData.isPresent()) {
            School _school = schoolData.get();
            _school.update(school);

            return schoolRepository.save(_school).toDTO();
        } else {
            return null;
        }
    }

    public HttpStatus deleteSchool(@PathVariable("id") ExternalID externalId) {
        try {
            schoolRepository.deleteById(externalId);
            return HttpStatus.NO_CONTENT;
        } catch (Exception e) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

    public HttpStatus deleteAllSchools() {
        try {
            schoolRepository.deleteAll();
            return HttpStatus.NO_CONTENT;
        } catch (Exception e) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

    }

    public InputSchoolDTO registerSandwiches(ExternalID id, List<SandwichID> sandwiches) {
            Optional<School> schoolData = schoolRepository.findById(id);
            if (schoolData.isPresent()) {
                School _school = schoolData.get();
                _school.registerSandwiches(sandwiches);
                return schoolRepository.save(_school).toDTO();
            } else {
                return null;
            }

    }
}