package arqsoft2.SchoolMicroservice.model.school;

import javax.persistence.*;

import arqsoft2.SchoolMicroservice.model.sandwich.SandwichID;
import arqsoft2.SchoolMicroservice.model.sandwich.ReturnSandwichDTO;

import eapli.framework.representations.dto.DTOable;

import java.util.List;

@Entity
public class School implements SchoolInterface, DTOable<InputSchoolDTO> {

    @EmbeddedId
    private ExternalID id;

    private InternalID internalID;
    private Name name;
    private Address address;
        
	@ElementCollection
    private List<SandwichID> sandwiches;

    public School() {

    }

    public School(ExternalID id, InternalID internalID, Name name, Address address, List<SandwichID> sandwiches ) {
        this.id = id;
        this.internalID = internalID;
        this.name = name;
        this.address = address;
        this.sandwiches = sandwiches;
    }

    public ExternalID getId() {
        return this.id;
    }

    public InternalID getInternalID() {
        return this.internalID;
    }

    public Name getName() {
        return this.name;
    }

    public Address getAddress() {
        return this.address;
    }

    public List<SandwichID> getSandwiches() {
        return this.sandwiches;
    }

    public void update(School school) {
        this.internalID = school.internalID;
        this.name = school.name;
        this.address = school.address;
        this.sandwiches = school.sandwiches;
    }

    public InputSchoolDTO toDTO() {
        return new InputSchoolDTO(id, internalID, name, address, sandwiches);
    }

    public ReturnSchoolDTO toReturnDTO(List<ReturnSandwichDTO> sandwiches) {
        return new ReturnSchoolDTO(id.getValue(), internalID.getValue(), name.getValue(), address.getValue(), sandwiches);
    }

    public void registerSandwiches(List<SandwichID> sandwiches){
        for (SandwichID sandwichID: sandwiches) {
            if (!this.sandwiches.contains(sandwichID)) {
                this.sandwiches.add(sandwichID);
            }
        }
    }
}