package arqsoft2.SchoolMicroservice.model.school;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class AddressTest {
    @Test
    void verifyIfAddressIsEmpty(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Address(""); });
        assertEquals("The address cannot be empty!", e.getMessage());
    }

    @Test
    void verifyIfAddressOnlyHasSpaces(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Address("                "); });
        assertEquals("The address cannot contain only spaces!", e.getMessage());
    }

    @Test
    void verifyIfAddressIsCorrect(){
        new Address("Rua da Faculdade");
        assertTrue(true);
    }
}