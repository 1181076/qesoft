package arqsoft2.SchoolMicroservice.model.school;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class InternalIDTest {
    @Test
    void verifyIfInternalIDIsEmpty(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new InternalID(""); });
        assertEquals("The internal ID cannot be empty!", e.getMessage());
    }

    @Test
    void verifyIfInternalIDOnlyHasSpaces(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new InternalID("           "); });
        assertEquals("The internal ID cannot contain only spaces!", e.getMessage());
    }

    @Test
    void verifyIfInternalIDIsCorrect(){
        new InternalID("internalID1");
        assertTrue(true);
    }
}