package arqsoft2.SchoolMicroservice.model.school;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class NameTest {
    @Test
    void verifyIfNameIsEmpty(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Name(""); });
        assertEquals("The name cannot be empty!", e.getMessage());
    }

    @Test
    void verifyIfNameOnlyHasSpaces(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Name("             "); });
        assertEquals("The name cannot contain only spaces!", e.getMessage());
    }

    @Test
    void verifyIfNameDoesntStartWithALetter(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Name("2Escola"); });
        assertEquals("The name must start with a letter!", e.getMessage());
    }

    @Test
    void verifyIfNameIsCorrect(){
        new Name("Escola");
        assertTrue(true);
    }
}