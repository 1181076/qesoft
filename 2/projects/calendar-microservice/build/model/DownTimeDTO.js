"use strict";
var DownTimeDTO = /** @class */ (function () {
    function DownTimeDTO(id, reason, startPeriod, endPeriod) {
        this.id = id;
        this.startPeriod = startPeriod;
        this.reason = reason;
        this.endPeriod = endPeriod;
    }
    DownTimeDTO.prototype.getId = function () {
        return this.id;
    };
    ;
    DownTimeDTO.prototype.getStartPeriod = function () {
        return this.startPeriod;
    };
    DownTimeDTO.prototype.setStartPeriod = function (newStartPeriod) {
        this.startPeriod = newStartPeriod;
    };
    DownTimeDTO.prototype.getEndPeriod = function () {
        return this.endPeriod;
    };
    DownTimeDTO.prototype.setEndPeriod = function (newEndPeriod) {
        this.endPeriod = newEndPeriod;
    };
    DownTimeDTO.prototype.getReason = function () {
        return this.reason;
    };
    DownTimeDTO.prototype.setReason = function (newReason) {
        this.reason = newReason;
    };
    return DownTimeDTO;
}());
