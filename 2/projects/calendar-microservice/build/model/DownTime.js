"use strict";
var DownTime = /** @class */ (function () {
    function DownTime(id, reason, startPeriod, endPeriod) {
        this.id = id;
        this.startPeriod = startPeriod;
        this.reason = reason;
        this.endPeriod = endPeriod;
    }
    DownTime.prototype.getId = function () {
        return this.id;
    };
    ;
    DownTime.prototype.getStartPeriod = function () {
        return this.startPeriod;
    };
    DownTime.prototype.getEndPeriod = function () {
        return this.endPeriod;
    };
    DownTime.prototype.getReason = function () {
        return this.reason;
    };
    DownTime.prototype.update = function (downtime) {
        this.id = downtime.id;
        this.startPeriod = downtime.startPeriod;
        this.reason = downtime.reason;
        this.endPeriod = downtime.endPeriod;
    };
    DownTime.prototype.toDTO = function () {
        return new DownTimeDTO(this.id, this.reason, this.startPeriod, this.endPeriod);
    };
    return DownTime;
}());
