"use strict";
var Reason = /** @class */ (function () {
    function Reason(value) {
        this.value = value;
    }
    Reason.prototype.getValue = function () {
        return this.value;
    };
    Reason.prototype.equals = function (reason) {
        return (reason === this.value);
    };
    return Reason;
}());
