"use strict";
var ID = /** @class */ (function () {
    function ID(value) {
        this.value = value;
    }
    ID.prototype.getValue = function () {
        return this.value;
    };
    ID.prototype.equals = function (id) {
        return (id === this.value);
    };
    return ID;
}());
