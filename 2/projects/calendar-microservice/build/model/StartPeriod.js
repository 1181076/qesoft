"use strict";
var StartPeriod = /** @class */ (function () {
    function StartPeriod(value) {
        this.value = value;
    }
    StartPeriod.prototype.getValue = function () {
        return this.value;
    };
    StartPeriod.prototype.equals = function (date) {
        return (date.getTime() === this.value.getTime());
    };
    return StartPeriod;
}());
