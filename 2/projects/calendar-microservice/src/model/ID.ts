export class ID{

    private value: number;

    constructor(value: number){
        this.value = value;
    }

    public getValue():number{
        return this.value;
    }

    public equals(id: number):boolean{
        return (id === this.value);
    }
}