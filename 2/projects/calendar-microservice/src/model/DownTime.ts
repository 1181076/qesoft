import {ID} from "../model/ID";
import {Reason} from "../model/Reason";
import {StartPeriod} from "../model/StartPeriod";
import {EndPeriod} from "../model/EndPeriod";
import {DownTimeInterface} from "./DownTimeInterface";
import { DownTimeDTO } from "./DownTimeDTO";

export class DownTime implements DownTimeInterface{

    private id:ID;
    private startPeriod:StartPeriod;
    private reason:Reason;
    private endPeriod:EndPeriod;

    constructor(id: ID, reason:Reason, startPeriod:StartPeriod, endPeriod:EndPeriod){
        this.id = id;
        this.startPeriod = startPeriod;
        this.reason = reason;
        this.endPeriod = endPeriod;
    }

    public getId(): ID{
        return this.id;
    };
    
    public getStartPeriod(): StartPeriod{
        return this.startPeriod;
    }
    public getEndPeriod():EndPeriod{
        return this.endPeriod;
    }

    public getReason():Reason{
        return this.reason;
    }

    public update(downtime: DownTime):void{
        this.id = downtime.id;
        this.startPeriod = downtime.startPeriod;
        this.reason = downtime.reason;
        this.endPeriod = downtime.endPeriod;
    }

    public toDTO():DownTimeDTO {
        return new DownTimeDTO(this.id, this.reason, this.startPeriod, this.endPeriod);
    }

}