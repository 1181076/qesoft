import {ID} from "../model/ID";
import {Reason} from "../model/Reason";
import {StartPeriod} from "../model/StartPeriod";
import {EndPeriod} from "../model/EndPeriod";
import { DownTime } from "./DownTime";
import { DownTimeDTO } from "./DownTimeDTO";

export interface DownTimeInterface {
    getId: () => ID;
    getStartPeriod: () => StartPeriod;
    getEndPeriod: () => EndPeriod;
    getReason: () => Reason;
    update: (downtime:DownTime) => void;
    toDTO: () => DownTimeDTO;
}