export class Reason{
    private value: String;

    constructor(value: String){
        this.value = value;
    }

    public getValue():String{
        return this.value;
    }

    public equals(reason: String):boolean{
        return (reason === this.value);
    }
}