import {ID} from "../model/ID";
import {Reason} from "../model/Reason";
import {StartPeriod} from "../model/StartPeriod";
import {EndPeriod} from "../model/EndPeriod";

export class DownTimeDTO{

    private id:ID;
    private startPeriod:StartPeriod;
    private reason:Reason;
    private endPeriod:EndPeriod;

    constructor(id: ID, reason:Reason, startPeriod:StartPeriod, endPeriod:EndPeriod){
        this.id = id;
        this.startPeriod = startPeriod;
        this.reason = reason;
        this.endPeriod = endPeriod;
    }

    public getId(): ID{
        return this.id;
    };
    
    public getStartPeriod(): StartPeriod{
        return this.startPeriod;
    }

    public setStartPeriod(newStartPeriod: StartPeriod):void{
        this.startPeriod = newStartPeriod;
    }

    public getEndPeriod():EndPeriod{
        return this.endPeriod;
    }

    public setEndPeriod(newEndPeriod: EndPeriod):void{
        this.endPeriod = newEndPeriod;
    }

    public getReason():Reason{
        return this.reason;
    }

    public setReason(newReason: Reason): void{
        this.reason = newReason;
    }

}