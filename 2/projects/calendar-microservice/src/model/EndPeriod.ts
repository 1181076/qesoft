export class EndPeriod{
    private value: Date;

    constructor(year: number,month: number,day: number,hour: number,min: number){
        this.value = new Date(year, month, day, hour, min);
    }

    public getValue():Date{
        return this.value;
    }

    public equals(date: Date):boolean{
        return (date.getTime() === this.value.getTime());
    }
}