import { DownTimeDTO } from "../model/DownTimeDTO";
import { Client } from 'ts-postgres';

// Calendar Repository

function getClient() {
    const client = new Client({ database: "arqsoft402", host: "localhost", port: 5432, password: "password", user: "postgres" });
    return client;
}

export const getAllDownTimes = async () => {
    const client = getClient();
    await client.connect();
    try {
        const result = await client.query("SELECT * FROM calendarschema.downtime");
        return result.rows;
    } catch (e) {
        console.log(e);
        return null;
    }
}

export const getDownTimeById = async (id: number) => {
    const client = getClient();
    await client.connect();
    try {
        const result = await client.query("SELECT * FROM calendarschema.downtime WHERE downtimeId = $1", [id]);
        await client.end();
        return result.rows;
    } catch (e) {
        console.log(e);
        return null;
    }
}

export const createDownTime = async (downtime: DownTimeDTO) => {
    const client = getClient();
    await client.connect();
    try {
        await client.query("CREATE Table IF NOT EXISTS calendarschema.downtime( downtimeId VARCHAR(255) PRIMARY KEY, startPeriod DATETIME, reason VARCHAR(255), endPeriod DATETIME)");
        const countStart = await client.query("SELECT COUNT(downtimeId) FROM calendarschema.downtime WHERE startPeriod BETWEEN $1 AND $2", [downtime.getStartPeriod().getValue(), downtime.getEndPeriod().getValue()]);
        const countEnd = await client.query("SELECT COUNT(downtimeId) FROM calendarschema.downtime WHERE endPeriod BETWEEN $1 AND $2", [downtime.getStartPeriod().getValue(), downtime.getEndPeriod().getValue()]);
        if (parseInt(countStart.rows[0].toString().replace("n", "").trim()) == 0) {
            if (parseInt(countEnd.rows[0].toString().replace("n", "").trim()) == 0) {
                //create
                const result = await client.query("INSERT INTO calendarschema.downtime(downtimeId, startPeriod, reason, endPeriod) VALUES ( $1 , $2 , $3 , $4 )", [downtime.getId().getValue(), downtime.getStartPeriod().getValue(), downtime.getReason().getValue() + "", downtime.getEndPeriod().getValue()]);
                await client.end();
                return result.status;
                
            } else {
                //update - set new endPeriod + add description
                const result = await client.query("UPDATE calendarschema.downtime SET reason = CONCAT(reason, $2::VARCHAR(255)), endPeriod = $3 WHERE endPeriod BETWEEN $1 AND $3", [downtime.getStartPeriod().getValue(), ";"+downtime.getReason().getValue(), downtime.getEndPeriod().getValue()]);
                await client.end();
                return result.status;
                
            }
        } else {
            if (parseInt(countEnd.rows[0].toString().replace("n", "").trim()) == 0) {
                //update - set new startPeriod + add description
                const result = await client.query("UPDATE calendarschema.downtime SET startPeriod = $1, reason = CONCAT(reason, $2::VARCHAR(255)) WHERE startPeriod BETWEEN $1 AND $3", [downtime.getStartPeriod().getValue(), ";"+downtime.getReason().getValue(), downtime.getEndPeriod().getValue()]);
                await client.end();
                return result.status;
            } else {
                //update - only add description 
                const result = await client.query("UPDATE calendarschema.downtime SET reason = CONCAT(reason, $2::VARCHAR(255))  WHERE startPeriod BETWEEN $1 AND $3 AND endPeriod BETWEEN $1 AND $3", [downtime.getStartPeriod().getValue(), ";"+downtime.getReason().getValue(), downtime.getEndPeriod().getValue()]);
                await client.end();
                return result.status;
            }
        }
    } catch (e) {
        console.log(e);
        return null;
    }

}

export const updateDownTime = async (id: number, downtime: DownTimeDTO) => {
    const client = getClient();
    await client.connect();
    try {
        const result = await client.query("UPDATE calendarschema.downtime SET downtimeId = $1, startPeriod = $2, reason = $3, endPeriod = $4 WHERE downtimeId = $1", [downtime.getId().getValue(), downtime.getStartPeriod().getValue(), downtime.getReason().getValue() + "", downtime.getEndPeriod().getValue()]);
        await client.end();
        return result.status;
    } catch (e) {
        console.log(e);
        return null;
    }

}

export const deleteDownTime = async (id: number) => {
    const client = getClient();
    await client.connect();
    try {
        const result = await client.query("DELETE FROM calendarschema.downtime WHERE downtimeId = $1", [id]);
        await client.end();
        return result.status;
    } catch (e) {
        console.log(e);
        return null;
    }
}
