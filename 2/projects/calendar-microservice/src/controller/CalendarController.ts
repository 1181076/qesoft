
import { Console } from "console";
import express, { Request, Response } from "express";
import * as CalendarService from "../service/CalendarService";
import {ID} from "../model/ID";
import {Reason} from "../model/Reason";
import {StartPeriod} from "../model/StartPeriod";
import {EndPeriod} from "../model/EndPeriod";
import { DownTime } from "../model/DownTime";

export const downtimeController = express.Router();

// GET - downtimes
downtimeController.get("/", async (req: Request, res: Response) => {
    try {
        const downtimes: any[] | null = await CalendarService.getAllDownTimes();

        res.status(200).send(downtimes);
    } catch (e) {
        res.status(500).send((e as any).message);
    }
});

// GET - downtimes/id
downtimeController.get("/:id", async (req: Request, res: Response) => {
    const id: number = parseInt(req.params.id, 10);

    try {
        const downtime: any = await CalendarService.getDownTimeById(id);

        if (downtime) {
            return res.status(200).send(downtime);
        }

        res.status(404).send("Downtime not found");
    } catch (e) {
        res.status(500).send((e as any).message);
    }
});

// POST - downtimes
downtimeController.post("/", async (req: Request, res: Response) => {
    try {
        let id:ID = new ID(req.body.id);
        let startPeriod: StartPeriod = new StartPeriod(req.body.startPeriod.year,req.body.startPeriod.month,req.body.startPeriod.day,req.body.startPeriod.hour,req.body.startPeriod.min);
        let reason: Reason = new Reason(req.body.reason);
        let endPeriod: EndPeriod = new EndPeriod(req.body.endPeriod.year,req.body.endPeriod.month,req.body.endPeriod.day,req.body.endPeriod.hour,req.body.endPeriod.min);
        const downtime: DownTime = new DownTime(id, reason, startPeriod, endPeriod);
        const newDowntime = await CalendarService.createDownTime(downtime);
        res.status(201).json(newDowntime);
    } catch (e) {
        res.status(500).send((e as any).message);
    }
});

// PUT - downtimes/id
downtimeController.put("/", async (req: Request, res: Response) => {
    try {
        let id:ID = new ID(req.body.id);
        let startPeriod: StartPeriod = new StartPeriod(req.body.startPeriod.year,req.body.startPeriod.month,req.body.startPeriod.day,req.body.startPeriod.hour,req.body.startPeriod.min);
        let reason: Reason = new Reason(req.body.reason);
        let endPeriod: EndPeriod = new EndPeriod(req.body.endPeriod.year,req.body.endPeriod.month,req.body.endPeriod.day,req.body.endPeriod.hour,req.body.endPeriod.min);
        const downtimeUpdate: DownTime = new DownTime(id, reason, startPeriod, endPeriod);

        const existingDowntime: any = await CalendarService.getDownTimeById(id.getValue());

        if (existingDowntime) {
            const updatedDowntime = await CalendarService.updateDownTime(id.getValue(), downtimeUpdate);
            return res.status(200).json(updatedDowntime);
        }

        const newDowntime = await CalendarService.createDownTime(downtimeUpdate);

        res.status(201).json(newDowntime);
    } catch (e) {
        res.status(500).send((e as any).message);
    }
});

// DELETE - downtimes/id
downtimeController.delete("/:id", async (req: Request, res: Response) => {
    try {
        const id: number = parseInt(req.params.id, 10);
        await CalendarService.deleteDownTime(id);

        res.sendStatus(204);
    } catch (e) {
        res.status(500).send((e as any).message);
    }
});