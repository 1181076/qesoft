/**
 * Required External Modules
 */
import * as dotenv from "dotenv";
import express from "express";
import cors from "cors";
import helmet from "helmet";
import {downtimeController} from "./controller/CalendarController";
import {errorHandler, notFoundHandler} from "./utils/ErrorResponse";
import { Client } from 'ts-postgres';
const eurekaHelper = require('./eureka-helper');

dotenv.config();


const client = new Client({database:"arqsoft402", host: "localhost", port: 5432, password: "password", user:"postgres"});
client.connect().then(async () => {
    await client.query("CREATE SCHEMA IF NOT EXISTS CalendarSchema AUTHORIZATION postgres");
    await client.query("CREATE Table IF NOT EXISTS calendarschema.downtime( downtimeId VARCHAR(255) PRIMARY KEY, startPeriod TIMESTAMP, reason VARCHAR(255), endPeriod TIMESTAMP)");
    await client.end();
});

/**
 * App Variables
 */
if (!process.env.PORT) {
    process.exit(1);
}

const PORT: number = parseInt(process.env.PORT as string, 10);
const app = express();

/**
 *  App Configuration
 */
app.use(helmet());
app.use(cors());
app.use(express.json());
app.use("/api/downtimes", downtimeController);
app.use(errorHandler);
app.use(notFoundHandler);
/**
 * Server Activation
 */
app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
});

eurekaHelper.registerWithEureka('calendar-microservice', PORT);