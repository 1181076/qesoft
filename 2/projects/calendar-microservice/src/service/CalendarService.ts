import { DownTime } from "../model/DownTime";
import * as CalendarRepository from "../repository/CalendarRepository";


export const getAllDownTimes = async () => {
    return CalendarRepository.getAllDownTimes();
}

export const getDownTimeById = async (id: number) => {
    return CalendarRepository.getDownTimeById(id);
}

export const createDownTime = async (downtime: DownTime) => {
    let downtimeDto = downtime.toDTO();
    return CalendarRepository.createDownTime(downtimeDto);
}

export const updateDownTime = async (id: number, downtime: DownTime) => {
    let downtimeDto = downtime.toDTO();
    return CalendarRepository.updateDownTime(id, downtimeDto);
}

export const deleteDownTime = async (id: number) => {
    return CalendarRepository.deleteDownTime(id);
}