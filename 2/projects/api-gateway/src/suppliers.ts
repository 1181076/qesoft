import { RESTDataSource } from "apollo-datasource-rest";

export class SupplierAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = "http://localhost:8085/api/";
  }

  async getAllSuppliers() {
    return this.get("suppliers");
  }

  async getSupplierByID(id: string) {
    return this.get(`suppliers/${id}`);
  }

  async getAllDeals() {
    return this.get("deals");
  }

  async getDealByID(id: string) {
    return this.get(`deals/${id}`);
  }

  async createSupplier(ingredient: any) {
    return this.post("suppliers", ingredient);
  }

  async updateSupplier(id: string, ingredient: any) {
    return this.put(`suppliers/${id}`, ingredient);
  }

  async deleteSupplier(id: string) {
    return this.delete(`suppliers/${id}`);
  }

  async deleteAllSuppliers() {
    return this.delete(`suppliers`);
  }

  async createDeal(ingredient: any) {
    return this.post("deals", ingredient);
  }

  async updateDeal(id: string, ingredient: any) {
    return this.put(`deals/${id}`, ingredient);
  }

  async deleteDeal(id: string) {
    return this.delete(`deals/${id}`);
  }

  async deleteAllDeals() {
    return this.delete(`deals`);
  }
}
