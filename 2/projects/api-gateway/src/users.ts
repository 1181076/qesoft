import { RESTDataSource } from "apollo-datasource-rest";

export class UserAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = "http://localhost:8081/api/";
  }

  async getAllOrders() {
    return this.get("orders");
  }

  async getOrderByID(id: string) {
    return this.get(`orders/${id}`);
  }

  async createOrder(order: any) {
    return this.post("orders", order);
  }

  async updateOrder(id: string, order: any) {
    return this.put(`orders/${id}`, order);
  }

  async deleteOrder(id: string) {
    return this.delete(`orders/${id}`);
  }

  async deleteAllOrders() {
    return this.delete(`orders`);
  }

  async authenticate(user: any) {
    return this.post("authenticate", user);
  }

  async register(user: any) {
    return this.post("register", user);
  }
}
