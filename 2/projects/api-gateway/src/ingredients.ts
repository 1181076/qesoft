import { RESTDataSource } from "apollo-datasource-rest";

export class IngredientsAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = "http://localhost:8084/api/";
  }

  async getAllIngredients() {
    return this.get("ingredients");
  }

  async getIngredientByID(id: string) {
    return this.get("ingredient", {
      id,
    });
  }

  async createIngredient(ingredient: any) {
    return this.post("ingredients", ingredient);
  }

  async updateIngredient(id: string, ingredient: any) {
    return this.put(`ingredients/${id}`, ingredient);
  }

  async deleteIngredient(id: string) {
    return this.delete(`ingredients/${id}`);
  }

  async deleteAllIngredients() {
    return this.delete(`ingredients`);
  }
}
