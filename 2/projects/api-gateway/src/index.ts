import { ApolloServer, gql } from "apollo-server-express";
import { ApolloServerPluginDrainHttpServer } from "apollo-server-core";
import express from "express";
import http from "http";
import { IngredientsAPI } from "./ingredients";
import { SandwichAPI } from "./sandwiches";
import { SchoolAPI } from "./schools";
import { SupplierAPI } from "./suppliers";
import { UserAPI } from "./users";

import { execute, subscribe } from "graphql";
import { SubscriptionServer } from "subscriptions-transport-ws";
import { makeExecutableSchema } from "@graphql-tools/schema";

import timeout from "connect-timeout";

import { PubSub } from "graphql-subscriptions";

const typeDefs = gql`
  input UserInput {
    email: InputString!
    name: InputString!
    password: InputString!
    role: InputString
    orders: [InputString]
    school: InputString
  }

  type UserOutput {
    email: OutputString!
    name: OutputString!
    password: OutputString!
    role: OutputString!
    orders: [OutputString]!
    school: OutputString!
  }

  input AuthenticationInput {
    email: String!
    password: String!
  }

  type AuthenticationOutput {
    email: String
    name: String
    role: String
    token: String
  }

  type Price {
    amount: Float!
    currency: String!
  }

  input PriceInput {
    amount: Float!
    currency: String!
  }

  type PriceOutput {
    amount: Float!
    currency: String!
  }

  type Sandwich {
    id: String!
    shortDescription: String!
    extendedDescription: String!
    price: Price!
    ingredients: [Ingredient]!
    categories: [Category]!
  }

  input SandwichInput {
    id: InputString!
    shortDescription: InputString!
    extendedDescription: InputString!
    price: PriceInput!
    ingredients: [InputString]!
    categories: [InputString]!
  }

  type SandwichOutput {
    id: OutputString!
    shortDescription: OutputString!
    extendedDescription: OutputString!
    price: PriceOutput!
    ingredients: [OutputString]!
    categories: [OutputString]!
  }

  type Category {
    description: String!
  }

  input CategoryInput {
    description: InputString!
  }

  type CategoryOutput {
    description: OutputString!
  }

  type LastUpdatedDate {
    amount: Int!
    date: String!
  }

  input LastUpdatedDateInput {
    amount: Int!
    date: String!
  }

  input InputString {
    value: String!
  }

  type OutputString {
    value: String!
  }

  input InputInt {
    value: Int!
  }

  type OutputInt {
    value: Int!
  }

  input InputFloat {
    value: Float!
  }

  type OutputFloat {
    value: Float!
  }

  type SandwichOrder {
    sandwich: String!
    amount: Int!
  }

  input SandwichOrderInput {
    sandwich: String!
    amount: Int!
  }

  type SandwichOrderOutput {
    sandwich: String!
    amount: Int!
  }

  type Order {
    id: String!
    day: String!
    sandwichOrder: [SandwichOrder]!
  }

  input OrderInput {
    id: InputString!
    day: InputString!
    sandwichOrder: [SandwichOrderInput]!
  }

  type OrderOutput {
    id: OutputString!
    day: OutputString!
    sandwichOrder: [SandwichOrderOutput]!
  }

  type School {
    id: String!
    internalID: String!
    name: String!
    address: String!
    sandwiches: [Sandwich]!
  }

  input SchoolInput {
    id: InputString!
    internalID: InputString!
    name: InputString!
    address: InputString!
    sandwiches: [InputString]!
  }

  type SchoolOutput {
    id: OutputString!
    internalID: OutputString!
    name: OutputString!
    address: OutputString!
    sandwiches: [OutputString]!
  }

  type Supplier {
    id: String!
    name: String!
    address: String!
    emails: [String]!
    contacts: [String]!
    description: String!
    deals: [Deal]!
  }

  input SupplierInput {
    id: InputString!
    name: InputString!
    address: InputString!
    emails: [InputString]!
    contacts: [InputString]!
    description: InputString!
    deals: [InputString]!
  }

  type SupplierOutput {
    id: OutputString!
    name: OutputString!
    address: OutputString!
    emails: [OutputString]!
    contacts: [OutputString]!
    description: OutputString!
    deals: [OutputString]!
  }

  type UnitPrice {
    unit: String!
    value: Float!
  }

  input UnitPriceInput {
    unit: String!
    value: Float!
  }

  type UnitPriceOutput {
    unit: String!
    value: Float!
  }

  type Deal {
    id: String!
    unitPrice: UnitPrice!
    expirationPriceDate: String!
    ingredient: Ingredient!
  }

  input DealInput {
    id: InputString!
    unitPrice: UnitPriceInput!
    expirationPriceDate: InputString!
    ingredient: InputString!
  }

  type DealOutput {
    id: OutputString!
    unitPrice: UnitPriceOutput!
    expirationPriceDate: OutputString!
    ingredient: OutputString!
  }

  input IngredientInput {
    id: InputString!
    description: InputString!
    quantity: InputInt!
    lastUpdatedDate: [LastUpdatedDateInput]
    upperLimit: InputInt!
    lowerLimit: InputInt!
    units: InputString!
  }

  type IngredientOutput {
    id: OutputString!
    description: OutputString!
    quantity: OutputInt!
    lastUpdatedDate: [LastUpdatedDate]
    upperLimit: OutputInt!
    lowerLimit: OutputInt!
    units: OutputString!
  }

  type Ingredient {
    id: String!
    description: String!
    quantity: Int!
    lastUpdatedDate: [LastUpdatedDate]
    upperLimit: Int!
    lowerLimit: Int!
    units: String!
  }

  type Query {
    sandwiches: [Sandwich]
    sandwich(id: String!): Sandwich
    ingredients: [Ingredient]
    ingredient(id: String!): Ingredient
    categories: [Category]
    category(id: String!): Category
    deals: [Deal]
    deal(id: String!): Deal
    suppliers: [Supplier]
    supplier(id: String!): Supplier
    schools: [School]
    school(id: String!): School
    orders: [Order]
    order(id: String!): Order
  }

  type Mutation {
    createIngredient(ingredient: IngredientInput!): IngredientOutput
    updateIngredient(
      id: String!
      ingredient: IngredientInput!
    ): IngredientOutput
    deleteIngredient(id: String!): Int!
    deleteAllIngredients: Int!

    createSandwich(sandwich: SandwichInput!): SandwichOutput
    updateSandwich(id: String!, sandwich: SandwichInput!): SandwichOutput
    deleteSandwich(id: String!): Int!
    deleteAllSandwiches: Int!

    createCategory(category: CategoryInput!): CategoryOutput
    deleteCategory(id: String!): Int!
    deleteAllCategories: Int!

    createSchool(school: SchoolInput!): SchoolOutput
    updateSchool(id: String!, school: SchoolInput!): SchoolOutput
    deleteSchool(id: String!): Int!
    deleteAllSchools: Int!

    createSupplier(supplier: SupplierInput!): SupplierOutput
    updateSupplier(id: String!, supplier: SupplierInput!): SupplierOutput
    deleteSupplier(id: String!): Int!
    deleteAllSuppliers: Int!

    createDeal(deal: DealInput!): DealOutput
    updateDeal(id: String!, deal: DealInput!): DealOutput
    deleteDeal(id: String!): Int!
    deleteAllDeals: Int!

    createOrder(order: OrderInput!): OrderOutput
    updateOrder(id: String!, order: OrderInput!): OrderOutput
    deleteOrder(id: String!): Int!
    deleteAllOrders: Int!

    authenticate(user: AuthenticationInput!): AuthenticationOutput
    register(user: UserInput!): UserOutput
  }

  type Subscription {
    sandwichesChanged: [Sandwich]
    ingredientsChanged: [Ingredient]
    categoriesChanged: [Category]
    dealsChanged: [Deal]
    suppliersChanged: [Supplier]
    schoolsChanged: [School]
    ordersChanged: [Order]
  }
`;

const pubsub = new PubSub();

const resolvers = {
  Query: {
    ingredients: (_root: any, args: any, { dataSources }: any) =>
      dataSources.ingredients.getAllIngredients(),
    ingredient: (_root: any, { id }: any, { dataSources }: any) =>
      dataSources.ingredients.getIngredientByID(id),
    sandwiches: (_root: any, args: any, { dataSources }: any) =>
      dataSources.sandwiches.getAllSandwiches(),
    sandwich: (_root: any, { id }: any, { dataSources }: any) =>
      dataSources.sandwiches.getSandwichByID(id),
    categories: (_root: any, args: any, { dataSources }: any) =>
      dataSources.sandwiches.getAllCategories(),
    category: (_root: any, { id }: any, { dataSources }: any) =>
      dataSources.sandwiches.getCategoryByID(id),
    suppliers: (_root: any, args: any, { dataSources }: any) =>
      dataSources.suppliers.getAllSuppliers(),
    supplier: (_root: any, { id }: any, { dataSources }: any) =>
      dataSources.suppliers.getSupplierByID(id),
    deals: (_root: any, args: any, { dataSources }: any) =>
      dataSources.suppliers.getAllDeals(),
    deal: (_root: any, { id }: any, { dataSources }: any) =>
      dataSources.suppliers.getDealByID(id),
    schools: (_root: any, args: any, { dataSources }: any) =>
      dataSources.schools.getAllSchools(),
    school: (_root: any, { id }: any, { dataSources }: any) =>
      dataSources.schools.getSchoolByID(id),
    orders: (_root: any, args: any, { dataSources }: any) =>
      dataSources.users.getAllOrders(),
    order: (_root: any, { id }: any, { dataSources }: any) =>
      dataSources.users.getOrderByID(id),
  },
  Mutation: {
    createIngredient: (
      _root: any,
      { ingredient }: any,
      { dataSources }: any
    ) => {
      dataSources.ingredients.createIngredient(ingredient);
      pubsub.publish("INGREDIENTS_CHANGED", {
        ingredientsChanged: dataSources.ingredients.getAllIngredients(),
      });
    },
    updateIngredient: (
      _root: any,
      { id, ingredient }: any,
      { dataSources }: any
    ) => {
      dataSources.ingredients.updateIngredient(id, ingredient),
        pubsub.publish("INGREDIENTS_CHANGED", {
          ingredientsChanged: dataSources.ingredients.getAllIngredients(),
        });
    },
    deleteIngredient: (_root: any, { id }: any, { dataSources }: any) => {
      dataSources.ingredients.deleteIngredient(id),
        pubsub.publish("INGREDIENTS_CHANGED", {
          ingredientsChanged: dataSources.ingredients.getAllIngredients(),
        });
    },
    deleteAllIngredients: (_root: any, args: any, { dataSources }: any) => {
      dataSources.ingredients.deleteAllIngredients(),
        pubsub.publish("INGREDIENTS_CHANGED", {
          ingredientsChanged: dataSources.ingredients.getAllIngredients(),
        });
    },

    createSandwich: (_root: any, { sandwich }: any, { dataSources }: any) => {
      dataSources.sandwiches.createSandwich(sandwich);
      pubsub.publish("SANDWICHES_CHANGED", {
        sandwichesChanged: dataSources.sandwiches.getAllSandwiches(),
      });
    },
    updateSandwich: (
      _root: any,
      { id, sandwich }: any,
      { dataSources }: any
    ) => {
      dataSources.sandwiches.updateSandwich(id, sandwich),
        pubsub.publish("SANDWICHES_CHANGED", {
          sandwichesChanged: dataSources.sandwiches.getAllSandwiches(),
        });
    },
    deleteSandwich: (_root: any, { id }: any, { dataSources }: any) => {
      dataSources.sandwiches.deleteSandwich(id),
        pubsub.publish("SANDWICHES_CHANGED", {
          sandwichesChanged: dataSources.sandwiches.getAllSandwiches(),
        });
    },
    deleteAllSandwiches: (_root: any, args: any, { dataSources }: any) => {
      dataSources.sandwiches.deleteAllSandwiches(),
        pubsub.publish("SANDWICHES_CHANGED", {
          sandwichesChanged: dataSources.sandwiches.getAllSandwiches(),
        });
    },

    createCategory: (_root: any, { category }: any, { dataSources }: any) => {
      dataSources.sandwiches.createCategory(category);
      pubsub.publish("CATEGORIES_CHANGED", {
        categoriesChanged: dataSources.sandwiches.getAllCategories(),
      });
    },
    deleteCategory: (_root: any, { id }: any, { dataSources }: any) => {
      dataSources.sandwiches.deleteCategory(id),
        pubsub.publish("CATEGORIES_CHANGED", {
          categoriesChanged: dataSources.sandwiches.getAllCategories(),
        });
    },
    deleteAllCategories: (_root: any, args: any, { dataSources }: any) => {
      dataSources.sandwiches.deleteAllCategories(),
        pubsub.publish("CATEGORIES_CHANGED", {
          categoriesChanged: dataSources.sandwiches.getAllCategories(),
        });
    },

    createSchool: (_root: any, { school }: any, { dataSources }: any) => {
      dataSources.schools.createSchool(school);
      pubsub.publish("SCHOOLS_CHANGED", {
        schoolsChanged: dataSources.schools.getAllSchools(),
      });
    },
    updateSchool: (_root: any, { id, school }: any, { dataSources }: any) => {
      dataSources.schools.updateSchool(id, school),
        pubsub.publish("SCHOOLS_CHANGED", {
          schoolsChanged: dataSources.schools.getAllSchools(),
        });
    },
    deleteSchool: (_root: any, { id }: any, { dataSources }: any) => {
      dataSources.schools.deleteSchool(id),
        pubsub.publish("SCHOOLS_CHANGED", {
          schoolsChanged: dataSources.schools.getAllSchools(),
        });
    },
    deleteAllSchools: (_root: any, args: any, { dataSources }: any) => {
      dataSources.schools.deleteAllSchools(),
        pubsub.publish("SCHOOLS_CHANGED", {
          schoolsChanged: dataSources.schools.getAllSchools(),
        });
    },

    createSupplier: (_root: any, { supplier }: any, { dataSources }: any) => {
      dataSources.suppliers.createSupplier(supplier);
      pubsub.publish("SUPPLIERS_CHANGED", {
        suppliersChanged: dataSources.suppliers.getAllSuppliers(),
      });
    },
    updateSupplier: (
      _root: any,
      { id, supplier }: any,
      { dataSources }: any
    ) => {
      dataSources.suppliers.updateSupplier(id, supplier),
        pubsub.publish("SUPPLIERS_CHANGED", {
          suppliersChanged: dataSources.suppliers.getAllSuppliers(),
        });
    },
    deleteSupplier: (_root: any, { id }: any, { dataSources }: any) => {
      dataSources.suppliers.deleteSupplier(id),
        pubsub.publish("SUPPLIERS_CHANGED", {
          suppliersChanged: dataSources.suppliers.getAllSuppliers(),
        });
    },
    deleteAllSuppliers: (_root: any, args: any, { dataSources }: any) => {
      dataSources.suppliers.deleteAllSuppliers(),
        pubsub.publish("SUPPLIERS_CHANGED", {
          suppliersChanged: dataSources.suppliers.getAllSuppliers(),
        });
    },

    createDeal: (_root: any, { deal }: any, { dataSources }: any) => {
      dataSources.suppliers.createDeal(deal);
      pubsub.publish("DEALS_CHANGED", {
        dealsChanged: dataSources.suppliers.getAllDeals(),
      });
    },
    updateDeal: (_root: any, { id, deal }: any, { dataSources }: any) => {
      dataSources.suppliers.updateDeal(id, deal),
        pubsub.publish("DEALS_CHANGED", {
          dealsChanged: dataSources.suppliers.getAllDeals(),
        });
    },
    deleteDeal: (_root: any, { id }: any, { dataSources }: any) => {
      dataSources.suppliers.deleteDeal(id),
        pubsub.publish("DEALS_CHANGED", {
          dealsChanged: dataSources.suppliers.getAllDeals(),
        });
    },
    deleteAllDeals: (_root: any, args: any, { dataSources }: any) => {
      dataSources.suppliers.deleteAllDeals(),
        pubsub.publish("DEALS_CHANGED", {
          dealsChanged: dataSources.suppliers.getAllDeals(),
        });
    },

    createOrder: (_root: any, { order }: any, { dataSources }: any) => {
      dataSources.users.createOrder(order);
      pubsub.publish("ORDERS_CHANGED", {
        ordersChanged: dataSources.users.getAllOrders(),
      });
    },
    updateOrder: (_root: any, { id, order }: any, { dataSources }: any) => {
      dataSources.users.updateOrder(id, order),
        pubsub.publish("ORDERS_CHANGED", {
          ordersChanged: dataSources.users.getAllOrders(),
        });
    },
    deleteOrder: (_root: any, { id }: any, { dataSources }: any) => {
      dataSources.users.deleteOrder(id),
        pubsub.publish("ORDERS_CHANGED", {
          ordersChanged: dataSources.users.getAllOrders(),
        });
    },
    deleteAllOrders: (_root: any, args: any, { dataSources }: any) => {
      dataSources.users.deleteAllOrders(),
        pubsub.publish("ORDERS_CHANGED", {
          ordersChanged: dataSources.users.getAllIngredients(),
        });
    },

    authenticate: (_root: any, { user }: any, { dataSources }: any) =>
      dataSources.users.authenticate(user),

    register: (_root: any, { user }: any, { dataSources }: any) =>
      dataSources.users.register(user),
  },

  Subscription: {
    ingredientsChanged: {
      subscribe: () => pubsub.asyncIterator(["INGREDIENTS_CHANGED"]),
    },
    sandwichesChanged: {
      subscribe: () => pubsub.asyncIterator(["SANDWICHES_CHANGED"]),
    },
    categoriesChanged: {
      subscribe: () => pubsub.asyncIterator(["CATEGORIES_CHANGED"]),
    },
    schoolsChanged: {
      subscribe: () => pubsub.asyncIterator(["SCHOOLS_CHANGED"]),
    },
    suppliersChanged: {
      subscribe: () => pubsub.asyncIterator(["SUPPLIERS_CHANGED"]),
    },
    dealsChanged: {
      subscribe: () => pubsub.asyncIterator(["DEALS_CHANGED"]),
    },
    ordersChanged: {
      subscribe: () => pubsub.asyncIterator(["ORDERS_CHANGED"]),
    },
  },
};

async function startApolloServer() {
  // Required logic for integrating with Express
  const app = express();
  const httpServer = http.createServer(app);

  const schema = makeExecutableSchema({ typeDefs, resolvers });

  app.use(timeout("5s"));
  app.use(haltOnTimedout);

  const subscriptionServer = SubscriptionServer.create(
    {
      // This is the `schema` we just created.
      schema,
      // These are imported from `graphql`.
      execute,
      subscribe,
    },
    {
      // This is the `httpServer` we created in a previous step.
      server: httpServer,
      // Pass a different path here if your ApolloServer serves at
      // a different path.
      path: "/",
    }
  );

  // Same ApolloServer initialization as before, plus the drain plugin.
  const server = new ApolloServer({
    typeDefs,
    resolvers,
    plugins: [
      ApolloServerPluginDrainHttpServer({ httpServer }),
      {
        async serverWillStart() {
          return {
            async drainServer() {
              subscriptionServer.close();
            },
          };
        },
      },
    ],
    dataSources: () => {
      return {
        ingredients: new IngredientsAPI(),
        sandwiches: new SandwichAPI(),
        suppliers: new SupplierAPI(),
        schools: new SchoolAPI(),
        users: new UserAPI(),
      };
    },
  });

  // More required logic for integrating with Express
  await server.start();
  server.applyMiddleware({
    app,

    // By default, apollo-server hosts its GraphQL endpoint at the
    // server root. However, *other* Apollo Server packages host it at
    // /graphql. Optionally provide this to match apollo-server.
    path: "/",
  });

  // Modified server startup
  await new Promise<void>((resolve) =>
    httpServer.listen({ port: 8080 }, resolve)
  );
  console.log(`🚀 Server ready at http://localhost:8080${server.graphqlPath}`);

  function haltOnTimedout(req: any, res: any, next: any) {
    if (!req.timedout) next();
  }
}

startApolloServer();
