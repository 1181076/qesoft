import { RESTDataSource } from "apollo-datasource-rest";

export class SandwichAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = "http://localhost:8083/api/";
  }

  async getAllSandwiches() {
    return this.get("sandwiches");
  }

  async getSandwichByID(id: string) {
    return this.get(`sandwiches/${id}`);
  }

  async getAllCategories() {
    return this.get("categories");
  }

  async getCategoryByID(id: string) {
    return this.get(`categories/${id}`);
  }

  async createCategory(category: any) {
    return this.post("categories", category);
  }

  async deleteCategory(id: string) {
    return this.delete(`categories/${id}`);
  }

  async deleteAllCategories() {
    return this.delete(`categories`);
  }

  async createSandwich(sandwich: any) {
    return this.post("sandwiches", sandwich);
  }

  async updateSandwich(id: string, sandwich: any) {
    return this.put(`sandwiches/${id}`, sandwich);
  }

  async deleteSandwich(id: string) {
    return this.delete(`sandwiches/${id}`);
  }

  async deleteAllSandwiches() {
    return this.delete(`sandwiches`);
  }
}
