import { RESTDataSource } from "apollo-datasource-rest";

export class SchoolAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = "http://localhost:8082/api/";
  }

  async getAllSchools() {
    return this.get("schools");
  }

  async getSchoolByID(id: string) {
    return this.get(`schools/${id}`);
  }


  async createSchool(school: any) {
    return this.post("schools", school);
  }

  async updateSchool(id: string, school: any) {
    return this.put(`schools/${id}`, school);
  }

  async deleteSchool(id: string) {
    return this.delete(`schools/${id}`);
  }

  async deleteAllSchools() {
    return this.delete(`schools`);
  }
}
