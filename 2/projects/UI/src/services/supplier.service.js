import { gql } from "@apollo/client";
import client from "../utils/apolloClient";

class SupplierDataService {
  getAll() {
    return client.query({
      query: gql`
        query Query {
          suppliers {
            id
            name
            address
            emails
            contacts
            description
            deals {
              id
              unitPrice {
                unit
                value
              }
              expirationPriceDate
              ingredient {
                id
                description
                quantity
                lastUpdatedDate {
                  amount
                  date
                }
                lowerLimit
                upperLimit
                units
              }
            }
          }
        }
      `,
    });
  }

  get(id) {
    return client.query({
      query: gql`
        query Query($supplierId: String!) {
          supplier(id: $supplierId) {
            id
            name
            address
            emails
            contacts
            description
            deals {
              id
              unitPrice {
                unit
                value
              }
              expirationPriceDate
              ingredient {
                id
                description
                quantity
                lastUpdatedDate {
                  amount
                  date
                }
                upperLimit
                lowerLimit
                units
              }
            }
          }
        }
      `,
      variables: {
        supplierId: id,
      },
    });
  }

  create(data) {
    console.log(data);
    return client.mutate({
      mutation: gql`
        mutation Mutation($supplier: SupplierInput!) {
          createSupplier(supplier: $supplier) {
            id {
              value
            }
          }
        }
      `,
      variables: {
        supplier: data,
      },
    });
  }

  update(id, data) {
    return client.mutate({
      mutation: gql`
        mutation UpdateSupplier(
          $updateSupplierId: String!
          $supplier: SupplierInput!
        ) {
          updateSupplier(id: $updateSupplierId, supplier: $supplier) {
            id {
              value
            }
          }
        }
      `,
      variables: {
        updateSupplierId: id,
        supplier: data,
      },
    });
  }

  delete(id) {
    return client.mutate({
      mutation: gql`
        mutation UpdateSupplier($deleteDealId: String!) {
          deleteDeal(id: $deleteDealId)
        }
      `,
      variables: {
        deleteDealId: id,
      },
    });
  }

  deleteAll() {
    return client.mutate({
      mutation: gql`
        mutation UpdateSupplier {
          deleteAllDeals
        }
      `,
    });
  }
}

export default new SupplierDataService();
