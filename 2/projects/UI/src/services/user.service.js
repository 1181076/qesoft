import http from "../http/http-users";

class UserDataService {
  
  registerSandwiches(id, school) {
    return http.post(`/users/${id}`, school);
  }
}

export default new UserDataService();
