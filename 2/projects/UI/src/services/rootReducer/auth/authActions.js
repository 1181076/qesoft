import * as AT from "./authTypes";
import axios from "axios";
import client from "../../../utils/apolloClient";
import { gql } from "@apollo/client";

export const authenticateUser = (email, password) => async (dispatch) => {
  dispatch(loginRequest());
  try {
    const response = await client.mutate({
      mutation: gql`
        mutation Authenticate($user: AuthenticationInput!) {
          authenticate(user: $user) {
            email
            token
            name
            role
          }
        }
      `,
      variables: {
        user: {
          email,
          password,
        },
      },
    });

    localStorage.setItem("jwtToken", response.data.authenticate.token);
    dispatch(
      success({
        username: response.data.authenticate.email,
        name: response.data.authenticate.name,
        isLoggedIn: "true",
        role: response.data.authenticate.role,
      })
    );
  } catch (error) {
    dispatch(failure());
  }
};

export const fetchSchools = () => {
  return (dispatch) => {
    dispatch(loginRequest());
    axios
      .get("http://localhost:8081/api/schools")
      .then((response) => {
        dispatch(success(response.data));
      })
      .catch((error) => {
        dispatch(failure(error.message));
      });
  };
};

export const logoutUser = () => {
  return (dispatch) => {
    dispatch(logoutRequest());
    localStorage.removeItem("jwtToken");
    dispatch(success({ username: "", isLoggedIn: false }));
  };
};

const loginRequest = () => {
  return {
    type: AT.LOGIN_REQUEST,
  };
};

const logoutRequest = () => {
  return {
    type: AT.LOGOUT_REQUEST,
  };
};

const success = (isLoggedIn) => {
  return {
    type: AT.SUCCESS,
    payload: isLoggedIn,
  };
};

const failure = () => {
  return {
    type: AT.FAILURE,
    payload: false,
  };
};
