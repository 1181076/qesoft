import { gql } from "@apollo/client";
import http from "../http/http-schools";
import client from "../utils/apolloClient";

class SchoolDataService {
  getAll() {
    return client.query({
      query: gql`
        query Query {
          schools {
            id
            internalID
            name
            address
            sandwiches {
              id
              shortDescription
              extendedDescription
              price {
                amount
                currency
              }
              ingredients {
                id
                description
                quantity
                lastUpdatedDate {
                  amount
                  date
                }
                upperLimit
                lowerLimit
                units
              }
              categories {
                description
              }
            }
          }
        }
      `,
    });
  }

  get(id) {
    return client.query({
      query: gql`
        query Query($schoolId: String!) {
          school(id: $schoolId) {
            id
            internalID
            name
            address
            sandwiches {
              id
              shortDescription
              extendedDescription
              price {
                amount
                currency
              }
              ingredients {
                id
                description
                quantity
                lastUpdatedDate {
                  amount
                  date
                }
                upperLimit
                lowerLimit
                units
              }
              categories {
                description
              }
            }
          }
        }
      `,
      variables: {
        schoolId: id,
      },
    });
  }

  create(data) {
    return client.mutate({
      mutation: gql`
        mutation Mutation($school: SchoolInput!) {
          createSchool(school: $school) {
            id {
              value
            }
          }
        }
      `,
      variables: {
        school: data,
      },
    });
  }

  update(id, data) {
    return client.mutate({
      mutation: gql`
        mutation Mutation($updateSchoolId: String!, $school: SchoolInput!) {
          updateSchool(id: $updateSchoolId, school: $school) {
            id {
              value
            }
          }
        }
      `,
      variables: {
        updateSchoolId: id,
        school: data,
      },
    });
  }

  delete(id) {
    return client.mutate({
      mutation: gql`
        mutation Mutation($deleteSchoolId: String!) {
          deleteSchool(id: $deleteSchoolId)
        }
      `,
      variables: {
        deleteSchoolId: id,
      },
    });
  }

  deleteAll() {
    return client.mutate({
      mutation: gql`
        mutation Mutation {
          deleteAllSchools
        }
      `,
    });
  }

  registerSandwiches(id, sandwiches) {
    console.log(sandwiches);
    return http.post(`/schools/${id}/registerSandwiches`, sandwiches);
  }
}

export default new SchoolDataService();
