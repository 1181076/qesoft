import { gql } from "@apollo/client";
import client from "../utils/apolloClient";

class DealDataService {
  getAll() {
    return client.query({
      query: gql`
        query Query {
          deals {
            id
            unitPrice {
              unit
              value
            }
            expirationPriceDate
            ingredient {
              id
              description
              quantity
              lastUpdatedDate {
                date
                amount
              }
              upperLimit
              lowerLimit
              units
            }
          }
        }
      `,
    });
  }

  get(id) {
    return client.query({
      query: gql`
        query Deal($dealId: String!) {
          deal(id: $dealId) {
            id
            unitPrice {
              value
              unit
            }
            expirationPriceDate
            ingredient {
              id
              description
              quantity
              lastUpdatedDate {
                amount
                date
              }
              upperLimit
              lowerLimit
              units
            }
          }
        }
      `,
      variables: {
        dealId: id,
      },
    });
  }

  create(data) {
    return client.mutate({
      mutation: gql`
        mutation CreateDeal($deal: DealInput!) {
          createDeal(deal: $deal) {
            id {
              value
            }
            unitPrice {
              unit
              value
            }
            expirationPriceDate {
              value
            }
            ingredient {
              value
            }
          }
        }
      `,
      variables: {
        deal: data,
      },
    });
  }

  update(id, data) {
    return client.mutate({
      mutation: gql`
        mutation Mutation($updateDealId: String!, $deal: DealInput!) {
          updateDeal(id: $updateDealId, deal: $deal) {
            id {
              value
            }
            unitPrice {
              unit
              value
            }
            expirationPriceDate {
              value
            }
            ingredient {
              value
            }
          }
        }
      `,
      variables: {
        updateDealId: id,
        deal: data,
      },
    });
  }

  delete(id) {
    return client.mutate({
      mutation: gql`
        mutation DeleteDeal($deleteDealId: String!) {
          deleteDeal(id: $deleteDealId)
        }
      `,
      variables: {
        deleteDealId: id,
      },
    });
  }

  deleteAll() {
    return client.mutate({
      mutation: gql`
        mutation DeleteDeal {
          deleteAllDeals
        }
      `,
    });
  }
}

export default new DealDataService();
