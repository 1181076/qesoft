import { gql } from "@apollo/client";
import http from "../http/http-sandwiches";
import client from "../utils/apolloClient";

class CategoryDataService {
  getAll() {
    return client.query({
      query: gql`
        query Query {
          categories {
            description
          }
        }
      `,
    });
  }

  get(id) {
    return client.query({
      query: gql`
        query Query($categoryId: String!) {
          category(id: $categoryId) {
            description
          }
        }
      `,
      variables: {
        categoryId: id,
      },
    });
  }

  create(data) {
    return client.mutate({
      mutation: gql`
        mutation Mutation($category: CategoryInput!) {
          createCategory(category: $category) {
            description {
              value
            }
          }
        }
      `,
      variables: {
        category: data,
      },
    });
  }

  delete(id) {
    return client.mutate({
      mutation: gql`
        mutation DeleteCategory($deleteCategoryId: String!) {
          deleteCategory(id: $deleteCategoryId)
        }
      `,
      variables: {
        deleteCategoryId: id,
      },
    });
  }

  deleteAll() {
    return client.mutate({
      mutation: gql`
        mutation Mutation {
          deleteAllCategories
        }
      `,
    });
  }
}

export default new CategoryDataService();
