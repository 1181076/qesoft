import { gql } from "@apollo/client";
import http from "../http/http-users";
import client from "../utils/apolloClient";

class OrderDataService {
  getAll() {
    return client.query({
      query: gql`
        query Query {
          orders {
            id
            day
            sandwichOrder {
              sandwich
              amount
            }
          }
        }
      `,
    });
  }

  get(id) {
    return client.query({
      query: gql`
        query Query($orderId: String!) {
          order(id: $orderId) {
            id
            day
            sandwichOrder {
              sandwich
              amount
            }
          }
        }
      `,
      variables: {
        orderId: id,
      },
    });
  }

  create(data) {
    return client.mutate({
      mutation: gql`
        mutation Mutation($order: OrderInput!) {
          createOrder(order: $order) {
            id {
              value
            }
            day {
              value
            }
            sandwichOrder {
              sandwich
              amount
            }
          }
        }
      `,
      variables: {
        order: data,
      },
    });
  }

  update(id, data) {
    return client.mutate({
      mutation: gql`
        mutation UpdateOrder($updateOrderId: String!, $order: OrderInput!) {
          updateOrder(id: $updateOrderId, order: $order) {
            id {
              value
            }
            day {
              value
            }
            sandwichOrder {
              sandwich
              amount
            }
          }
        }
      `,
      variables: {
        updateOrderId: id,
        order: data,
      },
    });
  }

  delete(id) {
    return client.mutate({
      mutation: gql`
        mutation DeleteOrder($deleteOrderId: String!) {
          deleteOrder(id: $deleteOrderId)
        }
      `,
      variables: {
        deleteOrderId: id,
      },
    });
  }

  deleteAll() {
    return client.mutate({
      mutation: gql`
        mutation Mutation {
          deleteAllOrders
        }
      `,
    });
  }

  getOrdersByDayInterval(startDay, endDay) {
    return http.get(`/orders/dayInterval/${startDay}/${endDay}`);
  }
}

export default new OrderDataService();
