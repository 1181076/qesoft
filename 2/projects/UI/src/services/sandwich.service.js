import { gql } from "@apollo/client";
import http from "../http/http-sandwiches";
import client from "../utils/apolloClient";

class IngredientDataService {
  getAll() {
    return client.query({
      query: gql`
        query Query {
          sandwiches {
            id
            shortDescription
            extendedDescription
            price {
              amount
              currency
            }
            ingredients {
              id
              description
              quantity
              lastUpdatedDate {
                amount
                date
              }
              upperLimit
              lowerLimit
              units
            }
            categories {
              description
            }
          }
        }
      `,
    });
  }

  get(id) {
    return client.query({
      query: gql`
        query Query($sandwichId: String!) {
          sandwich(id: $sandwichId) {
            id
            shortDescription
            extendedDescription
            price {
              amount
              currency
            }
            ingredients {
              id
              description
              quantity
              lastUpdatedDate {
                amount
                date
              }
              lowerLimit
              upperLimit
              units
            }
            categories {
              description
            }
          }
        }
      `,
      variables: {
        sandwichId: id,
      },
    });
  }

  create(data) {
    console.log(data);

    return client.mutate({
      mutation: gql`
        mutation Mutation($sandwich: SandwichInput!) {
          createSandwich(sandwich: $sandwich) {
            id {
              value
            }
          }
        }
      `,
      variables: {
        sandwich: data,
      },
    });
  }

  update(id, data) {
    return client.mutate({
      mutation: gql`
        mutation Mutation(
          $updateSandwichId: String!
          $sandwich: SandwichInput!
        ) {
          updateSandwich(id: $updateSandwichId, sandwich: $sandwich) {
            id {
              value
            }
          }
        }
      `,
      variables: {
        updateSandwichId: id,
        sandwich: data,
      },
    });
  }

  delete(id) {
    return client.mutate({
      mutation: gql`
        mutation Mutation($deleteSandwichId: String!) {
          deleteSandwich(id: $deleteSandwichId)
        }
      `,
      variables: {
        deleteSandwichId: id,
      },
    });
  }

  deleteAll() {
    return client.mutate({
      mutation: gql`
        mutation Mutation {
          deleteAllSandwiches
        }
      `,
    });
  }

  getSandwichesByPriceInterval(currency, minimumPrice, maximumPrice) {
    return http.get(
      `/sandwiches/priceFilter/${currency}/${minimumPrice}/${maximumPrice}`
    );
  }

  getSandwichesByIngredientList(list) {
    return http.get(`/sandwiches/ingredientsFilter/${list}`);
  }
}

export default new IngredientDataService();
