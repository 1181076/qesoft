import { gql } from "@apollo/client";
import http from "../http/http-ingredients";
import client from "../utils/apolloClient";

class IngredientDataService {
  getAll() {
    return client.query({
      query: gql`
        query Query {
          ingredients {
            id
            description
            quantity
            lastUpdatedDate {
              amount
              date
            }
            upperLimit
            lowerLimit
            units
          }
        }
      `,
    });
  }

  get(id) {
    return client.query({
      query: gql`
        query Query($ingredientId: String!) {
          ingredient(id: $ingredientId) {
            id
            description
            quantity
            lastUpdatedDate {
              amount
              date
            }
            upperLimit
            lowerLimit
            units
          }
        }
      `,
      variables: {
        ingredientId: id,
      },
    });
  }

  create(data) {
    return client.mutate({
      mutation: gql`
        mutation Mutation($ingredient: IngredientInput!) {
          createIngredient(ingredient: $ingredient) {
            id {
              value
            }
          }
        }
      `,
      variables: {
        ingredient: data,
      },
    });
  }

  update(id, data) {
    return client.mutate({
      mutation: gql`
        mutation Mutation(
          $updateIngredientId: String!
          $ingredient: IngredientInput!
        ) {
          updateIngredient(id: $updateIngredientId, ingredient: $ingredient) {
            id {
              value
            }
          }
        }
      `,
      variables: {
        updateIngredientId: id,
        ingredient: data,
      },
    });
  }

  delete(id) {
    return client.mutate({
      mutation: gql`
        mutation DeleteIngredient($deleteIngredientId: String!) {
          deleteIngredient(id: $deleteIngredientId)
        }
      `,
      variables: {
        deleteIngredientId: id,
      },
    });
  }

  deleteAll() {
    return client.mutate({
      mutation: gql`
        mutation Mutation {
          deleteAllIngredients
        }
      `,
    });
  }

  getStockInterval(id, startDate, endDate) {
    return http.get(`/ingredients/stockChange/${id}/${startDate}/${endDate}`);
  }

  // findByTitle(title) {
  //   return http.get(`/ingredients?title=${title}`);
  // }
}

export default new IngredientDataService();
