import React, { Component } from "react";
import ImportDataService from "../../services/import.service";
import { DropzoneDialog } from 'material-ui-dropzone';
import { AiFillFileAdd } from "react-icons/ai";
import { Button } from "react-bootstrap";

export default class Import extends Component {
    constructor(props) {
        super(props);

        this.handleOpen = this.handleOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleSave = this.handleSave.bind(this);

        this.state = {
            open: false,
            file: null,
        };
    }

    handleClose() {
        this.setState({
            open: false
        });
    }

    async handleSave(file) {
        this.setState({
            file: file,
            open: false
        });
        // const fileTmp = await file[0].text();
        // console.log(fileTmp);
        ImportDataService.createFromFile(file[0]);
    }

    handleOpen() {
        this.setState({
            open: true,
        });
    }


    render() {
        return (<div>
            <br></br><br></br><br></br>
            <h2 style={{marginLeft: '30%'}}>Import entities from a csv file:</h2>
            <br></br>
            <Button variant="contained" color="primary" style={{ background: '#9d9ea3', color: "#FFF", textTransform: 'none', marginLeft: '45%' }} size="small" onClick={this.handleOpen.bind(this)}>
                Import File
            </Button>
            <DropzoneDialog
                Icon={AiFillFileAdd}
                dialogTitle={"Provide CSV file"}
                acceptedFiles={[".CSV", ".csv"]}
                cancelButtonText={"cancel"}
                submitButtonText={"submit"}
                filesLimit={1}
                open={this.state.open}
                onClose={this.handleClose.bind(this)}
                onSave={this.handleSave.bind(this)}
                useChipsForPreview
                showFileNamesInPreview={true}
            />
            <br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
        </div>
        );
    }
}


