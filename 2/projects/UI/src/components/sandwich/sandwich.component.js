import React, { Component } from "react";
import SandwichDataService from "../../services/sandwich.service";
import IngredientsService from "../../services/ingredient.service";
import CategoriesService from "../../services/category.service";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import Autocomplete from "@material-ui/lab/Autocomplete";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

export default class Sandwich extends Component {
  constructor(props) {
    super(props);
    this.onChangeShortDescription = this.onChangeShortDescription.bind(this);
    this.onChangeExtendedDescription =
      this.onChangeExtendedDescription.bind(this);
    this.onChangePriceAmount = this.onChangePriceAmount.bind(this);
    this.onChangePriceCurrency = this.onChangePriceCurrency.bind(this);
    this.handleAddIngredients = this.handleAddIngredients.bind(this);
    this.handleRemoveIngredients = this.handleRemoveIngredients.bind(this);
    this.handleAddCategories = this.handleAddCategories.bind(this);
    this.handleRemoveCategories = this.handleRemoveCategories.bind(this);
    // this.retrieveSuppliers = this.retrieveSuppliers.bind(this);
    this.onChangeTempIngredients = this.onChangeTempIngredients.bind(this);
    this.onChangeTempCategories = this.onChangeTempCategories.bind(this);
    this.updateSandwich = this.updateSandwich.bind(this);
    this.deleteSandwich = this.deleteSandwich.bind(this);
    this.getIngredients = this.getIngredients.bind(this);
    this.getSandwich = this.getSandwich.bind(this);

    this.state = {
      currentSandwich: {
        id: null,
        shortDescription: null,
        extendedDescription: null,
        price: null,
        ingredients: [],
        categories: [],
      },

      tempIngredients: "",
      tempCategories: "",

      submitted: false,

      styles: (theme) => ({
        paper: {
          maxWidth: 936,
          margin: "auto",
          overflow: "hidden",
        },
        searchBar: {
          borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
        },
        searchInput: {
          fontSize: theme.typography.fontSize,
        },
        block: {
          display: "block",
        },
        addUser: {
          marginRight: theme.spacing(1),
        },
        contentWrapper: {
          margin: "40px 16px",
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 200,
        },
        selectEmpty: {
          marginTop: theme.spacing(2),
        },
        formControl: {
          margin: theme.spacing(1),
          minWidth: 120,
        },
        root: {
          display: "flex",
          flexWrap: "wrap",
          background: "#eaeff1",
        },
      }),

      availableIngredients: [],

      tags: null,
      error: undefined,
    };
  }

  componentDidMount() {
    this.retrieveIngredients();
    this.retrieveCategories();
    this.getSandwich(this.props.match.params.id);
  }

  retrieveIngredients() {
    IngredientsService.getAll()
      .then((response) => {
        this.setState({
          availableIngredients: response.data.sandwiches,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  retrieveCategories() {
    CategoriesService.getAll()
      .then((response) => {
        this.setState({
          availableCategories: response.data.categories,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  onChangeShortDescription = (e) => {
    const description = e.target.value;

    this.setState(function (prevState) {
      return {
        currentSandwich: {
          ...prevState.currentSandwich,
          shortDescription: { value: description },
        },
      };
    });
  };

  onChangeExtendedDescription = (e) => {
    const description = e.target.value;

    this.setState(function (prevState) {
      return {
        currentSandwich: {
          ...prevState.currentSandwich,
          extendedDescription: { value: description },
        },
      };
    });
  };

  onChangePriceAmount = (e) => {
    const amount = e.target.value;

    this.setState(function (prevState) {
      return {
        currentSandwich: {
          ...prevState.currentSandwich,
          price: { ...prevState.currentSandwich.price, amount },
        },
      };
    });
  };

  onChangePriceCurrency = (e) => {
    const currency = e.target.value;

    this.setState(function (prevState) {
      return {
        currentSandwich: {
          ...prevState.currentSandwich,
          price: { ...prevState.currentSandwich.price, currency },
        },
      };
    });
  };

  handleAddIngredients = () => {
    this.setState(function (prevState) {
      return {
        currentSandwich: {
          ...prevState.currentSandwich,
          ingredients: prevState.currentSandwich.ingredients.concat([
            { id: this.state.tempIngredients.id },
          ]),
        },
        tempSelf: false,
      };
    });

    document.getElementById("suppliersss").innerHTML +=
      "<p> ➤ " + this.state.tempIngredients.description + "</p>";
  };

  handleRemoveIngredients = () => {
    this.setState(function (prevState) {
      ingredients: prevState.currentSandwich.ingredients.pop();
    });

    var a = document.getElementById("suppliersss").innerHTML;
    var b = a.length - 4;
    var c = a.charAt(b);

    while (c != "/" && b != 0) {
      b--;
      c = a.charAt(b);
    }

    if (b != 0) {
      b = b + 3;
    }

    var d = a.substring(0, b);

    document.getElementById("suppliersss").innerHTML = d;
  };

  onChangeTempIngredients = (event, values) => {
    this.setState(function (prevState) {
      return {
        tempIngredients: values,
      };
    });
  };

  handleAddCategories = () => {
    this.setState(function (prevState) {
      return {
        currentSandwich: {
          ...prevState.currentSandwich,
          categories: prevState.currentSandwich.categories.concat([
            { description: { value: this.state.tempCategories.description } },
          ]),
        },
      };
    });

    document.getElementById("categoriesss").innerHTML +=
      "<p> ➤ " + this.state.tempCategories.description.value + "</p>";
  };

  handleRemoveCategories = () => {
    this.setState(function (prevState) {
      categories: prevState.currentSandwich.categories.pop();
    });

    var a = document.getElementById("categoriesss").innerHTML;
    var b = a.length - 4;
    var c = a.charAt(b);

    while (c != "/" && b != 0) {
      b--;
      c = a.charAt(b);
    }

    if (b != 0) {
      b = b + 3;
    }

    var d = a.substring(0, b);

    document.getElementById("categoriesss").innerHTML = d;
  };

  onChangeTempCategories = (event, values) => {
    this.setState(function (prevState) {
      return {
        tempCategories: values,
      };
    });
  };

  getSandwich(id) {
    SandwichDataService.get(id)
      .then((response) => {
        this.setState({
          currentSandwich: {
            id: { value: response.data.sandwich.id },
            shortDescription: {
              value: response.data.sandwich.shortDescription,
            },
            extendedDescription: {
              value: response.data.sandwich.extendedDescription,
            },
            price: response.data.sandwich.price,
          },
        });
        var ingredients = response.data.sandwich.ingredients;
        var categories = response.data.sandwich.categories;
        this.getIngredients(ingredients);
        this.getCategories(categories);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  postSandwich(data) {
    try {
      SandwichDataService.create(data);
    } catch (err) {
      console.log(err);
    }
  }

  getIngredients(ingredients) {
    var ings = [];

    var i,
      j,
      s = "";

    for (i = 0; i < ingredients.length; i++) {
      s += "<p> ➤ " + ingredients[i].description + "</p>";

      ings.concat({ id: ingredients[i].id });
    }
    this.setState({
      currentSandwich: { ingredients: ings },
    });
    document.getElementById("suppliersss").innerHTML = s;
  }

  getCategories(categories) {
    var cats = [];

    var i,
      j,
      s = "";
    for (i = 0; i < categories.length; i++) {
      s += "<p> ➤ " + categories[i].description + "</p>";

      cats.concat({ description: categories[i].description });
    }
    this.setState({
      currentSandwich: { categories: cats },
    });
    document.getElementById("categoriesss").innerHTML = s;
  }

  updateSandwich() {
    SandwichDataService.update(
      this.state.currentSandwich.id.value,
      this.state.currentSandwich
    )
      .then(() => {
        this.setState({
          error: null,
        });

        this.props.history.push("/sandwiches");
      })
      .catch((error) => {
        this.setState({
          error: error.response.data.message,
        });
      });
  }

  deleteSandwich() {
    SandwichDataService.delete(this.state.currentSandwich.id.value)
      .then(() => {
        this.props.history.push("/sandwiches");
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const { currentSandwich } = this.state;

    return (
      <div>
        {currentSandwich ? (
          <div className={this.state.styles.root}>
            <link
              rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            />
            <link
              rel="stylesheet"
              href="https://fonts.googleapis.com/icon?family=Material+Icons"
            />
            <br></br>
            <br></br>
            <h4 id="editCPTitle">Edit Sandwich</h4>

            <div>
              <div>
                <label id="label1" htmlFor="shortDescription">
                  Short Description
                </label>
                <TextField
                  id="presentationTitle"
                  variant="outlined"
                  fullWidth
                  value={
                    currentSandwich.shortDescription != null
                      ? currentSandwich.shortDescription.value
                      : ""
                  }
                  style={{ background: "#e9f2e9" }}
                  onChange={this.onChangeShortDescription}
                />
              </div>
              <br></br>
              <div>
                <label id="label2" htmlFor="extendedDescription">
                  Extended Description
                </label>
                <TextField
                  id="presentationTitle"
                  variant="outlined"
                  fullWidth
                  value={
                    currentSandwich.extendedDescription != null
                      ? currentSandwich.extendedDescription.value
                      : ""
                  }
                  style={{ background: "#e9f2e9" }}
                  onChange={this.onChangeExtendedDescription}
                />
              </div>
              <br></br>
              <div>
                <br></br>
                <label id="label3" htmlFor="amount">
                  Price Amount
                </label>
                <TextField
                  id="presentationTitle"
                  variant="outlined"
                  fullWidth
                  value={
                    currentSandwich.price != null
                      ? currentSandwich.price.amount
                      : ""
                  }
                  style={{ background: "#e9f2e9" }}
                  onChange={this.onChangePriceAmount}
                />
                <br></br>
                <label id="label3" htmlFor="currency">
                  Price Currency
                </label>
                <Select
                  labelId="labelCurrencyFilter"
                  id="CurrencyFilter"
                  variant="outlined"
                  fullWidth
                  value={
                    currentSandwich.price != null
                      ? currentSandwich.price.currency
                      : ""
                  }
                  onChange={this.onChangePriceCurrency}
                  style={{ background: "#e9f2e9" }}
                >
                  <MenuItem id="Euro" value="Euro">
                    Euro
                  </MenuItem>
                  <MenuItem id="US Dollar" value="US Dollar">
                    US Dollar
                  </MenuItem>
                  <MenuItem id="Australian Dollar" value="Australian Dolla">
                    Australian Dolla
                  </MenuItem>
                  <MenuItem id="Canadian Dollar" value="Canadian Dollar">
                    Canadian Dollar
                  </MenuItem>
                  <MenuItem id="Pound" value="Pound">
                    Pound
                  </MenuItem>
                  <MenuItem id="Yen" value="Yen">
                    Yen
                  </MenuItem>
                  <MenuItem id="Franc" value="Franc">
                    Franc
                  </MenuItem>
                  <MenuItem id="Won" value="Won">
                    Won
                  </MenuItem>
                  <MenuItem id="Peso" value="Peso">
                    Peso
                  </MenuItem>
                  <MenuItem id="Rupee" value="Rupee">
                    Rupee
                  </MenuItem>
                </Select>
              </div>
              <br></br>
              <div>
                <label id="label10" htmlFor="ingredients">
                  Ingredients
                </label>
                <div id="suppliersss" />

                <Autocomplete
                  id="free-solo-demo"
                  onChange={this.onChangeTempIngredients}
                  options={this.state.availableIngredients}
                  getOptionLabel={(option) => option.description}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      style={{ background: "#e9f2e9" }}
                      label="Add"
                      margin="normal"
                      variant="outlined"
                    />
                  )}
                />

                <Button
                  id="ingredientsAdd"
                  variant="contained"
                  style={{ background: "#20428c", color: "#FFF" }}
                  size="small"
                  className={this.state.styles.addUser}
                  onClick={this.handleAddIngredients}
                  className="small"
                >
                  Add Ingredient
                </Button>

                <IconButton
                  aria-label="delete"
                  style={{ color: "#e6556f" }}
                  onClick={this.handleRemoveIngredients}
                >
                  <DeleteIcon />
                </IconButton>
              </div>
              <br></br>
              <div>
                <label id="label7" htmlFor="suppliers">
                  Categories
                </label>
                <div id="categoriesss" />

                <Autocomplete
                  id="free-solo-demo"
                  onChange={this.onChangeTempCategories}
                  options={this.state.availableCategories}
                  getOptionLabel={(option) => option.description}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      style={{ background: "#e9f2e9" }}
                      label="Add"
                      margin="normal"
                      variant="outlined"
                    />
                  )}
                />

                <Button
                  id="categAdd"
                  variant="contained"
                  style={{ background: "#20428c", color: "#FFF" }}
                  size="small"
                  className={this.state.styles.addUser}
                  onClick={this.handleAddCategories}
                  className="small"
                >
                  Add Category
                </Button>

                <IconButton
                  aria-label="delete"
                  style={{ color: "#e6556f" }}
                  onClick={this.handleRemoveCategories}
                >
                  <DeleteIcon />
                </IconButton>
              </div>
              <br></br>
              <Button
                id="upd"
                variant="contained"
                style={{ background: "#a6ce39", color: "#FFF" }}
                size="small"
                className={this.state.styles.addUser}
                onClick={this.updateSandwich}
                disabled={
                  this.state.currentSandwich.id === "" ||
                  this.state.currentSandwich.shortDescription === "" ||
                  this.state.currentSandwich.extendedDescription === "" ||
                  this.state.currentSandwich.amount === null ||
                  this.state.currentSandwich.currency === null ||
                  this.state.currentSandwich.ingredients.length === 0
                }
              >
                Update
              </Button>
              &nbsp;
              <Button
                id="del"
                variant="contained"
                style={{ background: "#c94254", color: "#FFF" }}
                size="small"
                className={this.state.styles.addUser}
                onClick={this.deleteSandwich}
              >
                Delete
              </Button>
              <br></br>
            </div>

            <br></br>

            {this.state.error !== undefined ? (
              <p
                style={{
                  color: this.state.error !== null ? "red" : "green",
                }}
              >
                {this.state.error !== null
                  ? this.state.error
                  : "Sandwich Updated!"}
              </p>
            ) : undefined}
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on an Sandwich...</p>
          </div>
        )}
      </div>
    );
  }
}
