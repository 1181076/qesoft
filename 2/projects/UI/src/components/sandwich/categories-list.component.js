import React, { Component } from "react";
import CategoryDataService from "../../services/category.service";

import { Link } from "react-router-dom";

import ButtonGroup from "@material-ui/core/ButtonGroup";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import RefreshIcon from "@material-ui/icons/Refresh";
import DeleteIcon from "@material-ui/icons/Delete";

export default class CategoriesList extends Component {
  constructor(props) {
    super(props);
    this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this);
    this.retrieveCategories = this.retrieveCategories.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveCategory = this.setActiveCategory.bind(this);
    this.removeAllCategories = this.removeAllCategories.bind(this);
    this.deleteCategory = this.deleteCategory.bind(this);

    this.state = {
      categories: [],
      currentCategory: null,
      currentIndex: -1,
      searchTitle: "",

      styles: (theme) => ({
        paper: {
          maxWidth: 936,
          margin: "auto",
          overflow: "hidden",
        },
        searchBar: {
          borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
        },
        searchInput: {
          fontSize: theme.typography.fontSize,
        },
        block: {
          display: "block",
        },
        addUser: {
          marginRight: theme.spacing(1),
          textTransform: "none",
        },
        contentWrapper: {
          margin: "40px 16px",
        },
      }),
    };

    this.MyLink = (props) => <Link to="/addCategory" {...props} />;
  }

  componentDidMount() {
    this.retrieveCategories();
  }

  onChangeSearchTitle(e) {
    const searchTitle = e.target.value;

    this.setState({
      searchTitle: searchTitle,
    });
  }

  retrieveCategories() {
    CategoryDataService.getAll()
      .then((response) => {
        this.setState({
          categories: response.data.categories,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  refreshList() {
    this.retrieveCategories();
    this.setState({
      currentCategory: null,
      currentIndex: -1,
    });
  }

  setActiveCategory(category, index) {
    this.setState({
      currentCategory: category,
      currentIndex: index,
    });
  }

  deactiveCategory() {
    this.setState({
      currentCategory: null,
      currentIndex: -1,
    });

    CategoryDataService.getAll()
      .then((response) => {
        this.setState({
          categories: response.data,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  removeAllCategories() {
    CategoryDataService.deleteAll()
      .then((response) => {
        this.refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  }

  deleteCategory() {
    CategoryDataService.delete(this.state.currentCategory.description)
      .then(() => {
        this.refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  }

  searchTitle() {
    // this.setState({
    //   currentCategory: null,
    //   currentIndex: -1
    // });
    // CategoryDataService.findByTitle(this.state.searchTitle)
    //   .then(response => {
    //     this.setState({
    //       categories: response.data
    //     });
    //   })
    //   .catch(e => {
    //     console.log(e);
    //   });
  }

  render() {
    const { searchTitle, categories, currentCategory, currentIndex } =
      this.state;

    return (
      <div className="list row">
        <br></br>
        <br></br>

        <div className="col-md-8">
          <br></br>
          <br></br>

          <h4 id="listCategoriesTitle"> Categories List</h4>

          <br></br>

          <Paper className={this.state.styles.paper}>
            <AppBar
              className={this.state.styles.searchBar}
              position="static"
              color="default"
              elevation={0}
            >
              <Toolbar>
                <Grid container spacing={2} alignItems="center">
                  <Grid item>
                    <IconButton>
                      <SearchIcon
                        className={this.state.styles.block}
                        color="inherit"
                        onClick={this.searchTitle}
                      />
                    </IconButton>
                  </Grid>
                  <Grid item xs>
                    <TextField
                      fullWidth
                      placeholder="Search"
                      InputProps={{
                        disableUnderline: true,
                        className: this.state.styles.searchInput,
                      }}
                      onChange={this.onChangeSearchTitle}
                    />
                  </Grid>
                  <Grid item>
                    <Tooltip title="Reload">
                      <IconButton>
                        <RefreshIcon
                          className={this.state.styles.block}
                          color="inherit"
                          onClick={() => this.setActiveCategory(null, -1)}
                        />
                      </IconButton>
                    </Tooltip>
                  </Grid>
                </Grid>
              </Toolbar>
            </AppBar>
            <AppBar
              className={this.state.styles.searchBar}
              position="static"
              style={{ background: "#EFECEC", color: "black" }}
              elevation={0}
            ></AppBar>
          </Paper>
          <br></br>
        </div>

        <div className="col-md-6">
          <ul className="list-group">
            {categories &&
              categories.map((category, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveCategory(category, index)}
                  key={index}
                >
                  {category.description}
                </li>
              ))}
          </ul>

          <br></br>
          <Button
            id="add"
            variant="contained"
            style={{
              background: "#20428c",
              color: "#FFF",
              textTransform: "none",
            }}
            size="small"
            className={this.state.styles.addUser}
            component={this.MyLink}
          >
            Add Category
          </Button>

          <Button
            id="removeAll"
            variant="contained"
            style={{
              background: "#e6556f",
              color: "#FFF",
              textTransform: "none",
            }}
            size="small"
            className={this.state.styles.addUser}
            startIcon={<DeleteIcon />}
            onClick={this.removeAllCategories}
          >
            Remove All
          </Button>

          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>

          <br></br>

          <br></br>
        </div>
        <div className="col-md-6">
          {currentCategory ? (
            <div>
              <h4>Category</h4>
              <div id="shortDescription">
                <label>
                  <strong>Description:</strong>
                </label>{" "}
                {/* <br></br> */}
                {currentCategory.description != null
                  ? currentCategory.description
                  : "-----"}
              </div>

              <hr></hr>
              <br></br>

              <ButtonGroup variant="contained">
                <Button
                  style={{
                    background: "#e6556f",
                    color: "#FFF",
                    textTransform: "none",
                  }}
                  onClick={this.deleteCategory}
                >
                  Delete
                </Button>
              </ButtonGroup>
            </div>
          ) : (
            <div>
              <br />
            </div>
          )}
        </div>
      </div>
    );
  }
}
