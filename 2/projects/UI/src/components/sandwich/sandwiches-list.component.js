import React, { Component } from "react";
import SandwichDataService from "../../services/sandwich.service";

import { Link } from "react-router-dom";

import ButtonGroup from "@material-ui/core/ButtonGroup";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import RefreshIcon from "@material-ui/icons/Refresh";
import DeleteIcon from "@material-ui/icons/Delete";

import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";

import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

import IngredientsService from "../../services/ingredient.service";
import Autocomplete from "@material-ui/lab/Autocomplete";

export default class SandwichesList extends Component {
  constructor(props) {
    super(props);
    this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this);
    this.retrieveSandwiches = this.retrieveSandwiches.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveSandwich = this.setActiveSandwich.bind(this);
    this.removeAllSandwiches = this.removeAllSandwiches.bind(this);

    this.priceFilterClose = this.priceFilterClose.bind(this);
    this.priceFilterOpen = this.priceFilterOpen.bind(this);
    this.filterPrices = this.filterPrices.bind(this);

    this.ingredientsFilterClose = this.ingredientsFilterClose.bind(this);
    this.ingredientsFilterOpen = this.ingredientsFilterOpen.bind(this);
    this.filterIngredients = this.filterIngredients.bind(this);
    this.deactiveSandwich = this.deactiveSandwich.bind(this);

    this.state = {
      sandwiches: [],
      currentSandwich: null,
      currentIndex: -1,
      searchTitle: "",

      priceFilterOpen: false,
      filterCurrency: "",
      minimumPrice: 0,
      maximumPrice: 0,

      ingredientsFilterOpen: false,
      ingredientsFilter: [],
      ingredients: [],
      temp: "",

      categoryList: "",

      styles: (theme) => ({
        paper: {
          maxWidth: 936,
          margin: "auto",
          overflow: "hidden",
        },
        searchBar: {
          borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
        },
        searchInput: {
          fontSize: theme.typography.fontSize,
        },
        block: {
          display: "block",
        },
        addUser: {
          marginRight: theme.spacing(1),
          textTransform: "none",
        },
        contentWrapper: {
          margin: "40px 16px",
        },
      }),
    };

    this.MyLink = (props) => <Link to="/addSandwich" {...props} />;
  }

  componentDidMount() {
    this.retrieveSandwiches();
    this.retrieveIngredients();
  }

  onChangeSearchTitle(e) {
    const searchTitle = e.target.value;

    this.setState({
      searchTitle: searchTitle,
    });
  }

  retrieveSandwiches() {
    SandwichDataService.getAll()
      .then((response) => {
        console.log(response.data.sandwiches);
        this.setState({
          sandwiches: response.data.sandwiches,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  refreshList() {
    this.retrieveSandwiches();
    this.setState({
      currentSandwich: null,
      currentIndex: -1,
      categoryList: "",
    });
  }

  setActiveSandwich(sandwich, index) {
    var i;
    var list = "";
    for (i = 0; i < sandwich.categories.length - 1; i++) {
      list += sandwich.categories[i].description + ", ";
    }

    list += sandwich.categories[sandwich.categories.length - 1].description;

    this.setState({
      currentSandwich: sandwich,
      currentIndex: index,
      categoryList: list,
    });
  }

  deactiveSandwich() {
    this.setState({
      currentSandwich: null,
      currentIndex: -1,
      categoryList: "",
    });

    SandwichDataService.getAll()
      .then((response) => {
        this.setState({
          sandwiches: response.data,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  removeAllSandwiches() {
    SandwichDataService.deleteAll()
      .then((response) => {
        this.refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  }

  searchTitle() {
    // this.setState({
    //   currentSandwich: null,
    //   currentIndex: -1
    // });
    // SandwichDataService.findByTitle(this.state.searchTitle)
    //   .then(response => {
    //     this.setState({
    //       sandwiches: response.data
    //     });
    //   })
    //   .catch(e => {
    //     console.log(e);
    //   });
  }

  priceFilterOpen() {
    this.setState({
      priceFilterOpen: true,
    });
  }

  priceFilterClose() {
    this.setState({
      priceFilterOpen: false,
    });
  }

  onChangeFilterCurrency = (e) => {
    this.setState({
      filterCurrency: e.target.value,
    });
  };

  onChangeMaximumPrice = (e) => {
    this.setState({
      maximumPrice: e.target.value,
    });
  };

  onChangeMinimumPrice = (e) => {
    this.setState({
      minimumPrice: e.target.value,
    });
  };

  filterPrices() {
    this.setState({
      currentSandwich: null,
      currentIndex: -1,
    });

    SandwichDataService.getSandwichesByPriceInterval(
      this.state.filterCurrency,
      this.state.minimumPrice,
      this.state.maximumPrice
    )
      .then((response) => {
        this.setState({
          sandwiches: response.data,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  ingredientsFilterOpen() {
    this.setState({
      ingredientsFilterOpen: true,
    });
  }

  ingredientsFilterClose() {
    this.setState({
      ingredientsFilterOpen: false,
    });
  }

  retrieveIngredients() {
    IngredientsService.getAll()
      .then((response) => {
        this.setState({
          ingredients: response.data,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  handleAddIngredients = () => {
    this.setState({
      ingredientsFilter: this.state.ingredientsFilter.concat([
        this.state.temp.id,
      ]),
    });

    document.getElementById("ingredientsss").innerHTML +=
      "<p> ➤ " + this.state.temp.description + "</p>";
  };

  handleRemoveIngredients = () => {
    this.setState({
      ingredientsFilter: this.state.ingredientsFilter.splice(-1, 1),
    });

    var a = document.getElementById("ingredientsss").innerHTML;
    var b = a.length - 4;
    var c = a.charAt(b);

    while (c != "/" && b != 0) {
      b--;
      c = a.charAt(b);
    }

    if (b != 0) {
      b = b + 3;
    }

    var d = a.substring(0, b);

    document.getElementById("ingredientsss").innerHTML = d;
  };

  onChangeTempIngredients = (event, values) => {
    this.setState({
      temp: values,
    });
  };

  filterIngredients() {
    this.setState({
      currentSandwich: null,
      currentIndex: -1,
    });

    SandwichDataService.getSandwichesByIngredientList(
      this.state.ingredientsFilter
    )
      .then((response) => {
        this.setState({
          sandwiches: response.data,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const { searchTitle, sandwiches, currentSandwich, currentIndex } =
      this.state;

    return (
      <div className="list row">
        <br></br>
        <br></br>

        <div className="col-md-8">
          <br></br>
          <br></br>

          <h4 id="listSandwichesTitle"> Sandwiches List</h4>

          <br></br>

          <Paper className={this.state.styles.paper}>
            <AppBar
              className={this.state.styles.searchBar}
              position="static"
              color="default"
              elevation={0}
            >
              <Toolbar>
                <Grid container spacing={2} alignItems="center">
                  <Grid item>
                    <IconButton>
                      <SearchIcon
                        className={this.state.styles.block}
                        color="inherit"
                        onClick={this.searchTitle}
                      />
                    </IconButton>
                  </Grid>
                  <Grid item xs>
                    <TextField
                      fullWidth
                      placeholder="Search"
                      InputProps={{
                        disableUnderline: true,
                        className: this.state.styles.searchInput,
                      }}
                      onChange={this.onChangeSearchTitle}
                    />
                  </Grid>
                  <Grid item>
                    <Tooltip title="Reload">
                      <IconButton>
                        <RefreshIcon
                          className={this.state.styles.block}
                          color="inherit"
                          onClick={() => this.setActiveSandwich(null, -1)}
                        />
                      </IconButton>
                    </Tooltip>
                  </Grid>
                </Grid>
              </Toolbar>
            </AppBar>
            <AppBar
              className={this.state.styles.searchBar}
              position="static"
              style={{ background: "#EFECEC", color: "black" }}
              elevation={0}
            >
              <Toolbar>
                <Grid container spacing={2} alignItems="center">
                  Filters:
                  <Grid item>
                    <Button
                      id="filters"
                      size="small"
                      variant="contained"
                      style={{
                        textTransform: "none",
                      }}
                      onClick={this.ingredientsFilterOpen}
                    >
                      Ingredients
                    </Button>
                  </Grid>
                  <Grid item>
                    <Button
                      id="filters"
                      size="small"
                      variant="contained"
                      style={{
                        textTransform: "none",
                      }}
                      onClick={this.priceFilterOpen}
                    >
                      Price
                    </Button>
                  </Grid>
                  <Grid item>
                    <Button
                      id="filters"
                      size="small"
                      variant="contained"
                      style={{
                        background: "#C4C3C3",
                        textTransform: "none",
                      }}
                      onClick={this.deactiveSandwich}
                    >
                      Clear Filters
                    </Button>
                  </Grid>
                </Grid>
              </Toolbar>
            </AppBar>
          </Paper>
          <br></br>
        </div>

        <Dialog
          fullWidth={true}
          maxWidth={"sm"}
          open={this.state.priceFilterOpen}
          onClose={this.priceFilterClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogContent>
            <div style={{ border: "3px solid #fff", padding: "20px" }}>
              <h4>Set Price Interval Filter</h4>
              <br></br>

              <div className="form-group">
                <label id="labelCurrencyFilter" htmlFor="id">
                  Currency
                </label>
                <Select
                  labelId="labelCurrencyFilter"
                  id="CurrencyFilter"
                  variant="outlined"
                  fullWidth
                  value={this.state.filterCurrency}
                  onChange={this.onChangeFilterCurrency}
                  style={{ background: "#f0f2f7" }}
                >
                  <MenuItem id="Euro" value="Euro">
                    Euro
                  </MenuItem>
                  <MenuItem id="US Dollar" value="US Dollar">
                    US Dollar
                  </MenuItem>
                  <MenuItem id="Australian Dollar" value="Australian Dolla">
                    Australian Dolla
                  </MenuItem>
                  <MenuItem id="Canadian Dollar" value="Canadian Dollar">
                    Canadian Dollar
                  </MenuItem>
                  <MenuItem id="Pound" value="Pound">
                    Pound
                  </MenuItem>
                  <MenuItem id="Yen" value="Yen">
                    Yen
                  </MenuItem>
                  <MenuItem id="Franc" value="Franc">
                    Franc
                  </MenuItem>
                  <MenuItem id="Won" value="Won">
                    Won
                  </MenuItem>
                  <MenuItem id="Peso" value="Peso">
                    Peso
                  </MenuItem>
                  <MenuItem id="Rupee" value="Rupee">
                    Rupee
                  </MenuItem>
                </Select>
              </div>

              <br></br>

              <div className="form-group">
                <label id="labelminimumPrice" htmlFor="id">
                  Minimum
                </label>
                <TextField
                  id="minimumPrice"
                  variant="outlined"
                  fullWidth
                  value={this.state.minimumPrice}
                  style={{ background: "#f0f2f7" }}
                  onChange={this.onChangeMinimumPrice}
                />
              </div>
              <br></br>

              <div className="form-group">
                <label id="labelmaximumPrice" htmlFor="id">
                  Maximum
                </label>
                <TextField
                  id="maximumPrice"
                  variant="outlined"
                  fullWidth
                  value={this.state.maximumPrice}
                  style={{ background: "#f0f2f7" }}
                  onChange={this.onChangeMaximumPrice}
                />
              </div>

              <br></br>
              <ButtonGroup orientation="horizontal" variant="contained">
                <Button
                  id="apply"
                  style={{
                    background: "#c94254",
                    color: "#FFF",
                    textTransform: "none",
                  }}
                  onClick={this.filterPrices}
                >
                  {" "}
                  Apply Filter
                </Button>
                :
                <Button
                  id="cancel"
                  style={{
                    background: "gray",
                    color: "#FFF",
                    textTransform: "none",
                  }}
                  onClick={this.priceFilterClose}
                >
                  {" "}
                  Cancel
                </Button>
              </ButtonGroup>
            </div>
          </DialogContent>
        </Dialog>

        <Dialog
          fullWidth={true}
          maxWidth={"sm"}
          open={this.state.ingredientsFilterOpen}
          onClose={this.ingredientsFilterClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogContent>
            <div style={{ border: "3px solid #fff", padding: "20px" }}>
              <h4>Set Ingredient List Filter</h4>
              <br></br>
              Set ingredients that the desired sandwiches must contain.
              <br></br>
              <div className="form-group">
                <label id="labelIngredientsFilter" htmlFor="id">
                  Ingredients
                </label>

                <div id="ingredientsss" />

                <Autocomplete
                  id="free-solo-demo"
                  onChange={this.onChangeTempIngredients}
                  options={this.state.ingredients}
                  getOptionLabel={(option) => option.description}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      style={{ background: "#f0f2f7" }}
                      label="Add"
                      margin="normal"
                      variant="outlined"
                    />
                  )}
                />

                <Button
                  id="authorAdd"
                  variant="contained"
                  style={{
                    background: "#20428c",
                    color: "#FFF",
                    textTransform: "none",
                  }}
                  size="small"
                  className={this.state.styles.addUser}
                  onClick={this.handleAddIngredients}
                  className="small"
                >
                  Add Ingredient to List
                </Button>

                <IconButton
                  aria-label="delete"
                  style={{ color: "#e6556f" }}
                  onClick={this.handleRemoveIngredients}
                >
                  <DeleteIcon />
                </IconButton>
              </div>
              <br></br>
              <ButtonGroup orientation="horizontal" variant="contained">
                <Button
                  id="apply"
                  style={{
                    background: "#c94254",
                    color: "#FFF",
                    textTransform: "none",
                  }}
                  onClick={this.filterIngredients}
                >
                  {" "}
                  Apply Filter
                </Button>
                :
                <Button
                  id="cancel"
                  style={{
                    background: "gray",
                    color: "#FFF",
                    textTransform: "none",
                  }}
                  onClick={this.ingredientsFilterClose}
                >
                  {" "}
                  Cancel
                </Button>
              </ButtonGroup>
            </div>
          </DialogContent>
        </Dialog>

        <div className="col-md-6">
          <ul className="list-group">
            {sandwiches &&
              sandwiches.map((sandwich, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveSandwich(sandwich, index)}
                  key={index}
                >
                  {sandwich.shortDescription}
                </li>
              ))}
          </ul>

          <br></br>
          <Button
            id="add"
            variant="contained"
            style={{
              background: "#20428c",
              color: "#FFF",
              textTransform: "none",
            }}
            size="small"
            className={this.state.styles.addUser}
            component={this.MyLink}
          >
            Add Sandwich
          </Button>

          <Button
            id="removeAll"
            variant="contained"
            style={{
              background: "#e6556f",
              color: "#FFF",
              textTransform: "none",
            }}
            size="small"
            className={this.state.styles.addUser}
            startIcon={<DeleteIcon />}
            onClick={this.removeAllSandwiches}
          >
            Remove All
          </Button>

          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>

          <br></br>

          <br></br>
        </div>
        <div className="col-md-6">
          {currentSandwich ? (
            <div>
              <h4>Sandwich</h4>
              <div id="shortDescription">
                <label>
                  <strong>Short Description:</strong>
                </label>{" "}
                {/* <br></br> */}
                {currentSandwich.shortDescription != null
                  ? currentSandwich.shortDescription
                  : "-----"}
              </div>
              <div id="extendedDescription">
                <label>
                  <strong>Extended Description:</strong>
                </label>{" "}
                {currentSandwich.extendedDescription != null
                  ? currentSandwich.extendedDescription
                  : "-----"}
              </div>
              <div id="price">
                <label>
                  <strong>Price:</strong>
                </label>{" "}
                {currentSandwich.price != null
                  ? currentSandwich.price.amount +
                    " " +
                    currentSandwich.price.currency
                  : "-----"}
              </div>
              <div id="ingredients">
                <label>
                  <strong>Ingredients:</strong>
                </label>{" "}
                {currentSandwich.ingredients != null
                  ? currentSandwich.ingredients.length
                  : "-----"}
              </div>
              <div id="categories">
                <label>
                  <strong>Categories:</strong>
                </label>{" "}
                {currentSandwich.categories != null
                  ? this.state.categoryList
                  : "-----"}
              </div>

              <hr></hr>
              <br></br>

              <ButtonGroup variant="contained">
                <Button
                  style={{
                    background: "#ffc107",
                    color: "#FFF",
                    textTransform: "none",
                  }}
                >
                  <Link
                    id="edit"
                    className="badge badge-warning"
                    to={"/sandwiches/" + currentSandwich.id}
                  >
                    Edit
                  </Link>
                </Button>
              </ButtonGroup>
            </div>
          ) : (
            <div>
              <br />
            </div>
          )}
        </div>
      </div>
    );
  }
}
