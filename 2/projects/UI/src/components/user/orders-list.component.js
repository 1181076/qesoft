import React, { Component } from "react";
import OrderDataService from "../../services/order.service";

import { Link } from "react-router-dom";

import ButtonGroup from "@material-ui/core/ButtonGroup";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import RefreshIcon from "@material-ui/icons/Refresh";
import DeleteIcon from "@material-ui/icons/Delete";

import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";

import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

import SandwichesService from "../../services/sandwich.service";
import Autocomplete from "@material-ui/lab/Autocomplete";

export default class OrdersList extends Component {
  constructor(props) {
    super(props);
    this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this);
    this.retrieveOrders = this.retrieveOrders.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveOrder = this.setActiveOrder.bind(this);
    this.removeAllOrders = this.removeAllOrders.bind(this);

    this.dayFilterClose = this.dayFilterClose.bind(this);
    this.dayFilterOpen = this.dayFilterOpen.bind(this);
    this.filterDays = this.filterDays.bind(this);
    this.getSandwichOrders = this.getSandwichOrders.bind(this);
    this.deactiveOrder = this.deactiveOrder.bind(this);

    this.state = {
      orders: [],
      currentOrder: null,
      currentIndex: -1,
      searchTitle: "",

      orderPresentation: "",
      availableSandwiches: [],

      dayFilterOpen: false,
      startDay: null,
      endDay: null,

      styles: (theme) => ({
        paper: {
          maxWidth: 936,
          margin: "auto",
          overflow: "hidden",
        },
        searchBar: {
          borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
        },
        searchInput: {
          fontSize: theme.typography.fontSize,
        },
        block: {
          display: "block",
        },
        addUser: {
          marginRight: theme.spacing(1),
          textTransform: "none",
        },
        contentWrapper: {
          margin: "40px 16px",
        },
      }),
    };

    this.MyLink = (props) => <Link to="/addOrder" {...props} />;
  }

  componentDidMount() {
    this.retrieveOrders();
    this.retrieveSandwiches();
  }

  retrieveSandwiches() {
    SandwichesService.getAll()
      .then((response) => {
        this.setState({
          availableSandwiches: response.data.orders,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  onChangeSearchTitle(e) {
    const searchTitle = e.target.value;

    this.setState({
      searchTitle: searchTitle,
    });
  }

  retrieveOrders() {
    OrderDataService.getAll()
      .then((response) => {
        this.setState({
          orders: response.data.orders,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  getSandwichOrders() {
    var i,
      j,
      s = "";
    for (i = 0; i < this.state.currentOrder.sandwichOrders.length; i++) {
      for (j = 0; j < this.state.availableSandwiches.length; j++) {
        if (
          this.state.availableSandwiches[j].id ==
          this.state.currentOrder.sandwichOrders[i].sandwich
        ) {
          s +=
            "<p> ➤ " +
            this.state.currentOrder.sandwichOrders[i].amount +
            "->" +
            this.state.availableSandwiches[j].shortDescription +
            "</p>";
        }
      }
    }
    document.getElementById("ordersss").innerHTML = s;
  }

  refreshList() {
    this.retrieveOrders();
    this.setState({
      currentOrder: null,
      currentIndex: -1,
    });
  }

  setActiveOrder(order, index) {
    if (document.getElementById("ordersss") != null) {
      document.getElementById("ordersss").innerHTML = "";
    }

    this.setState({
      currentOrder: order,
      currentIndex: index,
    });
  }

  deactiveOrder() {
    document.getElementById("ordersss").innerHTML = "";
    this.setState({
      currentOrder: null,
      currentIndex: -1,
    });

    OrderDataService.getAll()
      .then((response) => {
        this.setState({
          orders: response.data,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  removeAllOrders() {
    OrderDataService.deleteAll()
      .then((response) => {
        this.refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  }

  searchTitle() {
    // this.setState({
    //   currentOrder: null,
    //   currentIndex: -1
    // });
    // OrderDataService.findByTitle(this.state.searchTitle)
    //   .then(response => {
    //     this.setState({
    //       orders: response.data
    //     });
    //   })
    //   .catch(e => {
    //     console.log(e);
    //   });
  }

  dayFilterOpen() {
    this.setState({
      dayFilterOpen: true,
    });
  }

  dayFilterClose() {
    this.setState({
      dayFilterOpen: false,
    });
  }

  onChangeFilterStartDay = (e) => {
    this.setState({
      startDay: e.target.value,
    });
  };

  onChangeFilterEndDay = (e) => {
    this.setState({
      endDay: e.target.value,
    });
  };

  filterDays() {
    this.setState({
      currentOrder: null,
      currentIndex: -1,
    });

    OrderDataService.getOrdersByDayInterval(
      this.state.startDay,
      this.state.endDay
    )
      .then((response) => {
        this.setState({
          orders: response.data,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  dayFilterOpen() {
    this.setState({
      dayFilterOpen: true,
    });
  }

  dayFilterClose() {
    this.setState({
      dayFilterOpen: false,
    });
  }

  render() {
    const { searchTitle, orders, currentOrder, currentIndex } = this.state;

    return (
      <div className="list row">
        <br></br>
        <br></br>

        <div className="col-md-8">
          <br></br>
          <br></br>

          <h4 id="listOrdersTitle"> Orders List</h4>

          <br></br>

          <Paper className={this.state.styles.paper}>
            <AppBar
              className={this.state.styles.searchBar}
              position="static"
              color="default"
              elevation={0}
            >
              <Toolbar>
                <Grid container spacing={2} alignItems="center">
                  <Grid item>
                    <IconButton>
                      <SearchIcon
                        className={this.state.styles.block}
                        color="inherit"
                        onClick={this.searchTitle}
                      />
                    </IconButton>
                  </Grid>
                  <Grid item xs>
                    <TextField
                      fullWidth
                      placeholder="Search"
                      InputProps={{
                        disableUnderline: true,
                        className: this.state.styles.searchInput,
                      }}
                      onChange={this.onChangeSearchTitle}
                    />
                  </Grid>
                  <Grid item>
                    <Tooltip title="Reload">
                      <IconButton>
                        <RefreshIcon
                          className={this.state.styles.block}
                          color="inherit"
                          onClick={() => this.setActiveOrder(null, -1)}
                        />
                      </IconButton>
                    </Tooltip>
                  </Grid>
                </Grid>
              </Toolbar>
            </AppBar>
            <AppBar
              className={this.state.styles.searchBar}
              position="static"
              style={{ background: "#EFECEC", color: "black" }}
              elevation={0}
            >
              <Toolbar>
                <Grid container spacing={2} alignItems="center">
                  Filters:
                  <Grid item>
                    <Button
                      id="filters"
                      size="small"
                      variant="contained"
                      style={{
                        textTransform: "none",
                      }}
                      onClick={this.dayFilterOpen}
                    >
                      Day
                    </Button>
                  </Grid>
                  <Grid item>
                    <Button
                      id="filters"
                      size="small"
                      variant="contained"
                      style={{
                        background: "#C4C3C3",
                        textTransform: "none",
                      }}
                      onClick={this.deactiveOrder}
                    >
                      Clear Filters
                    </Button>
                  </Grid>
                </Grid>
              </Toolbar>
            </AppBar>
          </Paper>
          <br></br>
        </div>

        <Dialog
          fullWidth={true}
          maxWidth={"sm"}
          open={this.state.dayFilterOpen}
          onClose={this.dayFilterClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogContent>
            <div style={{ border: "3px solid #fff", padding: "20px" }}>
              <h4>Set Day Interval Filter</h4>
              <br></br>

              <div className="form-group">
                <label id="label3" htmlFor="date">
                  Start Day
                </label>

                <br></br>
                <TextField
                  id="sday"
                  type="date"
                  variant="outlined"
                  style={{ background: "#e9f2e9" }}
                  className={this.state.styles.textField}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={this.onChangeFilterStartDay}
                />
              </div>
              <br></br>

              <div className="form-group">
                <label id="label3" htmlFor="date">
                  End Day
                </label>

                <br></br>
                <TextField
                  id="eday"
                  type="date"
                  variant="outlined"
                  style={{ background: "#e9f2e9" }}
                  className={this.state.styles.textField}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={this.onChangeFilterEndDay}
                />
              </div>

              <br></br>
              <ButtonGroup orientation="horizontal" variant="contained">
                <Button
                  id="apply"
                  style={{
                    background: "#c94254",
                    color: "#FFF",
                    textTransform: "none",
                  }}
                  onClick={this.filterDays}
                >
                  {" "}
                  Apply Filter
                </Button>
                :
                <Button
                  id="cancel"
                  style={{
                    background: "gray",
                    color: "#FFF",
                    textTransform: "none",
                  }}
                  onClick={this.dayFilterClose}
                >
                  {" "}
                  Cancel
                </Button>
              </ButtonGroup>
            </div>
          </DialogContent>
        </Dialog>

        <div className="col-md-6">
          <ul className="list-group">
            {orders &&
              orders.map((order, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveOrder(order, index)}
                  key={index}
                >
                  {order.id}
                </li>
              ))}
          </ul>

          <br></br>
          <Button
            id="add"
            variant="contained"
            style={{
              background: "#20428c",
              color: "#FFF",
              textTransform: "none",
            }}
            size="small"
            className={this.state.styles.addUser}
            component={this.MyLink}
          >
            Add Order
          </Button>

          <Button
            id="removeAll"
            variant="contained"
            style={{
              background: "#e6556f",
              color: "#FFF",
              textTransform: "none",
            }}
            size="small"
            className={this.state.styles.addUser}
            startIcon={<DeleteIcon />}
            onClick={this.removeAllOrders}
          >
            Remove All
          </Button>

          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>

          <br></br>

          <br></br>
        </div>
        <div className="col-md-6">
          {currentOrder ? (
            <div>
              <h4>Order</h4>
              <div id="day">
                <label>
                  <strong>Day:</strong>
                </label>{" "}
                {/* <br></br> */}
                {currentOrder.day != null ? currentOrder.day : "-----"}
              </div>
              <div id="sandwichOrders">
                <div>
                  <Button
                    id="filters"
                    size="small"
                    variant="contained"
                    style={{
                      textTransform: "none",
                    }}
                    onClick={this.getSandwichOrders}
                  >
                    Show Sandwiches
                  </Button>
                </div>

                <br></br>

                <div id="ordersss"></div>
              </div>

              <hr></hr>
              <br></br>

              <ButtonGroup variant="contained">
                <Button
                  style={{
                    background: "#ffc107",
                    color: "#FFF",
                    textTransform: "none",
                  }}
                >
                  <Link
                    id="edit"
                    className="badge badge-warning"
                    to={"/orders/" + currentOrder.id}
                  >
                    Edit
                  </Link>
                </Button>
              </ButtonGroup>
            </div>
          ) : (
            <div>
              <br />
            </div>
          )}
        </div>
      </div>
    );
  }
}
