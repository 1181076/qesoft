import React, { Component } from "react";
import OrderDataService from "../../services/order.service";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import { CallMerge } from "@material-ui/icons";
import Autocomplete from "@material-ui/lab/Autocomplete";
import SandwichesService from "../../services/sandwich.service";

export default class Order extends Component {
  constructor(props) {
    super(props);
    this.onChangeDay = this.onChangeDay.bind(this);
    this.handleAddSandwichOrders = this.handleAddSandwichOrders.bind(this);
    this.handleRemoveSandwichOrders =
      this.handleRemoveSandwichOrders.bind(this);
    // this.retrieveSuppliers = this.retrieveSuppliers.bind(this);
    this.onChangeTempSandwiches = this.onChangeTempSandwiches.bind(this);
    this.onChangeTempAmounts = this.onChangeTempAmounts.bind(this);
    this.updateOrder = this.updateOrder.bind(this);
    this.deleteOrder = this.deleteOrder.bind(this);
    this.getSandwichOrders = this.getSandwichOrders.bind(this);
    this.getOrder = this.getOrder.bind(this);

    this.state = {
      currentOrder: {
        id: "",
        day: null,
        sandwichOrders: [],
      },

      tempSandwiches: "",
      tempAmounts: 0,

      submitted: false,

      styles: (theme) => ({
        paper: {
          maxWidth: 936,
          margin: "auto",
          overflow: "hidden",
        },
        searchBar: {
          borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
        },
        searchInput: {
          fontSize: theme.typography.fontSize,
        },
        block: {
          display: "block",
        },
        addUser: {
          marginRight: theme.spacing(1),
        },
        contentWrapper: {
          margin: "40px 16px",
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 200,
        },
        selectEmpty: {
          marginTop: theme.spacing(2),
        },
        formControl: {
          margin: theme.spacing(1),
          minWidth: 120,
        },
        root: {
          display: "flex",
          flexWrap: "wrap",
          background: "#eaeff1",
        },
      }),

      availableSandwiches: [],

      tags: null,
      error: undefined,
    };
  }

  componentDidMount() {
    this.retrieveSandwiches();
    this.getOrder(this.props.match.params.id);
  }

  retrieveSandwiches() {
    SandwichesService.getAll()
      .then((response) => {
        this.setState({
          availableSandwiches: response.data,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  onChangeDay = (e) => {
    const day = e.target.value;

    this.setState(function (prevState) {
      return {
        currentOrder: {
          ...prevState.currentOrder,
          day: { value: day },
        },
      };
    });
  };

  handleAddSandwichOrders = () => {
    this.setState(function (prevState) {
      return {
        currentOrder: {
          ...prevState.currentOrder,
          sandwichOrders: prevState.currentOrder.sandwichOrders.concat([
            {
              sandwich: this.state.tempSandwiches.id,
              amount: this.state.tempAmounts,
            },
          ]),
        },
      };
    });

    document.getElementById("ordersss").innerHTML +=
      "<p> ➤ " +
      this.state.tempAmounts +
      " -> " +
      this.state.tempSandwiches.shortDescription +
      "</p>";
  };

  handleRemoveSandwichOrders = () => {
    this.setState(function (prevState) {
      return {
        currentOrder: {
          ...prevState.currentOrder,
          sandwichOrders: prevState.currentOrder.sandwichOrders.splice(-1, 1),
        },
      };
    });

    var a = document.getElementById("ordersss").innerHTML;
    var b = a.length - 4;
    var c = a.charAt(b);

    while (c != "/" && b != 0) {
      b--;
      c = a.charAt(b);
    }

    if (b != 0) {
      b = b + 3;
    }

    var d = a.substring(0, b);

    document.getElementById("ordersss").innerHTML = d;
  };

  onChangeTempSandwiches = (event, values) => {
    this.setState(function (prevState) {
      return {
        tempSandwiches: values,
      };
    });
  };

  onChangeTempAmounts = (e) => {
    this.setState({
      tempAmounts: e.target.value,
    });
  };

  getOrder(id) {
    OrderDataService.get(id)
      .then((response) => {
        this.setState({
          currentOrder: response.data,
        });
        console.log(response.data);

        var sandwichrds = response.data.sandwichOrders;

        this.getSandwichOrders(sandwichrds);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  postOrder(data) {
    try {
      OrderDataService.create(data);
    } catch (err) {
      console.log(err);
    }
  }

  getSandwichOrders(sandwichOrders) {
    var i,
      j,
      s = "";
    for (i = 0; i < sandwichOrders.length; i++) {
      for (j = 0; j < this.state.availableSandwiches.length; j++) {
        if (
          this.state.availableSandwiches[j].id ==
          sandwichOrders[i].sandwich
        ) {
          s +=
            "<p> ➤ " +
            sandwichOrders[i].amount +
            " -> " +
            this.state.availableSandwiches[j].shortDescription +
            "</p>";
        }
      }
    }
    document.getElementById("ordersss").innerHTML = s;
  }

  updateOrder() {
    console.log(this.state.currentOrder);
    OrderDataService.update(
      this.state.currentOrder.id.value,
      this.state.currentOrder
    )
      .then(() => {
        this.setState({
          error: null,
        });

        this.props.history.push("/orders");
      })
      .catch((error) => {
        this.setState({
          error: error.response.data.message,
        });
      });
  }

  deleteOrder() {
    OrderDataService.delete(this.state.currentOrder.id.value)
      .then((response) => {
        console.log(response.data);
        this.props.history.push("/orders");
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const { currentOrder } = this.state;

    return (
      <div>
        {currentOrder ? (
          <div className={this.state.styles.root}>
            <link
              rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            />
            <link
              rel="stylesheet"
              href="https://fonts.googleapis.com/icon?family=Material+Icons"
            />
            <br></br>
            <br></br>
            <h4 id="editCPTitle">Edit Order</h4>

            <div>
              <div>
                <label id="label3" htmlFor="date">
                  Last Updated Date
                </label>

                <br></br>
                <TextField
                  id="date"
                  type="date"
                  variant="outlined"
                  value={currentOrder.day != null ? currentOrder.day.value : ""}
                  style={{ background: "#e9f2e9" }}
                  className={this.state.styles.textField}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={this.onChangeDay}
                />
              </div>
              <br></br>
              <div>
                <label id="label10" htmlFor="sandwichOrders">
                  Sandwich Orders
                </label>
                <div id="ordersss" />

                <Autocomplete
                  id="free-solo-demo"
                  onChange={this.onChangeTempSandwiches}
                  options={this.state.availableSandwiches}
                  getOptionLabel={(option) => option.shortDescription}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      style={{ background: "#e9f2e9" }}
                      margin="normal"
                      variant="outlined"
                    />
                  )}
                />

                <div>
                  <TextField
                    id="amount"
                    variant="outlined"
                    fullWidth
                    value={this.state.tempAmounts}
                    style={{ background: "#e9f2e9" }}
                    onChange={this.onChangeTempAmounts}
                  />
                </div>

                <Button
                  id="ingredientsAdd"
                  variant="contained"
                  style={{ background: "#20428c", color: "#FFF" }}
                  size="small"
                  className={this.state.styles.addUser}
                  onClick={this.handleAddSandwichOrders}
                  className="small"
                >
                  Add Sandwich Order
                </Button>

                <IconButton
                  aria-label="delete"
                  style={{ color: "#e6556f" }}
                  onClick={this.handleRemoveSandwichOrders}
                >
                  <DeleteIcon />
                </IconButton>
              </div>
              <br></br>
              <Button
                id="upd"
                variant="contained"
                style={{ background: "#a6ce39", color: "#FFF" }}
                size="small"
                className={this.state.styles.addUser}
                onClick={this.updateOrder}
                disabled={
                  this.state.currentOrder.id === "" ||
                  this.state.currentOrder.day === null ||
                  this.state.currentOrder.sandwichOrders.length === 0
                }
              >
                Update
              </Button>
              &nbsp;
              <Button
                id="del"
                variant="contained"
                style={{ background: "#c94254", color: "#FFF" }}
                size="small"
                className={this.state.styles.addUser}
                onClick={this.deleteOrder}
              >
                Delete
              </Button>
              <br></br>
            </div>

            <br></br>

            {this.state.error !== undefined ? (
              <p
                style={{
                  color: this.state.error !== null ? "red" : "green",
                }}
              >
                {this.state.error !== null
                  ? this.state.error
                  : "Order Updated!"}
              </p>
            ) : undefined}
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on an Order...</p>
          </div>
        )}
      </div>
    );
  }
}
