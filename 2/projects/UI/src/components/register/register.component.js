import React, { Component } from "react";
import "./style.css";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

import Autocomplete from "@material-ui/lab/Autocomplete";

import { useState } from "react";
import { useDispatch } from "react-redux";
import { registerUser } from "../../services/rootReducer/auth";

const Register = (props) => {
  const initialState = {
    name: "",
    email: "",
    password: "",
    role: { roleValue: "SCHOOLREPRESENTATIVE" },
  };

  const [user, setUser] = useState(initialState);
  const [error, setError] = useState(undefined);

  const userChange = (event) => {
    const { name, value } = event.target;
    setUser({ ...user, [name]: value });
  };

  const dispatch = useDispatch();

  const saveUser = () => {
    var us = {
      name: { value: user.name },
      email: { value: user.email },
      password: { value: user.password },
    };
    dispatch(registerUser(us))
      .then(() => {
        resetRegisterForm();
        setTimeout(() => {
          props.history.push("/login");
        }, 2000);
      })
      .catch((error) => {
        setError(error.response.data.message);
      });
  };

  const resetRegisterForm = () => {
    setUser(initialState);
    setError(undefined);
  };

  return (
    <div>
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
      />
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/icon?family=Material+Icons"
      />
      <br></br>
      <br></br>

      <div
      // className="submit-form"
      >
        <div className="form">
          <h4 id="title">Register</h4>

          <br></br>

          <div className="form-group">
            <label id="label1" htmlFor="name">
              Name
            </label>
            <TextField
              id="presentationTitle"
              variant="outlined"
              required
              name="name"
              value={user.name}
              style={{ background: "#e9f2e9" }}
              onChange={userChange}
            />
          </div>

          <div className="form-group">
            <label id="label2" htmlFor="email">
              Email
            </label>
            <TextField
              id="presentationTitle"
              variant="outlined"
              required
              name="email"
              value={user.email}
              style={{ background: "#e9f2e9" }}
              onChange={userChange}
            />
          </div>

          <div className="form-group">
            <label id="label4" htmlFor="price">
              Password
            </label>

            <TextField
              required
              id="lowerLimit"
              variant="outlined"
              name="password"
              type="password"
              value={user.password}
              onChange={userChange}
              style={{ background: "#e9f2e9" }}
              name="password"
            />
            <br></br>
          </div>
          <Button
            id="submit"
            variant="contained"
            color="default"
            size="small"
            onClick={saveUser}
            disabled={
              user.name === "" || user.email === "" || user.password === ""
            }
          >
            Submit
          </Button>
          <br></br>
          {error !== undefined ? (
            <p style={{ color: "red" }}>{error}</p>
          ) : undefined}
        </div>

        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
      </div>
    </div>
  );
};

export default Register;
