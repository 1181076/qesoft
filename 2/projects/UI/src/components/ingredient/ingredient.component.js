import React, { Component } from "react";
import IngredientDataService from "../../services/ingredient.service";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

export default class Ingredient extends Component {
  constructor(props) {
    super(props);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeQuantity = this.onChangeQuantity.bind(this);
    this.onChangeUpperLimit = this.onChangeUpperLimit.bind(this);
    this.onChangeLowerLimit = this.onChangeLowerLimit.bind(this);
    this.onChangeUnits = this.onChangeUnits.bind(this);
    this.updateIngredient = this.updateIngredient.bind(this);
    this.deleteIngredient = this.deleteIngredient.bind(this);
    this.getIngredient = this.getIngredient.bind(this);

    this.state = {
      currentIngredient: {
        id: "",
        description: "",
        quantity: null,
        lastUpdatedDate: null,
        upperLimit: null,
        lowerLimit: null,
        units: "",
      },

      tempSuppliers: "",

      submitted: false,
      quantityChange: false,

      styles: (theme) => ({
        paper: {
          maxWidth: 936,
          margin: "auto",
          overflow: "hidden",
        },
        searchBar: {
          borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
        },
        searchInput: {
          fontSize: theme.typography.fontSize,
        },
        block: {
          display: "block",
        },
        addUser: {
          marginRight: theme.spacing(1),
        },
        contentWrapper: {
          margin: "40px 16px",
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 200,
        },
        selectEmpty: {
          marginTop: theme.spacing(2),
        },
        formControl: {
          margin: theme.spacing(1),
          minWidth: 120,
        },
        root: {
          display: "flex",
          flexWrap: "wrap",
          background: "#eaeff1",
        },
      }),

      suppliers: [],

      tags: null,
      error: undefined,
    };
  }

  componentDidMount() {
    this.getIngredient(this.props.match.params.id);
  }

  onChangeDescription = (e) => {
    const description = e.target.value;

    this.setState(function (prevState) {
      return {
        currentIngredient: {
          ...prevState.currentIngredient,
          description: { value: description },
        },
      };
    });
  };

  onChangeQuantity = (e) => {
    const quantity = e.target.value;

    this.setState(function (prevState) {
      return {
        currentIngredient: {
          ...prevState.currentIngredient,
          quantity: { value: quantity },
        },
        quantityChange: true,
      };
    });
  };

  onChangeUpperLimit = (e) => {
    const upperLimit = e.target.value;

    this.setState(function (prevState) {
      return {
        currentIngredient: {
          ...prevState.currentIngredient,
          upperLimit: { value: upperLimit },
        },
      };
    });
  };

  onChangeLowerLimit = (e) => {
    const lowerLimit = e.target.value;

    this.setState(function (prevState) {
      return {
        currentIngredient: {
          ...prevState.currentIngredient,
          lowerLimit: { value: lowerLimit },
        },
      };
    });
  };

  onChangeUnits = (e) => {
    const units = e.target.value;

    this.setState(function (prevState) {
      return {
        currentIngredient: {
          ...prevState.currentIngredient,
          units: { value: units },
        },
      };
    });
  };

  getIngredient(id) {
    IngredientDataService.get(id)
      .then((response) => {
        this.setState({
          currentIngredient: {
            id: { value: response.data.ingredient.id },
            description: { value: response.data.ingredient.description },
            quantity: { value: Number(response.data.ingredient.quantity) },
            lastUpdatedDate: response.data.ingredient.lastUpdatedDate,
            upperLimit: { value: Number(response.data.ingredient.upperLimit) },
            lowerLimit: { value: Number(response.data.ingredient.lowerLimit) },
            units: { value: response.data.ingredient.units },
          },
        });
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  updateIngredient() {
    if (
      parseInt(this.state.currentIngredient.upperLimit.value) <
      parseInt(this.state.currentIngredient.lowerLimit.value)
    ) {
      this.setState({
        error: "The upper limit cannot be below the lower limit!",
      });
      return;
    }

    if (
      parseInt(this.state.currentIngredient.quantity.value) <
        parseInt(this.state.currentIngredient.lowerLimit.value) ||
      parseInt(this.state.currentIngredient.quantity.value) >
        parseInt(this.state.currentIngredient.upperLimit.value)
    ) {
      this.setState({
        error: "The quantity must be between the lower and upper limit!",
      });
      return;
    }

    var dataTBS = this.state.currentIngredient;

    if (this.state.quantityChange) {
      var today = new Date();
      var currentDate = today.getFullYear() + "-";

      if (today.getMonth < 9) {
        currentDate += "0" + today.getMonth() + 1 + "-";
      } else {
        currentDate += today.getMonth() + 1 + "-";
      }

      if (today.getDate < 10) {
        currentDate += "0" + today.getDate() + "-";
      } else {
        currentDate += today.getMonth() + 1 + "-";
      }

      dataTBS.lastUpdatedDate = dataTBS.lastUpdatedDate.concat([
        { date: currentDate, amount: Number(dataTBS.quantity.value) },
      ]);
    }

    IngredientDataService.update(dataTBS.id.value, dataTBS)
      .then(() => {
        this.setState({
          error: null,
        });

        this.props.history.push("/ingredients");
      })
      .catch((error) => {
        this.setState({
          error: error.response.data.message,
        });
      });
  }

  deleteIngredient() {
    IngredientDataService.delete(this.state.currentIngredient.id.value)
      .then(() => {
        this.props.history.push("/ingredients");
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const { currentIngredient } = this.state;

    return (
      <div>
        {currentIngredient ? (
          <div className={this.state.styles.root}>
            <link
              rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            />
            <link
              rel="stylesheet"
              href="https://fonts.googleapis.com/icon?family=Material+Icons"
            />
            <br></br>
            <br></br>
            <h4 id="editCPTitle">Edit Ingredient</h4>

            <div>
              <div>
                <label id="label1" htmlFor="description">
                  Description
                </label>
                <TextField
                  id="presentationTitle"
                  variant="outlined"
                  fullWidth
                  value={
                    currentIngredient.description != null
                      ? currentIngredient.description.value
                      : ""
                  }
                  style={{ background: "#e9f2e9" }}
                  onChange={this.onChangeDescription}
                />
              </div>
              <br></br>
              <div>
                <label id="label2" htmlFor="description">
                  Quantity
                </label>
                <TextField
                  type="number"
                  id="quantity"
                  variant="outlined"
                  fullWidth
                  value={
                    currentIngredient.quantity != null
                      ? currentIngredient.quantity.value
                      : ""
                  }
                  style={{ background: "#e9f2e9" }}
                  onChange={this.onChangeQuantity}
                />
              </div>
              <br></br>
              <div>
                <label id="label4" htmlFor="upperLimit">
                  Upper Limit
                </label>
                <TextField
                  type="number"
                  id="upperLimit"
                  variant="outlined"
                  fullWidth
                  value={
                    currentIngredient.upperLimit != null
                      ? currentIngredient.upperLimit.value
                      : ""
                  }
                  style={{ background: "#e9f2e9" }}
                  onChange={this.onChangeUpperLimit}
                />
              </div>
              <br></br>
              <div>
                <label id="label5" htmlFor="host">
                  Lower Limit
                </label>
                <TextField
                  type="number"
                  id="lowerLimit"
                  variant="outlined"
                  fullWidth
                  value={
                    currentIngredient.lowerLimit != null
                      ? currentIngredient.lowerLimit.value
                      : ""
                  }
                  style={{ background: "#e9f2e9" }}
                  onChange={this.onChangeLowerLimit}
                />
              </div>
              <br></br>
              <br></br>
              <div>
                <label id="label6" htmlFor="units">
                  Units
                </label>

                <Select
                  labelId="labelCurrencyFilter"
                  id="CurrencyFilter"
                  variant="outlined"
                  fullWidth
                  value={
                    currentIngredient.units != null
                      ? currentIngredient.units.value
                      : ""
                  }
                  onChange={this.onChangeUnits}
                  style={{ background: "#e9f2e9" }}
                >
                  <MenuItem id="Grams" value="Grams">
                    Grams
                  </MenuItem>
                  <MenuItem id="Kilograms" value="Kilograms">
                    Kilograms
                  </MenuItem>
                  <MenuItem id="Milligrams" value="Milligrams">
                    Milligrams
                  </MenuItem>
                  <MenuItem id="Liters" value="Liters">
                    Liters
                  </MenuItem>
                  <MenuItem id="Milliliters" value="Milliliters">
                    Milliliters
                  </MenuItem>
                  <MenuItem id="Pounds" value="Pounds">
                    Pounds
                  </MenuItem>
                  <MenuItem id="Ounces" value="Ounces">
                    Ounces
                  </MenuItem>
                </Select>
              </div>
              <br></br>
              <br></br>
              <Button
                id="upd"
                variant="contained"
                style={{ background: "#a6ce39", color: "#FFF" }}
                size="small"
                className={this.state.styles.addUser}
                onClick={this.updateIngredient}
                disabled={
                  this.state.currentIngredient.id === "" ||
                  this.state.currentIngredient.description === "" ||
                  this.state.currentIngredient.quantity === null ||
                  this.state.currentIngredient.lowerLimit === null ||
                  this.state.currentIngredient.upperLimit === null ||
                  this.state.currentIngredient.units === "" ||
                  this.state.currentIngredient.lastUpdatedDate === null
                }
              >
                Update
              </Button>
              &nbsp;
              <Button
                id="del"
                variant="contained"
                style={{ background: "#c94254", color: "#FFF" }}
                size="small"
                className={this.state.styles.addUser}
                onClick={this.deleteIngredient}
              >
                Delete
              </Button>
              <br></br>
            </div>
            <br></br>

            {this.state.error !== undefined ? (
              <p
                style={{
                  color: this.state.error !== null ? "red" : "green",
                }}
              >
                {this.state.error !== null
                  ? this.state.error
                  : "Ingredient Updated!"}
              </p>
            ) : undefined}
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on an Ingredient...</p>
          </div>
        )}
      </div>
    );
  }
}
