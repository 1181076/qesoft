import React from "react";
import SchoolDataService from "../../services/school.service";
import Button from "@material-ui/core/Button";
import SandwichService from "../../services/sandwich.service";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import Autocomplete from "@material-ui/lab/Autocomplete";

export default class RegisterSandwiches extends React.Component {
  constructor(props) {
    super(props);
    this.handleAddSandwiches = this.handleAddSandwiches.bind(this);
    this.handleRemoveSandwiches = this.handleRemoveSandwiches.bind(this);
    this.onChangeSandwichTemp = this.onChangeSandwichTemp.bind(this);

    this.state = {
      currentSchool: null,
      availableSandwiches: [],
      tempSandwiches: null,
      sandwichIds: [],

      styles: (theme) => ({
        paper: {
          maxWidth: 936,
          margin: "auto",
          overflow: "hidden",
        },
        searchBar: {
          borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
        },
        searchInput: {
          fontSize: theme.typography.fontSize,
        },
        block: {
          display: "block",
        },
        addSchool: {
          marginRight: theme.spacing(1),
        },
        contentWrapper: {
          margin: "40px 16px",
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 200,
        },
        selectEmpty: {
          marginTop: theme.spacing(2),
        },
        formControl: {
          margin: theme.spacing(1),
          minWidth: 120,
        },
        root: {
          display: "flex",
          flexWrap: "wrap",
          background: "#eaeff1",
        },
        addSchool: {
          marginRight: theme.spacing(1),
        },
      }),
    };
  }

  componentDidMount() {
    this.retrieveAllSandwiches();
    this.retrieveSchool(this.props.match.params.id);
  }

  retrieveSchool(id) {
    SchoolDataService.get(id)
      .then((response) => {
        this.setState({
          currentSchool: response.data.school,
        });
        this.getRegisteredSandwichs();
      })
      .catch((e) => {
        console.log(e);
      });
  }

  getRegisteredSandwichs() {
    var i,
      s = "";
    for (i = 0; i < this.state.currentSchool.sandwiches.length; i++) {
      s +=
        "<p> ➤ " +
        this.state.currentSchool.sandwiches[i].id +
        " -> " +
        this.state.currentSchool.sandwiches[i].shortDescription +
        "</p>";
    }
    document.getElementById("sandsss").innerHTML = s;
  }

  registerSandwiches = (e) => {
    SchoolDataService.registerSandwiches(
      this.props.match.params.id,
      this.state.currentSchool.sandwiches
    );
  };

  onChangeDeals = (e) => {
    const vals = e.target.value;
    this.setState(function (prevState) {
      return {
        currentSupplier: {
          ...prevState.currentSupplier,
          deals: vals,
        },
      };
    });
  };

  handleAddSandwiches = (e) => {
    const vals = e.target.value;
    this.setState(function (prevState) {
      return {
        currentSchool: {
          ...prevState.currentSchool,
          sandwiches: this.state.currentSchool.sandwiches.concat([
            { value: this.state.tempSandwiches.id },
          ]),
        },
      };
    });

    var j,
      s = "";

    s +=
      "<p> ➤ " +
      this.state.tempSandwiches.id +
      " -> " +
      this.state.tempSandwiches.shortDescription +
      "</p>";

    document.getElementById("sandsss").innerHTML += s;
  };

  handleRemoveSandwiches = (e) => {
    this.setState(function (prevState) {
      return {
        sandwiches: this.state.currentSchool.sandwiches.pop(),
      };
    });
    var a = document.getElementById("sandsss").innerHTML;
    var b = a.length - 4;
    var c = a.charAt(b);

    while (c != "/" && b != 0) {
      b--;
      c = a.charAt(b);
    }

    if (b != 0) {
      b = b + 3;
    }

    var d = a.substring(0, b);

    document.getElementById("sandsss").innerHTML = d;
  };

  retrieveAllSandwiches() {
    SandwichService.getAll()
      .then((response) => {
        this.setState({
          availableSandwiches: response.data.sandwiches,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  onChangeSandwichTemp = (event, values) => {
    this.setState({
      tempSandwiches: values,
    });
  };

  render() {
    return (
      <div className={this.state.styles.root}>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/icon?family=Material+Icons"
        />
        <br></br>
        <br></br>
        <h4 id="title">Register Sandwiches</h4>

        <div>
          <br></br>

          <div>
            <label id="label10" htmlFor="sandwiches">
              Sandwiches
            </label>
            <div id="sandsss" />

            <Autocomplete
              id="free-solo-demo"
              freeSolo
              onChange={this.onChangeSandwichTemp}
              options={this.state.availableSandwiches}
              getOptionLabel={(option) => option.id}
              renderInput={(params) => (
                <TextField
                  {...params}
                  style={{ background: "#e9f2e9" }}
                  label="Add"
                  margin="normal"
                  variant="outlined"
                />
              )}
            />

            <Button
              id="authorAdd"
              variant="contained"
              style={{ background: "#20428c", color: "#FFF" }}
              size="small"
              onClick={this.handleAddSandwiches}
              className="small"
            >
              Add
            </Button>

            <IconButton
              aria-label="delete"
              style={{ color: "#e6556f" }}
              onClick={this.handleRemoveSandwiches}
            >
              <DeleteIcon />
            </IconButton>
          </div>
          <br></br>
          <br></br>
          <Button
            id="submit"
            variant="contained"
            color="default"
            size="small"
            onClick={this.registerSandwiches}
          >
            Register Sandwiches
          </Button>
        </div>

        <br></br>
      </div>
    );
  }
}
