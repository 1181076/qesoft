import React from "react";
import SchoolDataService from "../../services/school.service";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

export default class AddSchool extends React.Component {
  constructor(props) {
    super(props);
    this.onChangeID = this.onChangeID.bind(this);
    this.onChangeInternalID = this.onChangeInternalID.bind(this);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeAddress = this.onChangeAddress.bind(this);

    this.state = {
      id: "",
      internalID: "",
      name: "",
      address: "",

      styles: (theme) => ({
        paper: {
          maxWidth: 936,
          margin: "auto",
          overflow: "hidden",
        },
        searchBar: {
          borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
        },
        searchInput: {
          fontSize: theme.typography.fontSize,
        },
        block: {
          display: "block",
        },
        addSchool: {
          marginRight: theme.spacing(1),
        },
        contentWrapper: {
          margin: "40px 16px",
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 200,
        },
        selectEmpty: {
          marginTop: theme.spacing(2),
        },
        formControl: {
          margin: theme.spacing(1),
          minWidth: 120,
        },
        root: {
          display: "flex",
          flexWrap: "wrap",
          background: "#eaeff1",
        },
      }),

      error: undefined,
    };
  }

  onChangeID = (e) => {
    this.setState({
      id: e.target.value,
    });
  };

  onChangeInternalID = (e) => {
    this.setState({
      internalID: e.target.value,
    });
  };

  onChangeName = (e) => {
    this.setState({
      name: e.target.value,
    });
  };

  onChangeAddress = (e) => {
    this.setState({
      address: e.target.value,
    });
  };

  saveSchool = (e) => {
    let data = {
      id: {
        value: this.state.id,
      },
      internalID: {
        value: this.state.internalID,
      },
      name: {
        value: this.state.name,
      },
      address: {
        value: this.state.address,
      },
      sandwiches:[]

    };
    SchoolDataService.create(data)
      .then(() => {
        this.setState({
          error: null,
        });
      })
      .catch((error) => {
        this.setState({
          error: error.response.data.message,
        });
      });
  };

  render() {
    return (
      <div className={this.state.styles.root}>
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/icon?family=Material+Icons"
        />
        <br></br>
        <br></br>
        <h4 id="title">Add School</h4>

        <div>
          <br></br>
          <div>
            <label id="label0" htmlFor="id">
              ID
            </label>
            <TextField
              id="presentationTitle"
              variant="outlined"
              fullWidth
              value={this.state.id}
              style={{ background: "#e9f2e9" }}
              onChange={this.onChangeID}
            />
          </div>

          <div>
            <label id="label0" htmlFor="internalID">
              Internal ID
            </label>
            <TextField
              id="presentationTitle"
              variant="outlined"
              fullWidth
              value={this.state.internalID}
              style={{ background: "#e9f2e9" }}
              onChange={this.onChangeInternalID}
            />
          </div>

          <div>
            <label id="label1" htmlFor="name">
              Name
            </label>
            <TextField
              id="presentationTitle"
              variant="outlined"
              fullWidth
              value={this.state.name}
              style={{ background: "#e9f2e9" }}
              onChange={this.onChangeName}
            />
          </div>

          <div>
            <label id="label2" htmlFor="address">
              Address
            </label>
            <TextField
              id="presentationTitle"
              variant="outlined"
              fullWidth
              value={this.state.address}
              style={{ background: "#e9f2e9" }}
              onChange={this.onChangeAddress}
            />
          </div>

          <br></br>

          <Button
            id="submit"
            variant="contained"
            color="default"
            size="small"
            onClick={this.saveSchool}
            disabled={
              this.state.id === "" ||
              this.state.internalID === "" ||
              this.state.address === "" ||
              this.state.name === ""
            }
          >
            Submit
          </Button>
        </div>

        <br></br>

        {this.state.error !== undefined ? (
          <p
            style={{
              color: this.state.error !== null ? "red" : "green",
            }}
          >
            {this.state.error !== null
              ? this.state.error
              : "School Registered!"}
          </p>
        ) : undefined}
      </div>
    );
  }
}
