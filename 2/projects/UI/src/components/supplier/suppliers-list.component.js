import React, { Component } from "react";
import SupplierDataService from "../../services/supplier.service";
import DealsService from "../../services/deal.service";

import AddSupplier from "./add-supplier.component";

import { Switch, Route, Link } from "react-router-dom";

import ButtonGroup from "@material-ui/core/ButtonGroup";

import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import { withStyles } from "@material-ui/core/styles";
import SearchIcon from "@material-ui/icons/Search";
import RefreshIcon from "@material-ui/icons/Refresh";
import DeleteIcon from "@material-ui/icons/Delete";

import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";

export default class SuppliersList extends Component {
  constructor(props) {
    super(props);
    this.onChangeSearchTitle = this.onChangeSearchTitle.bind(this);
    this.retrieveSuppliers = this.retrieveSuppliers.bind(this);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveSupplier = this.setActiveSupplier.bind(this);
    this.removeAllSuppliers = this.removeAllSuppliers.bind(this);
    this.getDeals = this.getDeals.bind(this);

    this.state = {
      suppliers: [],
      currentSupplier: null,
      currentIndex: -1,
      searchTitle: "",
      deals: [],

      styles: (theme) => ({
        paper: {
          maxWidth: 936,
          margin: "auto",
          overflow: "hidden",
        },
        searchBar: {
          borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
        },
        searchInput: {
          fontSize: theme.typography.fontSize,
        },
        block: {
          display: "block",
        },
        addUser: {
          marginRight: theme.spacing(1),
          textTransform: "none",
        },
        contentWrapper: {
          margin: "40px 16px",
        },
      }),
    };

    this.MyLink = (props) => <Link to="/addSupplier" {...props} />;
  }

  componentDidMount() {
    this.retrieveSuppliers();
    this.retrieveDeals();
  }

  onChangeSearchTitle(e) {
    const searchTitle = e.target.value;

    this.setState({
      searchTitle: searchTitle,
    });
  }

  retrieveSuppliers() {
    SupplierDataService.getAll()
      .then((response) => {
        this.setState({
          suppliers: response.data.suppliers,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  retrieveDeals() {
    DealsService.getAll()
      .then((response) => {
        this.setState({
          deals: response.data.deals,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  refreshList() {
    this.retrieveSuppliers();
    this.setState({
      currentSupplier: null,
      currentIndex: -1,
    });
  }

  setActiveSupplier(supplier, index) {
    if (document.getElementById("dealsss") != null) {
      document.getElementById("dealsss").innerHTML = "";
    }

    this.setState({
      currentSupplier: supplier,
      currentIndex: index,
    });
  }

  removeAllSuppliers() {
    SupplierDataService.deleteAll()
      .then((response) => {
        this.refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  }

  getDeals() {
    var i,
      s = "";
    console.log(this.state.currentSupplier.deals);
    for (i = 0; i < this.state.currentSupplier.deals.length; i++) {
      s +=
        "<p> ➤ " +
        this.state.currentSupplier.deals[i].id +
        "  ->  " +
        this.state.currentSupplier.deals[i].ingredient.id +
        " : " +
        this.state.deals[i].unitPrice.value +
        " " +
        this.state.deals[i].unitPrice.unit +
        "</p>";
    }
    document.getElementById("dealsss").innerHTML = s;
  }

  searchTitle() {}

  render() {
    const { searchTitle, suppliers, currentSupplier, currentIndex } =
      this.state;
    return (
      <div className="list row">
        <br></br>
        <br></br>
        <div className="col-md-8">
          <br></br>
          <br></br>
          <h4 id="listSuppliersTitle"> Suppliers List</h4>
          <br></br>
          <Paper className={this.state.styles.paper}>
            <AppBar
              className={this.state.styles.searchBar}
              position="static"
              color="default"
              elevation={0}
            >
              <Toolbar>
                <Grid container spacing={2} alignItems="center">
                  <Grid item>
                    <IconButton>
                      <SearchIcon
                        className={this.state.styles.block}
                        color="inherit"
                        onClick={this.searchTitle}
                      />
                    </IconButton>
                  </Grid>
                  <Grid item xs>
                    <TextField
                      fullWidth
                      placeholder="Search"
                      InputProps={{
                        disableUnderline: true,
                        className: this.state.styles.searchInput,
                      }}
                      onChange={this.onChangeSearchTitle}
                    />
                  </Grid>
                  <Grid item>
                    <Tooltip title="Reload">
                      <IconButton>
                        <RefreshIcon
                          className={this.state.styles.block}
                          color="inherit"
                          onClick={() => this.setActiveSupplier(null, -1)}
                        />
                      </IconButton>
                    </Tooltip>
                  </Grid>
                </Grid>
              </Toolbar>
            </AppBar>
          </Paper>
          <br></br>
        </div>
        <div className="col-md-6">
          <ul className="list-group">
            {suppliers &&
              suppliers.map((supplier, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveSupplier(supplier, index)}
                  key={index}
                >
                  {supplier.description}
                </li>
              ))}
          </ul>
          <br></br>
          <Button
            id="add"
            variant="contained"
            style={{
              background: "#20428c",
              color: "#FFF",
              textTransform: "none",
            }}
            size="small"
            className={this.state.styles.addUser}
            component={this.MyLink}
          >
            Add Supplier
          </Button>
          <Button
            id="removeAll"
            variant="contained"
            style={{
              background: "#e6556f",
              color: "#FFF",
              textTransform: "none",
            }}
            size="small"
            className={this.state.styles.addUser}
            startIcon={<DeleteIcon />}
            onClick={this.removeAllSuppliers}
          >
            Remove All
          </Button>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
        </div>
        <div className="col-md-6">
          {currentSupplier ? (
            <div>
              <h4>Supplier</h4>
              <div id="name">
                <label>
                  <strong>Name:</strong>
                </label>{" "}
                {/* <br></br> */}
                {currentSupplier.name != null ? currentSupplier.name : "-----"}
              </div>
              <div id="address">
                <label>
                  <strong>Address:</strong>
                </label>{" "}
                {/* <br></br> */}
                {currentSupplier.address != null
                  ? currentSupplier.address
                  : "-----"}
              </div>
              <div id="emails">
                <label>
                  <strong>Emails:</strong>
                </label>{" "}
                {currentSupplier.emails != null
                  ? currentSupplier.emails.length
                  : "-----"}
              </div>
              <div id="contacts">
                <label>
                  <strong>Contacts:</strong>
                </label>{" "}
                {currentSupplier.contacts != null
                  ? currentSupplier.contacts.length
                  : "-----"}
              </div>
              <div id="description">
                <label>
                  <strong>Description:</strong>
                </label>{" "}
                {currentSupplier.description != null
                  ? currentSupplier.description
                  : "-----"}
              </div>
              <div id="deals">
                <label>
                  <strong>Deals:</strong>
                </label>{" "}
                {currentSupplier.deals != null
                  ? currentSupplier.deals.length
                  : "-----"}
              </div>
              <div id="deals">
                <div>
                  <Button
                    id="filters"
                    size="small"
                    variant="contained"
                    style={{
                      textTransform: "none",
                    }}
                    onClick={this.getDeals}
                  >
                    Show Deals
                  </Button>
                </div>

                <br></br>

                <div id="dealsss"></div>
              </div>

              <hr></hr>
              <br></br>
              <ButtonGroup variant="contained">
                <Button
                  style={{
                    background: "#ffc107",
                    color: "#FFF",
                    textTransform: "none",
                  }}
                >
                  <Link
                    id="edit"
                    className="badge badge-warning"
                    to={"/suppliers/" + currentSupplier.id}
                  >
                    {" "}
                    Edit{" "}
                  </Link>
                </Button>
              </ButtonGroup>
            </div>
          ) : (
            <div>
              <br />
            </div>
          )}
        </div>
      </div>
    );
  }
}
