import React, { Component } from "react";
import DealDataService from "../../services/deal.service";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Autocomplete from "@material-ui/lab/Autocomplete";
import IngredientService from "../../services/ingredient.service";

export default class Deal extends Component {
  constructor(props) {
    super(props);
    this.onChangeUnit = this.onChangeUnit.bind(this);
    this.onChangeAmount = this.onChangeAmount.bind(this);
    this.onChangeExpirationPriceDate =
      this.onChangeExpirationPriceDate.bind(this);
    this.onChangeIngredient = this.onChangeIngredient.bind(this);
    this.updateDeal = this.updateDeal.bind(this);
    this.deleteDeal = this.deleteDeal.bind(this);
    this.getDeal = this.getDeal.bind(this);

    this.state = {
      currentDeal: {
        id: "",
        unitPrice: null,
        expirationPriceDate: null,
        ingredient: null,
      },

      availableIngredients: [],
      ingredientDesc: null,

      submitted: false,
      styles: (theme) => ({
        paper: {
          maxWidth: 936,
          margin: "auto",
          overflow: "hidden",
        },
        searchBar: {
          borderBottom: "1px solid rgba(0, 0, 0, 0.12)",
        },
        searchInput: {
          fontSize: theme.typography.fontSize,
        },
        block: {
          display: "block",
        },
        addUser: {
          marginRight: theme.spacing(1),
        },
        contentWrapper: {
          margin: "40px 16px",
        },
        textField: {
          marginLeft: theme.spacing(1),
          marginRight: theme.spacing(1),
          width: 200,
        },
        selectEmpty: {
          marginTop: theme.spacing(2),
        },
        formControl: {
          margin: theme.spacing(1),
          minWidth: 120,
        },
        root: {
          display: "flex",
          flexWrap: "wrap",
          background: "#eaeff1",
        },
      }),
      tags: null,
      error: undefined,
    };
  }

  componentDidMount() {
    this.retrieveIngredients();
    this.getDeal(this.props.match.params.id);
  }

  retrieveIngredients() {
    IngredientService.getAll()
      .then((response) => {
        if (response.data != null) {
          if (response.data != null) {
            this.setState({
              availableIngredients: response.data.ingredients,
            });
          }
        }
        console.log(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  onChangeUnit = (e) => {
    const unit = e.target.value;
    this.setState(function (prevState) {
      return {
        currentDeal: {
          ...prevState.currentDeal,
          unitPrice: { ...prevState.currentDeal.unitPrice, unit },
        },
      };
    });
  };

  onChangeAmount = (e) => {
    console.log(this.state.currentDeal.expirationPriceDate);

    const value = e.target.value;
    this.setState(function (prevState) {
      return {
        currentDeal: {
          ...prevState.currentDeal,
          unitPrice: { ...prevState.currentDeal.unitPrice, value },
        },
      };
    });
  };

  onChangeExpirationPriceDate = (e) => {
    const vals = e.target.value;
    this.setState(function (prevState) {
      return {
        currentDeal: {
          ...prevState.currentDeal,
          expirationPriceDate: { value: vals },
        },
      };
    });
  };

  onChangeIngredient = (event, values) => {
    this.setState(function (prevState) {
      return {
        currentDeal: {
          ...prevState.currentDeal,
          ingredient: values,
        },
      };
    });
  };

  getDeal(id) {
    DealDataService.get(id)
      .then((response) => {
        this.setState({
          currentDeal: {
            id: { value: response.data.deal.id },
            unitPrice: response.data.deal.unitPrice,
            expirationPriceDate: {
              value: response.data.deal.expirationPriceDate,
            },
            ingredient: { value: response.data.deal.ingredient.id },
          },
        });
        var ingredientt = response.data.ingredient.id;

        this.getIngredient(ingredientt);

        document.getElementById("ingredientsss").innerHTML =
          "<p> ➤ " + ingredientt.description + "</p>";
      })
      .catch((e) => {
        console.log(e);
      });
  }

  updateDeal() {
    DealDataService.update(
      this.state.currentDeal.id.value,
      this.state.currentDeal
    )
      .then(() => {
        this.setState({
          error: null,
        });

        this.props.history.push("/deals");
      })
      .catch((error) => {
        this.setState({
          error: error.response.data.message,
        });
      });
  }

  deleteDeal() {
    DealDataService.delete(this.state.currentDeal.id.value)
      .then((response) => {
        console.log(response.data);
        this.props.history.push("/deals");
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const { currentDeal } = this.state;

    return (
      <div>
        {currentDeal ? (
          <div className={this.state.styles.root}>
            <link
              rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            />
            <link
              rel="stylesheet"
              href="https://fonts.googleapis.com/icon?family=Material+Icons"
            />
            <br></br>
            <br></br>
            <h4 id="editCPTitle">Edit Deal</h4>
            <div>
              <div>
                <label id="label4" htmlFor="price">
                  Unit Price Amount
                </label>

                <TextField
                  id="lowerLimit"
                  variant="outlined"
                  fullWidth
                  value={
                    currentDeal.unitPrice != null
                      ? currentDeal.unitPrice.value
                      : ""
                  }
                  onChange={this.onChangeAmount}
                  style={{ background: "#e9f2e9" }}
                  name="amount"
                />
                <br></br>
              </div>
              <div>
                <label id="label5" htmlFor="price">
                  Unit Price Currency
                </label>

                <Select
                  labelId="labelCurrencyFilter"
                  id="CurrencyFilter"
                  variant="outlined"
                  fullWidth
                  value={
                    currentDeal.unitPrice != null
                      ? currentDeal.unitPrice.unit
                      : ""
                  }
                  onChange={this.onChangeUnit}
                  style={{ background: "#e9f2e9" }}
                >
                  <MenuItem id="Euro" value="Euro">
                    Euro
                  </MenuItem>
                  <MenuItem id="US Dollar" value="US Dollar">
                    US Dollar
                  </MenuItem>
                  <MenuItem id="Australian Dollar" value="Australian Dolla">
                    Australian Dolla
                  </MenuItem>
                  <MenuItem id="Canadian Dollar" value="Canadian Dollar">
                    Canadian Dollar
                  </MenuItem>
                  <MenuItem id="Pound" value="Pound">
                    Pound
                  </MenuItem>
                  <MenuItem id="Yen" value="Yen">
                    Yen
                  </MenuItem>
                  <MenuItem id="Franc" value="Franc">
                    Franc
                  </MenuItem>
                  <MenuItem id="Won" value="Won">
                    Won
                  </MenuItem>
                  <MenuItem id="Peso" value="Peso">
                    Peso
                  </MenuItem>
                  <MenuItem id="Rupee" value="Rupee">
                    Rupee
                  </MenuItem>
                </Select>
              </div>

              <div>
                <label id="label3" htmlFor="date">
                  Price Expiration Date
                </label>
                <br></br>
                <TextField
                  id="date"
                  type="date"
                  variant="outlined"
                  style={{ background: "#e9f2e9" }}
                  value={
                    this.state.currentDeal.expirationPriceDate != null
                      ? this.state.currentDeal.expirationPriceDate.value
                      : ""
                  }
                  className={this.state.styles.textField}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={this.onChangeExpirationPriceDate}
                />
              </div>
              <br></br>

              <div>
                <label id="label7" htmlFor="ingredients">
                  Ingredient:
                </label>
                <div id="ingredientsss" />

                <Autocomplete
                  id="free-solo-demo"
                  onChange={this.onChangeIngredient}
                  options={this.state.availableIngredients}
                  getOptionLabel={(option) => option.description}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      style={{ background: "#e9f2e9" }}
                      margin="normal"
                      variant="outlined"
                    />
                  )}
                />
              </div>
              <br></br>
            </div>
            <br></br>
            <br></br>
            <Button
              id="upd"
              variant="contained"
              style={{ background: "#a6ce39", color: "#FFF" }}
              size="small"
              className={this.state.styles.addUser}
              onClick={this.updateDeal}
              disabled={
                this.state.currentDeal.id === "" ||
                this.state.currentDeal.unitPrice.unit === null ||
                this.state.currentDeal.expirationPriceDate === null ||
                this.state.currentDeal.ingredient === null ||
                this.state.currentDeal.unitPrice.amount === null
              }
            >
              Update
            </Button>
            &nbsp;
            <Button
              id="del"
              variant="contained"
              style={{ background: "#c94254", color: "#FFF" }}
              size="small"
              className={this.state.styles.addUser}
              onClick={this.deleteDeal}
            >
              Delete
            </Button>
            <br></br>
            <br></br>
            {this.state.error !== undefined ? (
              <p
                style={{
                  color: this.state.error !== null ? "red" : "green",
                }}
              >
                {this.state.error !== null ? this.state.error : "Deal Updated!"}
              </p>
            ) : undefined}
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on an Deal...</p>
          </div>
        )}
      </div>
    );
  }
}
