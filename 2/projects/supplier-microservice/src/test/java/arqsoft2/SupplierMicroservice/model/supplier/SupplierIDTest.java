package arqsoft2.SupplierMicroservice.model.supplier;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class SupplierIDTest {
    @Test
    void verifyIfIDisEmpty(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new SupplierID(""); });
        assertEquals("The ID cannot be empty!", e.getMessage());
    }

    @Test
    void verifyIfIDIsCorrect(){
        new SupplierID("ID1");
        assertTrue(true);
    }
}