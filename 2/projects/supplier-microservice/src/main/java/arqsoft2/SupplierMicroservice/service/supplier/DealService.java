package arqsoft2.SupplierMicroservice.service.supplier;

import java.util.*;

import arqsoft2.SupplierMicroservice.model.ingredient.IngredientDTO;
import arqsoft2.SupplierMicroservice.model.supplier.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import arqsoft2.SupplierMicroservice.repository.supplier.DealRepository;
import arqsoft2.SupplierMicroservice.repository.supplier.DealRepositoryImpl;

@Component
public class DealService {
    
    @Autowired
    DealRepository dealRepository;

    @Autowired
    DealRepositoryImpl dealRepositoryImpl;

    public DealService(DealRepository dealRepository, DealRepositoryImpl dealRepositoryImpl){
        this.dealRepository = dealRepository;
        this.dealRepositoryImpl = dealRepositoryImpl;
    }

    public List<ReturnDealDTO> getAllDeals() {
        try {
            List<Deal> deals = dealRepositoryImpl.findAllDeals();
            List<ReturnDealDTO> ret = new ArrayList<>();
            deals.forEach(element -> {
                IngredientDTO ingredient = IngredientMicroserviceCommunication.getIngredient(element);
                ret.add(element.toReturnDTO(ingredient));
            });
            return ret;
        } catch (Exception error) {
            return new ArrayList<>();
        }
    }

    public ReturnDealDTO getDealById(@PathVariable("id") String id) {

        // VERSION WITH REPOSITORYIMPL 
        Deal dealData = dealRepositoryImpl.findDealById(id);

        if (dealData!=null) {
            IngredientDTO ingredient = IngredientMicroserviceCommunication.getIngredient(dealData);
            return dealData.toReturnDTO(ingredient);
        } else {
            return null;
        }

        // Optional<Deal> dealData = dealRepository.findById(new ID(id));

        // if (dealData.isPresent()) {
        //     return dealData.get().toDTO();
        // } else {
        //     return null;
        // }
    }

    public InputDealDTO createDeal(@RequestBody Deal deal) throws Exception {
        try {
            IngredientMicroserviceCommunication.getIngredient(deal);
            Deal _deal = dealRepository.save(deal);
            return _deal.toDTO();
        } catch (Exception error) {
            System.out.println(error);
            throw new Exception("Error occurred while registering the desired deal!");
        }
    }

    public InputDealDTO updateDeal(@PathVariable("id") DealID id, @RequestBody Deal deal) throws Exception {
        try{
            Optional<Deal> dealData = dealRepository.findById(id);
            if (dealData.isPresent()) {
                Deal _deal = dealData.get();
                IngredientMicroserviceCommunication.getIngredient(deal);
                _deal.update(deal);
                return dealRepository.save(_deal).toDTO();
            } else {
                throw new Exception();
            }
        } catch (Exception e){
            throw new Exception("Error occurred while updating the desired deal!");
        }
    }

    public HttpStatus deleteDeal(@PathVariable("id") DealID id) {
        try{
            dealRepository.deleteById(id);
            return HttpStatus.NO_CONTENT;
        } catch ( Exception error){
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

    public HttpStatus deleteAllDeals() {
        try {
            dealRepository.deleteAll();
            return HttpStatus.NO_CONTENT;
        } catch (Exception error){
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }
}
