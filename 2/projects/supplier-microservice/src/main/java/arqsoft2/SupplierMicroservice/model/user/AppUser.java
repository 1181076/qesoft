package arqsoft2.SupplierMicroservice.model.user;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class AppUser {
	
    @EmbeddedId
    private Email email;

    private Name name;
    private Password password;
    private Role role;

    public AppUser(){

    }

    public AppUser(Email email, Name name, Password password, Role role){
        this.email = email;
        this.name = name;
        this.password = password;
        this.role = role;
    }

	public Email getEmail() {
		return this.email;
	}

	public Name getName() {
		return this.name;
	}

	public Role getRole() {
		return this.role;
	}

    public Password getPassword() {
        return password;
    }
}
