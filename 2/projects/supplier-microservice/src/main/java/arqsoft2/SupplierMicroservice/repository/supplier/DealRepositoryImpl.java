package arqsoft2.SupplierMicroservice.repository.supplier;

import java.util.List;

import arqsoft2.SupplierMicroservice.model.supplier.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

public class DealRepositoryImpl implements DealCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Deal> findAllDeals(){

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Deal> cq = cb.createQuery(Deal.class);
        Root<Deal> root = cq.from(Deal.class);
        
        cq.select(root);

        TypedQuery<Deal> q = em.createQuery(cq);

        return q.getResultList();

    }

    @Override
    public Deal findDealById(String id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Deal> cq = cb.createQuery(Deal.class);
        Root<Deal> root = cq.from(Deal.class);
        Path<String> idValue = root.get("id").get("value");
        
        cq.select(root).where(cb.equal(idValue, id));

        TypedQuery<Deal> q = em.createQuery(cq);

        return q.getSingleResult();
    }



}
