package arqsoft2.SupplierMicroservice.model.user;

public enum RoleValue {
    SCHOOLREPRESENTATIVE, SUPPLIERMANAGER, STOCKMANAGER, SCHOOLMANAGER
}
