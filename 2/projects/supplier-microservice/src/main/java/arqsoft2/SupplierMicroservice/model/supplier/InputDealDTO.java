package arqsoft2.SupplierMicroservice.model.supplier;

import arqsoft2.SupplierMicroservice.model.ingredient.IngredientID;

import java.util.List;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class InputDealDTO {
    
    private DealID id;
    private UnitPrice unitPrice;
    private ExpirationPriceDate expirationPriceDate;
    private IngredientID ingredient;

    public InputDealDTO(){
    }

    public InputDealDTO(DealID id, UnitPrice unitPrice, ExpirationPriceDate expirationPriceDate, IngredientID ingredient){
        this.id = id;
        this.unitPrice = unitPrice;
        this.expirationPriceDate = expirationPriceDate;
        this.ingredient = ingredient;
    }

    public DealID getId() {
        return this.id;
    }

    public UnitPrice getUnitPrice() {
        return this.unitPrice;
    }

    public ExpirationPriceDate getExpirationPriceDate() {
        return this.expirationPriceDate;
    }

    public IngredientID getIngredient() {
        return this.ingredient;
    }

    @Override
    public boolean equals(Object k){
        return true;
    }
}
