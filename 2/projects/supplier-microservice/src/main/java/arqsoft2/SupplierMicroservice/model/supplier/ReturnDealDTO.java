package arqsoft2.SupplierMicroservice.model.supplier;

import arqsoft2.SupplierMicroservice.model.ingredient.IngredientDTO;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Date;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class ReturnDealDTO {

    private String id;
    private UnitPrice unitPrice;
    private Date expirationPriceDate;
    private IngredientDTO ingredient;

    public ReturnDealDTO(){
    }

    public ReturnDealDTO(@JsonProperty("id") String id, @JsonProperty("unitPrice") UnitPrice unitPrice, @JsonProperty("expirationPriceDate") Date expirationPriceDate, @JsonProperty("ingredient") IngredientDTO ingredient){
        this.id = id;
        this.unitPrice = unitPrice;
        this.expirationPriceDate = expirationPriceDate;
        this.ingredient = ingredient;
    }

    public String getId() {
        return this.id;
    }

    public UnitPrice getUnitPrice() {
        return this.unitPrice;
    }

    public Date getExpirationPriceDate() {
        return this.expirationPriceDate;
    }

    public IngredientDTO getIngredient() {
        return this.ingredient;
    }

    @Override
    public boolean equals(Object k){
        return true;
    }
}
