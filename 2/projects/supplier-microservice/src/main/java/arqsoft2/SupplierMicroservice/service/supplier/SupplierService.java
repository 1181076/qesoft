package arqsoft2.SupplierMicroservice.service.supplier;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import arqsoft2.SupplierMicroservice.model.ingredient.IngredientDTO;
import arqsoft2.SupplierMicroservice.model.supplier.*;
import arqsoft2.SupplierMicroservice.repository.supplier.DealRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import arqsoft2.SupplierMicroservice.repository.supplier.SupplierRepository;
import arqsoft2.SupplierMicroservice.repository.supplier.SupplierRepositoryImpl;

@Component
public class SupplierService {

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    SupplierRepositoryImpl supplierRepositoryImpl;

    @Autowired
    DealRepository dealRepository;

    public SupplierService(SupplierRepository supplierRepository, SupplierRepositoryImpl supplierRepositoryImpl, DealRepository dealRepository){
        this.supplierRepository = supplierRepository;
        this.supplierRepositoryImpl = supplierRepositoryImpl;
        this.dealRepository = dealRepository;
    }

    public List<ReturnSupplierDTO> getAllSuppliers() {
        try {
            List<Supplier> suppliers = supplierRepositoryImpl.findAllSuppliers();
            List<ReturnSupplierDTO> ret = new ArrayList<>();
            suppliers.forEach(element -> {
                List<ReturnDealDTO> dealDTOs = new ArrayList<>();
                element.getDeals().forEach(deal -> {
                    IngredientDTO ingredientDTO = IngredientMicroserviceCommunication.getIngredient(deal);
                    dealDTOs.add(deal.toReturnDTO(ingredientDTO));
                });
                ret.add(element.toReturnDTO(dealDTOs));
            });
            return ret;
        } catch (Exception error) {
            return new ArrayList<>();
        }
    }

    public ReturnSupplierDTO getSupplierById(String id) {
        Supplier supplierData = supplierRepositoryImpl.findSupplierById(id);

        if (supplierData!=null) {
            List<ReturnDealDTO> dealDTOs = new ArrayList<>();
            supplierData.getDeals().forEach(deal -> {
                IngredientDTO ingredientDTO = IngredientMicroserviceCommunication.getIngredient(deal);
                dealDTOs.add(deal.toReturnDTO(ingredientDTO));
            });
            return supplierData.toReturnDTO(dealDTOs);
        } else {
            return null;
        }
    }

    public InputSupplierDTO createSupplier(InputSupplierDTO supplier) {
        try {
            List<Deal> deals = dealRepository.findAllById(supplier.getDeals());
            Supplier _supplier = new Supplier(
                    supplier.getId(),
                    supplier.getName(),
                    supplier.getAddress(),
                    supplier.getEmails(),
                    supplier.getContacts(),
                    supplier.getDescription(),
                    deals
            );
            supplierRepository.save(_supplier);
            return supplier;
        } catch (Exception error) {
            return null;
        }
    }

    public InputSupplierDTO updateSupplier(SupplierID id, InputSupplierDTO supplierDTO) {
        Optional<Supplier> supplierData = supplierRepository.findById(id);

        if (supplierData.isPresent()) {
            Supplier _supplier = supplierData.get();
            List<Deal> deals = dealRepository.findAllById(supplierDTO.getDeals());
            Supplier supplier = new Supplier(
                    supplierDTO.getId(),
                    supplierDTO.getName(),
                    supplierDTO.getAddress(),
                    supplierDTO.getEmails(),
                    supplierDTO.getContacts(),
                    supplierDTO.getDescription(),
                    deals
            );
            _supplier.update(supplier);
            return supplierDTO;
        } else {
            return null;
        }
    }

    public HttpStatus deleteSupplier(SupplierID id) {
        try{
            supplierRepository.deleteById(id);
            return HttpStatus.NO_CONTENT;
        } catch ( Exception error){
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

    public HttpStatus deleteAllSuppliers() {
        try {
            supplierRepository.deleteAll();
            return HttpStatus.NO_CONTENT;
        } catch (Exception error){
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }
}
