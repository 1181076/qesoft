package arqsoft2.SupplierMicroservice.controller.supplier;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import arqsoft2.SupplierMicroservice.model.supplier.ReturnSupplierDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import arqsoft2.SupplierMicroservice.model.supplier.SupplierID;
import arqsoft2.SupplierMicroservice.model.supplier.Supplier;
import arqsoft2.SupplierMicroservice.model.supplier.InputSupplierDTO;
import arqsoft2.SupplierMicroservice.service.supplier.SupplierService;

@CrossOrigin(origins = "http://localhost:8088")
@RestController
@RequestMapping("/api")
@Tag(name = "Supplier Controller", description = "All supplier operations")
public class SupplierController {
    
    @Autowired
	SupplierService supplierService;

    public SupplierController(SupplierService supplierService){
        this.supplierService = supplierService;
    }

	@Operation(summary = "Get all suppliers", operationId = "getSuppliers", description = "Get all registered suppliers")
    @GetMapping("/suppliers")
    public ResponseEntity<List<ReturnSupplierDTO>> getAllSuppliers(){
        try {
			List<ReturnSupplierDTO> suppliers = supplierService.getAllSuppliers();
			return new ResponseEntity<>(suppliers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

	@Operation(summary = "Get a specific supplier", operationId = "getSupplier", description = "Get a specific supplier by providing its identifier")
    @GetMapping("/suppliers/{id}")
	public ResponseEntity<ReturnSupplierDTO> getSupplierById(@PathVariable("id") String id) {
		ReturnSupplierDTO supplierData = supplierService.getSupplierById(id);

		if (supplierData != null) {
			return new ResponseEntity<>(supplierData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@Operation(summary = "Create a new supplier", operationId = "createSupplier", description = "Create a new supplier by providing its information")
    @PostMapping("/suppliers")
	public ResponseEntity<InputSupplierDTO> createSupplier(@RequestBody InputSupplierDTO supplier) {

		try {
			InputSupplierDTO _supplier = supplierService
						.createSupplier(supplier);
			return new ResponseEntity<>(_supplier, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Operation(summary = "Update a specific supplier", operationId = "updateSupplier", description = "Update a specific supplier by providing its identifier")
    @PutMapping("/suppliers/{id}")
	public ResponseEntity<InputSupplierDTO> updateSupplier(@PathVariable("id") String id, @RequestBody InputSupplierDTO supplierDTO) throws ParseException {
		InputSupplierDTO supplierData = supplierService.updateSupplier(new SupplierID(id), supplierDTO);

		if (supplierData != null) {
			return new ResponseEntity<>(supplierData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@Operation(summary = "Delete a specific supplier", operationId = "deleteSupplier", description = "Delete a specific supplier by providing its identifier")
	@DeleteMapping("/suppliers/{id}")
	public ResponseEntity<HttpStatus> deleteSupplier(@PathVariable("id") String id) {
			HttpStatus st = supplierService.deleteSupplier(new SupplierID(id));
			return new ResponseEntity<>(st);
	}

	@Operation(summary = "Delete all suppliers", operationId = "deleteSuppliers", description = "Delete all registered suppliers")
	@DeleteMapping("/suppliers")
	public ResponseEntity<HttpStatus> deleteAllSuppliers() {
			HttpStatus st = supplierService.deleteAllSuppliers();
			return new ResponseEntity<>(st);
		
	}

}
