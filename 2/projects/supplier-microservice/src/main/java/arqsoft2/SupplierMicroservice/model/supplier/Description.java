package arqsoft2.SupplierMicroservice.model.supplier;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;

public final class Description implements ValueObject {

    private final String value;

    @JsonCreator
    public Description(@JsonProperty("value") String value) {
        if(value.isEmpty()){
            throw new IllegalArgumentException("Description cannot be empty!");
        }

        if(value.trim().length() > 200){
            throw new IllegalArgumentException("Description cannot be over 200 characters!");
        }

        this.value = value;
    }

    public String value() {
        return value;
    }

    public final String getValue() {
        return this.value;
    }
}
