package arqsoft2.SupplierMicroservice.model.ingredient;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class IngredientDTO {

    private String id;
    private String description;
    private int quantity;
    private List<LastUpdatedDate> lastUpdatedDate;
    private int upperLimit;
    private int lowerLimit;
    private String units;

    public IngredientDTO(){

    }

    public IngredientDTO(@JsonProperty("id") String id, @JsonProperty("description") String description, @JsonProperty("quantity") int quantity, @JsonProperty("lastUpdatedDate") List<LastUpdatedDate> lastUpdatedDate, @JsonProperty("upperLimit") int upperLimit, @JsonProperty("lowerLimit") int lowerLimit, @JsonProperty("units") String units){
        this.id = id;
        this.description = description;
        this.quantity = quantity;
        this.lastUpdatedDate = lastUpdatedDate;
        this.upperLimit = upperLimit;
        this.lowerLimit = lowerLimit;
        this.units = units;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public int getQuantity() {
        return quantity;
    }

    public List<LastUpdatedDate> getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public int getUpperLimit() {
        return upperLimit;
    }

    public int getLowerLimit() {
        return lowerLimit;
    }

    public String getUnits() {
        return units;
    }
}
