package arqsoft2.SupplierMicroservice.model.supplier;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ReturnSupplierDTO {
    private String id;
    private String name;
    private String address;
    private List<String> emails;
    private List<Integer> contacts;
    private String description;
    private List<ReturnDealDTO> deals;

    public ReturnSupplierDTO(@JsonProperty("id") String id, @JsonProperty("name") String name, @JsonProperty("address") String address, @JsonProperty("emails") List<String> emails, @JsonProperty("contacts") List<Integer> contacts, @JsonProperty("description") String description, @JsonProperty("deals") List<ReturnDealDTO> deals){
        this.id = id;
        this.name = name;
        this.address = address;
        this.emails = emails;
        this.contacts = contacts;
        this.description = description;
        this.deals = deals;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public List<String> getEmails() {
        return emails;
    }

    public List<Integer> getContacts() {
        return contacts;
    }

    public String getDescription() {
        return description;
    }

    public List<ReturnDealDTO> getDeals() {
        return deals;
    }
}
