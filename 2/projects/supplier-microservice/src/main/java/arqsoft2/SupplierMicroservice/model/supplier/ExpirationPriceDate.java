package arqsoft2.SupplierMicroservice.model.supplier;

import java.sql.Date;
import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;

public final class ExpirationPriceDate implements ValueObject {
        
    private final Date value;

    @JsonCreator
    public ExpirationPriceDate(@JsonProperty("value") Date value){
        Date currentDate = new Date(Calendar.getInstance().getTime().getTime());
        if(value.before(currentDate)){
            throw new IllegalArgumentException("The expiration price date must be above the current date!");
        }

        this.value= value;
    }
    
    public Date value(){
        return value;
    }

    public final Date getValue() {
		return this.value;
	}
}
