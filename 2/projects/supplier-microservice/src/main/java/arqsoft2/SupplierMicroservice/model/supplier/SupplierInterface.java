package arqsoft2.SupplierMicroservice.model.supplier;

import java.util.List;

public interface SupplierInterface {

    public SupplierID getId();
    public Name getName();
    public Address getAddress();
    public List<Email> getEmails();
    public List<Contact> getContacts();
    public Description getDescription();
    public List<Deal> getDeals();

    public void update(Supplier supplier);

}
