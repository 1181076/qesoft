package arqsoft2.SupplierMicroservice.service.supplier;

import arqsoft2.SupplierMicroservice.model.ingredient.IngredientDTO;
import arqsoft2.SupplierMicroservice.model.ingredient.IngredientID;
import arqsoft2.SupplierMicroservice.model.supplier.Deal;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Component
public class IngredientMicroserviceCommunication {
    @Autowired
    static EurekaClient eurekaClient;

    public IngredientMicroserviceCommunication(EurekaClient ec) {
        eurekaClient = ec;
    }

    public static IngredientDTO getIngredient(Deal deal){
        Application application = eurekaClient.getApplication("Ingredient_Microservice");
        InstanceInfo instanceInfo = application.getInstances().get(0);
        String url = "http://" + instanceInfo.getIPAddr() + ":" + instanceInfo.getPort() + "/api/ingredient?id=" + deal.getIngredient().getValue();
        IngredientDTO dto = new RestTemplate().getForObject(url, IngredientDTO.class);
        return dto;
    }
}
