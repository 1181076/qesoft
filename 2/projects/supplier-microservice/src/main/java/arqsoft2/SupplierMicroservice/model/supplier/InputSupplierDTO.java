package arqsoft2.SupplierMicroservice.model.supplier;

import java.util.List;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class InputSupplierDTO {

    private SupplierID id;
    private Name name;
    private Address address;
    private List<Email> emails;
    private List<Contact> contacts;
    private Description description;
    private List<DealID> deals;

    public InputSupplierDTO() {
    }

    public InputSupplierDTO(SupplierID id, Name name, Address address, List<Email> emails, List<Contact> contacts, Description description, List<DealID> deals) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.emails = emails;
        this.contacts = contacts;
        this.description = description;
        this.deals = deals;
    }

    public SupplierID getId() {
        return this.id;
    }

    public Name getName() {
        return this.name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Address getAddress() {
        return this.address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Email> getEmails() {
        return this.emails;
    }

    public void setEmails(List<Email> emails) {
        this.emails = emails;
    }

    public List<Contact> getContacts() {
        return this.contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public Description getDescription() {
        return this.description;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    public List<DealID> getDeals() {
        return this.deals;
    }

    public void setDeals(List<DealID> deals) {
        this.deals = deals;
    }

    @Override
    public boolean equals(Object k){
        return true;
    }
}
