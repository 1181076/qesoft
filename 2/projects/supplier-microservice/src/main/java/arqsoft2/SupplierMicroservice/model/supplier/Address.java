package arqsoft2.SupplierMicroservice.model.supplier;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;

public final class Address implements ValueObject{
    
    private final String value;

    @JsonCreator
    public Address(@JsonProperty("value") String value){
        if(value.isEmpty()){
            throw new IllegalArgumentException("The address cannot be empty!");
        }
        this.value= value;
    }
    
    public String value(){
        return value;
    }

    public final String getValue() {
		return this.value;
	}
}
