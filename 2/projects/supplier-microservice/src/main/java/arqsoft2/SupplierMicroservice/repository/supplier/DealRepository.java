package arqsoft2.SupplierMicroservice.repository.supplier;

import arqsoft2.SupplierMicroservice.model.supplier.DealID;
import org.springframework.data.jpa.repository.JpaRepository;

import arqsoft2.SupplierMicroservice.model.supplier.Deal;

public interface DealRepository extends JpaRepository<Deal, DealID>, DealCustomRepository {

}
