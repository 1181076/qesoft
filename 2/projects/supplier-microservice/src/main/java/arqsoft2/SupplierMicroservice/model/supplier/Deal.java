package arqsoft2.SupplierMicroservice.model.supplier;

import javax.persistence.*;

import arqsoft2.SupplierMicroservice.model.ingredient.IngredientDTO;
import arqsoft2.SupplierMicroservice.model.ingredient.IngredientID;
import eapli.framework.representations.dto.DTOable;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

@Entity
public class Deal implements DealInterface, DTOable<InputDealDTO> {

    @EmbeddedId
    @Schema(name = "id")
    private DealID id;
    @Schema(name = "unitPrice")
    private UnitPrice unitPrice;
    @Schema(name = "expirationDate")
    private ExpirationPriceDate expirationPriceDate;
    @Schema(name = "ingredient")
    private IngredientID ingredient;

    public Deal(){
    }

    public Deal(DealID id, UnitPrice unitPrice, ExpirationPriceDate expirationPriceDate, IngredientID ingredient){
        this.id = id;
        this.unitPrice = unitPrice;
        this.expirationPriceDate = expirationPriceDate;
        this.ingredient = ingredient;
    }

    public DealID getId() {
        return this.id;
    }

    public UnitPrice getUnitPrice() {
        return this.unitPrice;
    }

    public ExpirationPriceDate getExpirationPriceDate() {
        return this.expirationPriceDate;
    }
    
    public IngredientID getIngredient() {
        return this.ingredient;
    }


    public void update(Deal deal){
        this.unitPrice = deal.unitPrice;
        this.expirationPriceDate = deal.expirationPriceDate;
        this.ingredient = deal.ingredient;
    }

    @Override
    public InputDealDTO toDTO() {
        return new InputDealDTO(id, unitPrice, expirationPriceDate, ingredient);
    }

    @Override
    public ReturnDealDTO toReturnDTO(IngredientDTO ingredient) {
        return new ReturnDealDTO(id.getValue(), unitPrice, expirationPriceDate.getValue(), ingredient);
    }
}
