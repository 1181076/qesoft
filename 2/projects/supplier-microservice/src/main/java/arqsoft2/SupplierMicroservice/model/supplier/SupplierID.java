package arqsoft2.SupplierMicroservice.model.supplier;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;

public final class SupplierID implements ValueObject {

    private final String value;

    public SupplierID(){
        value = "default";
    }

    public final String getValue() {
        return this.value;
    }

    @JsonCreator
    public SupplierID(@JsonProperty("value") String value) {
        if(value.isEmpty()){
            throw new IllegalArgumentException("The ID cannot be empty!");
        }
        this.value = value;
    }

    public String value() {
        return value;
    }

    @Override
    public boolean equals(Object o) {

        SupplierID id = (SupplierID) o;

        return id.value().equals(this.value);

    }
}
