package arqsoft2.SupplierMicroservice.model.ingredient;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class LastUpdatedDate {
    private final int amount;
    private final String date;

    public LastUpdatedDate(@JsonProperty("amount") int amount, @JsonProperty("date") String date){
        this.date= date;
        this.amount= amount;
    }

    public int getAmount() {
        return amount;
    }

    public String getDate() {
        return date;
    }
}

