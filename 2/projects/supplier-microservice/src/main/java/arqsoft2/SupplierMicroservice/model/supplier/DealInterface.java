package arqsoft2.SupplierMicroservice.model.supplier;

import arqsoft2.SupplierMicroservice.model.ingredient.IngredientDTO;
import arqsoft2.SupplierMicroservice.model.ingredient.IngredientID;

import java.util.List;

public interface DealInterface {

    public DealID getId();
    public UnitPrice getUnitPrice();
    public ExpirationPriceDate getExpirationPriceDate();
    public IngredientID getIngredient();


    public void update(Deal deal);

    public InputDealDTO toDTO();
    public ReturnDealDTO toReturnDTO(IngredientDTO ingredient);
}
