package arqsoft2.SupplierMicroservice.repository.supplier;

import java.util.List;

import arqsoft2.SupplierMicroservice.model.supplier.*;

public interface SupplierCustomRepository {

    public Supplier findSupplierById(String Id);
    
    public List<Supplier> findAllSuppliers();
    
}
