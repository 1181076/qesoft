package arqsoft2.SupplierMicroservice.controller.supplier;

import java.util.ArrayList;
import java.util.List;

import arqsoft2.SupplierMicroservice.model.supplier.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import arqsoft2.SupplierMicroservice.service.supplier.DealService;

@CrossOrigin(origins = "http://localhost:8088")
@RestController
@RequestMapping("/api")
@Tag(name = "Deal Controller", description = "All deal operations")
public class DealController {
    
    @Autowired
	DealService dealService;


	public DealController(DealService dealService){
		this.dealService = dealService;
	}

	@Operation(summary = "Get all deals", operationId = "getDeals", description = "Get all registered deals")
    @GetMapping("/deals")
	public ResponseEntity<List<ReturnDealDTO>> getAllDeals() {
		try {
			List<ReturnDealDTO> deals = dealService.getAllDeals();
			return new ResponseEntity<>(deals, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Operation(summary = "Get a specific deal", operationId = "getDeal", description = "Get a specific deal by providing its identifier")
    @GetMapping("/deals/{id}")
	public ResponseEntity<ReturnDealDTO> getDealById(@PathVariable("id") String id) {
		ReturnDealDTO dealData = dealService.getDealById(id);

		if (dealData != null) {
			return new ResponseEntity<>(dealData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@Operation(summary = "Create a new deal", operationId = "createDeal", description = "Create a new deal by providing its information")
	@PostMapping("/deals")
	public ResponseEntity<InputDealDTO> createDeal(@RequestBody Deal deal) throws Exception {
		InputDealDTO _deal = dealService
					.createDeal(deal);
		return new ResponseEntity<>(_deal, HttpStatus.CREATED);
	}

	@Operation(summary = "Update a specific deal", operationId = "updateDeal", description = "Update a specific deal by providing its identifier")
    @PutMapping("/deals/{id}")
	public ResponseEntity<InputDealDTO> updateDeal(@PathVariable("id") String id, @RequestBody Deal deal) throws Exception {
		InputDealDTO dealData = dealService.updateDeal(new DealID(id), deal);
		return new ResponseEntity<>(dealData, HttpStatus.OK);
	}

	@Operation(summary = "Delete a specific deal", operationId = "deleteDeal", description = "Delete a specific deal by providing its identifier")
	@DeleteMapping("/deals/{id}")
	public ResponseEntity<HttpStatus> deleteDeal(@PathVariable("id") String id) {
			HttpStatus st = dealService.deleteDeal(new DealID(id));
			return new ResponseEntity<>(st);
	}

	@Operation(summary = "Delete all deals", operationId = "deleteDeals", description = "Delete all registered deals")
	@DeleteMapping("/deals")
	public ResponseEntity<HttpStatus> deleteAllDeals() {
			HttpStatus st = dealService.deleteAllDeals();
			return new ResponseEntity<>(st);
		
	}
}
