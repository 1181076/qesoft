package arqsoft2.SupplierMicroservice.repository.supplier;

import org.springframework.data.jpa.repository.JpaRepository;

import arqsoft2.SupplierMicroservice.model.supplier.SupplierID;
import arqsoft2.SupplierMicroservice.model.supplier.Supplier;

public interface SupplierRepository extends JpaRepository<Supplier, SupplierID>, SupplierCustomRepository {
    
}
