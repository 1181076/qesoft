//package arqsoft2.SupplierMicroservice.service.supplier.user;
//
//import arqsoft2.SupplierMicroservice.model.user.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.Collection;
//
//@Service
//public class AppUserDetailsService implements UserDetailsService {
//
//	//@Autowired
//	//private AppUserRepositoryImpl userRepository;
//
//	@Override
//	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
//		//AppUser user = userRepository.findUserById(email);
//		AppUser user = new AppUser(new Email("1180964@isep.ipp.pt"), new Name("Francisco"), new Password(new BCryptPasswordEncoder().encode("iuhdawihg321#RE1")), new Role(RoleValue.SCHOOLMANAGER));
//
//		if (user == null) {
//			throw new UsernameNotFoundException("Email " + email + " not found");
//		}
//		return new org.springframework.security.core.userdetails.User(user.getEmail().getValue(), user.getPassword().getValue(),
//				getGrantedAuthority(user));
//	}
//
//	private Collection<GrantedAuthority> getGrantedAuthority(AppUser user) {
//		Collection<GrantedAuthority> authorities = new ArrayList<>();
//		authorities.add(new SimpleGrantedAuthority(user.getRole().getValue().name()));
//		return authorities;
//	}
//}