package arqsoft2.SupplierMicroservice.model.supplier;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import eapli.framework.representations.dto.DTOable;
import io.swagger.v3.oas.annotations.media.Schema;

@Entity
public class Supplier implements SupplierInterface {

    @EmbeddedId
    @Schema(name = "id")
    private SupplierID id;
    @Schema(name = "name")
    private Name name;
    @Schema(name = "address")
    private Address address;
    
	@ElementCollection
    @Schema(name = "emails")
    private List<Email> emails;
    
	@ElementCollection
    @Schema(name = "contacts")
    private List<Contact> contacts;
    @Schema(name = "description")
    private Description description;
    
    @ManyToMany
    @Schema(name = "deals")
    private List<Deal> deals;

    public Supplier() {
    }

    public Supplier(SupplierID id, Name name, Address address, List<Email> emails, List<Contact> contacts, Description description, List<Deal> deals) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.emails = emails;
        this.contacts = contacts;
        this.description = description;
        this.deals = deals;
    }

    public SupplierID getId() {
        return this.id;
    }

    public Name getName() {
        return this.name;
    }

    public Address getAddress() {
        return this.address;
    }

    public List<Email> getEmails() {
        return this.emails;
    }

    public List<Contact> getContacts() {
        return this.contacts;
    }

    public Description getDescription() {
        return this.description;
    }

    public List<Deal> getDeals() {
        return this.deals;
    }

    public void update(Supplier supplier){
        this.name = supplier.name;
        this.address = supplier.address;
        this.emails = supplier.emails;
        this.contacts = supplier.contacts;
        this.description = supplier.description;
        this.deals = supplier.deals;
    }

    public ReturnSupplierDTO toReturnDTO(List<ReturnDealDTO> deals){
        List<String> emails = new ArrayList<>();
        for (Email email: this.emails) {
            emails.add(email.getValue());
        }

        List<Integer> contacts = new ArrayList<>();
        for (Contact contact: this.contacts) {
            contacts.add(contact.getValue());
        }

        return new ReturnSupplierDTO(id.getValue(), name.getValue(), address.getValue(), emails, contacts, description.getValue(), deals);
    }

}
