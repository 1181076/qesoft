package arqsoft2.SupplierMicroservice.repository.supplier;

import java.util.List;

import arqsoft2.SupplierMicroservice.model.supplier.*;

public interface DealCustomRepository {

    public Deal findDealById(String Id);
    
    public List<Deal> findAllDeals();
    
}
