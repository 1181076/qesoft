package arqsoft2.UserMicroservice.model.user;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class IDTest {
    @Test
    void verifyIfIDisEmpty(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Email(""); });
        assertEquals("The ID cannot be empty!", e.getMessage());
    }

    @Test
    void verifyIfIDIsCorrect(){
        new Email("ID1");
        assertTrue(true);
    }
}