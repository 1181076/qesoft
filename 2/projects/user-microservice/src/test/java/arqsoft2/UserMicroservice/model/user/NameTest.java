package arqsoft2.UserMicroservice.model.user;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class NameTest {
    @Test
    void verifyIfNameIsEmpty(){
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> { new Name(""); });
        assertEquals("The name cannot be empty!", e.getMessage());
    }

    @Test
    void verifyIfNameIsCorrect(){
        new Name("Francisco");
        assertTrue(true);
    }
}