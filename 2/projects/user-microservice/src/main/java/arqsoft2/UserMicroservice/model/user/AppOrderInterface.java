package arqsoft2.UserMicroservice.model.user;

import java.util.List;

import javax.persistence.*;
import eapli.framework.representations.dto.DTOable;

public interface AppOrderInterface{

    public OrderID getId();
    public Day getDay();
    public List<SandwichOrder> getSandwichOrders();

    public void update(AppOrder order);

    public InputAppOrderDTO toDTO();
}
