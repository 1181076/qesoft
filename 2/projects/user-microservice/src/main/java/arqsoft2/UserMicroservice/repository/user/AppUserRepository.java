package arqsoft2.UserMicroservice.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;

import arqsoft2.UserMicroservice.model.user.Email;
import arqsoft2.UserMicroservice.model.user.AppUser;

public interface AppUserRepository extends JpaRepository<AppUser, Email>, AppUserCustomRepository {

}
