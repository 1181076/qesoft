package arqsoft2.UserMicroservice.model.user;

import arqsoft2.UserMicroservice.model.school.ReturnSchoolDTO;

import java.util.List;

public class ReturnAuthenticationDTO {
    private String email;
    private String name;
    private String role;
    private String token;

    public ReturnAuthenticationDTO(String email, String name, String role, String token) {
        this.email = email;
        this.name = name;
        this.role = role;
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getRole() {
        return role;
    }

    public String getToken() {
        return token;
    }
}
