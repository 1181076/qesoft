package arqsoft2.UserMicroservice.model.user;


import java.util.List;

import arqsoft2.UserMicroservice.model.school.ReturnSchoolDTO;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class ReturnAppUserDTO {

    
    private String email;
    private String name;
    private String password;
    private String role;
    private List<ReturnAppOrderDTO> orders;
	private ReturnSchoolDTO school;
    
    public ReturnAppUserDTO(){

    }

    public ReturnAppUserDTO(String email, String name, String password, String role, List<ReturnAppOrderDTO> orders, ReturnSchoolDTO school){
        this.email = email;
        this.name = name;
        this.password = password;
        this.role = role;
        this.orders = orders;
        this.school = school;
    }

    public String getEmail() {
		return this.email;
	}

	public String getName() {
		return this.name;
	}

	public String getPassword() {
		return this.password;
	}

	public String getRole() {
		return this.role;
	}

	public List<ReturnAppOrderDTO> getOrders() {
		return this.orders;
	}

    public ReturnSchoolDTO getSchool() {
		return this.school;
	}


    @Override
    public boolean equals(Object o) {
  
		AppUser u = (AppUser) o;

        return u.getEmail().equals(this.email); 
          
    }
}
