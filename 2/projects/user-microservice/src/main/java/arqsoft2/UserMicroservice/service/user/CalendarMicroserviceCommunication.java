package arqsoft2.UserMicroservice.service.user;

import arqsoft2.UserMicroservice.model.calendar.DownTimeDTO;
import io.swagger.v3.core.util.Json;

import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

@Component
public class CalendarMicroserviceCommunication {
    @Autowired
    static EurekaClient eurekaClient;

    public CalendarMicroserviceCommunication(EurekaClient ec) {
        eurekaClient = ec;
    }

    public static ArrayList<DownTimeDTO> getAllDowntimes() throws ParseException{
        Application application = eurekaClient.getApplication("CALENDAR-MICROSERVICE");
        InstanceInfo instanceInfo = application.getInstances().get(0);
        String url = "http://" + instanceInfo.getIPAddr() + ":" + instanceInfo.getPort() + "/api/downtimes";
        ResponseEntity<Object[]> responseEntity = new RestTemplate().getForEntity(url, Object[].class);
        String[] downtimes = Arrays.toString(responseEntity.getBody()).split("],");
        ArrayList<DownTimeDTO> result = new ArrayList<DownTimeDTO>();
        for(int i = 0; i < downtimes.length; i++) {
        	downtimes[i].replace("[", "");
        	downtimes[i].replace("]", "");
        	String[] params = downtimes[i].split(",");
        	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        	LocalDateTime s = LocalDateTime.parse(params[1].trim(), formatter);
        	LocalDateTime e = LocalDateTime.parse(params[3].replace("]", "").trim(), formatter);
        	Date startP = java.util.Date.from(s.atZone(ZoneId.systemDefault()).toInstant());
        	Date endP = java.util.Date.from(e.atZone(ZoneId.systemDefault()).toInstant());
        	DownTimeDTO dto = new DownTimeDTO(params[0].replace("[", "").trim(),startP,params[2],endP);
        	result.add(dto);
        }
        return result;
    }
}
