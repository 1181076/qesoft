package arqsoft2.UserMicroservice.controller.user;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.access.prepost.PreAuthorize;

import arqsoft2.UserMicroservice.model.user.OrderID;
import arqsoft2.UserMicroservice.model.user.ReturnAppOrderDTO;
import arqsoft2.UserMicroservice.service.user.AppOrderService;
import arqsoft2.UserMicroservice.model.user.AppOrder;
import arqsoft2.UserMicroservice.model.user.InputAppOrderDTO;
import arqsoft2.UserMicroservice.model.user.ReturnAppOrderDTO;

@CrossOrigin(origins = "http://localhost:8088")
@RestController
@RequestMapping("/api")
@Tag(name = "Order Controller", description = "All order operations")
public class AppOrderController {
    
    @Autowired
    AppOrderService orderService;

    public AppOrderController(AppOrderService orderService) {
        this.orderService = orderService;
    }

	@Operation(summary = "Get all orders", operationId = "getOrders", description = "Get all registered orders")
	@GetMapping("/orders")
    public ResponseEntity<List<ReturnAppOrderDTO>> getAllOrders() {
        try {
            List<ReturnAppOrderDTO> orders = orderService.getAllOrders();
            return new ResponseEntity<>(orders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Operation(summary = "Get order", operationId = "getOrder", description = "Get order by giving its id")
	@GetMapping("/orders/{id}")
    public ResponseEntity<ReturnAppOrderDTO> getOrderById(@PathVariable("id") String id) {
        ReturnAppOrderDTO orderData = orderService.getOrderById(id);

        if (orderData != null) {
            return new ResponseEntity<>(orderData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Get orders with filter", operationId = "getOrdersFiltered", description = "Get all orders by filter")
	@GetMapping("/orders/dayInterval/{startDay}/{endDay}")
    public ResponseEntity<List<ReturnAppOrderDTO>> getOrdersWithDayFilter(@PathVariable("startDay") Date startDay, @PathVariable("endDay") Date endDay) {
        try {
            List<ReturnAppOrderDTO> orders = new ArrayList<ReturnAppOrderDTO>();

            orders = orderService.getOrdersWithDayFilter(startDay, endDay);

            if (orders.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(orders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
    @Operation(summary = "Crate order", operationId = "createOrder", description = "Create an order by giving its information")
	// @PreAuthorize("hasAuthority('STUDENT')")
    @PostMapping("/orders")
    public ResponseEntity<InputAppOrderDTO> createOrder(@RequestBody AppOrder order) {

        try {
            InputAppOrderDTO _order = orderService.createOrder(order);
            return new ResponseEntity<>(_order, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Operation(summary = "Update order", operationId = "updateOrder", description = "Update an order by giving its information")
	@PutMapping("/orders/{id}")
    public ResponseEntity<InputAppOrderDTO> updateOrder(@PathVariable("id") String id, @RequestBody AppOrder order)
            throws ParseException {
                InputAppOrderDTO orderData = orderService.updateOrder(new OrderID(id), order);

        if (orderData != null) {
            return new ResponseEntity<>(orderData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Delet order", operationId = "deleteOrder", description = "Delete an order by giving its id")
	@DeleteMapping("/orders/{id}")
    public ResponseEntity<HttpStatus> deleteOrder(@PathVariable("id") String id) {
        HttpStatus st = orderService.deleteOrder(new OrderID(id));
        return new ResponseEntity<>(st);
    }

    @Operation(summary = "Delet all orders", operationId = "deleteOrders", description = "Delete all registered orders")
	@DeleteMapping("/orders")
    public ResponseEntity<HttpStatus> deleteAllOrders() {
        HttpStatus st = orderService.deleteAllOrders();
        return new ResponseEntity<>(st);

    }
}
