package arqsoft2.UserMicroservice.model.user;

import java.util.Date;
import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;

public class Day implements ValueObject {

    private final Date value;

    public Day(){
        this.value = new java.util.Date(Calendar.getInstance().getTime().getTime());
    }

    public final Date getValue() {
        return this.value;
    }

    @JsonCreator
    public Day(@JsonProperty("value") Date value) {
        this.value = value;
    }

    public Date value() {
        return value;
    }

    @Override
    public boolean equals(Object o) {

        Day day = (Day) o;

        return day.value().equals(this.value);

    }
}
