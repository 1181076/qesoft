package arqsoft2.UserMicroservice.model.sandwich;

import java.sql.Date;

public final class LastUpdatedDate {
    private final int amount;
    private final Date date;

    public LastUpdatedDate(Date date, int amount){
        this.date= date;
        this.amount= amount;
    }
}

