package arqsoft2.UserMicroservice.model.user;

public enum RoleValue {
    SCHOOLREPRESENTATIVE, SUPPLIERMANAGER, STOCKMANAGER, SCHOOLMANAGER
}
