package arqsoft2.UserMicroservice.model.calendar;

import java.util.Date;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class DownTimeDTO {

    private String id;
    private Date startPeriod;
    private String reason;
    private Date endPeriod;
    
    public DownTimeDTO(){

    }

    public DownTimeDTO(String id, Date startPeriod, String reason, Date endPeriod){
        this.id = id;
        this.startPeriod = startPeriod;
        this.reason = reason;
        this.endPeriod = endPeriod;
    }

    public String getId() {
		return this.id;
	}

	public Date getStartPeriod() {
		return this.startPeriod;
	}

	public void setStartPeriod(Date startPeriod) {
		this.startPeriod = startPeriod;
	}

    public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

    public Date getEndPeriod() {
		return this.endPeriod;
	}

	public void setEndPeriod(Date endPeriod) {
		this.endPeriod = endPeriod;
	}

    @Override
    public boolean equals(Object o) {
  
        return true; 
          
    }
}