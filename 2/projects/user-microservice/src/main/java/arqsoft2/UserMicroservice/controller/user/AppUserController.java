package arqsoft2.UserMicroservice.controller.user;

import java.text.ParseException;

import arqsoft2.UserMicroservice.model.user.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.http.MediaType;

import arqsoft2.UserMicroservice.security.JwtTokenProvider;
import arqsoft2.UserMicroservice.model.user.InputAppUserDTO;
import arqsoft2.UserMicroservice.service.user.AppUserService;

@CrossOrigin(origins = "http://localhost:8088")
@RestController
@RequestMapping("/api")
@Tag(name = "User Controller", description = "All user operations")
public class AppUserController {
    
    @Autowired
	AppUserService appUserService;

	@Autowired
	private AuthenticationManager authenticationManager;

	
	@Autowired
	private JwtTokenProvider tokenProvider;


	public AppUserController(AppUserService appUserService){
		this.appUserService = appUserService;
	}

	// @Operation(summary = "Get all users", operationId = "getUsers", description = "Return all users")
	// @GetMapping("/appUsers")	
	// public ResponseEntity<List<AppUserDTO>> getAllAppUsers(@RequestParam(required = false) String title) {
	// 	try {
	// 		List<AppUserDTO> appUsers = new ArrayList<AppUserDTO>();

	// 		appUsers = appUserService.getAllAppUsers(title);

	// 		if (appUsers.isEmpty()) {
	// 			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	// 		}

	// 		return new ResponseEntity<>(appUsers, HttpStatus.OK);
	// 	} catch (Exception e) {
	// 		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	// 	}
	// }

	// @Operation(summary = "Get a user", operationId = "getUser", description = "Return a user by providing its information")
	// @GetMapping("/appUsers/{id}")
	// public ResponseEntity<AppUserDTO> getAppUserById(@PathVariable("id") String email) {
	// 	AppUserDTO appUserData = appUserService.getAppUserById(email);

	// 	if (appUserData != null) {
	// 		return new ResponseEntity<>(appUserData, HttpStatus.OK);
	// 	} else {
	// 		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	// 	}
	// }

	@Operation(summary = "Create a new user", operationId = "createUser", description = "Create a new user by providing its information")
	@PostMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<InputAppUserDTO> createAppUser(@RequestBody AppUser user) {
		try {
			if(user.getRole() == null){
				user.update(new AppUser(
						user.getEmail(),
						user.getName(),
						new Password(new BCryptPasswordEncoder().encode(user.getPassword().getValue())),
						new Role(RoleValue.SCHOOLREPRESENTATIVE),
						user.getOrders(),
						user.getSchool()));
			} else {
				user.update(new AppUser(
						user.getEmail(),
						user.getName(),
						new Password(new BCryptPasswordEncoder().encode(user.getPassword().getValue())),
						user.getRole(),
						user.getOrders(),
						user.getSchool()));
			}


				InputAppUserDTO savedUser = appUserService.createAppUser(user);
			
			return new ResponseEntity<>(savedUser, HttpStatus.OK);
		} catch (Exception e) {

			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
	}

	@Operation(summary = "Authenticate", operationId = "authenticate", description = "Authenticate a user")
	@PostMapping(value = "/authenticate", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ReturnAuthenticationDTO> authenticate(@RequestBody InputAuthenticationDTO user) {
		try {
			Authentication authentication = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword()));
			if (authentication.isAuthenticated()) {
				String email = user.getEmail();
				ReturnAppUserDTO a = appUserService.getAppUserById(email);
				ReturnAuthenticationDTO dto = new ReturnAuthenticationDTO(
						authentication.getName(),
						a.getName(),
						authentication.getAuthorities().iterator().next().getAuthority(),
						tokenProvider.createToken(email, new Role( RoleValue.valueOf(appUserService.getAppUserById(email).getRole())))
				);
				return new ResponseEntity<>(dto, HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
		return null;
	}

	@Operation(summary = "Update a specific user", operationId = "updateUser", description = "Update a specific user by providing its identifier")
    @PutMapping("/appUsers/{id}")
	public ResponseEntity<InputAppUserDTO> updateAppUser(@PathVariable("id") String email, @RequestBody AppUser appUser) throws ParseException {
		InputAppUserDTO appUserData = appUserService.updateAppUser(new Email(email), appUser);

		if (appUserData != null) {
			return new ResponseEntity<>(appUserData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@Operation(summary = "Delete a specific user", operationId = "deleteUser", description = "Delete a specific user by providing its identifier")
	@DeleteMapping("/appUsers/{id}")
	public ResponseEntity<HttpStatus> deleteAppUser(@PathVariable("id") String email) {
			HttpStatus st = appUserService.deleteAppUser(new Email(email));
			return new ResponseEntity<>(st);
	}

	@Operation(summary = "Delete all users", operationId = "deleteUsers", description = "Delete all registered users")
	@DeleteMapping("/appUsers")
	public ResponseEntity<HttpStatus> deleteAllAppUsers() {
			HttpStatus st = appUserService.deleteAllAppUsers();
			return new ResponseEntity<>(st);
		
	}

	// @Operation(summary = "Assign school", operationId = "assignSchool", description = "Assign school")
	// @PostMapping("/appUsers/${id}")
	// public ResponseEntity<AppUserDTO> assignSchool(@PathVariable("id") String email, @RequestBody String school) {
	// 	AppUserDTO appUserData = appUserService.assignSchool(email, school);

	// 	if (appUserData != null) {
	// 		return new ResponseEntity<>(appUserData, HttpStatus.OK);
	// 	} else {
	// 		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	// 	}
	// }
}
