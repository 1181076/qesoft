package arqsoft2.UserMicroservice.model.school;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;

public class InternalID implements ValueObject {
    private final String value;

    public final String getValue() {
        return this.value;
    }

    @JsonCreator
    public InternalID(@JsonProperty("value") String value) {
        if(value.isEmpty()){
            throw new IllegalArgumentException("The internal ID cannot be empty!");
        }

        if(value.trim().equals("")){
            throw new IllegalArgumentException("The internal ID cannot contain only spaces!");
        }

        this.value = value;
    }

    public String value() {
        return value;
    }

    @Override
    public boolean equals(Object o) {

        InternalID internalID = (InternalID) o;

        return internalID.value().equals(this.value);

    }
}
