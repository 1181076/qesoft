package arqsoft2.UserMicroservice.model.sandwich;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;

public final class SandwichID implements ValueObject{

    private final String value;

    public final String getValue() {
		return this.value;
	}

    public SandwichID(){
        value = "default";
    }

    @JsonCreator
    public SandwichID(@JsonProperty("value") String value){
        if(value.isEmpty()){
            throw new IllegalArgumentException("The ID cannot be empty!");
        }
        this.value= value;
    }
    
    public String value(){
        return value;
    }

    @Override
    public boolean equals(Object o){

        SandwichID id = (SandwichID) o;

        return id.value().equals(this.value);

    }

}
