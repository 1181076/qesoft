package arqsoft2.UserMicroservice.model.user;

import java.util.List;

import javax.persistence.*;

import arqsoft2.UserMicroservice.model.school.SchoolDTO;
import eapli.framework.representations.dto.DTOable;

public interface AppUserInterface{
	
	public Email getEmail();
	public Name getName();
	public Password getPassword();
	public Role getRole();
	public List<AppOrder> getOrders();
	public SchoolDTO getSchool();

    public InputAppUserDTO toDTO();

	public void update(AppUser ing);


}
