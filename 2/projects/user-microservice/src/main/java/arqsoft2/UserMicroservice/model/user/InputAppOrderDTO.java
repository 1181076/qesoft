package arqsoft2.UserMicroservice.model.user;

import java.util.List;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class InputAppOrderDTO {

    private OrderID id;
    private Day day;
    private List<SandwichOrder> sandwichOrders;

    public InputAppOrderDTO() {
    }

    public InputAppOrderDTO(OrderID id, Day day, List<SandwichOrder> sandwichOrders) {
        this.id = id;
        this.day = day;
        this.sandwichOrders = sandwichOrders;
    }

    public OrderID getId() {
        return this.id;
    }

    public Day getDay() {
        return this.day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public List<SandwichOrder> getSandwichOrders() {
        return this.sandwichOrders;
    }

    public void setSandwichOrders(List<SandwichOrder> sandwichOrders) {
        this.sandwichOrders = sandwichOrders;
    }

    @Override
    public boolean equals(Object k) {
        return true;
    }
}
