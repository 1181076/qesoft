package arqsoft2.UserMicroservice.model.school;

import javax.persistence.*;

import arqsoft2.UserMicroservice.model.sandwich.SandwichID;
import eapli.framework.representations.dto.DTOable;

import java.util.ArrayList;
import java.util.List;

public interface SchoolInterface {

    public ExternalID getId();
    public String getInternalID();
    public String getName();
    public String getAddress();    
    public List<String> getSandwiches();

    public void update(SchoolDTO school);

    public InputSchoolDTO toDTO();

    public void registerSandwiches(List<String> sandwiches);
}