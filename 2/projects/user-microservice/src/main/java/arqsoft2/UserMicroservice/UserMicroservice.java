package arqsoft2.UserMicroservice;

import arqsoft2.UserMicroservice.controller.user.AppUserController;
import arqsoft2.UserMicroservice.model.user.*;
import arqsoft2.UserMicroservice.service.user.AppUserService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import java.util.ArrayList;

@EnableEurekaClient
@SpringBootApplication
@OpenAPIDefinition(
		info = @Info(
				title="User Microservice API",
				version = "1.0.0")
)
public class UserMicroservice implements CommandLineRunner {
	@Autowired
	private AppUserService userService;

	@Autowired
	private AppUserController userController;

	public static void main(String[] args) {
		SpringApplication.run(UserMicroservice.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		if (userService.getAllAppUsers("").isEmpty()) {

			AppUser stockManager = new AppUser(
					new Email("stock@arqsoft.com"),
					new Name("DefaultStockManager"),
					new Password("Arqsoft1!"),
					new Role( RoleValue.STOCKMANAGER),
					new ArrayList<OrderID>(),
					null);

			AppUser schoolManager = new AppUser(
					new Email("school@arqsoft.com"),
					new Name("DefaultSchoolManager"),
					new Password("Arqsoft1!"),
					new Role( RoleValue.SCHOOLMANAGER),
					new ArrayList<OrderID>(),
					null);

			AppUser supplierManager = new AppUser(
					new Email("supplier@arqsoft.com"),
					new Name("DefaultSupplierManager"),
					new Password("Arqsoft1!"),
					new Role( RoleValue.SUPPLIERMANAGER),
					new ArrayList<OrderID>(),
					null);


			userController.createAppUser(stockManager);
			userController.createAppUser(schoolManager);
			userController.createAppUser(supplierManager);
		}
	}
}
