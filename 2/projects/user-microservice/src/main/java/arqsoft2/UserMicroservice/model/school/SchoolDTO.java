package arqsoft2.UserMicroservice.model.school;

import javax.persistence.*;

import arqsoft2.UserMicroservice.model.sandwich.SandwichID;
import arqsoft2.UserMicroservice.model.sandwich.ReturnSandwichDTO;
import eapli.framework.representations.dto.DTOable;

import java.util.List;

public class SchoolDTO implements SchoolInterface, DTOable<InputSchoolDTO> {

    @EmbeddedId
    private ExternalID id;

    private String internalID;
    private String name;
    private String address;
        
	@ElementCollection
    private List<String> sandwiches;

    public SchoolDTO() {

    }

    public SchoolDTO(ExternalID id, String internalID, String name, String address, List<String> sandwiches ) {
        this.id = id;
        this.internalID = internalID;
        this.name = name;
        this.address = address;
        this.sandwiches = sandwiches;
    }

    public ExternalID getId() {
        return this.id;
    }

    public String getInternalID() {
        return this.internalID;
    }

    public String getName() {
        return this.name;
    }

    public String getAddress() {
        return this.address;
    }

    public List<String> getSandwiches() {
        return this.sandwiches;
    }

    public void update(SchoolDTO school) {
        this.internalID = school.internalID;
        this.name = school.name;
        this.address = school.address;
        this.sandwiches = school.sandwiches;
    }

    public InputSchoolDTO toDTO() {
        return new InputSchoolDTO(id, internalID, name, address, sandwiches);
    }

    public ReturnSchoolDTO toReturnDTO(List<ReturnSandwichDTO> sandwiches) {
        return new ReturnSchoolDTO(id, internalID, name, address, sandwiches);
    }

    public void registerSandwiches(List<String> sandwiches){
        this.sandwiches = sandwiches; 
    }
}