package arqsoft2.UserMicroservice.repository.user;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import arqsoft2.UserMicroservice.model.user.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

public class AppUserRepositoryImpl implements AppUserCustomRepository {

    @PersistenceContext
    private EntityManager em;

    
    @Override
    public List<AppUser> findAllUsers(){

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AppUser> cq = cb.createQuery(AppUser.class);
        Root<AppUser> root = cq.from(AppUser.class);
        
        cq.select(root);

        TypedQuery<AppUser> q = em.createQuery(cq);

        return q.getResultList();

    }

    @Override
    public AppUser findUserById(String id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AppUser> cq = cb.createQuery(AppUser.class);
        Root<AppUser> root = cq.from(AppUser.class);
        Path<String> idValue = root.get("email").get("value");
        
        cq.select(root).where(cb.equal(idValue, id));

        TypedQuery<AppUser> q = em.createQuery(cq);

        return q.getSingleResult();
    }

    @Override
    public AppUser findUserByName(String name) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AppUser> cq = cb.createQuery(AppUser.class);
        Root<AppUser> root = cq.from(AppUser.class);
        Path<String> nameValue = root.get("name").get("value");
        
        cq.select(root).where(cb.equal(nameValue, name));

        TypedQuery<AppUser> q = em.createQuery(cq);

        return q.getSingleResult();
    }


}
