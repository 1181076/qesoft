package arqsoft2.UserMicroservice.repository.user;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import arqsoft2.UserMicroservice.model.user.*;
import arqsoft2.UserMicroservice.model.user.AppOrder;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

public class AppOrderRepositoryImpl implements AppOrderCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<AppOrder> getOrdersByInterval(Date startDay, Date endDay) {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AppOrder> cq = cb.createQuery(AppOrder.class);
        Root<AppOrder> root = cq.from(AppOrder.class);
        Path<Date> day = root.get("day").<Date>get("value");
        
        cq.select(root).where(cb.between(day, startDay, endDay));

        TypedQuery<AppOrder> q = em.createQuery(cq);

        return q.getResultList();
    
    }

    @Override
    public List<AppOrder> findAllOrders(){

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AppOrder> cq = cb.createQuery(AppOrder.class);
        Root<AppOrder> root = cq.from(AppOrder.class);
        
        cq.select(root);

        TypedQuery<AppOrder> q = em.createQuery(cq);

        return q.getResultList();

    }

    @Override
    public AppOrder findOrderById(String id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<AppOrder> cq = cb.createQuery(AppOrder.class);
        Root<AppOrder> root = cq.from(AppOrder.class);
        Path<String> idValue = root.get("id").get("idValue");
        
        cq.select(root).where(cb.equal(idValue, id));

        TypedQuery<AppOrder> q = em.createQuery(cq);

        return q.getSingleResult();
    }


}
