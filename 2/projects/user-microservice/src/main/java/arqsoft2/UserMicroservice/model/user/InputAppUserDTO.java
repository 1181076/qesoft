package arqsoft2.UserMicroservice.model.user;


import java.util.List;

import arqsoft2.UserMicroservice.model.school.ExternalID;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class InputAppUserDTO {

    
    private Email email;
    private Name name;
    private Password password;
    private Role role;
    private List<OrderID> orders;
	private ExternalID school;
    
    public InputAppUserDTO(){

    }

    public InputAppUserDTO(Email email, Name name, Password password, Role role, List<OrderID> orders, ExternalID school){
        this.email = email;
        this.name = name;
        this.password = password;
        this.role = role;
        this.orders = orders;
        this.school = school;
    }

    public Email getEmail() {
		return this.email;
	}

	public Name getName() {
		return this.name;
	}

	public Password getPassword() {
		return this.password;
	}

	public Role getRole() {
		return this.role;
	}

	public List<OrderID> getOrders() {
		return this.orders;
	}

    public ExternalID getSchool() {
		return this.school;
	}


    @Override
    public boolean equals(Object o) {
  
		AppUser u = (AppUser) o;

        return u.getEmail().equals(this.email); 
          
    }
}
