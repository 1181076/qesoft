package arqsoft2.UserMicroservice.service.user;

import java.util.*;

import arqsoft2.UserMicroservice.model.calendar.*;
import arqsoft2.UserMicroservice.model.user.*;
import arqsoft2.UserMicroservice.repository.user.AppOrderRepository;
import arqsoft2.UserMicroservice.repository.user.AppOrderRepositoryImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;


@Component
public class AppOrderService { 

    @Autowired
    AppOrderRepositoryImpl orderRepositoryImpl;
    
    @Autowired
    AppOrderRepository orderRepository;

    public AppOrderService(AppOrderRepositoryImpl orderRepositoryImpl, AppOrderRepository orderRepository) {
        this.orderRepositoryImpl = orderRepositoryImpl;
        this.orderRepository = orderRepository;
    }

    public List<ReturnAppOrderDTO> getAllOrders() {
        try {
            List<AppOrder> orders = orderRepositoryImpl.findAllOrders();
            List<ReturnAppOrderDTO> ret = new ArrayList<>();
            orders.forEach(element -> ret.add(element.toReturnDTO()));
            return ret;
        } catch (Exception error) {
            return new ArrayList<>();
        }
    }

    public ReturnAppOrderDTO getOrderById(@PathVariable("id") String id) {

        // VERSION WITH REPOSITORYIMPL
        AppOrder orderData = orderRepositoryImpl.findOrderById(id);
        
        if (orderData != null) {
            return orderData.toReturnDTO();
        } else {
            return null;
        }

    //    Optional<AppOrder> orderData = orderRepository.findById(new ID(id));

    //     if (orderData.isPresent()) {
    //         return orderData.get().toDTO();
    //     } else {
    //         return null;
    //     }
    }

    public List<ReturnAppOrderDTO> getOrdersWithDayFilter(@PathVariable("startDay") Date startDay, @PathVariable("endDay") Date endDay) {
        try {
       
            // VERSION WITH REPOSITORYIMPL
            List<AppOrder> sandwiches = orderRepositoryImpl.getOrdersByInterval(startDay, endDay);

            // List<AppOrder> sandwiches = orderRepository.findByDayBetween(new Day(startDay), new Day(endDay));

            List<ReturnAppOrderDTO> ret = new ArrayList<>();

            sandwiches.forEach(e -> ret.add(e.toReturnDTO()));

            return ret;

        } catch (Exception e) {
            System.out.println(e);
            return new ArrayList<>();
        }
    }


    public InputAppOrderDTO createOrder(@RequestBody AppOrder order) {
        try {
        	ArrayList<DownTimeDTO> lst = CalendarMicroserviceCommunication.getAllDowntimes();
        	if(orderIsValid(order, lst)) {        		
        		AppOrder _order = orderRepository.save(order);
        		return _order.toDTO();
        	} else {
        		System.out.println("<< ORDER IS INVALID -> DURING ORDER'S DAY, APP IN DOWNTIME!!! >>");
        		return null;
        	}
        } catch (Exception error) {
            
        System.out.println(error);
            return null;
        }
    }

    private boolean orderIsValid(AppOrder order, ArrayList<DownTimeDTO> lst) {
		for(int i = 0; i < lst.size(); i++) {
			if(order.getDay().getValue().after(lst.get(i).getStartPeriod()) && order.getDay().getValue().before(lst.get(i).getEndPeriod())) {
				return false;
			}
		}
		return true;
	}

	public InputAppOrderDTO updateOrder(@PathVariable("id") OrderID id, @RequestBody AppOrder order) {
        Optional<AppOrder> orderData = orderRepository.findById(id);

        if (orderData.isPresent()) {
            
            try {
            	ArrayList<DownTimeDTO> lst = CalendarMicroserviceCommunication.getAllDowntimes();
            	if(orderIsValid(order, lst)) {        		
            		AppOrder _order = orderData.get();
                    _order.update(order);
                    return orderRepository.save(_order).toDTO();
            	} else {
            		System.out.println("<< UPDATE IN ORDER IS INVALID -> DURING NEW ORDER'S DAY, APP IN DOWNTIME!!! >>");
            		return null;
            	}
            } catch (Exception error) {
                
            System.out.println(error);
                return null;
            }
        } else {
            return null;
        }
    }

    public HttpStatus deleteOrder(@PathVariable("id") OrderID id) {
        try {
            orderRepository.deleteById(id);
            return HttpStatus.NO_CONTENT;
        } catch (Exception error) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

    public HttpStatus deleteAllOrders() {
        try {
            orderRepository.deleteAll();
            return HttpStatus.NO_CONTENT;
        } catch (Exception error) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }
}
