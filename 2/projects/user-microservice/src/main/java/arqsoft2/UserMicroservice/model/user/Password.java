package arqsoft2.UserMicroservice.model.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Password implements ValueObject{

    private final String value;

    @JsonCreator
    public Password(@JsonProperty("value") String value){
        // Regex Pattern got from:
        //https://stackoverflow.com/questions/19605150/regex-for-password-must-contain-at-least-eight-characters-at-least-one-number-a
        Pattern passwordPattern = Pattern.compile("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$");
        Matcher matcher = passwordPattern.matcher(value);

        if(!matcher.matches()){
            throw new IllegalArgumentException("The password must have at least 8 characters, one lowercase, one uppercase, one number and one special character!");
        }
        this.value= value;
    }
    
    public String value(){
        return value;
    }

    public final String getValue() {
		return this.value;
	}

}
