package arqsoft2.UserMicroservice.repository.user;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import arqsoft2.UserMicroservice.model.user.*;

public interface AppUserCustomRepository {

    public AppUser findUserById(String Id);
    
    public AppUser findUserByName(String name);
    
    public List<AppUser> findAllUsers();
    
}
