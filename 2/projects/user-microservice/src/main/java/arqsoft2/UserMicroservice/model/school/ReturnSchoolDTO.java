package arqsoft2.UserMicroservice.model.school;

import java.util.List;

import arqsoft2.UserMicroservice.model.sandwich.ReturnSandwichDTO;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class ReturnSchoolDTO {
    private String internalID;
    private ExternalID id;
    private String name;
    private String address;
    private List<ReturnSandwichDTO> sandwiches;

    public ReturnSchoolDTO() {

    }

    public ReturnSchoolDTO(ExternalID id, String internalID, String name, String address, List<ReturnSandwichDTO> sandwiches) {
        this.id = id;
        this.internalID = internalID;
        this.name = name;
        this.address = address;
        this.sandwiches = sandwiches;
    }

    public ExternalID getId() {
        return this.id;
    }

    public String getInternalID() {
        return this.internalID;
    }

    public String getName() {
        return this.name;
    }

    public String getAddress() {
        return this.address;
    }

    public List<ReturnSandwichDTO> getSandwiches() {return this.sandwiches;}

    @Override
    public boolean equals(Object o) {

        return true;

    }
}