package arqsoft2.UserMicroservice.service.user;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.http.HttpClient;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import arqsoft2.UserMicroservice.model.user.Email;
import arqsoft2.UserMicroservice.model.school.SchoolDTO;
import arqsoft2.UserMicroservice.model.user.AppUser;
import arqsoft2.UserMicroservice.model.user.InputAppUserDTO;
import arqsoft2.UserMicroservice.model.user.ReturnAppOrderDTO;
import arqsoft2.UserMicroservice.model.user.ReturnAppUserDTO;
import arqsoft2.UserMicroservice.repository.user.AppUserRepository;
import arqsoft2.UserMicroservice.repository.user.AppUserRepositoryImpl;

@Component
public class AppUserService {

	@Autowired
	AppUserRepository appUserRepository;
	
	@Autowired
	AppUserRepositoryImpl appUserRepositoryImpl;
	

	public AppUserService(AppUserRepository appUserRepository, AppUserRepositoryImpl appUserRepositoryImpl){
		this.appUserRepository = appUserRepository;
		this.appUserRepositoryImpl = appUserRepositoryImpl;
	}

    public List<ReturnAppUserDTO> getAllAppUsers(@RequestParam(required = false) String title) {
		try {
			List<AppUser> appUsers = appUserRepositoryImpl.findAllUsers();
			
			List<ReturnAppUserDTO> ret = new ArrayList<>();

			appUsers.forEach(e -> {
				//List<ReturnAppOrderDTO> orders  = IngredientMicroserviceCommunication.getIngredients(element);
				List<ReturnAppOrderDTO> orders  = new ArrayList();
				
				//ReturnSchoolDTO school  = IngredientMicroserviceCommunication.getIngredients(element);

				ret.add(e.toReturnDTO(orders, null));
			});

			return ret;
		} catch (Exception e) {
			return new ArrayList<>();
		}
	}

	public ReturnAppUserDTO getAppUserById(@PathVariable("id") String email) {

		// VERSION WITH REPOSITORYIMPL 
        AppUser appUserData = appUserRepositoryImpl.findUserById(email);

		//List<ReturnAppOrderDTO> orders  = IngredientMicroserviceCommunication.getIngredients(appUserData);
		List<ReturnAppOrderDTO> orders  = new ArrayList();
				
		//ReturnSchoolDTO school  = IngredientMicroserviceCommunication.getIngredients(element);

        if (appUserData!=null) {
            return appUserData.toReturnDTO(orders, null);
        } else {
            return null;
        }

		// Optional<AppUser> appUserData = appUserRepository.findById(new Email(email));

		// if (appUserData.isPresent()) {
		// 	return appUserData.get().toDTO();
		// } else {
		// 	return null;
		// }
	}


	public InputAppUserDTO createAppUser(@RequestBody AppUser appUser) {
		
		try {
			AppUser _appUser = appUserRepository
					.save(appUser);
						
			return _appUser.toDTO();
		} catch (Exception e) {
			
		System.out.println("e11111 -> " + e);
			return null;
		}
	}

	public InputAppUserDTO updateAppUser(@PathVariable("id") Email email, @RequestBody AppUser appUser) {
		Optional<AppUser> appUserData = appUserRepository.findById(email);
		
		if (appUserData.isPresent()) {
			AppUser _appUser = appUserData.get();
			_appUser.update(appUser);
			
			return appUserRepository.save(_appUser).toDTO();
		} else {
			return null;
		}
	}

	public HttpStatus deleteAppUser(@PathVariable("id") Email email) {
		try {
			appUserRepository.deleteById(email);
			return HttpStatus.NO_CONTENT;
		} catch (Exception e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
	}

	public HttpStatus deleteAllAppUsers() {
		try {
			appUserRepository.deleteAll();
			return HttpStatus.NO_CONTENT;
		} catch (Exception e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}

	}

	public InputAppUserDTO assignSchool(@PathVariable("id") String email, @PathVariable("school") SchoolDTO school) {
		Optional<AppUser> appUserData = appUserRepository.findById(new Email(email));
		
		if (appUserData.isPresent()) {
			AppUser _appUser = appUserData.get();
			AppUser temp = new AppUser(_appUser.getEmail(), _appUser.getName(), _appUser.getPassword(), _appUser.getRole(), _appUser.getOrders(), school.getId());
			_appUser.update(temp);
			
			return appUserRepository.save(_appUser).toDTO();
		} else {
			return null;
		}
	}
}
