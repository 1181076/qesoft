package arqsoft2.UserMicroservice.model.user;

import java.util.List;

import javax.persistence.*;

import arqsoft2.UserMicroservice.model.school.ExternalID;
import arqsoft2.UserMicroservice.model.school.ReturnSchoolDTO;
import eapli.framework.representations.dto.DTOable;

@Entity
public class AppUser implements DTOable<InputAppUserDTO>{
	
    @EmbeddedId
    private Email email;

    private Name name;
    private Password password;
    private Role role;
   
	@ElementCollection
    private List<OrderID> orders;

	private ExternalID school;

    public AppUser(){

    }

    public AppUser(Email email, Name name, Password password, Role role, List<OrderID> orderIds, ExternalID schoolID){
        this.email = email;
        this.name = name;
        this.password = password;
        this.role = role;
        this.orders = orderIds;
		this.school = schoolID;
    }

	public Email getEmail() {
		return this.email;
	}

	public Name getName() {
		return this.name;
	}

	public Password getPassword() {
		return this.password;
	}

	public Role getRole() {
		return this.role;
	}

	public List<OrderID> getOrders() {
		return this.orders;
	}

	public ExternalID getSchool() {
		return this.school;
	}

    @Override
    public InputAppUserDTO toDTO() {
        return new InputAppUserDTO(email, name, password, role, orders, school);
	}

	
    public ReturnAppUserDTO toReturnDTO(List<ReturnAppOrderDTO> orders, ReturnSchoolDTO school) {
		String newrole = "" + role.getValue();
        return new ReturnAppUserDTO(email.getValue(), name.getValue(), password.getValue(), newrole, orders, school);
    }

	public void update(AppUser ing){
		this.name = ing.name;
		this.password = ing.password;
		this.role = ing.role;
		this.orders = ing.orders;
		this.school = ing.school;
	} 


}
