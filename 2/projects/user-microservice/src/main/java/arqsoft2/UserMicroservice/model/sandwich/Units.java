package arqsoft2.UserMicroservice.model.sandwich;

import eapli.framework.domain.model.ValueObject;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public final class Units implements ValueObject {

    private final String value;

    @JsonCreator
    public Units(@JsonProperty("value")String value){
        if(value.isEmpty()){
            throw new IllegalArgumentException("The unit cannot be empty!");
        }
        this.value= value;
    }
    
    public String value(){
        return value;
    }

    public final String getValue() {
		return this.value;
	}
}
