package arqsoft2.UserMicroservice.model.user;

import java.util.Date;
import java.util.List;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class ReturnAppOrderDTO {

    private String id;
    private Date day;
    private List<SandwichOrder> sandwichOrders;

    public ReturnAppOrderDTO() {
    }

    public ReturnAppOrderDTO(String id, Date day, List<SandwichOrder> sandwichOrders) {
        this.id = id;
        this.day = day;
        this.sandwichOrders = sandwichOrders;
    }

    public String getId() {
        return this.id;
    }

    public Date getDay() {
        return this.day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public List<SandwichOrder> getSandwichOrders() {
        return this.sandwichOrders;
    }

    public void setSandwichOrders(List<SandwichOrder> sandwichOrders) {
        this.sandwichOrders = sandwichOrders;
    }

    @Override
    public boolean equals(Object k) {
        return true;
    }
}
