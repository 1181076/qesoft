package arqsoft2.UserMicroservice.model.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;

public final class Role implements ValueObject{

    private final RoleValue value;

    @JsonCreator
    public Role(@JsonProperty("value") RoleValue value){
        this.value= value;
    }
    
    public RoleValue value(){
        return value;
    }

    public final RoleValue getValue() {
		return this.value;
	}

}
