package arqsoft2.UserMicroservice.repository.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import arqsoft2.UserMicroservice.model.user.OrderID;
import arqsoft2.UserMicroservice.model.user.AppOrder;
import arqsoft2.UserMicroservice.model.user.Day;

public interface AppOrderRepository extends JpaRepository<AppOrder, OrderID>, AppOrderCustomRepository {

    List<AppOrder> findByDayBetween(Day startDay, Day endDay);

}

