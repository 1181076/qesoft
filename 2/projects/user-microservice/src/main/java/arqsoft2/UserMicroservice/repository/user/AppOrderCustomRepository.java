package arqsoft2.UserMicroservice.repository.user;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import arqsoft2.UserMicroservice.model.user.AppOrder;

public interface AppOrderCustomRepository {

    public AppOrder findOrderById(String Id);
    
    public List<AppOrder> findAllOrders();
    
    public List<AppOrder> getOrdersByInterval(Date startDay, Date endDay);
    
}
