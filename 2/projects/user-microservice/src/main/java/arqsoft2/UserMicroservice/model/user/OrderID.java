package arqsoft2.UserMicroservice.model.user;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;

public final class OrderID implements ValueObject {

    private final String idValue;

    public OrderID(){
        idValue = "default";
    }

    public final String getIdValue() {
        return this.idValue;
    }

    @JsonCreator
    public OrderID(@JsonProperty("value") String idValue) {
        if(idValue.isEmpty()){
            throw new IllegalArgumentException("The ID cannot be empty!");
        }
        this.idValue = idValue;
    }

    public String idValue() {
        return idValue;
    }

    @Override
    public boolean equals(Object o) {

        OrderID id = (OrderID) o;

        return id.idValue().equals(this.idValue);

    }
}
