package arqsoft2.SandwichMicroservice.repository;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import arqsoft2.SandwichMicroservice.model.sandwich.*;

public interface CategoryCustomRepository {

    public Category findCategoryById(String Id);

    public List<Category> findAllCategories();

}
