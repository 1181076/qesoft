package arqsoft2.SandwichMicroservice.controller;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import arqsoft2.SandwichMicroservice.model.sandwich.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import arqsoft2.SandwichMicroservice.service.CategoryService;

@CrossOrigin(origins = "http://localhost:8088")
@RestController
@RequestMapping("/api")
public class CategoryController {

	@Autowired
	CategoryService categoryService;

	public CategoryController(CategoryService categoryService) {
		this.categoryService = categoryService;
	}

	@GetMapping("/categories")
	public ResponseEntity<List<ReturnCategoryDTO>> getAllCategories() {
		try {
			List<ReturnCategoryDTO> categories = categoryService.getAllCategories();
			return new ResponseEntity<>(categories, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/categories/{id}")
	public ResponseEntity<ReturnCategoryDTO> getCategoryById(@PathVariable("id") String id) {
		ReturnCategoryDTO categoryData = categoryService.getCategoryById(id);

		if (categoryData != null) {
			return new ResponseEntity<>(categoryData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/categories")
	public ResponseEntity<InputCategoryDTO> createCategory(@RequestBody Category category) {

		try {
			InputCategoryDTO _category = categoryService
					.createCategory(category);
			return new ResponseEntity<>(_category, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/categories/{id}")
	public ResponseEntity<HttpStatus> deleteCategory(@PathVariable("id") String id) {
		HttpStatus st = categoryService.deleteCategory(new CategoryDescription(id));
		return new ResponseEntity<>(st);
	}

	@DeleteMapping("/categories")
	public ResponseEntity<HttpStatus> deleteAllCategories() {
		HttpStatus st = categoryService.deleteAllCategories();
		return new ResponseEntity<>(st);

	}

}
