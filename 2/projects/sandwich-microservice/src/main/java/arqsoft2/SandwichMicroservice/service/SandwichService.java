package arqsoft2.SandwichMicroservice.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.NamedNativeQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import arqsoft2.SandwichMicroservice.model.sandwich.*;
import arqsoft2.SandwichMicroservice.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import arqsoft2.SandwichMicroservice.model.ingredient.IngredientDTO;
import arqsoft2.SandwichMicroservice.repository.SandwichRepository;
import arqsoft2.SandwichMicroservice.repository.SandwichRepositoryImpl;

@Component
public class SandwichService {

    @Autowired
    SandwichRepository sandwichRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    SandwichRepositoryImpl sandwichRepositoryImpl;

    public SandwichService(SandwichRepository sandwichRepository, SandwichRepositoryImpl sandwichRepositoryImpl, CategoryRepository categoryRepository) {
        this.sandwichRepository = sandwichRepository;
        this.sandwichRepositoryImpl = sandwichRepositoryImpl;
        this.categoryRepository = categoryRepository;
    }

    public List<ReturnSandwichDTO> getAllSandwiches() {
        try {
            List<Sandwich> sandwiches = sandwichRepositoryImpl.findAllSandwiches();

            List<ReturnSandwichDTO> ret = new ArrayList<>();

            sandwiches.forEach(e -> {
                List<IngredientDTO> ingredients = IngredientMicroserviceCommunication.getIngredients(e.toDTO());

                List<ReturnCategoryDTO> categories = new ArrayList();

                e.getCategories().forEach(element -> categories.add(element.toReturnDTO()));

                ret.add(e.toReturnDTO(categories, ingredients));
            });

            return ret;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public ReturnSandwichDTO getSandwichById(@PathVariable("id") String id) {

        // VERSION WITH REPOSITORYIMPL
        Sandwich sandwichData = sandwichRepositoryImpl.findSandwichById(id);

        List<ReturnCategoryDTO> categories = new ArrayList();

        sandwichData.getCategories().forEach(e -> categories.add(e.toReturnDTO()));

        if (sandwichData != null) {
            List<IngredientDTO> ingredients = IngredientMicroserviceCommunication.getIngredients(sandwichData.toDTO());
            return sandwichData.toReturnDTO(categories, ingredients);
        } else {
            return null;
        }

        // Optional<Sandwich> sandwichData = sandwichRepository.findById(new ID(id));

        // if (sandwichData.isPresent()) {
        // return sandwichData.get().toDTO();
        // } else {
        // return null;
        // }
    }

    public List<ReturnSandwichDTO> getSandwichesWithPriceFilter(@PathVariable("currency") String currency,
            @PathVariable("minimumPrice") double minimumPrice, @PathVariable("maximumPrice") double maximumPrice) {
        try {

            List<Sandwich> sandwiches = sandwichRepositoryImpl.getSandwichesByPriceInterval(currency, minimumPrice,
                    maximumPrice);

            List<ReturnSandwichDTO> ret = new ArrayList<>();

            sandwiches.forEach(e -> {
                List<IngredientDTO> ingredients = IngredientMicroserviceCommunication.getIngredients(e.toDTO());

                List<ReturnCategoryDTO> categories = new ArrayList();
                e.getCategories().forEach(el -> categories.add(el.toReturnDTO()));

                ret.add(e.toReturnDTO(categories, ingredients));
            });

            return ret;
        } catch (Exception e) {
            System.out.println(e);
            return new ArrayList<>();
        }
    }

    public List<ReturnSandwichDTO> getSandwichesWithIngredientFilter(@PathVariable("list") String[] list) {
        try {

            // VERSION WITH REPOSITORYIMPL
            List<Sandwich> sandwiches = sandwichRepositoryImpl.findSandwichesWithIngredients(list);

            List<ReturnSandwichDTO> ret = new ArrayList<>();

            for (Sandwich s : sandwiches) {
                List<IngredientDTO> ingredients = IngredientMicroserviceCommunication.getIngredients(s.toDTO());

                List<ReturnCategoryDTO> categories = new ArrayList();

                s.getCategories().forEach(e -> categories.add(e.toReturnDTO()));

                ret.add(s.toReturnDTO(categories, ingredients));
            }

            return ret;

            // List<Sandwich> sandwiches = sandwichRepositoryImpl.findAllSandwiches();

            // List<InputSandwichDTO> ret = new ArrayList<>();

            // for(Sandwich s: sandwiches){

            // if(Arrays.equals(s.getIngredients().getList(), list)){
            // ret.add(s.toDTO());
            // }
            // }

            // return ret;
        } catch (Exception e) {
            System.out.println(e);
            return new ArrayList<>();
        }
    }

    public InputSandwichDTO createSandwich(InputSandwichDTO sandwichDTO) {

        try {
            IngredientMicroserviceCommunication.getIngredients(sandwichDTO);
            List<Category> categories = categoryRepository.findAllById(sandwichDTO.getCategories());

            Sandwich sandwich = new Sandwich(
                    sandwichDTO.getId(),
                    sandwichDTO.getShortDescription(),
                    sandwichDTO.getExtendedDescription(),
                    sandwichDTO.getPrice(),
                    sandwichDTO.getIngredients(),
                    categories
            );
            Sandwich _sandwich = sandwichRepository
                    .save(sandwich);

            return _sandwich.toDTO();
        } catch (Exception e) {
            return null;
        }
    }

    public InputSandwichDTO updateSandwich(@PathVariable("id") SandwichID id, @RequestBody InputSandwichDTO sandwichDTO) {
        Optional<Sandwich> sandwichData = sandwichRepository.findById(id);

        if (sandwichData.isPresent()) {
            Sandwich _sandwich = sandwichData.get();
            IngredientMicroserviceCommunication.getIngredients(sandwichDTO);

            List<Category> categories = categoryRepository.findAllById(sandwichDTO.getCategories());
            Sandwich sandwich = new Sandwich(
                    sandwichDTO.getId(),
                    sandwichDTO.getShortDescription(),
                    sandwichDTO.getExtendedDescription(),
                    sandwichDTO.getPrice(),
                    sandwichDTO.getIngredients(),
                    categories
            );

            _sandwich.update(sandwich);

            return sandwichRepository.save(_sandwich).toDTO();
        } else {
            return null;
        }
    }

    public HttpStatus deleteSandwich(@PathVariable("id") SandwichID id) {
        try {
            sandwichRepository.deleteById(id);
            return HttpStatus.NO_CONTENT;
        } catch (Exception e) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

    public HttpStatus deleteAllSandwiches() {
        try {
            sandwichRepository.deleteAll();
            return HttpStatus.NO_CONTENT;
        } catch (Exception e) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

    }

}