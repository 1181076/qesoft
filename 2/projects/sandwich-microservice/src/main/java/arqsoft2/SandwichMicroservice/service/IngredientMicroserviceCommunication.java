package arqsoft2.SandwichMicroservice.service;

import arqsoft2.SandwichMicroservice.model.ingredient.IngredientDTO;
import arqsoft2.SandwichMicroservice.model.ingredient.IngredientID;
import arqsoft2.SandwichMicroservice.model.sandwich.InputSandwichDTO;
import arqsoft2.SandwichMicroservice.model.sandwich.Sandwich;
import arqsoft2.SandwichMicroservice.model.sandwich.SandwichID;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Component
public class IngredientMicroserviceCommunication {
    @Autowired
    static EurekaClient eurekaClient;

    public IngredientMicroserviceCommunication(EurekaClient ec) {
        eurekaClient = ec;
    }

    public static List<IngredientDTO> getIngredients(InputSandwichDTO sandwich) {
        List<IngredientDTO> ingredientDTOList = new ArrayList<>();
        Application application = eurekaClient.getApplication("Ingredient_Microservice");
        InstanceInfo instanceInfo = application.getInstances().get(0);
        for (IngredientID ingredientID : sandwich.getIngredients()) {
            String url = "http://" + instanceInfo.getIPAddr() + ":" + instanceInfo.getPort() + "/api/ingredient?id="
                    + ingredientID.getValue();
            IngredientDTO dto = new RestTemplate().getForObject(url, IngredientDTO.class);
            ingredientDTOList.add(dto);
        }
        return ingredientDTOList;
    }
}
