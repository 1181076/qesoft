package arqsoft2.SandwichMicroservice.model.sandwich;

import java.util.List;

import arqsoft2.SandwichMicroservice.model.ingredient.IngredientDTO;
import arqsoft2.SandwichMicroservice.model.ingredient.IngredientID;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class ReturnSandwichDTO {

    private String id;
    private String shortDescription;
    private String extendedDescription;
    private Price price;
    private List<IngredientDTO> ingredients;
    private List<ReturnCategoryDTO> categories;

    public ReturnSandwichDTO() {

    }

    public ReturnSandwichDTO(String id, String shortDescription, String extendedDescription,
            Price price,
            List<ReturnCategoryDTO> categories,
            List<IngredientDTO> ingredients) {
        this.id = id;
        this.shortDescription = shortDescription;
        this.extendedDescription = extendedDescription;
        this.price = price;
        this.ingredients = ingredients;
        this.categories = categories;
    }

    public String getId() {
        return this.id;
    }

    public String getShortDescription() {
        return this.shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getExtendedDescription() {
        return this.extendedDescription;
    }

    public void setExtendedDescription(String extendedDescription) {
        this.extendedDescription = extendedDescription;
    }

    public Price getPrice() {
        return this.price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public List<IngredientDTO> getIngredients() {
        return this.ingredients;
    }

    public void setIngredients(List<IngredientDTO> ingredients) {
        this.ingredients = ingredients;
    }

    public List<ReturnCategoryDTO> getCategories() {
        return categories;
    }

    public void setCategories(List<ReturnCategoryDTO> categories) {
        this.categories = categories;
    }

    @Override
    public boolean equals(Object o) {

        return true;

    }
}
