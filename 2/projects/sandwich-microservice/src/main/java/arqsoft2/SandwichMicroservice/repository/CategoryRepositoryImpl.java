package arqsoft2.SandwichMicroservice.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import arqsoft2.SandwichMicroservice.model.sandwich.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CategoryRepositoryImpl implements CategoryCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Category> findAllCategories() {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Category> cq = cb.createQuery(Category.class);
        Root<Category> root = cq.from(Category.class);

        cq.select(root);

        TypedQuery<Category> q = em.createQuery(cq);

        return q.getResultList();

    }

    @Override
    public Category findCategoryById(String id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Category> cq = cb.createQuery(Category.class);
        Root<Category> root = cq.from(Category.class);
        Path<String> idValue = root.get("id").get("value");

        cq.select(root).where(cb.equal(idValue, id));

        TypedQuery<Category> q = em.createQuery(cq);

        return q.getSingleResult();
    }

}
