package arqsoft2.SandwichMicroservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import arqsoft2.SandwichMicroservice.model.sandwich.Sandwich;
import arqsoft2.SandwichMicroservice.model.sandwich.SandwichID;

public interface SandwichRepository extends JpaRepository<Sandwich, SandwichID>, SandwichCustomRepository {

}
