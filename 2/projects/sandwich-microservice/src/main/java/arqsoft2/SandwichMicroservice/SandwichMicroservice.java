package arqsoft2.SandwichMicroservice;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Sandwich Microservice API", version = "1.0.0"))
public class SandwichMicroservice {
	public static void main(String[] args) {
		SpringApplication.run(SandwichMicroservice.class, args);
	}
}
