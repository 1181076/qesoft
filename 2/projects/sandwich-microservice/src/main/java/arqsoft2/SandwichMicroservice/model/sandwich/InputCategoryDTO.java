package arqsoft2.SandwichMicroservice.model.sandwich;

import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class InputCategoryDTO {

    private CategoryDescription description;

    public InputCategoryDTO() {

    }

    public InputCategoryDTO(@JsonProperty("description") CategoryDescription description) {
        this.description = description;
    }

    public CategoryDescription getDescription() {
        return this.description;
    }

    public void setDescription(CategoryDescription description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {

        return true;

    }
}
