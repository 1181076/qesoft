package arqsoft2.SandwichMicroservice.repository;

import java.util.List;

import arqsoft2.SandwichMicroservice.model.sandwich.CategoryDescription;
import org.springframework.data.jpa.repository.JpaRepository;

import arqsoft2.SandwichMicroservice.model.sandwich.Description;
import arqsoft2.SandwichMicroservice.model.sandwich.Category;

public interface CategoryRepository extends JpaRepository<Category, CategoryDescription>, CategoryCustomRepository {

}
