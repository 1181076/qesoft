package arqsoft2.SandwichMicroservice.controller;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import arqsoft2.SandwichMicroservice.utils.ErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import arqsoft2.SandwichMicroservice.model.sandwich.Sandwich;
import arqsoft2.SandwichMicroservice.model.sandwich.InputSandwichDTO;
import arqsoft2.SandwichMicroservice.model.sandwich.ReturnSandwichDTO;
import arqsoft2.SandwichMicroservice.model.sandwich.SandwichID;
import arqsoft2.SandwichMicroservice.service.SandwichService;

@CrossOrigin(origins = "http://localhost:8088")
@RestController
@RequestMapping("/api")
@Tag(name = "Sandwich Controller", description = "All sandwich operations")
public class SandwichController {

    @Autowired
    SandwichService sandwichService;

    public SandwichController(SandwichService sandwichService) {
        this.sandwichService = sandwichService;
    }

    @Operation(summary = "Get all sandwiches", operationId = "getSandwiches", description = "Get all registered sandwiches")
    @GetMapping("/sandwiches")
    public ResponseEntity<List<ReturnSandwichDTO>> getAllSandwiches() {
        try {
            List<ReturnSandwichDTO> sandwiches = sandwichService.getAllSandwiches();
            return new ResponseEntity<>(sandwiches, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Operation(summary = "Get a specific sandwich", operationId = "getSandwich", description = "Get a specific sandwich by providing its identifier")
    @GetMapping("/sandwiches/{id}")
    public ResponseEntity<ReturnSandwichDTO> getSandwichById(@PathVariable("id") String id) {
        ReturnSandwichDTO sandwichData = sandwichService.getSandwichById(id);

        if (sandwichData != null) {
            return new ResponseEntity<>(sandwichData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Get sandiwches filtered by a specific price range", operationId = "getSandwichesByPriceRange", description = "Get all sandwiches within a specific price range")
    @GetMapping("/sandwiches/priceFilter/{currency}/{minimumPrice}/{maximumPrice}")
    public ResponseEntity<List<ReturnSandwichDTO>> getSandwichesWithPriceFilter(
            @PathVariable("currency") String currency,
            @PathVariable("minimumPrice") double minimumPrice, @PathVariable("maximumPrice") double maximumPrice) {
        try {
            List<ReturnSandwichDTO> sandwiches = new ArrayList<ReturnSandwichDTO>();

            sandwiches = sandwichService.getSandwichesWithPriceFilter(currency, minimumPrice, maximumPrice);

            if (sandwiches.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(sandwiches, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Operation(summary = "Get sandwiches filtered by ingredient", operationId = "getSandwichesByIngredient", description = "Get all sandwiches that include the provided ingredient")
    @GetMapping("/sandwiches/ingredientsFilter/{list}")
    public ResponseEntity<List<ReturnSandwichDTO>> getSandwichesWithIngredientFilter(
            @PathVariable("list") String[] list) {
        try {
            List<ReturnSandwichDTO> sandwiches = new ArrayList<ReturnSandwichDTO>();

            sandwiches = sandwichService.getSandwichesWithIngredientFilter(list);

            if (sandwiches.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(sandwiches, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Operation(summary = "Create a new sandwich", operationId = "createSandwich", description = "Create a new sandwich by providing its information")
    @PostMapping("/sandwiches")
    public ResponseEntity<InputSandwichDTO> createSandwich(@RequestBody InputSandwichDTO sandwich) {

        try {
            InputSandwichDTO _sandwich = sandwichService
                    .createSandwich(sandwich);
            return new ResponseEntity<>(_sandwich, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Operation(summary = "Update a specific sandwich", operationId = "updateSandwich", description = "Update a specific sandwich by providing its identifier")
    @PutMapping("/sandwiches/{id}")
    public ResponseEntity<InputSandwichDTO> updateSandwich(@PathVariable("id") String id,
            @RequestBody InputSandwichDTO sandwich)
            throws ParseException {
        InputSandwichDTO sandwichData = sandwichService.updateSandwich(new SandwichID(id), sandwich);

        if (sandwichData != null) {
            return new ResponseEntity<>(sandwichData, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Delete a specific sandwich", operationId = "deleteSandwich", description = "Delete a specific sandwich by providing its identifier")
    @DeleteMapping("/sandwiches/{id}")
    public ResponseEntity<HttpStatus> deleteSandwich(@PathVariable("id") String id) {
        HttpStatus st = sandwichService.deleteSandwich(new SandwichID(id));
        return new ResponseEntity<>(st);
    }

    @Operation(summary = "Delete all sandwiches", operationId = "deleteSandwiches", description = "Delete all registered sandwiches")
    @DeleteMapping("/sandwiches")
    public ResponseEntity<HttpStatus> deleteAllSandwiches() {
        HttpStatus st = sandwichService.deleteAllSandwiches();
        return new ResponseEntity<>(st);

    }
}
