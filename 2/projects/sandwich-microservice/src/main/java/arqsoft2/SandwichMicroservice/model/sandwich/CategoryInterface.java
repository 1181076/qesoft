package arqsoft2.SandwichMicroservice.model.sandwich;

public interface CategoryInterface {

    public CategoryDescription getDescription();

    public InputCategoryDTO toDTO();

}
