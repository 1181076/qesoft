package arqsoft2.SandwichMicroservice.model.sandwich;

import java.util.List;

import javax.persistence.*;

import eapli.framework.representations.dto.DTOable;
import io.swagger.v3.oas.annotations.media.Schema;

@Entity
public class Category implements CategoryInterface, DTOable<InputCategoryDTO> {

    @EmbeddedId
    private CategoryDescription description;

    public Category() {

    }

    public Category(CategoryDescription description) {
        this.description = description;
    }

    public CategoryDescription getDescription() {
        return this.description;
    }

    @Override
    public InputCategoryDTO toDTO() {
        return new InputCategoryDTO(description);
    }

    public ReturnCategoryDTO toReturnDTO() {
        return new ReturnCategoryDTO(description.getDescription());
    }

}
