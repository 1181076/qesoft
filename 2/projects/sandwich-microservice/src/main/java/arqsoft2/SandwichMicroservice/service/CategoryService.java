package arqsoft2.SandwichMicroservice.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.http.HttpClient;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import arqsoft2.SandwichMicroservice.model.sandwich.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import arqsoft2.SandwichMicroservice.repository.CategoryRepository;
import arqsoft2.SandwichMicroservice.repository.CategoryRepositoryImpl;

@Component
public class CategoryService {

	@Autowired
	CategoryRepository categoryRepository;

	@Autowired
	CategoryRepositoryImpl categoryRepositoryImpl;

	public CategoryService(CategoryRepository categoryRepository, CategoryRepositoryImpl categoryRepositoryImpl) {
		this.categoryRepository = categoryRepository;
		this.categoryRepositoryImpl = categoryRepositoryImpl;
	}

	public List<ReturnCategoryDTO> getAllCategories() {
		try {
			List<Category> categories = categoryRepositoryImpl.findAllCategories();

			List<ReturnCategoryDTO> ret = new ArrayList<>();

			categories.forEach(e -> ret.add(e.toReturnDTO()));

			return ret;
		} catch (Exception e) {
			return new ArrayList<>();
		}
	}

	public ReturnCategoryDTO getCategoryById(@PathVariable("id") String id) {
		Optional<Category> categoryData = categoryRepository.findById(new CategoryDescription(id));

		if (categoryData.isPresent()) {
			Category _category = categoryData.get();
			return _category.toReturnDTO();
		} else {
			return null;
		}
	}

	public InputCategoryDTO createCategory(Category category) {

		try {
			Category _category = categoryRepository
					.save(category);
			System.out.println(category.getDescription().getDescription());

			return _category.toDTO();
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}

	public HttpStatus deleteCategory(@PathVariable("id") CategoryDescription id) {
		try {
			categoryRepository.deleteById(id);
			return HttpStatus.NO_CONTENT;
		} catch (Exception e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}
	}

	public HttpStatus deleteAllCategories() {
		try {
			categoryRepository.deleteAll();
			return HttpStatus.NO_CONTENT;
		} catch (Exception e) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		}

	}

}