package arqsoft2.SandwichMicroservice.repository;

import java.util.List;
import arqsoft2.SandwichMicroservice.model.sandwich.Sandwich;

public interface SandwichCustomRepository {

    public Sandwich findSandwichById(String Id);

    public List<Sandwich> findAllSandwiches();

    public List<Sandwich> getSandwichesByPriceInterval(String currency, double minimumPrice, double maximumPrice);

    public List<Sandwich> findSandwichesWithIngredients(String[] list);

}
