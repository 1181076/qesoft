package arqsoft2.SandwichMicroservice.model.sandwich;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import arqsoft2.SandwichMicroservice.model.ingredient.IngredientDTO;
import arqsoft2.SandwichMicroservice.model.ingredient.IngredientID;
import eapli.framework.representations.dto.DTOable;
import io.swagger.v3.oas.annotations.media.Schema;

@Entity
public class Sandwich implements SandwichInterface, DTOable<InputSandwichDTO> {

    @EmbeddedId
    @Schema(name = "id")
    private SandwichID id;

    @Schema(name = "shortDescription")
    private ShortDescription shortDescription;

    @Schema(name = "extendedDescription")
    private Description extendedDescription;

    @Embedded
    @Schema(name = "price")
    private Price price;

    @ElementCollection
    @Schema(name = "ingredients")
    private List<IngredientID> ingredients;

    @ManyToMany
    @Schema(name = "categories")
    private List<Category> categories;

    public Sandwich() {

    }

    public Sandwich(SandwichID id, ShortDescription shortDescription, Description extendedDescription, Price price,
            List<IngredientID> ingredients, List<Category> categories) {
        this.id = id;
        this.shortDescription = shortDescription;
        this.extendedDescription = extendedDescription;
        this.price = price;
        this.ingredients = ingredients;
        this.categories = categories;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public SandwichID getId() {
        return this.id;
    }

    public ShortDescription getShortDescription() {
        return this.shortDescription;
    }

    public Description getExtendedDescription() {
        return this.extendedDescription;
    }

    public Price getPrice() {
        return this.price;
    }

    public List<IngredientID> getIngredients() {
        return this.ingredients;
    }

    public void update(Sandwich sandwich) {
        this.shortDescription = sandwich.shortDescription;
        this.extendedDescription = sandwich.extendedDescription;
        this.price = sandwich.price;
        this.ingredients = sandwich.ingredients;
        this.categories = sandwich.categories;
    }

    public InputSandwichDTO toDTO() {
        List<CategoryDescription> categoryDTOs = new ArrayList<>();
        for (Category c: this.categories) {
            categoryDTOs.add(c.getDescription());
        }

        return new InputSandwichDTO(id, shortDescription, extendedDescription, price, categoryDTOs, ingredients);
    }

    @Override
    public ReturnSandwichDTO toReturnDTO(List<ReturnCategoryDTO> categories, List<IngredientDTO> ingredients) {
        return new ReturnSandwichDTO(id.getValue(), shortDescription.getValue(), extendedDescription.getValue(), price, categories,
                ingredients);
    }
}
