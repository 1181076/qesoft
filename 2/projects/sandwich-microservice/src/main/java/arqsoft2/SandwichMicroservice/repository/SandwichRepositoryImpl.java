package arqsoft2.SandwichMicroservice.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import arqsoft2.SandwichMicroservice.model.sandwich.Sandwich;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SandwichRepositoryImpl implements SandwichCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Sandwich> getSandwichesByPriceInterval(String currency, double minimumPrice, double maximumPrice) {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Sandwich> cq = cb.createQuery(Sandwich.class);
        Root<Sandwich> root = cq.from(Sandwich.class);

        List<Predicate> predicates = new ArrayList<Predicate>();

        predicates.add(
                cb.equal(root.get("price").get("currency"), currency));

        predicates.add(
                cb.between(root.get("price").<Double>get("amount"), minimumPrice, maximumPrice));

        cq.select(root).where(predicates.toArray(new Predicate[] {}));

        TypedQuery<Sandwich> q = em.createQuery(cq);

        return q.getResultList();

    }

    @Override
    public List<Sandwich> findAllSandwiches() {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Sandwich> cq = cb.createQuery(Sandwich.class);
        Root<Sandwich> root = cq.from(Sandwich.class);

        cq.select(root);

        TypedQuery<Sandwich> q = em.createQuery(cq);

        return q.getResultList();

    }

    @Override
    public Sandwich findSandwichById(String id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Sandwich> cq = cb.createQuery(Sandwich.class);
        Root<Sandwich> root = cq.from(Sandwich.class);
        Path<String> idValue = root.get("id").get("value");

        cq.select(root).where(cb.equal(idValue, id));

        TypedQuery<Sandwich> q = em.createQuery(cq);

        return q.getSingleResult();
    }

    @Override
    public List<Sandwich> findSandwichesWithIngredients(String[] list) {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Sandwich> cq = cb.createQuery(Sandwich.class);
        Root<Sandwich> root = cq.from(Sandwich.class);

        List<Predicate> predicates = new ArrayList<Predicate>();

        for (String ing : list) {

            predicates.add(
                    cb.equal(root.join("ingredients").get("id").get("value"), ing));

        }
        cq.select(root).where(predicates.toArray(new Predicate[] {}));

        TypedQuery<Sandwich> q = em.createQuery(cq);

        return q.getResultList();

    }

}
