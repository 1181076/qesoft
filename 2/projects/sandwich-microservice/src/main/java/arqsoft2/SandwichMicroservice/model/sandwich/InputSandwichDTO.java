package arqsoft2.SandwichMicroservice.model.sandwich;

import java.util.List;

import arqsoft2.SandwichMicroservice.model.ingredient.IngredientID;

@SuppressWarnings("squid:ClassVariableVisibilityCheck")
public class InputSandwichDTO {

    private SandwichID id;
    private ShortDescription shortDescription;
    private Description extendedDescription;
    private Price price;
    private List<IngredientID> ingredients;
    private List<CategoryDescription> categories;

    public InputSandwichDTO() {

    }

    public InputSandwichDTO(SandwichID id, ShortDescription shortDescription, Description extendedDescription,
            Price price, List<CategoryDescription> categories,
            List<IngredientID> ingredients) {
        this.id = id;
        this.shortDescription = shortDescription;
        this.extendedDescription = extendedDescription;
        this.price = price;
        this.ingredients = ingredients;
        this.categories = categories;
    }

    public SandwichID getId() {
        return this.id;
    }

    public ShortDescription getShortDescription() {
        return this.shortDescription;
    }

    public void setShortDescription(ShortDescription shortDescription) {
        this.shortDescription = shortDescription;
    }

    public Description getExtendedDescription() {
        return this.extendedDescription;
    }

    public void setExtendedDescription(Description extendedDescription) {
        this.extendedDescription = extendedDescription;
    }

    public Price getPrice() {
        return this.price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public List<IngredientID> getIngredients() {
        return this.ingredients;
    }

    public void setIngredients(List<IngredientID> ingredients) {
        this.ingredients = ingredients;
    }

    public List<CategoryDescription> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryDescription> categories) {
        this.categories = categories;
    }

    @Override
    public boolean equals(Object o) {

        return true;

    }
}
