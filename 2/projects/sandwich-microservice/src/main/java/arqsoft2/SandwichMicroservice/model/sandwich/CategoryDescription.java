package arqsoft2.SandwichMicroservice.model.sandwich;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import eapli.framework.domain.model.ValueObject;

public final class CategoryDescription implements ValueObject {

    private final String description;

    public CategoryDescription() {
        description = "default";
    }

    @JsonCreator
    public CategoryDescription(@JsonProperty("value") String description) {
        if (description.isEmpty()) {
            throw new IllegalArgumentException("Description cannot be empty!");
        }

        if (description.trim().length() > 200) {
            throw new IllegalArgumentException("Description cannot be over 200 characters!");
        }

        this.description = description;
    }

    public String description() {
        return description;
    }

    public final String getDescription() {
        return this.description;
    }

}
