package arqsoft2.SandwichMicroservice.model.sandwich;

import java.util.List;

import javax.persistence.*;

import arqsoft2.SandwichMicroservice.model.ingredient.IngredientDTO;
import arqsoft2.SandwichMicroservice.model.ingredient.IngredientID;
import eapli.framework.representations.dto.DTOable;

public interface SandwichInterface {

    public SandwichID getId();

    public ShortDescription getShortDescription();

    public Description getExtendedDescription();

    public Price getPrice();

    public List<IngredientID> getIngredients();

    public void update(Sandwich sandwich);

    public InputSandwichDTO toDTO();

    public ReturnSandwichDTO toReturnDTO(List<ReturnCategoryDTO> categories, List<IngredientDTO> ingredients);
}
