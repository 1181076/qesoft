package arqsoft2.SandwichMicroservice.model.sandwich;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PriceTest {
    @Test
    void verifyIfPriceAmountIsNegative() {
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Price(-21, "euro");
        });
        assertEquals("The price cannot be negative!", e.getMessage());
    }

    @Test
    void verifyIfPriceCurrentIsEmpty() {
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Price(21, "");
        });
        assertEquals("The currency must not be empty!", e.getMessage());
    }

    @Test
    void verifyIfPriceIsCorrect() {
        new Price(21, "euro");
        assertTrue(true);
    }
}