package arqsoft2.SandwichMicroservice.model.sandwich;

import org.assertj.core.api.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class DescriptionTest {
    @Test
    void verifyIfDescriptionIsEmpty() {
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Description("");
        });
        assertEquals("Description cannot be empty!", e.getMessage());
    }

    @Test
    void verifyIfDescriptionIsLongerThan200Characters() {
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Description(
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec iaculis elementum risus at mollis. Sed interdum augue nec nisi accumsan, id hendrerit est finibus. Pellentesque malesuada sit amet nisl in fringilla. Mauris ultricies venenatis lorem in commodo. Praesent ut lacus erat. Vestibulum purus eros, tempor a fermentum vel, bibendum sed lacus. Nunc tempus fermentum risus, rhoncus iaculis mi efficitur a. Morbi fermentum magna at justo consectetur euismod. Nam viverra ultrices purus maximus semper. Phasellus ultricies libero vel mi tempus, id accumsan neque ultrices. Ut vestibulum metus at urna tempus sollicitudin. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed commodo at enim sed volutpat. Nullam vitae sem ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec iaculis elementum risus at mollis. Sed interdum augue nec nisi accumsan, id hendrerit est finibus. Pellentesque malesuada sit amet nisl in fringilla. Mauris ultricies venenatis lorem in commodo. Praesent ut lacus erat. Vestibulum purus eros, tempor a fermentum vel, bibendum sed lacus. Nunc tempus fermentum risus, rhoncus iaculis mi efficitur a. Morbi fermentum magna at justo consectetur euismod. Nam viverra ultrices purus maximus semper. Phasellus ultricies libero vel mi tempus, id accumsan neque ultrices. Ut vestibulum metus at urna tempus sollicitudin. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed commodo at enim sed volutpat. Nullam vitae sem ex.");
        });
        assertEquals("Description cannot be over 200 characters!", e.getMessage());
    }

    @Test
    void verifyIfDescriptionIsCorrect() {
        new Description("This is an extended description");
        assertTrue(true);
    }
}