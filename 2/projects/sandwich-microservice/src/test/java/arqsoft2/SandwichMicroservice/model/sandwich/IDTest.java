package arqsoft2.SandwichMicroservice.model.sandwich;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import arqsoft2.SandwichMicroservice.model.sandwich.SandwichID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class IDTest {
    @Test
    void verifyIfIDisEmpty() {
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new SandwichID("");
        });
        assertEquals("The ID cannot be empty!", e.getMessage());
    }

    @Test
    void verifyIfIDIsCorrect() {
        new SandwichID("ID1");
        assertTrue(true);
    }
}